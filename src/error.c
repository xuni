/*! \file error.c
    
    Error handling code. Allows multiple file names and streams to be
    registered; errors are output to each of them. Errors of various types can
    be reported with log_message(), which supports printf()-style format
    strings.
    
    If a file is specified as a stream, it is not closed automatically.
    (Useful for using "stderr" and such.) Files specified as file names are
    opened just before an error message needs to be written to them. (If a
    file cannot be opened, this is only announced on stderr. There is a limit
    to how far this sort of thing should go.) If no files could be opened, or
    no files were registered, the error is printed to stderr.
    
    See get_error_type() for the types of errors allowed and their brief
    descriptions.
    
    This code does not use xuni's reference counter (in memory.c); rather,
    the reference counter uses these error handling functions.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>

#include "SDL.h"

#include "error.h"
#include "graphics.h"
#include "memory.h"
#include "version.h"

/* This code is quite similar to memory.c in many respects. */

struct error_stream_t {
    const char *filename;
    FILE *fp;
    int opened;
};

struct error_stream_array_t {
    struct error_stream_t *stream;
    size_t alloc, n;
};

static struct error_stream_array_t stream = {0, 0, 0};

static void xuni_error_grow_stream_alloc(void);

/*! Initializes xuni's error handling subsystem.
*/
void xuni_error_initialize(void) {
    
}

static void xuni_error_grow_stream_alloc(void) {
    struct error_stream_t *p;
    
    /* since few error streams are usually registered, add only one */
    stream.alloc ++;
    /*if(!stream.alloc) stream.alloc = 1;
    else stream.alloc *= 2;*/
    
    /* Avoid using xuni's reference counter, because it uses this error-
        handling code to report errors.
    */
    p = realloc(stream.stream, stream.alloc * sizeof(*stream.stream));
    if(!p) {
        log_message(ERROR_TYPE_MEMORY, 0, __FILE__, __LINE__,
            "Error allocating %lu struct error_stream_t's",
            (unsigned long)stream.alloc);
        return;
    }
    stream.stream = p;
}

/*! Registers a file name or stream to print error messages to. Either
    \a filename or \a fp must be non-NULL to specify the file as a filename or
    a stream, respectively.
    
    If \a filename is not NULL, the file specified by this string will be
    opened and have error messages written to it only when errors are
    encountered. (In other words, the file is kept closed whenever possible.)
    
    If \a fp is not NULL, errors will be written to this open file stream.
    This stream will not be closed, allowing stderr or user-managed files to
    be used.
    
    If both arguments, or neither, are NULL, the behaviour of this function is
    undefined.
    
    \param filename The name of the file to register.
    \param fp An opened file stream to register. Works for standard streams
        like stderr.
*/
void xuni_error_add_stream(const char *filename, FILE *fp) {
    if(stream.n == stream.alloc) xuni_error_grow_stream_alloc();
    
    if(filename) {
        stream.stream[stream.n].filename
            = xuni_memory_duplicate_string(filename);
    }
    else stream.stream[stream.n].filename = 0;
    
    stream.stream[stream.n].fp = fp;
    stream.stream[stream.n].opened = !fp;
    
    stream.n ++;
}

/*! Frees resources allocated for xuni's error handling subsystem. This means
    the list of opened files that were added. Any opened files are closed,
    too, but currently no files remain open long enough for this function to
    have to deal with them.
*/
void xuni_error_quit(void) {
    size_t x;
    
    for(x = 0; x < stream.n; x ++) {
        xuni_memory_free((void *)stream.stream[x].filename);
        
        if(stream.stream[x].fp && stream.stream[x].opened) {
            fclose(stream.stream[x].fp);
        }
    }
    
    free(stream.stream);
}

static const char *get_ctime(void);
static const char *get_error_type(enum error_type_t type);
static void log_message_stream(FILE *stream, enum error_type_t type,
    enum error_flag_t flag, const char *file, int line, const char *date,
    const char *format, va_list arg);

static const char *get_ctime(void) {
    time_t tt = time(0);
    
    return ctime(&tt);
}

static const char *get_error_type(enum error_type_t type) {
    static const char *type_name[] = {
        "Log message",     /* ERROR_TYPE_LOG */
        "Warning",         /* ERROR_TYPE_WARNING */
        "Fatal error",     /* ERROR_TYPE_FATAL */
        "Settings error",  /* ERROR_TYPE_SETTING */
        "Resource error",  /* ERROR_TYPE_RESOURCE */
        "Memory error",    /* ERROR_TYPE_MEMORY */
        "Data structure error",  /* ERROR_TYPE_DATASTRUCT */
        "Lookup table error"     /* ERROR_TYPE_LOOKUP_TABLE */
    };
    
    if(type >= sizeof(type_name)/sizeof(*type_name)) type = 0;
    
    return type_name[type];
}

static void log_message_stream(FILE *stream, enum error_type_t type,
    enum error_flag_t flag, const char *file, int line, const char *date,
    const char *format, va_list arg) {
    
    if(!(flag & ERROR_FLAG_CONTINUED)) {
        const char *filename;
        
        for(filename = file; *filename; filename ++) {
            if(strchr(DIRECTORY_SEPARATORS, *filename)) {
                file = filename + 1;
            }
        }
        
        /* note: date is terminated by a newline */
        fprintf(stream, "xuni: %s from %s line %i, at %s    ",
            get_error_type(type), file, line, date);
    }
    else fputs("    [continued] ", stream);
    
    vfprintf(stream, format, arg);
    
    if(flag & ERROR_FLAG_SDL) {
        fputs("\n    SDL: ", stream);
        fputs(SDL_GetError(), stream);
    }
    
    putc('\n', stream);
}

/*! Records an error, which can be one of many different types. The message,
    which consists mainly of \a format with other information thrown in, is
    output to all registered error streams.
    
    \param type The type of error. Affects the string describing what type of
        error the error message is.
    \param flag More specific information about the error. Affects which other
        error reporting functions should be called. For example,
        \c ERROR_FLAG_SDL causes the output of SDL_GetError() to be recorded.
    \param file The file in which the error occurred. Should be \c __FILE__.
    \param line The line at which the error occurred. Should be \c __LINE__.
    \param format The printf()-compatible string describing the error. The
        variable arguments afterwards can be used to match arguments to format
        specifiers, denoted with percent signs (%) in this format string.
*/
void log_message(enum error_type_t type, enum error_flag_t flag,
    const char *file, int line, const char *format, ...) {
    
    const char *date = get_ctime();
    va_list arg;
    size_t x, wrote = 0;
    
    for(x = 0; x < stream.n; x ++) {
        if(!stream.stream[x].fp) {
            stream.stream[x].fp = fopen(stream.stream[x].filename, "a");
            if(!stream.stream[x].fp) {
                fprintf(stderr, "Can't open log file \"%s\"\n",
                    stream.stream[x].filename);
                continue;
            }
        }
        
        va_start(arg, format);
        log_message_stream(stream.stream[x].fp, type, flag, file, line, date,
            format, arg);
        va_end(arg);
        
        if(stream.stream[x].fp && stream.stream[x].filename) {
            fclose(stream.stream[x].fp);
            stream.stream[x].fp = 0;
        }
        
        wrote ++;
    }
    
    if(!wrote) {
        va_start(arg, format);
        log_message_stream(stderr, type, flag, file, line, date, format, arg);
        va_end(arg);
    }
}
