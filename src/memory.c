/*! \file memory.c
    
    This source file implements a simple reference counter. It keeps track of
    the number of variables that are pointing to a block of memory -- the
    number of shallow copies of a block. Every time a copy is made of the
    object, this number goes up -- and every time a copy is destroyed, the
    number goes down. When the number reaches zero, there are no more pointers
    to the object and it can be safely freed.
*/

#include <stdlib.h>
#include <string.h>
#include "memory.h"
#include "error.h"
#include "utility.h"

/* A minimalistic implementation which never frees any memory, save when
    realloc()'ing a block to a smaller size.
*/
#if !1

size_t allocated_sdl_surface(SDL_Surface *surface) {
    return 0;
}

void decrement_allocated(size_t size) {
    
}

void increment_allocated(size_t size) {
    
}

void xuni_memory_initialize(void) {
    fprintf(stderr, "Warning: compiled with minimalistic reference counter"
        " that never frees any\nmemory. Memory consumption will increase"
        " very quickly.");
}

void *xuni_memory_allocate_func(size_t size, const char *func) {
    return malloc(size);
}

char *xuni_memory_duplicate_string(const char *data) {
    return xuni_memory_duplicate_string_len(data, strlen(data));
}

char *xuni_memory_duplicate_string_len(const char *data, size_t len) {
    char *p = malloc(len + 1);
    memcpy(p, data, len);
    p[len] = 0;
    return p;
}

void *xuni_memory_resize_func(void *data, size_t size, const char *func) {
    return realloc(data, size);
}

void xuni_memory_add_block_func(void *data, const char *func,
    enum xuni_memory_block_type_t type) {
    
}

void xuni_memory_increment(void *data) {
    
}

void *xuni_memory_decrement(void *data) {
    return 0;
}

void xuni_memory_add_count(void *data, int count) {
    
}

size_t xuni_memory_get_count(void *data) {
    return (size_t)-1;
}

void xuni_memory_free(void *data) {
    
}

void xuni_memory_keep_freed_blocks(int keep) {
    
}

void xuni_memory_free_all(void) {
    
}

#else

#define GARBAGE_COLLECT_ALL
/*#define RECORD_MAXIMUMS*/
#define DEBUG_UNALLOCATED_DECREMENT
#define DUMP_EXTRA_MEMORY

#ifdef RECORD_MAXIMUMS
static size_t maxcount = 0;
static size_t sizenow = 0;
static size_t maxsize = 0;
#endif

struct xuni_memory_p_t {
    void *data;
    size_t count;
    const char *func;
    size_t size;
    enum xuni_memory_block_type_t type;
};

struct xuni_memory_t {
    struct xuni_memory_p_t *data;
    size_t alloc, n;
    int keepfreed;
};

/*! List of all dynamically allocated blocks of memory. */
static struct xuni_memory_t memory = {0, 0, 0, 0};

static int memory_find_data_compare(void *data, size_t n, void *find);
static int memory_find_data(void *data, size_t *pos);
static int memory_find_last_data(void *data, size_t *pos);
static void memory_increase_blocks(void);
static void memory_insert_data(void *data, size_t pos, size_t size,
    const char *func, enum xuni_memory_block_type_t type);
static void memory_remove_data(size_t pos);
static void xuni_memory_free_block(void *data,
    enum xuni_memory_block_type_t type);

static void print_block(size_t x);

static int memory_find_data_compare(void *data, size_t n, void *find) {
    struct xuni_memory_p_t *array = data;
    void *a = find;
    void *b = array[n].data;
    
    if(a < b) return -1;
    else if(a > b) return 1;
    
    return 0;
}

/*! Finds the position in which \a data is located in the memory structure,
    or, if it is not present, the "closest" place. The closest place is the
    position of the last element that is smaller than \a data. It is used as a
    parameter to memory_insert_data().
    
    \param data The memory block to search for.
    \param pos Set to the actual or closest position of \a data within the
        memory structure.
    \return True if \a data is actually present in the memory structure, false
        otherwise. Note that if \a data is not present, \a pos will still be
        set.
*/
static int memory_find_data(void *data, size_t *pos) {
#if 1
    return binary_insertion_sort(memory.data, memory.n, data, pos,
        memory_find_data_compare);
#else
    size_t mid = (size_t)-1, first = 0, last = memory.n - 1;
    
    *pos = 0;
    
    if(!memory.n) return 0;
    
    while(first <= last && last != (size_t)-1) {
        mid = (first + last) / 2;
        
        if(data < memory.data[mid].data) {
            last = mid - 1;
        }
        else if(data > memory.data[mid].data) {
            first = mid + 1;
        }
        else {
            *pos = mid;
            return 1;
        }
    }
    
    if(data < memory.data[mid].data) {
        *pos = mid;
    }
    else {
        *pos = mid + 1;
    }
    
    return 0;
#endif
}

static int memory_find_last_data(void *data, size_t *pos) {
    /* first check for the most common case: that data is the last index */
    if(memory.n && data > memory.data[memory.n - 1].data) {
        *pos = memory.n;
        return 0;
    }
    
    return memory_find_data(data, pos);
}

size_t allocated_sdl_surface(SDL_Surface *surface) {
#ifdef RECORD_MAXIMUMS
    size_t size = sizeof(*surface);
    
    if(!surface) return size;
    
    if(surface->format) {
        size += sizeof(*surface->format);
        if(surface->format->palette) {
            size += sizeof(*surface->format->palette);
            size += sizeof(*surface->format->palette->colors) *
                surface->format->palette->ncolors;
        }
    }
    
    if(surface->pixels) {
        size += surface->h * surface->pitch
            + surface->w * surface->format->BytesPerPixel;
    }
    
    /* surface->hwdata and surface->map not counted properly */
    /*if(surface->hwdata) size += sizeof(*surface->hwdata);
    if(surface->map) size += sizeof(*surface->map);*/
    
    return size;
#else
    return 0;
#endif
}

void decrement_allocated(size_t size) {
#ifdef RECORD_MAXIMUMS
    sizenow -= size;
#endif
}

void increment_allocated(size_t size) {
#ifdef RECORD_MAXIMUMS
    sizenow += size;
    if(sizenow > maxsize) maxsize = sizenow;
#endif
}

/*! Increases the memory allocated for the memory structure. Called when a new
    block needs to be added, but all of the allocated memory has been used up.
    
    Doubles the currently allocated memory, which is wasteful for space but
    much more efficient when many memory blocks are involved, which is usually
    the case.
*/
static void memory_increase_blocks(void) {
    void *p;
    
    if(memory.alloc == memory.n) {
        if(!memory.alloc) memory.alloc = 1;
        else memory.alloc *= 2;
        
        p = realloc(memory.data, memory.alloc * sizeof(*memory.data));
        if(!p) {
            log_message(ERROR_TYPE_MEMORY, 0, __FILE__, __LINE__,
                "Error allocating %lu struct xuni_memory_p_t's",
                (unsigned long)memory.alloc);
            return;
        }
        memory.data = p;
    }
}

static void memory_insert_data(void *data, size_t pos, size_t size,
    const char *func, enum xuni_memory_block_type_t type) {
    
    memory_increase_blocks();
    
    if(pos < memory.n) {
        memmove(memory.data + pos + 1, memory.data + pos,
            (memory.n - pos) * sizeof(*memory.data));
    }
    
    memory.data[pos].data = data;
#ifdef GARBAGE_COLLECT_ALL
    memory.data[pos].count = 1;
#else
    memory.data[pos].count = 2;
#endif
    memory.data[pos].func = func;
    memory.data[pos].size = size;
    memory.data[pos].type = type;
    
    memory.n ++;
}

static void memory_remove_data(size_t pos) {
#ifdef RECORD_MAXIMUMS
    if(memory.n > maxcount) maxcount = memory.n;
    decrement_allocated(memory.data[pos].size);
#endif
    
    memmove(memory.data + pos, memory.data + pos + 1,
        (memory.n - pos - 1) * sizeof(*memory.data));
    
    memory.n --;
}

/*! Initializes the reference counter. */
void xuni_memory_initialize(void) {
    /* can't do this unless this function is called before allocate_xuni() */
    /*memory.data = 0;
    memory.alloc = 0;
    memory.n = 0;
    memory.keepfreed = 1;*/
}

/*! Allocates a chunk of memory of \a size bytes, storing the pointer in the
    list \c memory and assuming one reference to the block.
    
    Replacement for \c malloc().
    
    \param size The requested bytes of memory to allocate.
*/
void *xuni_memory_allocate_func(size_t size, const char *func) {
    void *data = malloc(size);
    size_t pos;
    
    if(!data) {
        log_message(ERROR_TYPE_MEMORY, 0, __FILE__, __LINE__,
            "Error allocating %lu bytes of memory\n", (unsigned long)size);
    }
    else {
#ifdef GARBAGE_COLLECT_ALL
        if(!memory_find_last_data(data, &pos)) {
            memory_insert_data(data, pos, size, func,
                MEMORY_BLOCK_TYPE_NORMAL);
        }
        else if(!memory.data[pos].count) {
            memory.data[pos].count ++;
            memory.data[pos].type = MEMORY_BLOCK_TYPE_NORMAL;
            memory.data[pos].size = size;
            memory.data[pos].func = func;
        }
        else {
            log_message(ERROR_TYPE_MEMORY, 0, __FILE__, __LINE__,
                "Attempting to add already-existing block %p\n", data);
        }
#endif
#ifdef RECORD_MAXIMUMS
        increment_allocated(size);
#endif
    }
    
    /*memset(data, 0xff, size);*/
    
    return data;
}

/*! Allocates a copy of the string \a data, just like the non-standard
    function strdup().
    \param data The string to make a dynamically allocated copy of.
    \return A pointer to the new memory, initialized with the character data
        in \a data.
*/
char *xuni_memory_duplicate_string(const char *data) {
    if(!data) return 0;
    
    return xuni_memory_duplicate_string_len(data, strlen(data));
}

/*! Allocates \c len+1 bytes, copies \a data to this new memory, and returns
    a pointer to the new memory.
    
    This would be sort of like strndup(), if it existed.
    
    \param data The character data to use. This need not be NULL-terminated.
    \param len The number of characters to copy from \a data. A NULL is added
        regardless, so that \c data[len] does not necessarily have to be
        '\\0'.
    \return A pointer to the new memory, initialized with the character data
        in \a data.
*/
char *xuni_memory_duplicate_string_len(const char *data, size_t len) {
    char *p = xuni_memory_allocate(len + 1);
    
    if(data && len) {
        memcpy(p, data, len);
        p[len] = 0;
    }
    else *p = 0;
    
    return p;
}

#if 0
char *xuni_memory_insert_string(char *data, char *insert, size_t pos,
    size_t len) {
    
    size_t datalen, x;
    char *p;
    int usedata = data && *data;
    int useinsert = insert && *insert && len;
    
    printf("        xuni_memory_insert_string(\"%s\", \"%s\", %i, %i)\n",
        data, insert, (int)pos, (int)len);
    
    if(!usedata && !useinsert) return 0;
    else if(usedata && useinsert) {
        datalen = strlen(data);
        
        p = xuni_memory_resize(data, datalen + len + 1);
        if(!p) return data;
        data = p;
        
        /*strcpy(data, insert);*/
        
        printf("        pos:%i datalen:%i len:%i\n", (int)pos,
            (int)datalen, (int)len);
        
        printf("        [%i->%i] '%c'\n", (int)pos, (int)(pos + len),
            data[pos]);
        memmove(data + 1, data, datalen + 1);
        
        /*memmove(data + pos + len, data + pos, datalen - pos + 1);
        memcpy(data + pos, insert, len);*/
        
        printf("        -> \"%s\"\n", data);
        
        return data;
    }
    else if(!usedata) {
        return xuni_memory_duplicate_string(insert, strlen(insert));
    }
    else {
        return xuni_memory_duplicate_string(data, strlen(data));
    }
}
#endif

/*! Resizes the amount of dynamically allocated memory reserved for \a data
    to \a size bytes, returning the pointer to the new memory. This pointer
    may, but not necessarily, be the same value as the original \a data. If
    it is a different value, the original data in \a data is copied over to
    the new location.
    
    When \a data is NULL, this function acts the same way as
    \c xuni_memory_allocate(). Only works on memory blocks of type
    MEMORY_BLOCK_TYPE_NORMAL.
    
    Replacement for \c realloc().
    
    \param data The block of memory to resize, or NULL if no block was
        previously allocated.
    \param size The number of bytes that \a data should be resized to point
        to.
    \return The newly resized block of memory, possibly equal to \a data.
*/
void *xuni_memory_resize_func(void *data, size_t size, const char *func) {
    size_t pos;
    void *p;
    
#ifdef GARBAGE_COLLECT_ALL
    if(!size) {
        if(memory_find_data(data, &pos)) {
            if(!memory.data[pos].count) {
                log_message(ERROR_TYPE_MEMORY, 0, __FILE__, __LINE__,
                    "Can't resize to zero a block with no remaining pointers:"
                    " %p", data);
            }
            else {
                memory_remove_data(pos);
            }
        }
        
        free(data);
        
        return 0;
    }
    
    if(data && memory_find_data(data, &pos)) {
        if(memory.data[pos].type != MEMORY_BLOCK_TYPE_NORMAL) {
            log_message(ERROR_TYPE_MEMORY, 0, __FILE__, __LINE__,
                "Can only resize normal memory blocks, not ones of type %lu",
                (unsigned long)memory.data[pos].type);
            return data;
        }
        
        if(!memory.data[pos].count) {
            log_message(ERROR_TYPE_MEMORY, 0, __FILE__, __LINE__,
                "Can't resize a block with no remaining pointers: %p", data);
            return data;
        }
        
        p = realloc(data, size);
        if(!p) {
            log_message(ERROR_TYPE_MEMORY, 0, __FILE__, __LINE__,
                "Error resizing pre-allocated memory block to %lu bytes",
                (unsigned long)size);
            return data;
        }
        
        if(p != data) {
            memory_remove_data(pos);
            if(!memory_find_last_data(p, &pos)) {
                memory_insert_data(p, pos, size, func,
                    MEMORY_BLOCK_TYPE_NORMAL);
            }
            else if(!memory.data[pos].count) {
                memory.data[pos].count ++;
                memory.data[pos].type = MEMORY_BLOCK_TYPE_NORMAL;
                memory.data[pos].size = size;
                memory.data[pos].func = func;
            }
            else {
                log_message(ERROR_TYPE_MEMORY, 0, __FILE__, __LINE__,
                    "Adding a block that already exists: %p", p);
            }
        }
        
        return p;
    }
    else {
        return xuni_memory_allocate(size);
    }
#else
    return realloc(data, size);
#endif
}

/*! Adds a reference to a block of memory allocated by some other allocation
    function instead of xuni_memory_allocate() -- e.g.,
    SDL_CreateRGBSurface().
    \param data The pointer to add to the list of allocated blocks.
    \param type The type of the pointer \a data which will be added to the
        list of memory blocks.
*/
void xuni_memory_add_block_func(void *data, const char *func,
    enum xuni_memory_block_type_t type) {
    
    size_t pos;
    
    if(!data) return;
    
    if(memory_find_last_data(data, &pos)) {
        memory.data[pos].count ++;
    }
    else {
        memory_insert_data(data, pos, 0, func, type);
    }
}

/*! Increments the reference count for the pointer \a data, which points to a
    previously (dynamically) allocated block of memory.
    \param data The pointer for which to increase the reference count.
*/
void xuni_memory_increment(void *data) {
    size_t pos;
    
    if(!data) return;
    
    if(memory_find_data(data, &pos) && memory.data[pos].count) {
        memory.data[pos].count ++;
    }
    else {
#ifdef GARBAGE_COLLECT_ALL
        log_message(ERROR_TYPE_MEMORY, 0, __FILE__, __LINE__,
            "xuni_memory_increment() passed an invalid pointer: %p", data);
#else
        printf("Size of block %p not recorded\n", data);
        memory_insert_data(data, pos, 0, __func__);
#endif
    }
}

/*! Decrements the reference counter for the pointer \a data.
    
    Low-level replacement for functions like free() and SDL_FreeSurface().
    
    \param data The pointer to decrement the reference count for.
    \return \a data if it needs to be freed, or NULL if it doesn't.
*/
void *xuni_memory_decrement(void *data) {
    size_t pos;
    
    if(!data) return 0;
    
    if(memory_find_data(data, &pos)) {
        if(!memory.data[pos].count) {
            log_message(ERROR_TYPE_MEMORY, 0, __FILE__, __LINE__,
                "Trying to decrement a block with no pointers to it: %p",
                data);
        }
        else if(!--memory.data[pos].count) {
            memory_remove_data(pos);
            return data;
        }
    }
#ifdef DEBUG_UNALLOCATED_DECREMENT
    else {
        log_message(ERROR_TYPE_MEMORY, 0, __FILE__, __LINE__,
            "xuni_memory_decrement() passed an unallocated pointer: %p",
            data);
    }
#endif
    
    return 0;
}

/*void xuni_memory_add_count(void *data, int count) {
    size_t pos;
    
    if(!data) return;
    
    if(memory_find_data(data, &pos)) {
        memory.data[pos].count += count;
    }
    else {
        log_message(ERROR_TYPE_MEMORY, 0, __FILE__, __LINE__,
            "xuni_memory_add_count() passed an invalid pointer: %p", data);
    }
}

size_t xuni_memory_get_count(void *data) {
    size_t pos;
    
    if(!data) return (size_t)-1;
    
    if(memory_find_data(data, &pos)) {
        return memory.data[pos].count;
    }
    
    log_message(ERROR_TYPE_MEMORY, 0, __FILE__, __LINE__,
        "xuni_memory_get_count() passed an invalid pointer: %p", data);
    
    return (size_t)-1;
}*/

/*! Decrements the reference counter for the pointer \a data, freeing the data
    pointed to by that same pointer with the correct free function if
    necessary. The "correct free function" is determined from the type of
    block, which was assigned when the block was allocated.
    
    Replacement for free() and SDL_FreeSurface() etc.
    
    \param data The pointer to decrement the reference count for.
*/
void xuni_memory_free(void *data) {
    size_t pos;
    enum xuni_memory_block_type_t type;
    
    if(!data) return;
    
    if(memory_find_data(data, &pos)) {
        if(!memory.data[pos].count) {
            log_message(ERROR_TYPE_MEMORY, 0, __FILE__, __LINE__,
                "Memory block freed too many times: %p", data);
        }
        else if(!--memory.data[pos].count) {
            type = memory.data[pos].type;
            
            xuni_memory_free_block(data, type);
            
            if(!memory.keepfreed) memory_remove_data(pos);
        }
    }
#ifdef DEBUG_UNALLOCATED_DECREMENT
    else {
        log_message(ERROR_TYPE_MEMORY, 0, __FILE__, __LINE__,
            "xuni_memory_free() passed an unallocated pointer: %p", data);
    }
#endif
}

static void xuni_memory_free_block(void *data,
    enum xuni_memory_block_type_t type) {
    
    switch(type) {
    case MEMORY_BLOCK_TYPE_NORMAL:
        free(data);
        break;
    case MEMORY_BLOCK_TYPE_SDL_SURFACE:
        SDL_FreeSurface(data);
        break;
    case MEMORY_BLOCK_TYPE_LOADSO_OBJECT:
        /*xuni_loadso_free_object(data);*/
        break;
    default:
        log_message(ERROR_TYPE_MEMORY, 0, __FILE__, __LINE__,
            "Trying to free unknown memory block type: %lu",
            (unsigned long)type);
    }
}

void xuni_memory_keep_freed_blocks(int keep) {
    memory.keepfreed = keep;
}

/*! Frees all memory still allocated that has been recorded by the reference
    counter. Should probably only be called once, at the program's exit.
    
    Other freeing functions like free_xuni() could make this function
    unnecessary.
*/
void xuni_memory_free_all(void) {
    size_t x;
    
#ifdef RECORD_MAXIMUMS
    printf("*** Maximum blocks of memory allocated: %lu\n",
        (unsigned long)maxcount);
    printf("*** Maximum amount of memory allocated: %lu\n",
        (unsigned long)maxsize);
#endif
    
#ifdef DUMP_EXTRA_MEMORY
    /*printf("Freeing all extra memory:\n");*/
#endif
    
    for(x = 0; x < memory.n; x ++) {
        if(!memory.data[x].count) continue;
        
#ifdef DUMP_EXTRA_MEMORY
        printf("Freeing ");
        print_block(x);
        
        if(!strcmp(memory.data[x].func, "xuni_memory_duplicate_string")) {
            printf("duplicated string: \"%s\"\n",
                (char *)memory.data[x].data);
        }
#endif
        
        free(memory.data[x].data);
    }
    
    free(memory.data);
    memory.n = 0;
}

static void print_block(size_t x) {
    printf("([%lu], %p, #%lu, \"%s\", %lu bytes)\n", (unsigned long)x,
        memory.data[x].data, (unsigned long)memory.data[x].count,
        memory.data[x].func, (unsigned long)memory.data[x].size);
}

/*static void check_memory(void) {
    size_t x;
    
    for(x = 0; x + 1 < memory.n; x ++) {
        if(memory.data[x].data >= memory.data[x+1].data) {
            printf("*** Out of order: ");
            print_block(x);
            printf("    and ");
            print_block(x+1);
        }
    }
}*/

/*static void dump_memory(void) {
    size_t x;
    
    printf("Memory dump:\n");
    for(x = 0; x < memory.n; x ++) {
        print_block(x);
    }
}*/

#endif
