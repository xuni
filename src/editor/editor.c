/*! \file editor.c

*/

#include <string.h>

#include "editor.h"

#include "xuni.h"
#include "error.h"
#include "graphics.h"
#include "loadso.h"
#include "loop.h"
#include "resource/resource.h"
#include "memory.h"
#include "utility.h"
#include "version.h"

#include "widget/dump.h"
#include "widget/widgets.h"

static void load_resource(struct resource_t *settings);
static void save_resource(struct resource_t *resource);

static void init_loop_data(struct gui_t *gui);

#ifdef VERSION_WIN32
#include <windows.h>
int STDCALL WinMain(HINSTANCE hInst, HINSTANCE hPrev, LPSTR lpCmd, int nShow) {
#else
int main(int argc, char *argv[]) {
#endif
    struct resource_t settings;
    struct xuni_t *xuni = allocate_xuni();
    
    xuni_error_initialize();
    xuni_error_add_stream(0, stderr);
    xuni_error_add_stream("editor.log", 0);
    
    load_resource(&settings);
    
    init_xuni(xuni, &settings,
        lookup_resource_string(&settings, 0, "xuni-resource", "icon", 0));
    
    init_loop_data(xuni->gui);
    
    call_init_funcs(xuni, xuni->gui->widget, &settings);
    main_loop(xuni, 0, PANEL_EDITOR);
    
    xuni_memory_keep_freed_blocks(1);
    
    save_resource(&settings);
    free_resource(&settings);
    free_xuni(xuni);
    
    quit_sdl_libraries();
    xuni_error_quit();
    xuni_memory_free_all();
    
    return 0;
}

static void load_resource(struct resource_t *settings) {
    init_resource(settings);
    
    if(parse_resource(settings->data, SETTINGS_FILE)) {
        log_message(ERROR_TYPE_FATAL, 0, __FILE__, __LINE__,
            "Failed to load resource \"%s\"", SETTINGS_FILE);
    }
}

static void save_resource(struct resource_t *resource) {
    /*write_resource(resource, SETTINGS_FILE ".generated");*/
}

static int editor_init(struct xuni_t *xuni, struct panel_data_t *data);
static int editor_start(struct xuni_t *xuni, struct panel_data_t *data);
static int editor_event(struct xuni_t *xuni, struct panel_data_t *data);
static int editor_click(struct xuni_t *xuni, struct panel_data_t *data);
static int editor_deactivate(struct xuni_t *xuni, struct panel_data_t *data);
static int editor_paint(struct xuni_t *xuni, struct panel_data_t *data);
static int editor_free(struct xuni_t *xuni, struct panel_data_t *data);

static void handle_cancel_action(struct xuni_t *xuni,
    struct editor_data_t *data);
static void set_editor_mode(struct xuni_t *xuni, struct editor_data_t *data,
    enum editor_mode_t mode);

enum wid_t {
    WID_QUIT,
    WID_MODE_LABEL,
    WID_POSITION_LABEL,
    WID_MODE_TEST,
    WID_CANCEL_ACTION,
    WID_MODE_DELETE_WIDGET,
    WID_MODE_ADD_BUTTON,
    WID_AREA,
    WID_AREA_BOX,
    WID_AREA_CURSOR_BOX,
    WIDS
};

static void init_loop_data(struct gui_t *gui) {
    struct widget_t *panel;
    void *data;
    
    init_wid(gui->widget, gui->widget, PANEL_EDITOR, "editor");
    
    panel = widget_nameid_access(gui->widget, PANEL_EDITOR);
    
    data = xuni_memory_allocate(sizeof(struct editor_data_t));
    set_panel_data(panel, data, 0);
    set_panel_callback(panel, PANEL_EVENT_INIT, editor_init);
    set_panel_callback(panel, PANEL_EVENT_START, editor_start);
    set_panel_callback(panel, PANEL_EVENT_EVENT, editor_event);
    set_panel_callback(panel, PANEL_EVENT_SEL, default_panel_sel);
    set_panel_callback(panel, PANEL_EVENT_CLICK, editor_click);
    set_panel_callback(panel, PANEL_EVENT_DEACTIVATE, editor_deactivate);
    set_panel_callback(panel, PANEL_EVENT_PAINT, editor_paint);
    set_panel_callback(panel, PANEL_EVENT_FREE, editor_free);
    
    /*set_panel_callbacks(widget_nameid_access(gui->widget, PANEL_EDITOR),
        data, 0, editor_init, editor_start, editor_event,
        default_panel_sel, editor_click, editor_deactivate,
        editor_paint, editor_free);*/
}

static const char *get_mode_name(enum editor_mode_t mode) {
    static const char *name[] = {
        "test",
        "delete",
        "button"
    };
    
    if((size_t)mode < sizeof(name)/sizeof(*name)) {
        return name[mode];
    }
    else return "ERROR";
}

static int editor_init(struct xuni_t *xuni, struct panel_data_t *data) {
    struct editor_data_t *edata = data->data;
    struct widget_t *panel = data->event[PANEL_EVENT_INIT].p.init.panel;
    
    init_wid(panel, panel, WID_QUIT, "quit");
    init_wid(panel, panel, WID_MODE_LABEL, "mode label");
    init_wid(panel, panel, WID_POSITION_LABEL, "position label");
    init_wid(panel, panel, WID_MODE_TEST, "resource tab/test");
    init_wid(panel, panel, WID_CANCEL_ACTION,
        "resource tab/cancel action");
    init_wid(panel, panel, WID_MODE_DELETE_WIDGET,
        "resource tab/delete widget");
    init_wid(panel, panel, WID_MODE_ADD_BUTTON,
        "resource tab/add button");
    init_wid(panel, panel, WID_AREA, "area");
    init_wid(panel, panel, WID_AREA_BOX, "area/box");
    init_wid(panel, panel, WID_AREA_CURSOR_BOX, "area/cursor box");
    
    add_widget_accelerator(xuni, panel, panel, SDLK_SPACE, KMOD_NONE);
    add_widget_accelerator(xuni, panel, widget_nameid_access(panel,
        WID_CANCEL_ACTION), SDLK_ESCAPE, KMOD_NONE);
    
    edata->mode = EDITOR_MODE_TEST;
    
    return 0;
}

static int editor_start(struct xuni_t *xuni, struct panel_data_t *data) {
    set_caption("xuni editor");
    
    return 0;
}

static int editor_event(struct xuni_t *xuni, struct panel_data_t *data) {
    struct editor_data_t *edata = data->data;
    struct widget_t *cbox;
    int repaint = 0;
    panel_type_t *mode = data->event[PANEL_EVENT_EVENT].p.event.mode;
    SDL_Event *event = data->event[PANEL_EVENT_EVENT].p.event.event;
    
    switch(event->type) {
    case SDL_QUIT:
        *mode = (size_t)-1;
        break;
    case SDL_KEYDOWN:
        switch(event->key.keysym.sym) {
        case SDLK_F4:
            dump_widget_tree(xuni, widget_nameid_follow(xuni->gui->widget,
                PANEL_EDITOR, WID_AREA, (size_t)-1));
            break;
        case SDLK_F3:
            dump_widget_tree_xml(xuni, widget_nameid_follow(xuni->gui->widget,
                PANEL_EDITOR, WID_AREA, (size_t)-1), "editor-dump.xml");
            break;
        default:
            break;
        }
        
        break;
    case SDL_MOUSEMOTION:
        if(widget_nameid_follow(xuni->gui->widget,
            PANEL_EDITOR, WID_AREA_BOX, (size_t)-1)->sel) {
            
            char buffer[BUFSIZ], **bdata;
            struct widget_t *label;
            struct widget_t *box = widget_nameid_follow(xuni->gui->widget,
                PANEL_EDITOR, WID_AREA, (size_t)-1);
            
            label = widget_nameid_follow(xuni->gui->widget,
                PANEL_EDITOR, WID_POSITION_LABEL, (size_t)-1);
            bdata = (char **)&label->p.label->text;
            
            sprintf(buffer, "(%.2f,%.2f)",
                (event->motion.x - box->pos->real.x)
                    / (xuni->smode->width * (box->pos->scale.w / 100.0))
                    * 100.0,
                (event->motion.y - box->pos->real.y)
                    / (xuni->smode->height * (box->pos->scale.h / 100.0))
                    * 100.0);
            
            xuni_memory_free(*bdata);
            *bdata = xuni_memory_duplicate_string(buffer);
            
            widget_event(xuni, label, WIDGET_EVENT_RESCALE);
            
            repaint = 1;
        }
        
        if(edata->mode == EDITOR_MODE_ADD_BUTTON) {
            cbox = widget_nameid_follow(xuni->gui->widget,
                PANEL_EDITOR, WID_AREA_CURSOR_BOX, (size_t)-1);
            if(cbox->visibility & WIDGET_VISIBILITY_VISIBLE) {
                /* !!! this really needs to be simplified */
                cbox->pos->scale.w = (event->motion.x - cbox->pos->real.x)
                    / ((xuni->smode->width / 100.0)
                    * (cbox->base->pos->scale.w / 100.0));
                cbox->pos->scale.h = (event->motion.y - cbox->pos->real.y)
                    / ((xuni->smode->height / 100.0)
                    * (cbox->base->pos->scale.h / 100.0));
                if(cbox->pos->scale.w < 0) cbox->pos->scale.w = 0;
                if(cbox->pos->scale.h < 0) cbox->pos->scale.h = 0;
                
                widget_event(xuni, cbox->base, WIDGET_EVENT_RESCALE);
                
                repaint = 1;
            }
        }
        
        break;
    default:
        break;
    }
    
    return repaint;
}

#if !1
static int set_editor_widget_sel(panel_type_t mode, int xp, int yp, int click,
    void *vdata, struct xuni_t *xuni) {
    
    struct editor_data_t *data = vdata;
    struct widget_t *widget;
    
    switch(data->mode) {
    case EDITOR_MODE_ADD_BUTTON:
        widget = widget_nameid_follow(xuni->gui->widget,
            PANEL_EDITOR, WID_AREA_BOX, (size_t)-1);
        
        clear_widget_sel(xuni->gui->widget);
        
        if(pos_in_rect(xp, yp, widget->pos)) {
            widget->sel = 1;
            /*return 1;*/
        }
        
        break;
    default:
        break;
    }
    
    return set_widget_sel_repaint(&xuni->gui->sel, xp, yp, click,
        widget_nameid_access(xuni->gui->widget, mode));
}
#endif

static void handle_cancel_action(struct xuni_t *xuni, 
    struct editor_data_t *data) {
    
    if(data->mode == EDITOR_MODE_ADD_BUTTON) {
        struct widget_t *panel = widget_nameid_follow(xuni->gui->widget,
            PANEL_EDITOR, WID_AREA_CURSOR_BOX, (size_t)-1);
        
        panel->visibility &= ~WIDGET_VISIBILITY_VISIBLE;
        panel->visibility &= ~WIDGET_VISIBILITY_SELABLE;
    }
}

static void set_editor_mode(struct xuni_t *xuni, struct editor_data_t *data,
    enum editor_mode_t mode) {
    
    struct widget_t *modelabel;
    const char *text;
    
    if(mode == data->mode) return;
    
    data->mode = mode;
    
    handle_cancel_action(xuni, data);
    
    modelabel = widget_nameid_follow(xuni->gui->widget,
        PANEL_EDITOR, WID_MODE_LABEL, (size_t)-1);
    
    xuni_memory_free((void *)modelabel->p.label->text);
    text = get_mode_name(data->mode);
    text = xuni_memory_duplicate_string(text);
    modelabel->p.label->text = text;
    widget_event(xuni, modelabel, WIDGET_EVENT_RESCALE);
}

static void set_all_widget_visibilities(struct widget_t *widget,
    enum widget_visibility_t visibility) {
    
    size_t x;
    
    widget->visibility &= ~visibility;
    
    if(widget->compose) {
        for(x = 0; x < widget->compose->widgets; x ++) {
            set_all_widget_visibilities(widget->compose->widget[x],
                visibility);
        }
    }
}

static int editor_click(struct xuni_t *xuni, struct panel_data_t *data) {
    struct editor_data_t *edata = data->data;
    panel_type_t *mode = data->event[PANEL_EVENT_CLICK].p.click.mode;
    struct widget_t *widget = data->event[PANEL_EVENT_CLICK].p.click.widget;
    struct widget_t *cbox;
    int repaint = 0;
    int xp, yp;
    
    switch(widget->id) {
    case WID_QUIT:
        *mode = (size_t)-1;
        break;
    case WID_MODE_TEST:
        widget_nameid_follow(xuni->gui->widget, PANEL_EDITOR, WID_AREA_BOX,
            (size_t)-1)->visibility &= ~WIDGET_VISIBILITY_CLICKABLE;
        
        set_editor_mode(xuni, edata, EDITOR_MODE_TEST);
        
        break;
    case WID_CANCEL_ACTION:
        handle_cancel_action(xuni, edata);
        
        repaint = 1;
        break;
    case WID_MODE_DELETE_WIDGET:
        widget_nameid_follow(xuni->gui->widget, PANEL_EDITOR, WID_AREA_BOX,
            (size_t)-1)->visibility &= ~WIDGET_VISIBILITY_CLICKABLE;
        
        set_editor_mode(xuni, edata, EDITOR_MODE_DELETE_WIDGET);
        
        break;
    case WID_MODE_ADD_BUTTON:
        widget_nameid_follow(xuni->gui->widget, PANEL_EDITOR, WID_AREA_BOX,
            (size_t)-1)->visibility |= WIDGET_VISIBILITY_CLICKABLE;
        
        /*set_all_widget_visibilities(widget_nameid_follow(xuni->gui->widget,
            PANEL_EDITOR, WID_AREA_BOX, (size_t)-1),
                WIDGET_VISIBILITY_SELABLE);*/
        
        set_editor_mode(xuni, edata, EDITOR_MODE_ADD_BUTTON);
        
        break;
    case WID_AREA_BOX:
        if(edata->mode == EDITOR_MODE_ADD_BUTTON) {
            cbox = widget_nameid_access(widget->base->base,
                WID_AREA_CURSOR_BOX);
            
            if(cbox->visibility & WIDGET_VISIBILITY_VISIBLE) {
                cbox->visibility &= ~WIDGET_VISIBILITY_VISIBLE;
                
                /* Only add the button if its width and height are nonzero. */
                if(cbox->pos->scale.w && cbox->pos->scale.h) {
                    add_allocate_widget_compose(widget->base, "button");
                    
                    init_widget_pos(last_compose_widget(widget->base),
                        cbox->pos->scale.x, cbox->pos->scale.y,
                        cbox->pos->scale.w, cbox->pos->scale.h,
                        POS_PACK_NONE);
                    init_button(last_compose_widget(widget->base), 0);
                    
                    widget_event(xuni, last_compose_widget(widget->base),
                        WIDGET_EVENT_RESCALE);
                }
            }
            else {
                cbox->visibility |= WIDGET_VISIBILITY_VISIBLE;
                
                SDL_GetMouseState(&xp, &yp);
                xp -= cbox->base->pos->real.x;
                yp -= cbox->base->pos->real.y;
                cbox->pos->scale.x = (double)xp / cbox->base->pos->real.w
                    * 100.0;
                cbox->pos->scale.y = (double)yp / cbox->base->pos->real.h
                    * 100.0;
                cbox->pos->scale.w = 0;
                cbox->pos->scale.h = 0;
                
                widget_event(xuni, cbox, WIDGET_EVENT_RESCALE);
            }
        }
        
        repaint = 1;
        break;
    default:
        if(widget_is_parent(widget_nameid_follow(xuni->gui->widget,
            PANEL_EDITOR, WID_AREA, (size_t)-1), widget)) {
            
            if(edata->mode == EDITOR_MODE_DELETE_WIDGET) {
                delete_widget_pointer(xuni, widget);
            }
        }
        
        break;
    }
    
    return repaint;
}

static int editor_deactivate(struct xuni_t *xuni, struct panel_data_t *data) {
    return 0;
}

static int editor_paint(struct xuni_t *xuni, struct panel_data_t *data) {
    /*struct widget_t *area = widget_nameid_follow(xuni->gui->widget,
        PANEL_EDITOR, WID_AREA, (size_t)-1);*/
    
    clear_screen(xuni->smode->screen);
    
    widget_event(xuni, widget_nameid_access(xuni->gui->widget, PANEL_EDITOR),
        WIDGET_EVENT_PAINT);
    
    /*{
        int x, y;
        SDL_GetMouseState(&x, &y);
        lineRGBA(xuni->smode->screen, area->pos->real.x, area->pos->real.y,
            x, y, 255, 255, 255, 255);
    }*/
    
    update_screen(xuni);
    
    return 0;
}

static int editor_free(struct xuni_t *xuni, struct panel_data_t *data) {
    return 0;
}

#ifdef LOADSO_STATIC_VERSION
func_point_t xuni_loadso_load_function(loadso_t object, const char *func) {
    struct string_function_t data[] = {
        {0, (func_point_t)0}
    };
    func_point_t funcp
        = string_to_function(data, sizeof(data) / sizeof(*data), func);
    
    if(!funcp) {
        log_message(ERROR_TYPE_RESOURCE, 0, __FILE__, __LINE__,
            "Unknown function: \"%s\"", func);
    }
    
    return funcp;
}
#endif
