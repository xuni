/*! \file editor.h

*/

#ifndef XUNI_GUARD_EDITOR_H
#define XUNI_GUARD_EDITOR_H

#define SETTINGS_FILE "./editor.xml"
#define LOG_FILE "./editor.log"

#ifdef __cplusplus
extern "C" {
#endif

enum panel_type_name_t {
    PANEL_NONE = -1,
    PANEL_EDITOR,
    PANELS
};

enum editor_mode_t {
    EDITOR_MODE_TEST,
    EDITOR_MODE_DELETE_WIDGET,
    EDITOR_MODE_ADD_BUTTON
};

struct editor_data_t {
    enum editor_mode_t mode;
};

#ifdef __cplusplus
}
#endif

#endif
