/*! \file graphics.c

*/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>  /* for save_screenshot()'s calls */

#include "SDL_image.h"

#include "error.h"
#include "graphics.h"
#include "gui.h"
#include "memory.h"
#include "resource/resource.h"

static void load_icon(const char *file);
static Uint32 get_flags(const struct smode_t *smode);
static SDL_Surface *set_graphics_mode(const struct smode_t *smode);
static int try_resizing_screen(struct smode_t *smode);
static void call_blit_surface(SDL_Surface *from, SDL_Rect *fromrect,
    SDL_Surface *to, SDL_Rect *torect);
static void fill_area_no_alpha(SDL_Surface *screen,
    int x, int y, int w, int h, Uint8 r, Uint8 g, Uint8 b);
static Uint32 get_pixel(SDL_Surface *surface, int x, int y);

void restrict_int(int *value, int max) {
    if(*value < 0) *value = 0;
    else if(*value > max) *value = max;
}

void restrict_pos(double *pos) {
    if(*pos < 0.0) *pos = 0.0;
    else if(*pos > 100.0) *pos = 100.0;
}

void wrap_int(int *value, int max) {
    if(*value < 0) *value = max - (-*value % max);
    else *value %= max;
}

void init_smode(struct smode_t *smode, struct resource_t *settings) {
    smode->screen = 0;
    smode->width = (int)lookup_resource_number(settings, 640,
        "xuni-resource", "screenmode", "width", 0);
    smode->height = (int)lookup_resource_number(settings, 480,
        "xuni-resource", "screenmode", "height", 0);
    smode->depth = (int)lookup_resource_number(settings, 0,
        "xuni-resource", "screenmode", "depth", 0);
    smode->fullscreen = (int)lookup_resource_number(settings, 0,
        "xuni-resource", "screenmode", "fullscreen", 0);
    
    smode->focus = -1;
    
    smode->restrictfocus = (int)lookup_resource_number(settings, 0,
        "xuni-resource", "screenmode", "restrict", 0);
}

/*! Initializes the SDL and SDL_ttf. Also sets the screen mode, sets the
    caption to "Loading", disables SDL UNICODE translation, sets the focus,
    enables key repeating, loads the icon for the window, etc.
    \param smode The screen mode structure to read the screen mode from.
    \param icon The icon to use for the xuni application window.
*/
void init_sdl_libraries(struct smode_t *smode, const char *icon) {
    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0) {
        log_message(ERROR_TYPE_FATAL, ERROR_FLAG_SDL, __FILE__, __LINE__,
            "Can't init SDL");
    }
    
    load_icon(icon);
    
    smode->screen = set_graphics_mode(smode);
    if(!smode->screen) {
        /* try default screen mode? */
        log_message(ERROR_TYPE_FATAL, ERROR_FLAG_SDL, __FILE__, __LINE__,
            "Can't set graphics mode %ix%ix%i (%s)",
            smode->width, smode->height, smode->depth,
            smode->fullscreen ? "fullscreen" : "windowed");
    }
    
    set_caption("Loading . . .");
    
    smode->restrictfocus = set_focus(smode->restrictfocus);
    
    focus_changed(smode, SDL_APPACTIVE);
    
    SDL_EnableUNICODE(0);
    
    if(TTF_Init() < 0) {
        /* !!! this doesn't have to be a fatal error */
        log_message(ERROR_TYPE_FATAL, 0, __FILE__, __LINE__,
            "Can't init SDL_ttf: %s", TTF_GetError());
    }
    
    SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY,
        SDL_DEFAULT_REPEAT_INTERVAL);
}

/*! Tries to set the input grab mode ("focus") to \a mode, but only if the
    current grab mode differs from the requested grab mode. If the modes are
    the same, it does nothing. Regardless, it returns the new grab mode.
    \param mode The mode to set the grab mode ("focus") to.
    \return The new grab mode. This could differ from \a mode if the function
        failed to set the grab mode.
*/
SDL_GrabMode set_focus(SDL_GrabMode mode) {
    if(SDL_WM_GrabInput(SDL_GRAB_QUERY) != mode) {
        return SDL_WM_GrabInput(mode);
    }
    
    return mode;
}

/*! Determines whether the xuni windows has a specific type of focus or not.
    \param focus The OR'd focus flags to check for, SDL_APP*.
    \return True if the window has the focus specified in \a focus.
*/
int focus_in(Uint8 focus) {
    return (SDL_GetAppState() & focus) == focus;
}

/*! Sets \a smode->focus, based on the flags in \a focus.
    \param smode The structure which contains the \c focus member to set.
    \param focus The focus which, if true, to consider the xuni window active.
    \return Nonzero if the focus has changed.
*/
int focus_changed(struct smode_t *smode, Uint8 focus) {
    /*printf("focus_changed(): mouse=%i input=%i active=%i\n",
        SDL_GetAppState() & SDL_APPMOUSEFOCUS,
        SDL_GetAppState() & SDL_APPINPUTFOCUS,
        SDL_GetAppState() & SDL_APPACTIVE);*/
    
    if(focus_in(focus)) {
        if(!smode->focus) {
            smode->focus = 1;
            return 1;
        }
    }
    else if(smode->focus) {
        smode->focus = 0;
        return 1;
    }
    
    return 0;
}

/*! Uninitializes the SDL and SDL_ttf libraries; the other SDL libraries
    require no such de-initialization.
*/
void quit_sdl_libraries(void) {
    TTF_Quit();
    SDL_Quit();
}

void free_smode(struct smode_t *smode) {

}

/*! Frees the memory allocated for a theme_t structure.
    \param theme The theme_t structure to free the contents of.
*/
void free_theme(struct theme_t *theme) {
    SDL_FreeCursor(theme->cursors.text);
}

/*! Loads and sets an icon for the SDL window. If the icon could not be
    opened, prints a warning instead.
    
    \param file The file to use as an icon, in any format supported by
        SDL_image. Note that Windows icons must be 32x32 to work correctly.
*/
static void load_icon(const char *file) {
    SDL_Surface *icon;
    
    if(!file) return;
    
    icon = IMG_Load(file);
    
    if(icon) {
        SDL_WM_SetIcon(icon, NULL);
        SDL_FreeSurface(icon);
    }
    else {
        log_message(ERROR_TYPE_WARNING, ERROR_FLAG_SDL, __FILE__, __LINE__,
            "Can't open application icon \"%s\"", file);
    }
}

/*! Save screenshots to shot_<time>[_<number>].bmp, where number is the Nth
    screenshot this second. A number is only appended if a screenshot has
    already been taken this second.
    
    time is of the format mm-dd-yy_at_hh-mm-ss; hours are in 24-hour time.
    
    This function is not thread-safe, because it calls localtime().
    
    \param screen The surface to save to the file (usually the main screen).
*/
void save_screenshot(SDL_Surface *screen) {
    char filename[BUFSIZ];
    static time_t prevt = (time_t)-1;  /* error return value of mktime() */
    static int count = 0;
    time_t t = time(NULL);
    struct tm *local = localtime(&t);
    int pos;
    
    pos = sprintf(filename, "shot_%02i-%02i-%02i_at_%02i-%02i-%02i",
        local->tm_mday, local->tm_mon + 1, local->tm_year % 100,
        local->tm_hour, local->tm_min, local->tm_sec);
    
    if(t == prevt) {
        /* +1 to start at _2 */
        pos += sprintf(filename + pos, "_%i", ++count + 1);
    }
    else count = 0;
    
    prevt = t;  /* hopefully time_ts can be assigned */
    
    strcpy(filename + pos, ".bmp");
    
    if(SDL_SaveBMP(screen, filename)) {
        log_message(ERROR_TYPE_WARNING, ERROR_FLAG_SDL, __FILE__, __LINE__,
            "Can't save screenshot \"%s\"", filename);
    }
    else {
        log_message(ERROR_TYPE_LOG, 0, __FILE__, __LINE__,
            "Screenshot saved as \"%s\"", filename);
        
        /* perhaps compress the screenshot here? */
    }
}

void paint_cursor(struct xuni_t *xuni, struct widget_t *cursor) {
    int xp, yp;
    
    if(!cursor || !focus_in(SDL_APPMOUSEFOCUS)) return;
    
    SDL_GetMouseState(&xp, &yp);
    
    prepare_paint_image(xuni, cursor);
    
    blit_surface(xuni->smode->screen,
        cursor->p.image->image,
        xp - cursor->p.image->image->w / 2,
        yp - cursor->p.image->image->h / 2);
}

void show_cursor(int enable) {
    int prev = SDL_ShowCursor(enable ? SDL_ENABLE : SDL_DISABLE);
    
    /* Handles a bug in the X11 video driver: when the cursor is disabled,
        moved, and then re-enabled, it is painted in the original position,
        but otherwise the SDL thinks it is in the new position. Calling
        SDL_GetMouseState() returns the new position. It takes a mouse motion
        event (generated here by SDL_WarpMouse()) for everything to work
        properly.
        
        (This is with SDL 1.2.12.)
    */
    if(prev == SDL_DISABLE && enable) {
        int x, y;
        SDL_GetMouseState(&x, &y);
        SDL_WarpMouse(x, y);
    }
}

/*! Opens the image \a filename as an SDL_Surface. Logs a warning if the file
    could not be opened.
    \param filename The image to open (in any format supported by SDL_image).
    \return The opened image (as an SDL_Surface) or NULL on error.
*/
SDL_Surface *load_image(const char *filename) {
    SDL_Surface *intermediate, *image;
    
    if(!filename || !*filename) return 0;
    
    intermediate = IMG_Load(filename);
    
    if(!intermediate) {
        log_message(ERROR_TYPE_WARNING, ERROR_FLAG_SDL, __FILE__, __LINE__,
            "Can't open image \"%s\"", filename);
        return 0;
    }
    
    image = SDL_DisplayFormatAlpha(intermediate);
    SDL_FreeSurface(intermediate);
    
    increment_allocated(allocated_sdl_surface(image));
    
    return image;
}

int resize_screen(struct smode_t *smode, const SDL_ResizeEvent *resize) {
    int x = smode->width;
    int y = smode->height;
    
    smode->width = resize->w;
    smode->height = resize->h;
    
    if(try_resizing_screen(smode)) {
        smode->width = x;
        smode->height = y;
        
        return 1;
    }
    
    return 0;
}

/*! Toggles fullscreen mode for the xuni window.
    
    First tries SDL_WM_ToggleFullScreen(); if that fails, it then tries
    try_resizing_screen().
    
    \param smode The current screen mode that the xuni window is in.
    \return Zero on success, nonzero on failure.
*/
int toggle_fullscreen(struct smode_t *smode) {
    smode->fullscreen = !smode->fullscreen;
    
    /*return 0;*/
    
    if(SDL_WM_ToggleFullScreen(smode->screen)) return 0;
    
    if(!try_resizing_screen(smode)) return 0;
    
    smode->fullscreen = !smode->fullscreen;  /* undo previous toggling */
    log_message(ERROR_TYPE_WARNING, ERROR_FLAG_SDL, __FILE__, __LINE__,
        "Can't toggle fullscreen to \"%s\"",
        smode->fullscreen ? "fullscreen" : "windowed");
    
    return 1;
}

void use_screen_mode(struct xuni_t *xuni, int width, int height) {
    SDL_Event event;
    
    event.resize.w = width;
    event.resize.h = height;
    
    if(!resize_screen(xuni->smode, &event.resize)) {
        widget_event(xuni, xuni->gui->widget, WIDGET_EVENT_RESCALE);
    }
}

/*! Changes the title of the xuni window to the printf-compatible arguments
    passed.
    \param format The format string to change the title of the xuni window to.
    \param ... The printf-compatible arguments matching format specifiers in
        \a format.
*/
void set_caption(const char *format, ...) {
    va_list arg;
    char *wt;
    
    va_start(arg, format);
    wt = malloc(BUFSIZ + 1);
    
    vsprintf(wt, format, arg);
    
    SDL_WM_SetCaption(wt, NULL);
    
    free(wt);
    va_end(arg);
}

static Uint32 get_flags(const struct smode_t *smode) {
    Uint32 flags = SDL_SWSURFACE | SDL_RESIZABLE | SDL_ANYFORMAT;
    if(smode->fullscreen) flags |= SDL_FULLSCREEN;
    
    return flags;
}

static SDL_Surface *set_graphics_mode(const struct smode_t *smode) {
    return SDL_SetVideoMode(smode->width, smode->height, smode->depth,
        get_flags(smode));
}

SDL_Rect **list_graphics_modes(struct smode_t *smode) {
    return SDL_ListModes(NULL, get_flags(smode));
}

static int try_resizing_screen(struct smode_t *smode) {
    SDL_Surface *nscreen = set_graphics_mode(smode);
    
    if(nscreen) smode->screen = nscreen;
    
    return nscreen == 0;
}

/*! Locks the video surface so that data can be written directly to, pixel by
    pixel.
    
    \param screen The surface to lock (should be the screen, the main video
        surface).
*/
void lock_screen(SDL_Surface *screen) {
    if(SDL_MUSTLOCK(screen)) {
        if(SDL_LockSurface(screen) < 0) {
            log_message(ERROR_TYPE_WARNING, ERROR_FLAG_SDL,
                __FILE__, __LINE__, "Can't lock screen (%p)", (void *)screen);
        }
    }
}

/*! Reverts the effects of lock_screen(). That is, unlocks the video surface
    so that it can be written to normally, with surface blitting and so on.
    
    \param screen The surface to unlock (should be the screen, the main video
        surface).
*/
void unlock_screen(SDL_Surface *screen) {
    if(SDL_MUSTLOCK(screen)) {
        SDL_UnlockSurface(screen);
    }
}

void clear_screen(SDL_Surface *screen) {
    SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 0, 0, 0));
}

SDL_Surface *new_surface(int w, int h, int d, int alpha) {
    SDL_Surface *surface = SDL_CreateRGBSurface(SDL_SWSURFACE, w, h, d,
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
        0xff000000,  /* red */
        0x00ff0000,  /* green */
        0x0000ff00,  /* blue */
        alpha && 0x000000ff
#else
        0x000000ff,  /* red */
        0x0000ff00,  /* green */
        0x00ff0000,  /* blue */
        alpha && 0xff000000
#endif
        );
    
    if(!surface) {
        log_message(ERROR_TYPE_FATAL, ERROR_FLAG_SDL, __FILE__, __LINE__,
            "Can't create new %ix%ix%i surface in %s edian", w, h, d,
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
            "big"
#else
            "little"
#endif
            );
    }
    
    return surface;
}

void free_surface(SDL_Surface *surface) {
    decrement_allocated(allocated_sdl_surface(surface));
    
    if(/*xuni_memory_decrement*/(surface)) SDL_FreeSurface(surface);
}

static void call_blit_surface(SDL_Surface *from, SDL_Rect *fromrect,
    SDL_Surface *to, SDL_Rect *torect) {
    
    if(!from || !to) {
        log_message(ERROR_TYPE_WARNING, 0, __FILE__, __LINE__,
            "Can't blit NULL surface: blitting %p to %p",
            (void *)from, (void *)to);
        return;
    }
    
    if(SDL_BlitSurface(from, fromrect, to, torect) < 0) {
        if(fromrect) {
            log_message(ERROR_TYPE_WARNING, ERROR_FLAG_SDL, __FILE__,
                __LINE__,
                "Can't blit %lux%lu from (%lu,%lu) of surface %p (%lux%lu)",
                (unsigned long)fromrect->w, (unsigned long)fromrect->h,
                (unsigned long)fromrect->x, (unsigned long)fromrect->y,
                (void *)from, (unsigned long)from->w, (unsigned long)from->h);
        }
        else {
            log_message(ERROR_TYPE_WARNING, ERROR_FLAG_SDL, __FILE__,
                __LINE__,
                "Can't blit all (NULL rect) of surface %p (%lux%lu)",
                (void *)from, (unsigned long)from->w, (unsigned long)from->h);
        }
        
        if(torect) {
            log_message(ERROR_TYPE_WARNING, ERROR_FLAG_CONTINUED, __FILE__,
                __LINE__,
                "to surface %p (%lux%lu) at (%lu,%lu)",
                (void *)to, (unsigned long)to->w, (unsigned long)to->h,
                (unsigned long)torect->x, (unsigned long)torect->y);
        }
        else {
            log_message(ERROR_TYPE_WARNING, ERROR_FLAG_CONTINUED, __FILE__,
                __LINE__,
                "to surface %p (%lux%lu) at (0,0) (NULL rect)",
                (void *)to, (unsigned long)to->w, (unsigned long)to->h);
        }
    }
}

void blit_surface(SDL_Surface *screen, SDL_Surface *image, int xp, int yp) {
    SDL_Rect rect;
    
    if(!image) return;
    
    rect.x = xp;
    rect.y = yp;
    
    call_blit_surface(image, NULL, screen, &rect);
}

void blit_surface_area(SDL_Surface *screen, SDL_Surface *image,
    int tx, int ty, int fx, int fy, int fw, int fh) {
    
    SDL_Rect from, to;
    
    from.x = fx;
    from.y = fy;
    from.w = fw;
    from.h = fh;
    to.x = tx;
    to.y = ty;
    
    call_blit_surface(image, &from, screen, &to);
}

void blit_surface_repeat(SDL_Surface *screen, SDL_Surface *image,
    int xp, int yp, int w, int h) {

    int x, y;
    SDL_Rect srect, drect;

    srect.x = 0;
    srect.y = 0;

    for(x = 0; x < w; x += image->w) {
        for(y = 0; y < h; y += image->h) {
            drect.x = xp + x;
            drect.y = yp + y;

            if(x + image->w > w) srect.w = w - x;
            else srect.w = image->w;
            if(y + image->h > h) srect.h = h - y;
            else srect.h = image->h;

            call_blit_surface(image, &srect, screen, &drect);
        }
    }
}

#if !1
/* !!! this function accounts for nearly 100% of xuni's execution time */
void blit_surface_repeat_area(SDL_Surface *screen, SDL_Surface *image,
    int tx, int ty, int tw, int th, int fx, int fy, int fw, int fh) {
    
    int x, y;
    SDL_Rect srect, drect;
    
    srect.x = fx;
    srect.y = fy;
    
    for(x = 0; x < tw; x += fw) {
        for(y = 0; y < th; y += fh) {
            drect.x = tx + x;
            drect.y = ty + y;
            
            if(x + fw > tw) srect.w = tw - x;
            else srect.w = fw;
            if(y + fh > th) srect.h = th - y;
            else srect.h = fh;
            
            call_blit_surface(image, &srect, screen, &drect);
        }
    }
}
#else
void blit_surface_repeat_area(SDL_Surface *screen, SDL_Surface *image,
    int tx, int ty, int tw, int th, int fx, int fy, int fw, int fh) {
    
    int x, y;
    SDL_Rect srect, drect;
    
    srect.x = fx;
    srect.y = fy;
    
    srect.w = fw;
    
    for(x = 0; x < tw; x += fw) {
        drect.x = tx + x;
        
        if(x + fw > tw) srect.w = tw - x;
        
        srect.h = fh;
        
        for(y = 0; y < th; y += fh) {
            drect.y = ty + y;
            
            if(y + fh > th) srect.h = th - y;
            
            call_blit_surface(image, &srect, screen, &drect);
        }
    }
}
#endif

void blit_surface_fill_from(SDL_Surface *screen, SDL_Surface *image,
    int tx, int ty, int tw, int th, int fx1, int fy1) {
    
    int x, y;
    SDL_Rect srect, drect;
    
    /*x = image->w - fx1 < tw ? image->w - fx1 : tw;
    y = image->h - fy1 < th ? image->h - fy1 : th;
    blit_surface_area(screen, image,
        tx, ty, fx1, fy1, x, y);*/
    
    for(x = fx1 - image->w; x < tw; x += image->w) {
        for(y = fy1 - image->h; y < th; y += image->h) {
            if(x < 0) {
                drect.x = tx;
                srect.x = -x;
                srect.w = image->w - -x;
            }
            else {
                drect.x = tx + x;
                
                srect.x = 0;
                if(x + image->w > tw) srect.w = tw - x;
                else srect.w = image->w;
            }
            
            if(y < 0) {
                drect.y = ty;
                srect.y = -y;
                srect.h = image->h - -y;
            }
            else {
                drect.y = ty + y;
                
                srect.y = 0;
                if(y + image->h > th) srect.h = th - y;
                else srect.h = image->h;
            }
            
            call_blit_surface(image, &srect, screen, &drect);
        }
    }
}

void update_screen(struct xuni_t *xuni) {
    SDL_UpdateRect(xuni->smode->screen, 0, 0, 0, 0);
}

/*! Returns true if the position (\a xp, \a yp) is inside the rectangle
    between (\a x, \a y) and (\c x+w, \c y+h).
    \param xp The x coordinate of the position to check.
    \param yp The y coordinate as the position to check.
    \param x The x coordinate of the upper-left corner of the rectangle.
    \param y The y coordinate of the upper-left corner of the rectangle.
    \param w The width of the rectangle.
    \param h The height of the rectangle.
    \return True if the position is inside the rectangle.
*/
int in_rect(int xp, int yp, int x, int y, int w, int h) {
    return xp >= x && xp < x + w && yp >= y && yp < y + h;
}

/*! Returns true if the position (\a xp, \a yp) is inside the rectangle
    delimited by the SDL_Rect \a r.
    \param xp The x-coordinate of the position to check.
    \param yp The y-coordinate of the position to check.
    \param r The rectangle to look for a position inside.
    \return True if the position is inside the rectangle.
*/
int in_sdl_rect(int xp, int yp, const SDL_Rect *r) {
    return xp >= r->x && xp < r->x + r->w && yp >= r->y && yp < r->y + r->h;
}

int pos_in_rect(int xp, int yp, const struct pos_t *pos) {
    /*if(!pos->clip)*/ return in_sdl_rect(xp, yp, &pos->real);
    
#if 0
    xp -= pos->real.x;
    yp -= pos->real.y;
    
#if !1
    printf("(%i,%i) in (%i+%i=%i,%i+%i=%i) by (%i,%i)\n",
        xp, yp,
        pos->real.x, pos->clip->xoff, /*pos->clip->xclip,*/
            pos->real.x + pos->clip->xoff /*+ pos->clip->xclip*/,
        pos->real.y, pos->clip->yoff, /*pos->clip->yclip,*/
            pos->real.y + pos->clip->yoff /*+ pos->clip->yclip*/,
        pos->clip->wclip, pos->clip->hclip);
    
    printf("origin values: (%i,%i) is in (%i,%i)\n",
        xp - pos->real.x - pos->clip->xoff,
        yp - pos->real.y - pos->clip->yoff,
        pos->clip->wclip, pos->clip->hclip);
#else
    printf("(%i,%i) in (%i,%i) by (%i,%i)\n", xp, yp,
        pos->clip->xoff /*+ pos->clip->xclip*/,
        pos->clip->yoff /*+ pos->clip->yclip*/,
        pos->clip->wclip, pos->clip->hclip);
#endif
    
    printf(" is %i\n", in_rect(xp, yp,
        pos->clip->xoff /*+ pos->clip->xclip*/,
        pos->clip->yoff /*+ pos->clip->yclip*/,
        pos->clip->wclip, pos->clip->hclip));
    
    return in_rect(xp, yp,
        pos->clip->xoff /*+ pos->clip->xclip*/,
        pos->clip->yoff /*+ pos->clip->yclip*/,
        pos->clip->wclip, pos->clip->hclip);
#endif
}

#if 0
static struct widget_t *widget_selable(struct widget_t *widget) {
    if(!widget) return 0;
    
    /*printf("widget_selable(): \"%s\"\n", widget->name);*/
    
    while(widget->selable && widget->base) {
        widget = widget->base;
        /*printf("    -> \"%s\"\n", widget->name);*/
    }
    
    return widget;
}
#endif

static void fill_area_no_alpha(SDL_Surface *screen,
    int x, int y, int w, int h, Uint8 r, Uint8 g, Uint8 b) {
    
    SDL_Rect rect;
    
    rect.x = x;
    rect.y = y;
    rect.w = w;
    rect.h = h;
    
    SDL_FillRect(screen, &rect, SDL_MapRGB(screen->format, r, g, b));
}

void fill_area(SDL_Surface *screen, int x, int y, int w, int h,
    SDL_Surface *image, int ix, int iy) {
    
    Uint8 r, g, b, a;
    
    SDL_GetRGBA(get_pixel(image, ix, iy), image->format, &r, &g, &b, &a);
    
    if(a == 255) {  /* opaque, can use fast filling */
        fill_area_no_alpha(screen, x, y, w, h, r, g, b);
    }
    else {
        blit_surface_repeat_area(screen, image, x, y, w, h, ix, iy, 1, 1);
    }
}

static Uint32 get_pixel(SDL_Surface *surface, int x, int y) {
    Uint8 *p;
    
    if(!surface->pixels) return 0;
    
    p = (Uint8 *)surface->pixels
        + y * surface->pitch
        + x * surface->format->BytesPerPixel;
    
    if(x < 0 || y < 0 || x >= surface->w || y >= surface->h) return 0;
    
    switch(surface->format->BytesPerPixel) {
    case 1:
        return *p;
    case 2:
        return *(Uint16 *)p;
    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
            return p[0] << 16 | p[1] << 8 | p[2];
        }
        else return p[0] | p[1] << 8 | p[2] << 16;
    case 4:
        return *(Uint32 *)p;
    default:
        return 0;
    }
}
