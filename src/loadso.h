/*! \file loadso.h

*/

#ifndef XUNI_GUARD_LOADSO_H
#define XUNI_GUARD_LOADSO_H

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

#if !defined(LOADSO_SDL_VERSION) && !defined(LOADSO_DLOPEN_VERSION) \
    && !defined(LOADSO_STATIC_VERSION)
    
    #define LOADSO_SDL_VERSION
#endif

typedef void *loadso_t;
typedef void (*func_point_t)(void);

struct loadso_history_t {
    loadso_t object;
    const char *name;
};

struct loadso_array_t {
    struct loadso_history_t *object;
    size_t objects;
};

loadso_t xuni_loadso_load_object(struct loadso_array_t *loadso,
    const char *filename);
func_point_t xuni_loadso_load_function(loadso_t object, const char *func);
void xuni_loadso_free_object(loadso_t object);

void init_loadso(struct loadso_array_t *loadso);
void free_loadso(struct loadso_array_t *loadso);

#ifdef __cplusplus
}
#endif

#endif
