/*! \file xmlwrite.h

*/

#ifndef XUNI_GUARD_XML_WRITE_H
#define XUNI_GUARD_XML_WRITE_H

#include "resource.h"

#ifdef __cplusplus
extern "C" {
#endif

void xmlwrite_write_resource(struct resource_data_t *resource,
    const char *filename);

#ifdef __cplusplus
}
#endif

#endif
