/*! \file libmxml.h

*/

#ifndef XUNI_GUARD_LIBMXML_H
#define XUNI_GUARD_LIBMXML_H

#include "resource.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef RESOURCE_LIBRARY_MXML

void parse_resource_file(struct resource_data_t *resource,
    const char *filename, FILE *fp);

#endif

#ifdef __cplusplus
}
#endif

#endif
