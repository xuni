/*! \file resource.c

*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>

#include "../error.h"
#include "../memory.h"

#include "calcfunc.h"
#include "resource.h"

static struct resource_specific_xml_t *new_whitespace_data(void);

static size_t filename_path_length(const char *filename);
static void prepend_directory_to_filename(struct resource_list_t *list,
    char **path);

static const char *lookup_resource_va(struct resource_t *resource,
    va_list arg);

void init_resource(struct resource_t *resource) {
    const char *name = "xuni-resource";
    
    resource->data = new_resource_data(RESOURCE_TAG, 0,
        RESOURCE_SPECIFIC_NONE);
    resource->data->data.tag
        = xuni_memory_allocate(sizeof(*resource->data->data.tag));
    resource->data->data.tag->name
        = xuni_memory_duplicate_string(name);
    resource->data->data.tag->list = 0;
}

/*! Writes the resource tree \a resource into the file \a filename. At the
    moment, this file will always be in semi-readable XML. (Information about
    which resource file a resource element came from is not saved, so only one
    file is generated with the entire tree.)
    
    \param resource The resource tree to recursively write to the file.
    \param filename The file to save the resource tree to.
*/
void write_resource(struct resource_data_t *resource, const char *filename) {
    xmlwrite_write_resource(resource, filename);
}

/*! Allocates and initializes an empty struct resource_list_t.
    \return The newly allocated resource_list_t.
*/
struct resource_list_t *new_resource_list(void) {
    struct resource_list_t *list = xuni_memory_allocate(sizeof(*list));
    
    list->data = 0;
    list->n = 0;
    
    return list;
}

struct resource_data_t *new_resource_data(
    enum resource_data_type_t type, struct resource_data_t *base,
    enum resource_specific_type_t specifictype) {
    
    struct resource_data_t *data = xuni_memory_allocate(sizeof(*data));
    
    data->base = base;
    data->type = type;
    data->specific.data = 0;
    data->specific.type = specifictype;
    
    return data;
}

struct resource_data_t *create_element(struct resource_data_t *base,
    const char *name, enum resource_specific_type_t specifictype) {
    
    struct resource_data_t *element
        = new_resource_data(RESOURCE_TAG, base, specifictype);
    
    element->data.tag = xuni_memory_allocate(sizeof(*element->data.tag));
    element->data.tag->name = xuni_memory_duplicate_string(name);
    element->data.tag->list = 0;
    
    return element;
}

struct resource_list_t *append_resource_list(
    struct resource_list_t *list, struct resource_data_t *data) {
    
    if(!list) {
        list = new_resource_list();
    }
    
    list->data = xuni_memory_resize(
        list->data, (list->n + 1) * sizeof(*list->data));
    list->data[list->n ++] = data;
    
    return list;
}

static struct resource_specific_xml_t *new_whitespace_data(void) {
    struct resource_specific_xml_t *xml;
    
    xml = xuni_memory_allocate(sizeof(*xml));
    xml->whitespace = 0;
    xml->whitespaces = 0;
    
    return xml;
}

void add_whitespace(struct resource_data_t *data, char *whitespace, int len) {
    struct resource_specific_xml_t *xml;
    
    if(data->specific.type != RESOURCE_SPECIFIC_XML) {
        log_message(ERROR_TYPE_RESOURCE, 0, __FILE__, __LINE__,
            "Attempting to record whitespace for an unsupported tag type: %i",
            (int)data->specific.type);
        return;
    }
    
    if(!data->specific.data) {
        data->specific.data = new_whitespace_data();
    }
    
    xml = data->specific.data;
    
    xml->whitespace = xuni_memory_resize(xml->whitespace,
        (xml->whitespaces + 1) * sizeof(*xml->whitespace));
    xml->whitespace[xml->whitespaces]
        = xuni_memory_duplicate_string_len(whitespace, len);
    xml->whitespaces ++;
}

/*! Parses the resource file \a filename, storing the resulting resource parse
    tree in \a resource. All resource files referenced by the file \a filename
    are recursively included by calling make_paths_relative_to_xuni().
    
    This is the function that users of xuni should call to parse resource
    files.
    
    \param resource The data structure in which to store the data read from
        the resource file \a filename.
    \param filename The name of the resource file to parse recursively.
    \return Zero if the file \a filename was parsed successfully, nonzero
        otherwise.
*/
int parse_resource(struct resource_data_t *resource, const char *filename) {
    struct resource_list_t *list;
    
    parse_resource_filename(resource, filename);
    
    list = resource->data.tag->list;
    if(list && list->n && list->data[list->n - 1]->type == RESOURCE_TAG) {
        char *path = xuni_memory_duplicate_string_len(filename,
            filename_path_length(filename));
        
        /*search_resource_tag(list->data[list->n - 1], "include",
            1, include_resources, 0);*/
        
        make_paths_relative_to_xuni(list->data[list->n - 1], &path);
        xuni_memory_free(path);
        
        /*xuni_memory_decrement(
            set_relative_paths(list->data[list->n - 1], 0, 0, 0));*/
    }
    
    return 0;
}

/*! Parses the resource file \a filename, storing the resulting resource parse
    tree in \a resource. Uses {library}_parse_resource_file() to do the
    parsing. This function is not recursive -- make_paths_relative_to_xuni()
    does that.
    
    \param resource The data structure in which to store the data read from
        the resource file \a filename.
    \param filename The name of the resource file to parse. Currently, this
        must be in XML format.
    \return Zero if the file \a filename was parsed successfully, nonzero
        otherwise.
*/
int parse_resource_filename(struct resource_data_t *resource,
    const char *filename) {
    
    FILE *fp = fopen(filename, "r");
    
    if(!fp) {
        log_message(ERROR_TYPE_RESOURCE, 0, __FILE__, __LINE__,
            "Couldn't open resource file \"%s\"", filename);
        return 1;
    }
    
#if 0
    printf("--- Parsing resource file \"%s\"", filename);
#ifdef RESOURCE_LIBRARY_EXPAT
    puts(" with libexpat");
#elif defined(RESOURCE_LIBRARY_MXML)
    puts(" with libmxml");
#endif
#endif
    
    parse_resource_file(resource, filename, fp);
    
    fclose(fp);
    
    return 0;
}

/*static void include_resources(void *vdata, struct resource_data_t *data) {
    const char *filename;
    
    filename = first_resource_text(data->data.tag->list);
    if(filename) parse_resource(data, filename);
}*/

/*! Recursively examines the resource tree \a data, looking for "path" and
    "include" tags. Path tags are modified so that their path is relative to
    the xuni executable instead of to the original resource file, and include
    tags are used to recursively specify other resource files.
    
    \param data The existing resource tree to add to or modify.
    \param path The file path thus far, to the current resource file.
*/
void make_paths_relative_to_xuni(struct resource_data_t *data, char **path) {
    struct resource_list_t *list;
    const char *filename;
    size_t x, len, newlen;
    
    if(data->type != RESOURCE_TAG) return;
    
    list = data->data.tag->list;
    
    /* Empty tags (tags with no text or other tags inside of them) do not need
        to be examined.
    */
    if(!list) return;
    
    if(!strcmp(data->data.tag->name, "include")) {
        /* This is somewhat inefficient, but it is not executed very often. */
        
        prepend_directory_to_filename(list, path);
        
        filename = first_resource_text(list);
        
        newlen = filename_path_length(filename);
        
        *path = xuni_memory_resize(*path, newlen + 1);
        strncpy(*path, filename, newlen);
        (*path)[newlen] = 0;
        
        if(!first_resource_tag(list, "xuni-resource")) {
            parse_resource_filename(data, filename);
        }
    }
    else if(!strcmp(data->data.tag->name, "path")) {
        prepend_directory_to_filename(list, path);
    }
    
    len = strlen(*path);
    for(x = 0; x < list->n; x ++) {
        make_paths_relative_to_xuni(list->data[x], path);
        (*path)[len] = 0;
    }
    
    return;
}

/*! Calculates how much of \a filename is the path to the file.
    
    \param filename The filename to examine, which could include a relative or
        absolute path.
    \return The position in the string \a filename that the path ends and the
        filename begins. (The trailing directory separator is included in the
        path, if found.)
*/
static size_t filename_path_length(const char *filename) {
    size_t x;
    for(x = strlen(filename); x; x --) {
        if(filename[x] == '/' || filename[x] == '\\') break;
    }
    
    /* If a directory separator was found, include it in the length. */
    return x ? x + 1 : 0;
}

static void prepend_directory_to_filename(struct resource_list_t *list,
    char **path) {
    
#if 1
    char **relpath = first_resource_text_address(list);
    size_t relpathlen = strlen(*relpath);
    size_t pathlen = strlen(*path);
    
    *relpath = xuni_memory_resize(*relpath, relpathlen + pathlen + 1);
    memmove(*relpath + pathlen, *relpath, relpathlen + 1);
    memcpy(*relpath, *path, pathlen);
#else
    char *tempname;
    char **relpath = first_resource_text_address(list);
    size_t tlen;
    
    tempname = xuni_memory_duplicate_string(*path);
    tlen = strlen(tempname);
    tempname = xuni_memory_resize(tempname, tlen + strlen(*relpath) + 1);
    strcpy(tempname + tlen, *relpath);
    
    xuni_memory_free(*relpath);
    *relpath = tempname;
#endif
}

struct resource_list_t *first_resource_tag(struct resource_list_t *list,
    const char *name) {
    
    size_t x;
    
    if(!list) return 0;
    
    for(x = 0; x < list->n; x ++) {
        if(list->data[x]->type == RESOURCE_TAG
            || list->data[x]->type == RESOURCE_REF) {
            
            if(!strcmp(list->data[x]->data.tag->name, name)) {
                return list->data[x]->data.tag->list;
            }
        }
    }
    
    return 0;
}

char *first_resource_text(struct resource_list_t *list) {
    size_t x;
    
    if(!list) return 0;
    
    for(x = 0; x < list->n; x ++) {
        if(list->data[x]->type == RESOURCE_TEXT) {
            return list->data[x]->data.text->data;
        }
    }
    
    return 0;
}

char **first_resource_text_address(struct resource_list_t *list) {
    size_t x;
    
    if(!list) return 0;
    
    for(x = 0; x < list->n; x ++) {
        if(list->data[x]->type == RESOURCE_TEXT) {
            return &list->data[x]->data.text->data;
        }
    }
    
    return 0;
}

char *find_resource_text(struct resource_list_t *list, const char *data) {
    return first_resource_text(first_resource_tag(list, data));
}

static const char *lookup_resource_va(struct resource_t *resource,
    va_list arg) {
    
    struct resource_list_t *p = resource->data->data.tag->list;
    size_t x;
    const char *name;
    
    if(!p) return 0;
    
    while((name = va_arg(arg, const char *))) {
        for(x = 0; x < p->n; x ++) {
            if((p->data[x]->type == RESOURCE_TAG
                || p->data[x]->type == RESOURCE_REF)
                && !strcmp(p->data[x]->data.tag->name, name)) {
                
                p = p->data[x]->data.tag->list;
                x = (size_t)-1;
                break;
            }
        }
        
        if(x != (size_t)-1) return 0;
    }
    
    if(p->n && p->data[0]->type == RESOURCE_TEXT) {
        /*printf("Found: \"%s\"\n", p->data[0]->data.text->data);*/
        return p->data[0]->data.text->data;
    }
    
    return 0;
}

const char *lookup_resource_string(struct resource_t *resource,
    const char *def, ...) {
    
    const char *data;
    va_list arg;
    
    va_start(arg, def);
    data = lookup_resource_va(resource, arg);
    va_end(arg);
    
    return data ? data : def;
}

double lookup_resource_number(struct resource_t *resource, double def, ...) {
    const char *data;
    va_list arg;
    
    va_start(arg, def);
    data = lookup_resource_va(resource, arg);
    va_end(arg);
    
    return get_expression_value(data, def);
}

struct resource_list_t *lookup_resource_tag(struct resource_t *resource,
    struct resource_list_t *def, ...) {
    
    struct resource_list_t *list = resource->data->data.tag->list;
    const char *name;
    va_list arg;
    
    va_start(arg, def);
    
    while(list && (name = va_arg(arg, const char *))) {
        list = first_resource_tag(list, name);
    }
    
    va_end(arg);
    
    return list ? list : def;
}

void search_resource_tag(struct resource_data_t *data, const char *find,
    int recursive, search_resource_func_t search_resource_func, void *vdata) {
    
    struct resource_list_t *list;
    size_t x;
    
    if(!data || data->type != RESOURCE_TAG) return;
    list = data->data.tag->list;
    if(!list) return;
    
    for(x = 0; x < list->n; x ++) {
        if(list->data[x]->type == RESOURCE_TAG
            || list->data[x]->type == RESOURCE_REF) {
            
            if(!strcmp(list->data[x]->data.tag->name, find)) {
                if(list->data[x]->data.tag->list) {
                    (*search_resource_func)(vdata, list->data[x]);
                }
            }
            else if(recursive) {
                search_resource_tag(list->data[x],
                    find, /*recursive > 0 ? recursive - 1 : -1*/ recursive,
                    search_resource_func, vdata);
            }
        }
    }
}

void free_resource_data(struct resource_data_t *data) {
    if(!data) return;
    
    switch(data->type) {
    case RESOURCE_TEXT:
        xuni_memory_free(data->data.text->data);
        xuni_memory_free(data->data.text);
        break;
    case RESOURCE_TAG:
        free_resource_list(data->data.tag->list);
        xuni_memory_free(data->data.tag->name);
        xuni_memory_free(data->data.tag);
        break;
    case RESOURCE_REF:
        free_resource_data(xuni_memory_decrement(data->data.ref));
        break;
    default:
        printf("*** Unknown resource type: %i\n", (int)data->type);
        break;
    }
    
    xuni_memory_free(data);
}

void free_resource_list(struct resource_list_t *list) {
    size_t x;
    
    if(!list) return;
    
    for(x = 0; x < list->n; x ++) {
        free_resource_data(list->data[x]);
    }
    
    xuni_memory_free(list->data);
    xuni_memory_free(list);
}

void free_resource(struct resource_t *resource) {
    if(resource) {
        /*printf("==== Resources dump prior to freeing:\n");
        dump_list(resource->list);*/
        
        free_resource_data(resource->data);
    }
}

static int dumpi = 0;

void dump_data(struct resource_data_t *data);

static void dump_indent(void) {
    int x;
    
    for(x = 0; x < dumpi * 4; x ++) putchar(' ');
}

static void dump_list(struct resource_data_t *data) {
    struct resource_list_t *list = data->data.tag->list;
    size_t x;
    
    if(!list) {
        printf("*** NULL list\n");
        return;
    }
    
    dumpi ++;
    for(x = 0; x < list->n; x ++) {
        dump_data(list->data[x]);
    }
    dumpi --;
}

void dump_data(struct resource_data_t *data) {
    if(!data) {
        printf("*** NULL data\n");
        return;
    }
    
    dump_indent();
    
    if(data->type == RESOURCE_TEXT) {
        printf("\"%s\"\n", data->data.text->data);
    }
    else if(data->type == RESOURCE_TAG) {
        printf("[%s]\n", data->data.tag->name);
        dump_list(data);
    }
    else {
        printf("*** Invalid resource_data_type_t: %i\n", (int)data->type);
    }
}
