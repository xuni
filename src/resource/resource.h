/*! \file resource.h

*/

#ifndef XUNI_GUARD_RESOURCE_H
#define XUNI_GUARD_RESOURCE_H

#include <stdio.h>  /* for BUFSIZ */
#include <stdarg.h>

#ifdef __cplusplus
extern "C" {
#endif

enum {
    RESOURCE_BUFFER_SIZE = BUFSIZ
};

enum resource_data_type_t {
    RESOURCE_TEXT,
    RESOURCE_TAG,
    RESOURCE_REF,
    RESOURCES
};

struct resource_text_t {
    char *data;
};

struct resource_tag_t {
    char *name;
    struct resource_list_t *list;
};

struct resource_ref_t {
    char *name;
    struct resource_data_t *ref;
};

struct resource_specific_xml_t {
    char **whitespace;
    size_t whitespaces;
};

enum resource_specific_type_t {
    RESOURCE_SPECIFIC_NONE,
    RESOURCE_SPECIFIC_XML
};

struct resource_specific_t {
    void *data;
    enum resource_specific_type_t type;
};

struct resource_data_t {
    struct resource_data_t *base;
    enum resource_data_type_t type;
    
    union {
        struct resource_text_t *text;
        struct resource_tag_t *tag;
        struct resource_ref_t *ref;
    } data;
    
    struct resource_specific_t specific;
};

struct resource_list_t {
    struct resource_data_t **data;
    size_t n;
};

struct resource_t {
    struct resource_data_t *data;
};

typedef void (*search_resource_func_t)(void *vdata,
    struct resource_data_t *data);

/* Public functions. */

void init_resource(struct resource_t *resource);
void write_resource(struct resource_data_t *resource, const char *filename);
void update_resource(struct resource_t *resource, const char *filename);
int parse_resource(struct resource_data_t *resource, const char *filename);
int parse_resource_filename(struct resource_data_t *resource,
    const char *filename);
struct resource_list_t *first_resource_tag(struct resource_list_t *list,
    const char *name);
char *first_resource_text(struct resource_list_t *list);
char **first_resource_text_address(struct resource_list_t *list);
char *find_resource_text(struct resource_list_t *list, const char *data);
const char *lookup_resource_string(struct resource_t *resource,
    const char *def, ...);
double lookup_resource_number(struct resource_t *resource, double def, ...);
struct resource_list_t *lookup_resource_tag(struct resource_t *resource,
    struct resource_list_t *def, ...);
void search_resource_tag(struct resource_data_t *list, const char *data,
    int recursive, search_resource_func_t search_resource_func, void *vdata);
void free_resource(struct resource_t *resource);

/* Resource functions. */

struct resource_list_t *new_resource_list(void);
struct resource_data_t *new_resource_data(
    enum resource_data_type_t type, struct resource_data_t *base,
    enum resource_specific_type_t specifictype);
struct resource_data_t *create_element(struct resource_data_t *base,
    const char *name, enum resource_specific_type_t specifictype);
struct resource_list_t *append_resource_list(
    struct resource_list_t *list, struct resource_data_t *data);
void add_whitespace(struct resource_data_t *data, char *whitespace, int len);

void make_paths_relative_to_xuni(struct resource_data_t *data, char **path);

void free_resource_data(struct resource_data_t *data);
void free_resource_list(struct resource_list_t *list);

#ifdef __cplusplus
}
#endif

#include "../config.h"

#ifdef HAVE_LIBEXPAT
    #define RESOURCE_LIBRARY_EXPAT
    #include "libexpat.h"
    #include "xmlwrite.h"
#elif defined(HAVE_LIBMXML)
    #define RESOURCE_LIBRARY_MXML
    #include "libmxml.h"
#else
    #error "No resource parsing library specified"
#endif

#endif
