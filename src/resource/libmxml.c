/*! \file libmxml.c

*/

#include "resource.h"
#ifdef RESOURCE_LIBRARY_MXML

#include "libmxml.h"

#include "resource.h"
#include "mxml.h"
#include "../memory.h"

static void add_more_text(struct resource_data_t *element, const char *text);
static void resize_existing_string(char **str, const char *text, size_t len);
static void mxml_handle_text(struct resource_data_t *p, const char *text);
static void convert_mxml_tree(struct resource_data_t *resource,
    mxml_node_t *node);
static void convert_mxml_attributes(struct resource_data_t *resource,
    mxml_node_t *node);

void parse_resource_file(struct resource_data_t *resource,
    const char *filename, FILE *fp) {
    
    mxml_node_t *tree = mxmlLoadFile(NULL, fp, MXML_NO_CALLBACK);
    
    /*mxml_process_includes(resource, tree);*/
    convert_mxml_tree(resource, tree);
    /*mxml_convert_tree(resource, tree);*/
    
#if !1
    if(!strcmp(filename, "./xuni.xml")) {
        puts("initial");
        /*dump_data(resource);*/
    }
#endif
    
    mxmlDelete(tree);
}

static void add_more_text(struct resource_data_t *element, const char *text) {
    struct resource_data_t *data
        = new_resource_data(RESOURCE_TEXT, element, RESOURCE_SPECIFIC_XML);
    
    data->data.text = xuni_memory_allocate(sizeof(*element->data.text));
    data->data.text->data = xuni_memory_duplicate_string(text);
    
    if(element->type != RESOURCE_TAG) {
        printf("**** ERROR: type is not TAG: %i\n", element->type);
    }
    else {
        element->data.tag->list
            = append_resource_list(element->data.tag->list, data);
    }
}

static void resize_existing_string(char **str, const char *text, size_t len) {
    size_t slen = strlen(*str);
    
    *str = xuni_memory_resize(*str, slen + len + 1);
    memcpy(*str + slen, text, len);
    (*str)[slen + len] = 0;
}

static void mxml_handle_text(struct resource_data_t *p, const char *text) {
    size_t len = strlen(text);
    
    if(!p->data.tag->list) {
        p->data.tag->list = new_resource_list();
    }
    
    if(!p->data.tag->list->n
        || p->data.tag->list->data[p->data.tag->list->n - 1]->type
        != RESOURCE_TEXT) {
        
        while(len && isspace(*text)) {
            text ++;
            len --;
        }
        
        if(len) add_more_text(p, text);
    }
    else {
        resize_existing_string(
            &p->data.tag->list->data[
            p->data.tag->list->n - 1]->data.text->data, " ", 1);
        resize_existing_string(
            &p->data.tag->list->data[
            p->data.tag->list->n - 1]->data.text->data, text, len);
    }
}

static void convert_mxml_tree(struct resource_data_t *resource,
    mxml_node_t *node) {
    
    struct resource_list_t *list;
    struct resource_data_t *data;
    
    while(node) {
        data = resource;
        
        if(node->type == MXML_ELEMENT) {
            resource->data.tag->list
                = append_resource_list(resource->data.tag->list,
                    create_element(resource, node->value.element.name,
                        RESOURCE_SPECIFIC_XML));
            
            convert_mxml_attributes(resource, node);
            
            list = resource->data.tag->list;
            if(list) data = list->data[list->n - 1];
        }
        else if(node->type == MXML_TEXT) {
            mxml_handle_text(resource, node->value.text.string);
        }
        
#if !1
        convert_mxml_tree(data, node->child);
        
        node = node->next;
#else  /* non-recursive version */
        if(node->child) node = node->child;
        else if(node->next) node = node->next;
        else {
            node = node->parent;
            if(node) node = node->next;  /* skip already-processed child */
        }
#endif
    }
}

static void convert_mxml_attributes(struct resource_data_t *resource,
    mxml_node_t *node) {
    
    struct resource_list_t *list = resource->data.tag->list, *list2;
    size_t x;
    
    for(x = 0; x < (size_t)node->value.element.num_attrs; x ++) {
        list->data[list->n - 1]->data.tag->list
            = append_resource_list(list->data[list->n - 1]->data.tag->list,
                create_element(resource, node->value.element.attrs[x].name,
                    RESOURCE_SPECIFIC_XML));
        
        list2 = list->data[list->n - 1]->data.tag->list;
        
        mxml_handle_text(list2->data[list2->n - 1],
            node->value.element.attrs[x].value);
    }
}
#endif
