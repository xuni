/*! \file xmlwrite.c

*/

#include <string.h>

#include "resource.h"
#include "xmlwrite.h"

#include "../error.h"
#include "../memory.h"

static void print_indent(FILE *fp, size_t indent);
static void write_resource_list(struct resource_data_t *data, FILE *fp,
    size_t indent);

void xmlwrite_write_resource(struct resource_data_t *resource,
    const char *filename) {
    
    FILE *fp;
    
    rename(filename, "resource.oldfile");
    
    fp = fopen(filename, "w");
    if(!fp) {
        log_message(ERROR_TYPE_SETTING, 0, __FILE__, __LINE__,
            "Can't create resource file \"%s\"", filename);
        return;
    }
    
    fputs("<?xml version=\"1.0\"?>\n"
        "<!-- ", fp);
    fputs(filename, fp);
    fputs(" -->\n"
        "<xuni-resource>\n", fp);
    
    write_resource_list(resource, fp, 1);
    
    fputs("</xuni-resource>\n", fp);
    
    fclose(fp);
}

static void print_indent(FILE *fp, size_t indent) {
    size_t x;
    
    for(x = 0; x < indent; x ++) fputs("    ", fp);
}

static void write_resource_list(struct resource_data_t *data, FILE *fp,
    size_t indent) {
    
    size_t x;
    
    print_indent(fp, indent);
    
    switch(data->type) {
    case RESOURCE_TEXT:
        fputs(data->data.text->data, fp);
        putc('\n', fp);
        
        break;
    case RESOURCE_TAG:
        if(!data->data.tag->list) break;
        
        if(data->data.tag->list->n <= 1) {
            if(data->data.tag->list->n == 0
                || (data->data.tag->list->data[0]->type == RESOURCE_TEXT
                    && !strchr(data->data.tag->list->data[0]
                        ->data.text->data, '\n'))) {
                
                fprintf(fp, "<%s>%s</%s>\n",
                    data->data.tag->name,
                    data->data.tag->list->data[0]->data.text->data,
                    data->data.tag->name);
                break;
            }
        }
        
        fprintf(fp, "<%s>\n", data->data.tag->name);
        
        for(x = 0; x < data->data.tag->list->n; x ++) {
            write_resource_list(data->data.tag->list->data[x], fp,
                indent + 1);
        }
        
        print_indent(fp, indent);
        fprintf(fp, "</%s>\n", data->data.tag->name);
        
        break;
    default:
        log_message(ERROR_TYPE_RESOURCE, 0, __FILE__, __LINE__,
            "Unknown resource type: %lu", (unsigned long)data->type);
        break;
    }
    
    /*for(x = 0; x < list->n; x ++) {
        write_resource_list(list->data[x], fp, indent + 1);
    }*/
}
