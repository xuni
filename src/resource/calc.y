%{
#include <stdio.h>   /* for printf() */
#include <stdlib.h>  /* for strtod() */
#include <ctype.h>   /* for isspace() */
#include <math.h>    /* for pow() */

#include "calcfunc.h"

#define YYSTYPE double

int yylex(void);
void yyerror(const char *);

YYSTYPE yyreturn;
%}

%token NUMBER TRUE FALSE
%left '-' '+'
%left '*' '/'
%left NEG      /* negation: unary minus */
%right '^'     /* exponentation */

%%

line: exp                { yyreturn = $1; }
;

exp: NUMBER              { $$ = $1; }
    | 'pi'               { $$ = CONSTANT_PI; }
    | exp '+' exp        { $$ = $1 + $3; }
    | exp '-' exp        { $$ = $1 - $3; }
    | exp '*' exp        { $$ = $1 * $3; }
    | exp '/' exp        { $$ = $1 / $3; }
    | '-' exp %prec NEG  { $$ = -$2; }
    | exp '^' exp        { $$ = pow($1, $3); }
    | '(' exp ')'        { $$ = $2; }
;

%%

/*#define EXPRESSION_VERBOSE*/

static const char *yydata;

/*! Returns the value of the expression contained in \a data. Returns
    \a defval (default value) instead if \a data could not be parsed.
    \param data The expression to parse.
    \param defval The default value, used when the expression in \a data
        could not be parsed.
    \return The value of the expression, or \a defval if an error was
        encountered while parsing the expression.
*/
double get_expression_value(const char *data, double defval) {
    double d;
    
    if(parse_expression(data, &d)) {
        return defval;
    }
    
    return d;
}

/*! Parses the expression contained in \a data, storing the result in \a d.
    \param data The string containing the expression to parse.
    \param d The variable to store the value of the expression in. Left
        unchanged if an error was encountered by parsing the expression.
    \return Zero upon success, nonzero upon error.
*/
int parse_expression(const char *data, double *d) {
    int r;
    
#ifdef EXPRESSION_VERBOSE
    printf("%s = ", data);
#endif
    
    if(!data) {
#ifdef EXPRESSION_VERBOSE
        printf(" [NULL]\n");
#endif
        return 1;
    }
    
    yydata = data;
    r = yyparse();
    
    *d = yyreturn;
#ifdef EXPRESSION_VERBOSE
    printf("%f\n", yyreturn);
#endif
    
    return r;
}

int yylex(void) {
    char *p;
    
    while(isspace(*yydata)) yydata ++;
    
    if(!*yydata) return 0;
    
    if(isdigit(*yydata) || *yydata == '.') {
        yylval = strtod(yydata, &p);
        if(p == yydata) return 0;
        yydata = p;
        
        while(isspace(*yydata)) yydata ++;
        
        return NUMBER;
    }
    
    return *yydata++;
}

void yyerror(const char *s) {
    fprintf(stderr, __FILE__ ": %s: \"%s\"\n", s, yydata);
}
