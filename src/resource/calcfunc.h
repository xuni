/*! \file calcfunc.h

*/

#ifndef XUNI_GUARD_CALC_H
#define XUNI_GUARD_CALC_H

#include "utility.h"

#ifdef __cplusplus
extern "C" {
#endif

int parse_expression(const char *data, double *d);
double get_expression_value(const char *data, double defval);

#ifdef __cplusplus
}
#endif

#endif
