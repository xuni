/*! \file libexpat.c

*/

#include "resource.h"
#ifdef RESOURCE_LIBRARY_EXPAT

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "expat.h"

#include "libexpat.h"

#include "../error.h"
#include "../memory.h"

struct resource_stack_data_t {
    struct resource_data_t **data;
    size_t n;
};

struct resource_stack_t {
    struct resource_data_t *resource;
    
    struct resource_stack_data_t *stack;
    
    XML_Parser parser;
};

static struct resource_stack_t *new_resource_stack(
    struct resource_data_t *resource);
static void free_resource_stack(struct resource_stack_t *stack);
static void push_resource_stack(struct resource_stack_data_t *stack,
    struct resource_data_t *data);
static void pop_resource_stack(struct resource_stack_data_t *stack);
static struct resource_data_t *top_of_stack(struct resource_stack_t *stack);
static void push_resource_tag(struct resource_stack_t *stack,
    struct resource_data_t *tag);
static void add_new_string(struct resource_data_t *element, const char *text,
    size_t len);
static void resize_existing_string(char **str, const char *text, size_t len);

static void XMLCALL start_element(void *vdata, const XML_Char *name,
    const XML_Char **attribute);
static void XMLCALL default_handler(void *vdata, const XML_Char *text,
    int len);
static void process_attribute(struct resource_stack_t *data, const char *name,
    const char *text);
static void XMLCALL end_element(void *vdata, const XML_Char *name);
static void XMLCALL handle_text(void *vdata, const XML_Char *text, int len);

static void resource_parse_error(const char *filename, XML_Parser parser);

void parse_resource_file(struct resource_data_t *resource,
    const char *filename, FILE *fp) {
    
    char buffer[RESOURCE_BUFFER_SIZE];
    size_t len;
    struct resource_stack_t *stack = new_resource_stack(resource);
    stack->parser = XML_ParserCreate(NULL);
    
    XML_SetUserData(stack->parser, stack);
    XML_SetDefaultHandler(stack->parser, default_handler);
    XML_SetElementHandler(stack->parser, start_element, end_element);
    XML_SetCharacterDataHandler(stack->parser, handle_text);
    
    while((len = fread(buffer, 1, sizeof(buffer), fp))) {
        if(XML_Parse(stack->parser, buffer, len,
            feof(fp) || ferror(fp)) == XML_STATUS_ERROR) {
            
            resource_parse_error(filename, stack->parser);
            
            break;
        }
    }
    
    XML_ParserFree(stack->parser);
    free_resource_stack(stack);
}

static struct resource_stack_t *new_resource_stack(
    struct resource_data_t *resource) {
    
    struct resource_stack_t *stack = xuni_memory_allocate(sizeof(*stack));
    
    stack->resource = resource;
    stack->stack = xuni_memory_allocate(sizeof(*stack->stack));
    stack->stack->data = 0;
    stack->stack->n = 0;
    
    return stack;
}

static void free_resource_stack(struct resource_stack_t *stack) {
    xuni_memory_free(stack->stack->data);
    xuni_memory_free(stack->stack);
    
    xuni_memory_free(stack);
}

static void push_resource_stack(struct resource_stack_data_t *stack,
    struct resource_data_t *data) {
    
    stack->data = xuni_memory_resize(
        stack->data, (stack->n + 1) * sizeof(*stack->data));
    stack->data[stack->n] = data;
    
    stack->n ++;
}

static void pop_resource_stack(struct resource_stack_data_t *stack) {
    if(!stack->n) return;
    
    /* !!! might as well keep this memory . . . . */
    stack->data = xuni_memory_resize(
        stack->data, (-- stack->n) * sizeof(*stack->data));
}

static struct resource_data_t *top_of_stack(struct resource_stack_t *stack) {
    if(!stack->stack->n) return 0;
    
    return stack->stack->data[stack->stack->n - 1];
}

static void push_resource_tag(struct resource_stack_t *stack,
    struct resource_data_t *tag) {
    
    struct resource_tag_t *temp;
    
    if(!stack->stack->n) {
        stack->resource->data.tag->list
            = append_resource_list(stack->resource->data.tag->list, tag);
    }
    else {
        temp = stack->stack->data[stack->stack->n - 1]->data.tag;
        
        temp->list = append_resource_list(temp->list, tag);
    }
}

static struct resource_data_t *add_new_element(struct resource_stack_t *data,
    const char *name) {
    
    struct resource_data_t *element
        = create_element(top_of_stack(data), name, RESOURCE_SPECIFIC_XML);
    
    push_resource_tag(data, element);
    
    return element;
}

static void add_new_string(struct resource_data_t *element, const char *text,
    size_t len) {
    
    struct resource_data_t *rdata
        = new_resource_data(RESOURCE_TEXT, element, RESOURCE_SPECIFIC_XML);
    
    rdata->data.text = xuni_memory_allocate(sizeof(*element->data.text));
    
    rdata->data.text->data
        = xuni_memory_duplicate_string_len(text, len);
    
    if(element->type != RESOURCE_TAG) {
        printf("**** ERROR: type is not TAG: %i\n", element->type);
    }
    else {
        element->data.tag->list = append_resource_list(
            element->data.tag->list, rdata);
    }
}

static void resize_existing_string(char **str, const char *text, size_t len) {
    size_t slen = strlen(*str);
    
    *str = xuni_memory_resize(*str, slen + len + 1);
    memcpy(*str + slen, text, len);
    (*str)[slen + len] = 0;
}

static void XMLCALL default_handler(void *vdata, const XML_Char *text,
    int len) {
    
    /*printf("default_handler(): \"%.*s\" [%i]\n", len, text, len);*/
}

static void XMLCALL start_element(void *vdata, const XML_Char *name,
    const XML_Char **attribute) {
    
    struct resource_stack_t *data = vdata;
    
    /*printf("start_element(): \"%s\"\n", name);*/
    
    push_resource_stack(data->stack, add_new_element(data, name));
    
    while(*attribute) {
        process_attribute(data, attribute[0], attribute[1]);
        attribute += 2;
    }
}

static void process_attribute(struct resource_stack_t *data, const char *name,
    const char *text) {
    
    add_new_string(add_new_element(data, name), text, strlen(text));
}

static void XMLCALL end_element(void *vdata, const XML_Char *name) {
    struct resource_stack_t *data = vdata;
    struct resource_data_t *top = top_of_stack(data);
    
    if(top->type != RESOURCE_TAG) {
        printf("*** end_element() called at data of type: %i\n",
            (int)top->type);
    }
    else if(strcmp(top->data.tag->name, name) != 0) {
        printf("*** end_element(): tag \"%s\" closed with \"%s\"\n",
            top->data.tag->name, name);
    }
    
    pop_resource_stack(data->stack);
}

static void XMLCALL handle_text(void *vdata, const XML_Char *text, int len) {
    struct resource_stack_t *data = vdata;
    struct resource_data_t *p = top_of_stack(data);
    
    /*printf("handle_text(): \"%.*s\"\n", len, text);*/
    
    if(!p->data.tag->list) {
        p->data.tag->list = new_resource_list();
    }
    
    if(!p->data.tag->list->n
        || p->data.tag->list->data[p->data.tag->list->n - 1]->type
        != RESOURCE_TEXT) {
        
        while(len && isspace(*text)) {
            text ++;
            len --;
        }
        
        if(len) add_new_string(p, text, len);
    }
    else {
        resize_existing_string(
            &p->data.tag->list->data[
            p->data.tag->list->n - 1]->data.text->data, text, len);
    }
}

static void resource_parse_error(const char *filename, XML_Parser parser) {
    log_message(ERROR_TYPE_RESOURCE, 0, __FILE__, __LINE__,
        "Parse error in %s line %i column %i:", filename,
        (int)XML_GetCurrentLineNumber(parser),
        (int)XML_GetCurrentColumnNumber(parser));
    
    log_message(ERROR_TYPE_RESOURCE, ERROR_FLAG_CONTINUED, __FILE__, __LINE__,
        "expat: %s", XML_ErrorString(XML_GetErrorCode(parser)));
}
#endif
