/*! \file graphics.h

*/

#ifndef XUNI_GUARD_GRAPHICS_H
#define XUNI_GUARD_GRAPHICS_H

#include "SDL.h"

#include "loadso.h"
#include "resource/resource.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef size_t panel_type_t;

#if 1
enum pos_pack_t {
    POS_PACK_NONE,
    POS_PACK_TOP,
    POS_PACK_BOTTOM
};

struct scale_pos_t {
    double x, y, w, h;
};

struct clip_pos_t {
    int xoff, yoff;
    int xclip, yclip, wclip, hclip;
};

struct pos_t {
    enum pos_pack_t pack;
    struct scale_pos_t scale;
    struct clip_pos_t *clip;
    SDL_Rect real;
};
#else
enum pos_type_t {
    POS_NUMBER,
    POS_SCALE,
    POS_PACK
};

enum pos_pack_t {
    POS_PACK_TOP,
    POS_PACK_BOTTOM
};

struct axis_pos_t {
    enum pos_type_t type;
    union {
        double scale;
        int absolute;
        enum pos_pack_t pack;
    } num;
};

struct xy_pos_t {
    struct axis_pos_t x, y, w, h;
};

struct pos_t {
    struct xy_pos_t pos;
    struct xy_pos_t *offset;
    struct xy_pos_t *clip;
    SDL_Rect real;
};
#endif

/*struct string_t {
    char *data;
    size_t len;
};*/

/*struct strings_t {
    char **data;
    size_t *len;
    size_t num;
};*/

#if !1
struct font_t {
    /*struct string_t mononame, sansname;*/
    struct font_data_t *font;
    size_t fonts;
};
#endif

struct widget_p_t {
    struct widget_t *widget;
};

struct widget_edit_t {
    struct label_t *data, *prev;  /* need to be widget_ts */
    struct widget_t *datawidget;
    size_t alloc, len, pos;
};

struct widget_sel_t {
    /*Uint8 button;
    int xp, yp;*/
    int wasin, clickin;
    struct widget_p_t p;
    
    struct {
        int xp, yp;
        double original;
    } scrollbar;
};

struct widget_tab_t {
    panel_type_t panel;
    size_t sel;
};

struct gui_t {
    struct widget_t *widget;
    
    struct widget_sel_t sel;
    struct widget_edit_t edit;
    struct widget_p_t active;
    struct widget_tab_t tab;  /* !!! set, but not read from yet */
};

enum cursor_t {
    CURSOR_NORMAL,
    CURSOR_TEXT
};

struct cursors_t {
    SDL_Cursor *normal, *text;
    enum cursor_t which;
};

struct theme_t {
    struct widget_t *current;
    struct cursors_t cursors;
};

struct smode_t {
    SDL_Surface *screen;
    int width, height;
    int depth;
    int fullscreen;
    int focus;
    SDL_GrabMode restrictfocus;
};

struct xuni_t {
    struct smode_t *smode;
    /*struct font_t *font;*/
    struct theme_t *theme;
    struct gui_t *gui;
    struct wtype_array_t *wtype;
    struct loadso_array_t *loadso;
};

typedef void (*xuni_callback_func_t)(void *vdata, struct xuni_t *xuni);

struct xuni_callback_t {
    void *vdata;
    xuni_callback_func_t func;
};

void restrict_int(int *value, int max);
void restrict_pos(double *pos);
void wrap_int(int *value, int max);
void init_smode(struct smode_t *smode, struct resource_t *settings);
void init_sdl_libraries(struct smode_t *smode, const char *icon);
SDL_GrabMode set_focus(SDL_GrabMode mode);
int focus_in(Uint8 focus);
int focus_changed(struct smode_t *smode, Uint8 focus);
void quit_sdl_libraries(void);
void free_smode(struct smode_t *smode);
void free_theme(struct theme_t *theme);
void save_screenshot(SDL_Surface *screen);
void paint_cursor(struct xuni_t *xuni, struct widget_t *cursor);
void show_cursor(int enable);
SDL_Surface *load_image(const char *filename);
int resize_screen(struct smode_t *smode, const SDL_ResizeEvent *resize);
int toggle_fullscreen(struct smode_t *smode);
void use_screen_mode(struct xuni_t *xuni, int width, int height);
void set_caption(const char *format, ...)
#ifdef __GNUC__  /* enables printf()-style format string checking */
    __attribute__ ((__format__ (__printf__, 1, 2)))
#endif
    ;
SDL_Rect **list_graphics_modes(struct smode_t *smode);
void lock_screen(SDL_Surface *screen);
void unlock_screen(SDL_Surface *screen);
void clear_screen(SDL_Surface *screen);
SDL_Surface *new_surface(int w, int h, int d, int alpha);
void free_surface(SDL_Surface *surface);
void blit_surface(SDL_Surface *screen, SDL_Surface *image, int xp, int yp);
void blit_surface_area(SDL_Surface *screen, SDL_Surface *image,
    int tx, int ty, int fx, int fy, int fw, int fh);
void blit_surface_repeat(SDL_Surface *screen, SDL_Surface *image,
    int xp, int yp, int w, int h);
void blit_surface_repeat_area(SDL_Surface *screen, SDL_Surface *image,
    int tx, int ty, int tw, int th, int fx, int fy, int fw, int fh);
void update_screen(struct xuni_t *xuni);
int in_rect(int xp, int yp, int x, int y, int w, int h);
int in_sdl_rect(int xp, int yp, const SDL_Rect *r);
int pos_in_rect(int xp, int yp, const struct pos_t *pos);
void blit_surface_fill_from(SDL_Surface *screen, SDL_Surface *image,
    int tx, int ty, int tw, int th, int fx1, int fy1);
void fill_area(SDL_Surface *screen, int x, int y, int w, int h,
    SDL_Surface *image, int ix, int iy);

#ifdef __cplusplus
}
#endif

#endif
