/*! \file loadso.c

*/

#include "loadso.h"
#include "error.h"
#include "memory.h"

#ifdef LOADSO_SDL_VERSION
    #include "SDL_loadso.h"
#elif defined(LOADSO_DLOPEN_VERSION)
    #include <dlfcn.h>
#endif

#ifdef LOADSO_STATIC_VERSION
#include "utility.h"

#include "test/game.h"
#include "test/menu.h"
#include "test/options.h"

typedef void (*init_menu_type)(void *vdata, struct xuni_t *xuni,
    struct widget_t *panel, struct resource_t *settings);

loadso_t xuni_loadso_load_object(struct loadso_array_t *loadso,
    const char *filename) {
    
    /* could validate filename here */
    
    return 0;
}

void xuni_loadso_free_object(loadso_t object) {
    /* do nothing */
}
#else

static void loadso_add_object(struct loadso_array_t *loadso,
    const char *name, loadso_t object);

loadso_t xuni_loadso_load_object(struct loadso_array_t *loadso,
    const char *filename) {
    
    loadso_t object;
    
    if(!filename || !*filename) return 0;
    
#ifdef LOADSO_SDL_VERSION
    object = SDL_LoadObject(filename);
#elif defined(LOADSO_DLOPEN_VERSION)
    object = dlopen(filename, RTLD_LAZY);
#endif
    
    if(!object) {
#ifdef LOADSO_SDL_VERSION
        log_message(ERROR_TYPE_WARNING, ERROR_FLAG_SDL, __FILE__, __LINE__,
            "Can't load object \"%s\"", filename);
#elif defined(LOADSO_DLOPEN_VERSION)
        log_message(ERROR_TYPE_WARNING, 0, __FILE__, __LINE__,
            "Can't load object \"%s\": %s", filename, dlerror());
#endif
    }
    else loadso_add_object(loadso, filename, object);
    
    return object;
}

func_point_t xuni_loadso_load_function(loadso_t object, const char *func) {
    func_point_t sofunc;
    
    /* Trying to load a function from a NULL object will fail.
        No error message is displayed because xuni_loadso_load_object() will
        have already displayed one.
    */
    if(!object) {
        return 0;
    }
    
    if(!func || !*func) {
        log_message(ERROR_TYPE_WARNING, 0, __FILE__, __LINE__,
            "Can't load function with NULL or empty name");
        return 0;
    }
    
    /* This generates an unavoidable warning, because function pointers
        cannot be stored in void pointers and vise versa, and dlsym() etc
        chose to use void pointers for some reason.
    */
    sofunc = (func_point_t)
#ifdef LOADSO_SDL_VERSION
        SDL_LoadFunction(object, func);
#elif defined(LOADSO_DLOPEN_VERSION)
        dlsym(object, func);
#endif
    
    if(!sofunc) {
#ifdef LOADSO_SDL_VERSION
        log_message(ERROR_TYPE_WARNING, ERROR_FLAG_SDL, __FILE__, __LINE__,
            "Can't load function \"%s\"", func);
#elif defined(LOADSO_DLOPEN_VERSION)
        log_message(ERROR_TYPE_WARNING, 0, __FILE__, __LINE__,
            "Can't load function \"%s\": %s", func, dlerror());
#endif
    }
    
    return sofunc;
}

void xuni_loadso_free_object(loadso_t object) {
#ifdef LOADSO_SDL_VERSION
    SDL_UnloadObject(object);
#elif defined(LOADSO_DLOPEN_VERSION)
    dlclose(object);
#endif
}

static void loadso_add_object(struct loadso_array_t *loadso,
    const char *name, loadso_t object) {
    
    loadso->object = xuni_memory_resize(loadso->object, (loadso->objects + 1)
        * sizeof(*loadso->object));
    loadso->object[loadso->objects].object = object;
    loadso->object[loadso->objects].name = xuni_memory_duplicate_string(name);
    
    loadso->objects ++;
}
#endif

void init_loadso(struct loadso_array_t *loadso) {
    loadso->object = 0;
    loadso->objects = 0;
}

void free_loadso(struct loadso_array_t *loadso) {
    size_t x;
    
    for(x = 0; x < loadso->objects; x ++) {
        xuni_loadso_free_object(loadso->object[x].object);
        xuni_memory_free((void *)loadso->object[x].name);
    }
    
    xuni_memory_free(loadso->object);
    xuni_memory_free(loadso);
}
