/*! \file utility.c

*/

#include <stdlib.h>
#include <string.h>

#include "utility.h"

func_point_t string_to_function(const struct string_function_t *data,
    size_t n, const char *find) {
    
    size_t mid = (size_t)-1, first = 0, last = n - 1;
    int compare;
    
    if(!find) return 0;
    
    while(first <= last && last != (size_t)-1) {
        mid = (first + last) / 2;
        
        compare = strcmp(find, data[mid].str);
        
        if(compare < 0) {
            last = mid - 1;
        }
        else if(compare > 0) {
            first = mid + 1;
        }
        else {
            return data[mid].data;
        }
    }
    
    return 0;
}

size_t string_to_index(const struct string_index_t *str, size_t n,
    const char *find) {
    
    size_t mid = (size_t)-1, first = 0, last = n - 1;
    int compare;
    
    if(!find) return (size_t)-1;
    
    while(first <= last && last != (size_t)-1) {
        mid = (first + last) / 2;
        
        compare = strcmp(find, str[mid].str);
        
        if(compare < 0) {
            last = mid - 1;
        }
        else if(compare > 0) {
            first = mid + 1;
        }
        else {
            return str[mid].index;
        }
    }
    
    return (size_t)-1;
}

double degrees_to_radians(double angle) {
    return angle * CONSTANT_PI / 180.0;
}

double radians_to_degrees(double angle) {
    return angle * 180.0 / CONSTANT_PI;
}

/*! Finds the position in which \a find is located in the array \a data, or,
    if it is not present, the "closest" place. The closest place is the
    position of the last element that is smaller than \a find. It is used as
    the place to insert the element \a find.
    
    Useful for binary insertion sorts.
    
    \param data The array of elements to search for \a find for.
    \param n The number of elements in \a data.
    \param find The memory block to search for.
    \param pos Set to the actual or closest position of \a data within the
        memory structure.
    \param compare The function to call to compare two elements in \a data.
        Must return a negative number, 0, or a positive number based on the
        parameters, just like \c strcmp().
    \return True if \a find is actually present in \a data, false otherwise.
        Note that if \a find is not present, \a pos will still be set.
*/
int binary_insertion_sort(void *data, size_t n, void *find, size_t *pos,
    int (*compare)(void *data, size_t n, void *find)) {
    
    size_t mid = (size_t)-1, first = 0, last = n - 1;
    int c = -1;
    
    *pos = 0;
    
    if(!n) return 0;
    
    while(first <= last && last != (size_t)-1) {
        mid = (first + last) / 2;
        
        c = (*compare)(data, mid, find);
        
        if(c < 0) {
            last = mid - 1;
        }
        else if(c > 0) {
            first = mid + 1;
        }
        else {
            *pos = mid;
            return 1;
        }
    }
    
    if(c < 0) {
        *pos = mid;
    }
    else {
        *pos = mid + 1;
    }
    
    return 0;
}

int set_bit(int original, int bit, int set) {
    if(set) {
        original |= bit;
    }
    else {
        original &= ~bit;
    }
    
    return original;
}

void set_bit_p(int *original, int bit, int set) {
    *original = set_bit(*original, bit, set);
}
