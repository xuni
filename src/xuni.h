/*! \file xuni.h

*/

#ifndef XUNI_GUARD_XUNI_H
#define XUNI_GUARD_XUNI_H

#include "graphics.h"  /* !!! for struct xuni_t */
#include "resource/resource.h"

#ifdef __cplusplus
extern "C" {
#endif

struct xuni_t *allocate_xuni(void);
void init_xuni(struct xuni_t *xuni, struct resource_t *settings,
    const char *icon);
void free_xuni(struct xuni_t *xuni);
const char *get_xuni_version_string(void);

#ifdef __cplusplus
}
#endif

#endif
