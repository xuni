/*! \file utility.h

*/

#ifndef XUNI_GUARD_UTILITY_H
#define XUNI_GUARD_UTILITY_H 1

#include <stdlib.h>
#include <math.h>

#include "loadso.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef PI
#define CONSTANT_PI PI
#elif defined(M_PI)
#define CONSTANT_PI M_PI
#else
#define CONSTANT_PI 3.14159265358979323846264338327950288419716939937510582097
#endif

struct string_function_t {
    const char *str;
    func_point_t data;
};

struct string_index_t {
    const char *str;
    size_t index;
};

func_point_t string_to_function(const struct string_function_t *data,
    size_t n, const char *find);
size_t string_to_index(const struct string_index_t *str, size_t n,
    const char *find);

double degrees_to_radians(double angle);
double radians_to_degrees(double angle);

int binary_insertion_sort(void *data, size_t n, void *find, size_t *pos,
    int (*compare)(void *data, size_t n, void *find));

int set_bit(int original, int bit, int set);
void set_bit_p(int *original, int bit, int set);

#ifdef __cplusplus
}
#endif

#endif
