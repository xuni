/*! \file gui.h

*/

#ifndef XUNI_GUARD_GUI_H
#define XUNI_GUARD_GUI_H

#include "widget/widgets.h"

#ifdef __cplusplus
extern "C" {
#endif

struct wid_init_t {
    size_t id;
    const char *name;
};

void init_theme_structure(struct theme_t *theme);
void init_gui(struct xuni_t *xuni);
void init_edit(struct widget_edit_t *edit);
void clear_gui(struct xuni_t *xuni, panel_type_t panel, int keep);
void clear_sel(struct widget_sel_t *sel);
void clear_edit(struct widget_edit_t *edit);
void clear_active(struct xuni_t *xuni, struct widget_p_t *wp, int keep);
void clear_tab(struct widget_tab_t *tab, panel_type_t panel);
void edit_add_char(struct widget_edit_t *edit, char c);
void edit_del_chars(struct widget_edit_t *edit, size_t n);
int clear_widget_sel(struct widget_t *widget);
int clear_widget_sel_keep(struct widget_t *widget, struct widget_t *keep,
    int n);
struct widget_t *set_widget_sel_rec(struct widget_sel_t *sel, int xp, int yp,
    struct widget_t *widget, int *repaint);
int set_widget_sel_repaint(struct xuni_t *xuni, struct widget_sel_t *sel,
    int xp, int yp, int click, struct widget_t *widget);
int set_widget_sel(struct widget_sel_t *sel, int xp, int yp, int click,
    struct widget_t *widget);
int set_widget_repaint(struct xuni_t *xuni, struct widget_sel_t *sel,
    int xp, int yp, int click, struct widget_t *widget);
int set_compose_sel(struct widget_sel_t *sel, int xp, int yp, int any);
void load_resource_widgets(struct xuni_t *xuni, struct widget_t **widget,
    struct resource_data_t *data);
struct nameid_t *allocate_nameid(void);
void init_wid_widget(struct nameid_t *nameid, struct widget_t *widget,
    size_t id, const char *name);
void init_wid_possible_zero(struct widget_t *base, struct nameid_t *nameid,
    size_t id, const char *name);
void init_wid(struct widget_t *base, struct widget_t *use, size_t id,
    const char *name);
void init_wid_array(struct widget_t *base, struct widget_t *use,
    struct wid_init_t *init, size_t n);
int widget_process_character(struct xuni_t *xuni, SDL_keysym *sym);
int widget_can_be_active(struct widget_t *widget);
void update_widget(struct xuni_t *xuni, struct widget_t *widget);
void enable_unicode(const struct widget_t *widget);
void activate_widget(struct widget_t *widget, struct xuni_t *xuni,
    int xp, int yp);
void revert_widget(struct xuni_t *xuni, struct widget_t *widget);
void deactivate_widget(struct widget_t *widget, struct xuni_t *xuni);
void perform_widget_click(struct xuni_t *xuni, struct widget_t *widget);
void free_gui(struct xuni_t *xuni);

#ifdef __cplusplus
}
#endif

#endif
