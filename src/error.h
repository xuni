/*! \file error.h

*/

#ifndef XUNI_GUARD_ERROR_H
#define XUNI_GUARD_ERROR_H

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

/*! The type of error being reported. This only affects a phrase used in the
    header of the error message.
    
    The order of these enumerated values affects the following arrays:
        - \c get_error_type::type_name
*/
enum error_type_t {
    ERROR_TYPE_LOG,
    ERROR_TYPE_WARNING,
    ERROR_TYPE_FATAL,  /*!< Note: this does not actually exit the program. */
    ERROR_TYPE_SETTING,
    ERROR_TYPE_RESOURCE,
    ERROR_TYPE_MEMORY,
    ERROR_TYPE_DATASTRUCT,
    ERROR_TYPE_LOOKUP_TABLE,
    ERROR_TYPES
};

enum error_flag_t {
    ERROR_FLAG_NONE      = 0,
    ERROR_FLAG_CONTINUED = 1 << 0,
    ERROR_FLAG_SDL       = 1 << 1
};

void xuni_error_initialize(void);
void xuni_error_add_stream(const char *filename, FILE *fp);
void xuni_error_quit(void);

void log_message(enum error_type_t type, enum error_flag_t flag,
    const char *file, int line, const char *format, ...)
#ifdef __GNUC__  /* enables printf()-style format string checking */
        __attribute__ ((__format__ (__printf__, 5, 6)))
#endif
        ;

#ifdef __cplusplus
}
#endif

#endif
