/*! \file help.c

*/

#include <stdio.h>
#include <string.h>

#include "help.h"

#include "graphics.h"
#include "gui.h"
#include "loop.h"
#include "menu.h"

#include "widget/widgets.h"

#include "main.h"

#include "loadso.h"

enum wid_t {
    WID_BACK,
    WID_README,
    WIDS
};

static void get_readme_text(struct xuni_t *xuni, struct widget_t *panel);

int help_init(struct xuni_t *xuni, struct panel_data_t *data) {
    struct widget_t *panel = data->event[PANEL_EVENT_INIT].p.init.panel;
    
    init_wid(xuni->gui->widget, xuni->gui->widget, PANEL_HELP, "help");
    
    init_wid(panel, panel, WID_BACK, "back");
    init_wid(panel, panel, WID_README, "readme");
    
    add_widget_accelerator(xuni, panel, widget_nameid_access(panel, WID_BACK),
        SDLK_ESCAPE, KMOD_NONE);
    
    get_readme_text(xuni, panel);
    
    return 0;
}

static void get_readme_text(struct xuni_t *xuni, struct widget_t *panel) {
    FILE *file = fopen(README_FILE, "r");
    char line[BUFSIZ], *p;
    
    if(!file) return;
    
    while(fgets(line, sizeof(line), file)) {
        if((p = strchr(line, '\n'))) *p = 0;
        
        /* !!! blank labels do not work properly
            the height of a blank label cannot be determined (with SDL_ttf)
        */
        if(!*line) {
            strcpy(line, " ");
        }
        
        add_listbox_item(xuni, widget_nameid_access(panel, WID_README)
            ->compose->widget[WID_LISTBOX_DATA],
            THEME_FONT_SANS, line);
    }
    
    widget_event(xuni, widget_nameid_access(panel, WID_README),
        WIDGET_EVENT_RESCALE);
    
    fclose(file);
}

int help_start(struct xuni_t *xuni, struct panel_data_t *data) {
    set_caption("Help and information");
    
    return 0;
}

int help_event(struct xuni_t *xuni, struct panel_data_t *data) {
    panel_type_t *mode = data->event[PANEL_EVENT_EVENT].p.event.mode;
    SDL_Event *event = data->event[PANEL_EVENT_EVENT].p.event.event;
    
    switch(event->type) {
    case SDL_QUIT:
        *mode = PANEL_MAIN_MENU;
        break;
    default:
        break;
    }
    
    return 0;
}

int help_paint(struct xuni_t *xuni, struct panel_data_t *data) {
    struct widget_t *panel
        = widget_nameid_access(xuni->gui->widget, PANEL_HELP);
    panel_type_t mode = data->event[PANEL_EVENT_PAINT].p.paint.mode;
    
    clear_screen(xuni->smode->screen);
    
    widget_event(xuni, widget_nameid_access(xuni->gui->widget, PANEL_HELP),
        WIDGET_EVENT_PAINT);
    
    update_screen(xuni);
    
    return 0;
}

int help_click(struct xuni_t *xuni, struct panel_data_t *data) {
    panel_type_t *mode = data->event[PANEL_EVENT_CLICK].p.click.mode;
    struct widget_t *widget = data->event[PANEL_EVENT_CLICK].p.click.widget;
    
    switch(widget->id) {
    case WID_BACK:
        *mode = PANEL_MAIN_MENU;
        break;
    }
    
    return 0;  /* ignored */
}

int help_free(struct xuni_t *xuni, struct panel_data_t *data) {
    return 0;
}
