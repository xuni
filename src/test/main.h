/*! \file main.h

*/

#ifndef XUNI_GUARD_MAIN_H
#define XUNI_GUARD_MAIN_H

#define SETTINGS_FILE "./test.xml"
#define LOG_FILE "./test.log"
#define README_FILE "README"

#include "utility.h"

#ifdef __cplusplus
extern "C" {
#endif

enum panel_type_name_t {
    PANEL_NONE = -1,
    PANEL_MAIN_MENU,
    PANEL_GAME,
    PANEL_GAME_MENU,
    PANEL_OPTIONS,
    PANEL_OPTIONS_GRAPHICS,
    PANEL_OPTIONS_THEME,
    PANEL_HELP,
    PANELS
};

enum main_menu_background_t {
    MAIN_MENU_BACKGROUND_MOON_WITH_PARALLAX_CLOUDS,
    MAIN_MENU_BACKGROUND_ORION_NEBULA,
    MAIN_MENU_BACKGROUND_ORION_NEBULA_FRAME,
    MAIN_MENU_BACKGROUND_PARALLAX_CLOUDS,
    MAIN_MENU_BACKGROUND_BLACK,
    MAIN_MENU_BACKGROUNDS
};

extern struct string_index_t
    main_menu_background_names[MAIN_MENU_BACKGROUNDS];

#ifdef __cplusplus
}
#endif

#endif
