/*! \file game.h

*/

#ifndef XUNI_GUARD_GAME_H
#define XUNI_GUARD_GAME_H

#include "graphics.h"
#include "gui.h"
#include "loop.h"

#ifdef __cplusplus
extern "C" {
#endif

struct game_data_t {
    int unused;
};

int game_init(struct xuni_t *xuni, struct panel_data_t *data);
int game_start(struct xuni_t *xuni, struct panel_data_t *data);
int game_event(struct xuni_t *xuni, struct panel_data_t *data);
int game_paint(struct xuni_t *xuni, struct panel_data_t *data);
int game_click(struct xuni_t *xuni, struct panel_data_t *data);
int game_free(struct xuni_t *xuni, struct panel_data_t *data);

#ifdef __cplusplus
}
#endif

#endif
