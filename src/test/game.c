/*! \file game.c

*/

#include "game.h"
#include "menu.h"  /* for paint_menu_fps() */

#include "graphics.h"
#include "gui.h"
#include "loop.h"
#include "menu.h"

#include "widget/widgets.h"

#include "main.h"

#include "loadso.h"

enum wid_t {
    WID_MENU,
    WID_MENU_BACK,
    WID_MENU_QUIT,
    WIDS
};

int game_init(struct xuni_t *xuni, struct panel_data_t *data) {
    struct widget_t *panel = data->event[PANEL_EVENT_INIT].p.init.panel;
    struct widget_t *temppanel;
    
    init_wid(xuni->gui->widget, xuni->gui->widget,
        PANEL_GAME, "game");
    init_wid(xuni->gui->widget, xuni->gui->widget,
        PANEL_GAME_MENU, "game menu");
    
    init_wid(xuni->gui->widget, panel, WID_MENU, "game/menu");
    init_wid(xuni->gui->widget, panel, WID_MENU_BACK, "game menu/back");
    init_wid(xuni->gui->widget, panel, WID_MENU_QUIT, "game menu/quit");
    
    /*add_widget_accelerator(panel,
        widget_nameid_access(panel, WID_MENU), SDLK_F10, KMOD_NONE);*/
    
    temppanel = widget_nameid_access(xuni->gui->widget, PANEL_GAME_MENU);
    add_widget_accelerator(xuni, temppanel,
        widget_nameid_access(panel, WID_MENU_BACK), SDLK_ESCAPE, KMOD_NONE);
    /*add_widget_accelerator(temppanel,
        widget_nameid_access(panel, WID_MENU_BACK), SDLK_F10, KMOD_NONE);*/
    
    return 0;
}

int game_start(struct xuni_t *xuni, struct panel_data_t *data) {
    set_caption("Explore the Universe");
    
    return 0;
}

int game_event(struct xuni_t *xuni, struct panel_data_t *data) {
    panel_type_t *mode = data->event[PANEL_EVENT_EVENT].p.event.mode;
    SDL_Event *event = data->event[PANEL_EVENT_EVENT].p.event.event;
    
    switch(event->type) {
    case SDL_KEYDOWN:
        switch(event->key.keysym.sym) {
        case SDLK_ESCAPE:
            /* there is currently no widget for this */
            if(*mode == PANEL_GAME) {
                *mode = PANEL_MAIN_MENU;
            }
            
            break;
        default: break;
        }
        break;
    case SDL_QUIT:
        *mode = PANEL_MAIN_MENU;
        break;
    default:
        break;
    }
    
    return 0;
}

int game_paint(struct xuni_t *xuni, struct panel_data_t *data) {
    struct widget_t *panel
        = widget_nameid_access(xuni->gui->widget, PANEL_GAME);
    panel_type_t mode = data->event[PANEL_EVENT_PAINT].p.paint.mode;
    
    clear_screen(xuni->smode->screen);
    
    widget_event(xuni, widget_nameid_access(xuni->gui->widget, PANEL_GAME),
        WIDGET_EVENT_PAINT);
    
    if(mode == PANEL_GAME_MENU) {
        widget_event(xuni, widget_nameid_access(xuni->gui->widget,
            PANEL_GAME_MENU), WIDGET_EVENT_PAINT);
    }
    
    paint_menu_fps(xuni, THEME_FONT_MONO);
    
    if(panel->p.panel->frameupdate) {
        paint_cursor(xuni, get_theme_widget(xuni, THEME_DEFAULT_CURSOR));
    }
    
    update_screen(xuni);
    
    return 0;
}

int game_click(struct xuni_t *xuni, struct panel_data_t *data) {
    panel_type_t *mode = data->event[PANEL_EVENT_CLICK].p.click.mode;
    struct widget_t *widget = data->event[PANEL_EVENT_CLICK].p.click.widget;
    
    switch(widget->id) {
    case WID_MENU:
        *mode = PANEL_GAME_MENU;
        
        clear_gui(xuni, PANEL_GAME_MENU, 0);
        break;
    case WID_MENU_BACK:
        *mode = PANEL_GAME;
        
        clear_gui(xuni, PANEL_GAME, 0);
        break;
    case WID_MENU_QUIT:
        *mode = PANEL_MAIN_MENU;
        break;
    }
    
    return 0;  /* ignored */
}

int game_free(struct xuni_t *xuni, struct panel_data_t *data) {
    return 0;
}
