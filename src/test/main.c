/*! \file main.c
    
    This is the driver for the xuni test program "test". It implements main()
    and not much else.
    
    The other source files in src/test/ implement the code for specific
    panels. They are loaded dynamically at runtime, if the XML files in
    gui/data/ are as expected, and if xuni was compiled with dynamic library
    loading support. (If it was not, this file supplies a
    xuni_loadso_load_function() which is used to the same effect, only the
    other source files in src/test/ are statically linked to the program.)
*/

#include "xuni.h"
#include "error.h"
#include "graphics.h"
#include "loadso.h"
#include "loop.h"
#include "resource/resource.h"
#include "memory.h"
#include "utility.h"
#include "version.h"

#include "game.h"
#include "menu.h"
#include "options.h"

#include "widget/widgets.h"

#include "main.h"

/*! \def BUILTIN_THEME
    When defined, tests the new and badly implemented feature that allows
    static themes. It is designed to allow the program to supply a theme in
    case the XML data files do not contain any.
    
    It is badly implemented because the theme widget that is created is
    currently not added to the widget tree -- i.e., it will not be rescaled,
    and so on.
*/
/*#define BUILTIN_THEME*/

static void load_resource(struct resource_t *settings);
static void save_resource(struct resource_t *resource);

#ifdef VERSION_WIN32
#include <windows.h>
int STDCALL WinMain(HINSTANCE hInst, HINSTANCE hPrev, LPSTR lpCmd, int nShow) {
#else
int main(int argc, char *argv[]) {
#endif
    struct resource_t settings;
    struct xuni_t *xuni = allocate_xuni();
#ifdef BUILTIN_THEME
    struct widget_t *theme = allocate_widget("default theme", 0);
#endif
    
    xuni_error_initialize();
    xuni_error_add_stream(0, stderr);
    xuni_error_add_stream(LOG_FILE, 0);
    
    load_resource(&settings);
    
    init_xuni(xuni, &settings,
        lookup_resource_string(&settings, 0, "xuni-resource", "icon", 0));
    
#ifdef BUILTIN_THEME
    {
        init_widget_pos(theme, 0, 0, 100, 100, POS_PACK_NONE);
        build_theme(xuni, theme, 100.0 / 60, 100.0 / 60,
            "gui/FreeMono.ttf",
            "gui/FreeSans.ttf",
            "gui/alienglow/cursor.png",
            "gui/alienglow/check.png",
            "gui/shadow/corners.png",
            "gui/shadow/corners.png",
            "gui/shadow/corners.png",
            "gui/alienglow/corners/out/normal.png",
            "gui/alienglow/corners/out/hover.png",
            "gui/alienglow/corners/out/active.png",
            "gui/alienglow/scrollbar.png",
            "gui/alienglow/scrollbar.png");
        xuni->theme->current = theme;
        widget_event(xuni, theme, WIDGET_EVENT_RESCALE);
        widget_event(xuni, xuni->gui->widget, WIDGET_EVENT_RESCALE);
    }
#endif
    
    /*use_theme(xuni, find_widget(xuni->gui->widget, "theme"));*/
    
    call_init_funcs(xuni, xuni->gui->widget, &settings);
    main_loop(xuni, 0, PANEL_MAIN_MENU);
    
    xuni_memory_keep_freed_blocks(1);
    
    save_resource(&settings);
    free_resource(&settings);
#ifdef BUILTIN_THEME
    widget_event(xuni, theme, WIDGET_EVENT_FREE);
#endif
    free_xuni(xuni);
    
    quit_sdl_libraries();
    xuni_error_quit();
    xuni_memory_free_all();
    
    return 0;
}

static void load_resource(struct resource_t *settings) {
    init_resource(settings);
    
    if(parse_resource(settings->data, SETTINGS_FILE)) {
        log_message(ERROR_TYPE_RESOURCE, 0, __FILE__, __LINE__,
            "Failed to load resource \"%s\"", SETTINGS_FILE);
    }
}

static void save_resource(struct resource_t *resource) {
    write_resource(resource->data, SETTINGS_FILE ".generated");
}

#ifdef LOADSO_STATIC_VERSION
func_point_t xuni_loadso_load_function(loadso_t object, const char *func) {
    struct string_function_t data[] = {
        {"game_click", (func_point_t)game_click},
        {"game_event", (func_point_t)game_event},
        {"game_free", (func_point_t)game_free},
        {"game_init", (func_point_t)game_init},
        {"game_paint", (func_point_t)game_paint},
        {"game_start", (func_point_t)game_start},
        {"menu_click", (func_point_t)menu_click},
        {"menu_event", (func_point_t)menu_event},
        {"menu_free", (func_point_t)menu_free},
        {"menu_init", (func_point_t)menu_init},
        {"menu_paint", (func_point_t)menu_paint},
        {"menu_start", (func_point_t)menu_start},
        {"options_click", (func_point_t)options_click},
        {"options_event", (func_point_t)options_event},
        {"options_free", (func_point_t)options_free},
        {"options_graphics_deactivate",
            (func_point_t)options_graphics_deactivate},
        {"options_init", (func_point_t)options_init},
        {"options_paint", (func_point_t)options_paint},
        {"options_start", (func_point_t)options_start},
        {"options_theme_deactivate", (func_point_t)options_theme_deactivate}
    };
    func_point_t funcp
        = string_to_function(data, sizeof(data) / sizeof(*data), func);
    
    if(!funcp) {
        log_message(ERROR_TYPE_RESOURCE, 0, __FILE__, __LINE__,
            "Unknown function: \"%s\"", func);
    }
    
    return funcp;
}
#endif
