/*! \file options.c

*/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "options.h"

#include "graphics.h"
#include "gui.h"
#include "loop.h"
#include "memory.h"
#include "menu.h"
#include "utility.h"

#include "widget/widgets.h"
#include "widget/checkbox.h"
#include "widget/combobox.h"
#include "widget/listbox.h"
#include "widget/panel.h"
#include "widget/textbox.h"

#include "main.h"

enum wid_t {
    WID_GRAPHICS_BUTTON,
    WID_THEME_BUTTON,
    WID_BACK,
    WID_MAIN_MENU_BACKGROUND,
    WID_GRAPHICS_BACK,
    WID_GRAPHICS_FULLSCREEN,
    WID_GRAPHICS_RESTRICTFOCUS,
    WID_GRAPHICS_TEXTSCREENMODE,
    WID_GRAPHICS_LISTSCREENMODE,
    WID_THEME_BACK,
    WID_THEME_FONT_FILENAME,
    WID_THEME_THEME_NAME,
    WID_THEME_THEME_ROUNDNESS,
    WID_THEME_THEME_LIST,
    WID_THEME_FONT_COMBO,
    WID_THEME_FONTSAMPLE_LIST,
    WIDS
};

static void get_screen_modes(struct xuni_t *xuni, struct widget_t *widget);
static void get_theme_names(struct xuni_t *xuni, struct widget_t *panel);
static void get_font_names(struct xuni_t *xuni, struct widget_t *panel);
static void get_main_menu_backgrounds(struct xuni_t *xuni,
    struct widget_t *panel);
static void get_font_samples(struct xuni_t *xuni, struct widget_t *panel);
static void get_names_rec(struct xuni_t *xuni, struct widget_t *area,
    struct widget_t *widget, enum widget_type_t type);
static void screen_mode_from_string(struct xuni_t *xuni, const char *mode);

int options_init(struct xuni_t *xuni, struct panel_data_t *data) {
    struct widget_t *panel = data->event[PANEL_EVENT_INIT].p.init.panel;
    struct widget_t *temppanel;
    
    init_wid(xuni->gui->widget, xuni->gui->widget,
        PANEL_OPTIONS, "options");
    init_wid(xuni->gui->widget, xuni->gui->widget,
        PANEL_OPTIONS_GRAPHICS, "options graphics");
    init_wid(xuni->gui->widget, xuni->gui->widget,
        PANEL_OPTIONS_THEME, "options theme");
    
    init_wid(panel, panel, WID_GRAPHICS_BUTTON, "graphics");
    init_wid(panel, panel, WID_THEME_BUTTON, "theme");
    init_wid(panel, panel, WID_BACK, "back");
    init_wid(panel, panel, WID_MAIN_MENU_BACKGROUND, "main menu background");
    
    temppanel
        = widget_nameid_access(xuni->gui->widget, PANEL_OPTIONS_GRAPHICS);
    
    init_wid(temppanel, panel, WID_GRAPHICS_BACK, "back");
    init_wid(temppanel, panel, WID_GRAPHICS_FULLSCREEN, "fullscreen");
    init_wid(temppanel, panel, WID_GRAPHICS_RESTRICTFOCUS,
        "restrictfocus");
    init_wid(temppanel, panel, WID_GRAPHICS_TEXTSCREENMODE,
        "textscreenmode");
    init_wid(temppanel, panel, WID_GRAPHICS_LISTSCREENMODE,
        "listscreenmode");
    
    init_wid(xuni->gui->widget, panel, WID_THEME_BACK,
        "options theme/back");
    init_wid(xuni->gui->widget, panel, WID_THEME_FONT_FILENAME,
        "options theme/font filename");
    init_wid(xuni->gui->widget, panel, WID_THEME_THEME_NAME,
        "options theme/theme name");
    init_wid(xuni->gui->widget, panel, WID_THEME_THEME_ROUNDNESS,
        "options theme/theme roundness");
    init_wid(xuni->gui->widget, panel, WID_THEME_THEME_LIST,
        "options theme/theme list");
    init_wid(xuni->gui->widget, panel, WID_THEME_FONT_COMBO,
        "options theme/font combo");
    init_wid(xuni->gui->widget, panel, WID_THEME_FONTSAMPLE_LIST,
        "options theme/fontsample listbox");
    
    add_widget_accelerator(xuni, panel,
        widget_nameid_access(panel, WID_GRAPHICS_BUTTON), SDLK_g, KMOD_NONE);
    add_widget_accelerator(xuni, panel,
        widget_nameid_access(panel, WID_THEME_BUTTON), SDLK_t, KMOD_NONE);
    add_widget_accelerator(xuni, panel,
        widget_nameid_access(panel, WID_BACK), SDLK_ESCAPE, KMOD_NONE);
    
    temppanel
        = widget_nameid_access(xuni->gui->widget, PANEL_OPTIONS_GRAPHICS);
    add_widget_accelerator(xuni, temppanel,
        widget_nameid_access(panel, WID_GRAPHICS_BACK),
        SDLK_ESCAPE, KMOD_NONE);
    
    temppanel = widget_nameid_access(xuni->gui->widget, PANEL_OPTIONS_THEME);
    add_widget_accelerator(xuni, temppanel,
        widget_nameid_access(panel, WID_THEME_BACK), SDLK_ESCAPE, KMOD_NONE);
    
    get_theme_names(xuni, panel);
    get_font_names(xuni, panel);
    get_main_menu_backgrounds(xuni, panel);
    get_font_samples(xuni, panel);
    
    return 0;
}

static void get_screen_modes(struct xuni_t *xuni, struct widget_t *widget) {
    SDL_Rect **mode = list_graphics_modes(xuni->smode);
    char buffer[BUFSIZ];
    size_t x;
    struct widget_t *area = widget->compose->widget[WID_LISTBOX_DATA];
    
    xuni_memory_increment(area->compose);
    free_panel(xuni, area->compose);
    
    if(!mode) {
        add_listbox_item(xuni, area, THEME_FONT_SANS, "No screen modes");
    }
    else if(mode == (SDL_Rect **)-1) {
        add_listbox_item(xuni, area, THEME_FONT_SANS, "All screen modes");
    }
    else {
        for(x = 0; mode[x]; x ++) {
            sprintf(buffer, "%i x %i", (int)mode[x]->w, (int)mode[x]->h);
            
            add_listbox_item(xuni, area, THEME_FONT_SANS, buffer);
        }
    }
    
    /* RESCALE isn't needed, just REPOSITION works */
    /*widget_event(xuni, widget, WIDGET_EVENT_RESCALE);*/
    widget_event(xuni, widget, WIDGET_EVENT_REPOSITION);
}

static void get_theme_names(struct xuni_t *xuni, struct widget_t *panel) {
    struct widget_t *area = widget_nameid_access(panel, WID_THEME_THEME_LIST)
        ->compose->widget[WID_LISTBOX_DATA];
    
    xuni_memory_increment(area->compose);
    free_panel(xuni, area->compose);
    
    get_names_rec(xuni, area, xuni->gui->widget, WIDGET_THEME);
    
    widget_event(xuni, widget_nameid_access(panel, WID_THEME_THEME_LIST),
        WIDGET_EVENT_RESCALE);
}

static void get_font_names(struct xuni_t *xuni, struct widget_t *panel) {
    struct widget_t *area = widget_nameid_access(panel, WID_THEME_FONT_COMBO)
        ->compose->widget[WID_COMBOBOX_DROPDOWN]
        ->compose->widget[WID_LISTBOX_DATA];
    
    xuni_memory_increment(area->compose);
    free_panel(xuni, area->compose);
    
    get_names_rec(xuni, area, find_widget(xuni->gui->widget, "font"),
        WIDGET_FONT);
    
    widget_event(xuni, widget_nameid_access(panel, WID_THEME_FONT_COMBO),
        WIDGET_EVENT_RESCALE);
}

static void get_main_menu_backgrounds(struct xuni_t *xuni,
    struct widget_t *panel) {
    
    size_t x;
    
    for(x = 0; x < sizeof(main_menu_background_names)
        / sizeof(*main_menu_background_names); x ++) {
        
        add_listbox_item(xuni, widget_nameid_access(panel,
                WID_MAIN_MENU_BACKGROUND)->compose->widget[WID_LISTBOX_DATA],
            THEME_FONT_SANS, main_menu_background_names[x].str);
    }
    
    widget_event(xuni, widget_nameid_access(panel, WID_MAIN_MENU_BACKGROUND),
        WIDGET_EVENT_RESCALE);
}

static void get_font_samples(struct xuni_t *xuni, struct widget_t *panel) {
    struct widget_t *area
        = widget_nameid_access(panel, WID_THEME_FONTSAMPLE_LIST)
            ->compose->widget[WID_LISTBOX_DATA];
    struct widget_t *themepanel = widget_nameid_access(xuni->gui->widget,
        PANEL_OPTIONS_THEME);
    struct widget_t *widget;
    size_t x;
    
    xuni_memory_increment(area->compose);
    free_panel(xuni, area->compose);
    
    for(x = 0; x < themepanel->compose->widgets; x ++) {
        widget = themepanel->compose->widget[x];
        
        if(strstr(widget->name, "fontsample text")) {
            add_allocate_widget_compose(area, "listbox item");
            
            init_widget_pos(last_compose_widget(area),
                0, 0, 0, 0, POS_PACK_TOP);
            
            last_compose_widget(area)->type = WIDGET_LABEL;
            
            xuni_memory_increment(widget->p.label);
            last_compose_widget(area)->p.label = widget->p.label;
            
            last_compose_widget(area)->visibility
                &= ~WIDGET_VISIBILITY_INDEPENDENT;
        }
    }
    
    widget_event(xuni, widget_nameid_access(panel, WID_THEME_FONTSAMPLE_LIST),
        WIDGET_EVENT_RESCALE);
}

static void get_names_rec(struct xuni_t *xuni, struct widget_t *area,
    struct widget_t *widget, enum widget_type_t type) {
    
    const char *text;
    size_t x;
    
    if(!widget) return;
    
    if(widget->type == (size_t)type) {
        if(type == WIDGET_FONT) {
            text = widget->p.font->name;
        }
        else text = widget->name;
        
        add_listbox_item(xuni, area, THEME_FONT_SANS, text);
    }
    
    if(widget->compose) {
        for(x = 0; x < widget->compose->widgets; x ++) {
            get_names_rec(xuni, area, widget->compose->widget[x], type);
        }
    }
}

int options_start(struct xuni_t *xuni, struct panel_data_t *data) {
    set_caption("Options and preferences");
    
    return 0;
}

int options_event(struct xuni_t *xuni, struct panel_data_t *data) {
    panel_type_t *mode = data->event[PANEL_EVENT_EVENT].p.event.mode;
    SDL_Event *event = data->event[PANEL_EVENT_EVENT].p.event.event;
    int repaint = 0;
    
    switch(event->type) {
    case SDL_QUIT:
        *mode = PANEL_MAIN_MENU;
        break;
    default:
        break;
    }
    
    return repaint;
}

int options_click(struct xuni_t *xuni, struct panel_data_t *data) {
    panel_type_t *mode = data->event[PANEL_EVENT_CLICK].p.click.mode;
    struct widget_t *widget = data->event[PANEL_EVENT_CLICK].p.click.widget;
    struct widget_t *w;
    
    switch(widget->id) {
    /* Options menu */
    case WID_GRAPHICS_BUTTON:
        *mode = PANEL_OPTIONS_GRAPHICS;
        clear_gui(xuni, PANEL_OPTIONS_GRAPHICS, 0);
        break;
    case WID_THEME_BUTTON:
        *mode = PANEL_OPTIONS_THEME;
        clear_gui(xuni, PANEL_OPTIONS_THEME, 0);
        break;
    case WID_BACK:
        *mode = PANEL_MAIN_MENU;
        break;
    case WID_MAIN_MENU_BACKGROUND:
        w = listbox_sel_item(widget);
        if(w) {
            switch_main_menu_background(
                widget_nameid_access(xuni->gui->widget, PANEL_MAIN_MENU),
                w->p.label->text);
        }
        
        break;
    
    /* Options graphics menu */
    case WID_GRAPHICS_BACK:
        *mode = PANEL_OPTIONS;
        clear_gui(xuni, PANEL_OPTIONS, 0);
        break;
    case WID_GRAPHICS_FULLSCREEN:
        if(!toggle_fullscreen(xuni->smode)) {
            get_screen_modes(xuni,
                widget_nameid_follow(xuni->gui->widget,
                    PANEL_OPTIONS, WID_GRAPHICS_LISTSCREENMODE, (size_t)-1));
            
            return 1;
        }
        
        break;
    case WID_GRAPHICS_RESTRICTFOCUS:
        xuni->smode->restrictfocus = set_focus(!xuni->smode->restrictfocus);
        break;
    case WID_GRAPHICS_LISTSCREENMODE:
        w = listbox_sel_item(widget);
        if(w) screen_mode_from_string(xuni, w->p.label->text);
        
        break;
        
    /* Options theme menu */
    case WID_THEME_BACK:
        *mode = PANEL_OPTIONS;
        clear_gui(xuni, PANEL_OPTIONS, 0);
        break;
    case WID_THEME_THEME_LIST:
        w = listbox_sel_item(widget);
        if(w) {
            use_theme_by_name(xuni, w->p.label->text);
            widget_event(xuni, xuni->gui->widget, WIDGET_EVENT_REPOSITION);
        }
        
        break;
    }
    
    if(widget->base && widget->base->id == WID_THEME_FONT_COMBO) {
        if(widget->type == WIDGET_LISTBOX) {
            struct widget_t *w = listbox_sel_item(widget);
            
            if(w) {
                use_font_by_name(xuni, THEME_FONT_SANS,
                    load_font(xuni->gui->widget, w->p.label->text));
                
                get_font_names(xuni,
                    widget_nameid_access(xuni->gui->widget, PANEL_OPTIONS));
            }
        }
    }
    
    /*if(compare_widget_name(widget, "options", "back", "box", 0)) {
        *mode = PANEL_MAIN_MENU;
    }*/
    
    return 0;
}

/*void perform_back_click(struct widget_t *widget,
    panel_type_t *mode, void *vdata, struct xuni_t *xuni) {
    
    if(compare_widget_name(widget, "options", "back", "box", 0)) {
        *mode = PANEL_MAIN_MENU;
    }
    else if(compare_widget_name(widget, "options graphics", "back", "box", 0)
        || compare_widget_name(widget, "options theme", "back", "box", 0)) {
        
        *mode = PANEL_OPTIONS;
        clear_gui(xuni, PANEL_OPTIONS, 0);
    }
}*/

static void screen_mode_from_string(struct xuni_t *xuni, const char *mode) {
    long w, h;
    char *p;
    
    w = strtol(mode, &p, 0);
    if(mode == p) return;
    while(!isdigit(*p)) p ++;
    
    mode = p;
    h = strtol(mode, &p, 0);
    if(mode == p) return;
    
    use_screen_mode(xuni, w, h);
}

int options_graphics_deactivate(struct xuni_t *xuni,
    struct panel_data_t *data) {
    
    struct widget_t *widget
        = data->event[PANEL_EVENT_DEACTIVATE].p.deactivate.widget;
    
    switch(widget->id) {
    case WID_GRAPHICS_TEXTSCREENMODE:
        screen_mode_from_string(xuni, get_textbox_data(widget));
        
        break;
    }
    
    return 0;
}

int options_theme_deactivate(struct xuni_t *xuni,
    struct panel_data_t *data) {
    
    struct widget_t *widget
        = data->event[PANEL_EVENT_DEACTIVATE].p.deactivate.widget;
    
    switch(widget->id) {
    case WID_THEME_FONT_FILENAME:
        use_font_by_name(xuni, THEME_FONT_SANS,
            load_font(xuni->gui->widget, get_textbox_data(widget)));
        
        get_font_names(xuni,
            widget_nameid_access(xuni->gui->widget, PANEL_OPTIONS));
        
        break;
    case WID_THEME_THEME_NAME:
        if(use_theme_by_name(xuni, get_textbox_data(widget))) {
            puts("*** NYI: revert to previous theme name");
        }
        
        break;
    case WID_THEME_THEME_ROUNDNESS:
        use_theme_roundness(xuni, xuni->theme->current,
            100.0 / (strtod(get_textbox_data(widget), 0) * 2));
        widget_event(xuni, xuni->gui->widget, WIDGET_EVENT_REPOSITION);
        break;
    default:
        break;
    }
    
    if(widget->base && widget->base->id == WID_THEME_FONT_COMBO) {
        if(widget->type == WIDGET_TEXTBOX) {
            const char *text = get_textbox_data(widget);
            
            use_font_by_name(xuni, THEME_FONT_SANS,
                load_font(xuni->gui->widget, text));
            
            get_font_names(xuni,
                widget_nameid_access(xuni->gui->widget, PANEL_OPTIONS));
        }
    }
    
    return 0;
}

int options_paint(struct xuni_t *xuni, struct panel_data_t *data) {
    panel_type_t mode = data->event[PANEL_EVENT_PAINT].p.paint.mode;
    /*struct options_data_t *data = vdata;*/
    /*static int fullscreen = -1;*/
    
    /*xuni->gui->panel->widget[PANEL_OPTIONS_GRAPHICS]->compose->widget[2]
        ->p.checkbox->checked = (xuni->smode->restrictfocus == SDL_GRAB_ON);*/
    
    /*if(fullscreen != xuni->smode->fullscreen) {
        fullscreen = xuni->smode->fullscreen;
        
        get_screen_modes(xuni->smode, xuni->font,
            xuni->gui->panel->widget[PANEL_OPTIONS_GRAPHICS]->compose
            ->widget[4]);
        
        set_checkbox(
            xuni->gui->panel->widget[PANEL_OPTIONS_GRAPHICS]->compose
            ->widget[1], xuni->smode->fullscreen);
    }*/
    
    set_checkbox(widget_nameid_follow(xuni->gui->widget,
        PANEL_OPTIONS, WID_GRAPHICS_FULLSCREEN, (size_t)-1),
        xuni->smode->fullscreen);
    
    /*clear_widget_clip(xuni, widget_nameid_follow(xuni->gui->widget,
        PANEL_OPTIONS, WID_THEME_THEME_LIST, (size_t)-1)
        ->compose->widget[WID_LISTBOX_DATA]);*/
    
    clear_screen(xuni->smode->screen);
    
    if(mode == PANEL_OPTIONS) {
        widget_event(xuni, widget_nameid_access(xuni->gui->widget,
            PANEL_OPTIONS), WIDGET_EVENT_PAINT);
    }
    else if(mode == PANEL_OPTIONS_GRAPHICS) {
        widget_event(xuni, widget_nameid_access(xuni->gui->widget,
            PANEL_OPTIONS_GRAPHICS), WIDGET_EVENT_PAINT);
    }
    else if(mode == PANEL_OPTIONS_THEME) {
        widget_event(xuni, widget_nameid_access(xuni->gui->widget,
            PANEL_OPTIONS_THEME), WIDGET_EVENT_PAINT);
    }
    
    paint_menu_fps(xuni, THEME_FONT_MONO);
    
    update_screen(xuni);
    
    return 0;
}

int options_free(struct xuni_t *xuni, struct panel_data_t *data) {
    return 0;
}
