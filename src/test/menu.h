/*! \file menu.h

*/

#ifndef XUNI_GUARD_MENU_H
#define XUNI_GUARD_MENU_H

#include "graphics.h"
#include "gui.h"

#ifdef __cplusplus
extern "C" {
#endif

struct menu_data_t {
    int unused;
};

void switch_main_menu_background(struct widget_t *panel, const char *name);
void paint_menu_fps(struct xuni_t *xuni, size_t font);

int menu_init(struct xuni_t *xuni, struct panel_data_t *data);
int menu_start(struct xuni_t *xuni, struct panel_data_t *data);
int menu_event(struct xuni_t *xuni, struct panel_data_t *data);
int menu_click(struct xuni_t *xuni, struct panel_data_t *data);
int menu_paint(struct xuni_t *xuni, struct panel_data_t *data);
int menu_free(struct xuni_t *xuni, struct panel_data_t *data);

#ifdef __cplusplus
}
#endif

#endif
