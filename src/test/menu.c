/*! \file menu.c

*/

#include <string.h>

#include "menu.h"

#include "error.h"
#include "graphics.h"
#include "gui.h"
#include "loop.h"
#include "memory.h"
#include "xuni.h"
#include "resource/resource.h"

#include "widget/widgets.h"
#include "widget/button.h"
#include "widget/image.h"

#include "main.h"
#include "version.h"

enum wid_t {
    WID_ORION_BACKGROUND,
    WID_MOON_BACKGROUND,
    WID_PARALLAX_BACKGROUND_0,
    WID_PARALLAX_BACKGROUND_1,
    WID_PARALLAX_BACKGROUND_2,
    WID_PARALLAX_BACKGROUND_3,
    WID_VERSION_LABEL,
    WID_NEWGAME,
    WID_MULTIPLAYER,
    WID_LOADGAME,
    WID_OPTIONS,
    WID_HELP,
    WID_QUIT,
    WID_NEWGAME_IMAGE,
    WID_MULTIPLAYER_IMAGE,
    WID_LOADGAME_IMAGE,
    WID_OPTIONS_IMAGE,
    WID_HELP_IMAGE,
    WID_QUIT_IMAGE,
    WIDS
};

struct string_index_t main_menu_background_names[MAIN_MENU_BACKGROUNDS] = {
    {"Black", MAIN_MENU_BACKGROUND_BLACK},
    {"Moon with parallax clouds",
        MAIN_MENU_BACKGROUND_MOON_WITH_PARALLAX_CLOUDS},
    {"Orion nebula", MAIN_MENU_BACKGROUND_ORION_NEBULA},
    {"Orion nebula frame update", MAIN_MENU_BACKGROUND_ORION_NEBULA_FRAME},
    {"Parallax clouds", MAIN_MENU_BACKGROUND_PARALLAX_CLOUDS}
};

static void set_hover_visible(struct gui_t *gui, struct widget_t *menu);

void switch_main_menu_background(struct widget_t *panel, const char *name) {
    size_t index = string_to_index(main_menu_background_names,
        sizeof(main_menu_background_names)
        / sizeof(*main_menu_background_names), name);
    static size_t wids[] = {
        WID_ORION_BACKGROUND,
        WID_MOON_BACKGROUND,
        WID_PARALLAX_BACKGROUND_0,
        WID_PARALLAX_BACKGROUND_1,
        WID_PARALLAX_BACKGROUND_2,
        WID_PARALLAX_BACKGROUND_3
    };
    static struct {
        int frameupdate;
        int enable[6];
    } data[] = {
        {1, {0, 1, 0, 1, 1, 1}},
        {0, {1, 0, 0, 0, 0, 0}},
        {1, {1, 0, 0, 0, 0, 0}},
        {1, {0, 0, 1, 1, 0, 0}},
        {0, {0, 0, 0, 0, 0, 0}}
    };
    size_t x;
    
    if(index != (size_t)-1) {
        panel->p.panel->frameupdate = data[index].frameupdate;
        
        for(x = 0; x < sizeof(wids) / sizeof(*wids); x ++) {
            set_widget_visibility(
                widget_nameid_access(panel, wids[x]),
                WIDGET_VISIBILITY_VISIBLE,
                data[index].enable[x]);
        }
    }
}

int menu_init(struct xuni_t *xuni, struct panel_data_t *data) {
    struct widget_t *panel = data->event[PANEL_EVENT_INIT].p.init.panel;
    
    init_wid(xuni->gui->widget, xuni->gui->widget,
        PANEL_MAIN_MENU, "main menu");
    
    init_wid(xuni->gui->widget, panel, WID_ORION_BACKGROUND,
        "main menu/orion background");
    init_wid(xuni->gui->widget, panel, WID_MOON_BACKGROUND,
        "main menu/moon background");
    init_wid(xuni->gui->widget, panel, WID_PARALLAX_BACKGROUND_0,
        "main menu/parallax background 0");
    init_wid(xuni->gui->widget, panel, WID_PARALLAX_BACKGROUND_1,
        "main menu/parallax background 1");
    init_wid(xuni->gui->widget, panel, WID_PARALLAX_BACKGROUND_2,
        "main menu/parallax background 2");
    init_wid(xuni->gui->widget, panel, WID_PARALLAX_BACKGROUND_3,
        "main menu/parallax background 3");
    init_wid(xuni->gui->widget, panel, WID_VERSION_LABEL,
        "main menu/version label");
    init_wid(xuni->gui->widget, panel, WID_NEWGAME, "main menu/newgame");
    init_wid(xuni->gui->widget, panel, WID_MULTIPLAYER,
        "main menu/multiplayer");
    init_wid(xuni->gui->widget, panel, WID_LOADGAME, "main menu/loadgame");
    init_wid(xuni->gui->widget, panel, WID_OPTIONS, "main menu/options");
    init_wid(xuni->gui->widget, panel, WID_HELP, "main menu/help");
    init_wid(xuni->gui->widget, panel, WID_QUIT, "main menu/quit");
    init_wid(xuni->gui->widget, panel, WID_NEWGAME_IMAGE,
        "main menu/newgame image");
    init_wid(xuni->gui->widget, panel, WID_MULTIPLAYER_IMAGE,
        "main menu/multiplayer image");
    init_wid(xuni->gui->widget, panel, WID_LOADGAME_IMAGE,
        "main menu/loadgame image");
    init_wid(xuni->gui->widget, panel, WID_OPTIONS_IMAGE,
        "main menu/options image");
    init_wid(xuni->gui->widget, panel, WID_HELP_IMAGE,
        "main menu/help image");
    init_wid(xuni->gui->widget, panel, WID_QUIT_IMAGE,
        "main menu/quit image");
    
    add_widget_accelerator(xuni, panel,
        widget_nameid_access(panel, WID_NEWGAME), SDLK_n, KMOD_NONE);
    add_widget_accelerator(xuni, panel,
        widget_nameid_access(panel, WID_OPTIONS), SDLK_o, KMOD_NONE);
    add_widget_accelerator(xuni, panel,
        widget_nameid_access(panel, WID_HELP), SDLK_h, KMOD_NONE);
    add_widget_accelerator(xuni, panel,
        widget_nameid_access(panel, WID_QUIT), SDLK_q, KMOD_NONE);
    
    xuni_memory_free((void *)
        widget_nameid_access(panel, WID_VERSION_LABEL)->p.label->text);
    widget_nameid_access(panel, WID_VERSION_LABEL)->p.label->text
        = xuni_memory_duplicate_string(get_xuni_version_string());
    
    widget_event(xuni, widget_nameid_access(panel, WID_VERSION_LABEL),
        WIDGET_EVENT_RESCALE);
    
    /*data->quality.x0 = 300.0;
    data->quality.y0 = 500.0;
    data->quality.x1 = 600.0;
    data->quality.y1 = 200.0;*/
    
    return 0;
}

int menu_start(struct xuni_t *xuni, struct panel_data_t *data) {
    set_caption("Main menu");
    
    return 0;
}

int menu_event(struct xuni_t *xuni, struct panel_data_t *data) {
    panel_type_t *mode = data->event[PANEL_EVENT_EVENT].p.event.mode;
    SDL_Event *event = data->event[PANEL_EVENT_EVENT].p.event.event;
    int repaint = 0;
    
    switch(event->type) {
    case SDL_KEYDOWN:
        switch(event->key.keysym.sym) {
        case SDLK_F4:
            if(event->key.keysym.mod & KMOD_ALT) *mode = PANEL_NONE;
            break;
        default: break;
        }
        break;
    case SDL_QUIT:
        *mode = PANEL_NONE;
        break;
    case SDL_KEYUP:
    case SDL_MOUSEMOTION:
        break;
    default:
        repaint = 1;
        break;
    }
    
    return repaint;
}

int menu_click(struct xuni_t *xuni, struct panel_data_t *data) {
    panel_type_t *mode = data->event[PANEL_EVENT_CLICK].p.click.mode;
    struct widget_t *widget = data->event[PANEL_EVENT_CLICK].p.click.widget;
    
    switch(widget->id) {
    case WID_NEWGAME:
        *mode = PANEL_GAME;
        break;
    case WID_OPTIONS:
        *mode = PANEL_OPTIONS;
        break;
    case WID_HELP:
        *mode = PANEL_HELP;
        break;
    case WID_QUIT:
        *mode = PANEL_NONE;
        break;
    default:
        break;
    }
    
    return 0;  /* ignored */
}

static void set_hover_visible(struct gui_t *gui, struct widget_t *menu) {
    size_t x;
    
    for(x = WID_NEWGAME_IMAGE; x <= WID_QUIT_IMAGE; x ++) {
        int set = (gui->sel.p.widget
            && gui->sel.p.widget == widget_nameid_follow(menu,
            x + (WID_NEWGAME - WID_NEWGAME_IMAGE), (size_t)-1));
        
        set_widget_visibility(widget_nameid_access(menu, x),
            WIDGET_VISIBILITY_VISIBLE, set);
    }
}

int menu_paint(struct xuni_t *xuni, struct panel_data_t *data) {
    struct widget_t *panel
        = widget_nameid_access(xuni->gui->widget, PANEL_MAIN_MENU);
    
    /*lock_screen(xuni->smode->screen);*/
    clear_screen(xuni->smode->screen);
    /*unlock_screen(xuni->smode->screen);*/
    
    set_hover_visible(xuni->gui, panel);
    
    widget_event(xuni, panel, WIDGET_EVENT_PAINT);
    
    paint_menu_fps(xuni, THEME_FONT_MONO);
    
    if(panel->p.panel->frameupdate) {
        paint_cursor(xuni, get_theme_widget(xuni, THEME_DEFAULT_CURSOR));
    }
    
    update_screen(xuni);
    
    return 0;
}

void paint_menu_fps(struct xuni_t *xuni, size_t font) {
    static unsigned long count = 0, total = 0, lastfps = 0;
    static Uint32 lasttick = 0;
    Uint32 tick = SDL_GetTicks();
    char buffer[BUFSIZ];
    SDL_Surface *text;
    
    if(tick >= lasttick + 1000) {
        lasttick = tick;
        lastfps = count;
        count = 0;
    }
    
    count ++;
    total ++;
    
    sprintf(buffer, "%3lu FPS | %lu (+%3lu) frames | %lu.%03lu seconds",
        lastfps, total, count, (unsigned long)tick / 1000,
        (unsigned long)tick % 1000);
    text = render_text(xuni, get_theme_widget(xuni, font), buffer,
        255, 255, 255);
    
    blit_surface(xuni->smode->screen, text, 10, 10);
    
    free_surface(text);
}

int menu_free(struct xuni_t *xuni, struct panel_data_t *data) {
    return 0;
}
