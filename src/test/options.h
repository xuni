/*! \file options.h

*/

#ifndef XUNI_GUARD_OPTIONS_H
#define XUNI_GUARD_OPTIONS_H

#include "graphics.h"
#include "gui.h"
#include "loop.h"

#ifdef __cplusplus
extern "C" {
#endif

struct options_data_t {
    int unused;
};

int options_init(struct xuni_t *xuni, struct panel_data_t *data);
int options_start(struct xuni_t *xuni, struct panel_data_t *data);
int options_event(struct xuni_t *xuni, struct panel_data_t *data);
int options_click(struct xuni_t *xuni, struct panel_data_t *data);
int options_paint(struct xuni_t *xuni, struct panel_data_t *data);
int options_graphics_deactivate(struct xuni_t *xuni,
    struct panel_data_t *data);
int options_theme_deactivate(struct xuni_t *xuni, struct panel_data_t *data);
int options_free(struct xuni_t *xuni, struct panel_data_t *data);

#ifdef __cplusplus
}
#endif

#endif
