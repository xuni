/*! \file help.h

*/

#ifndef XUNI_GUARD_HELP_H
#define XUNI_GUARD_HELP_H

#include "graphics.h"
#include "gui.h"
#include "loop.h"

#ifdef __cplusplus
extern "C" {
#endif

int help_init(struct xuni_t *xuni, struct panel_data_t *data);
int help_start(struct xuni_t *xuni, struct panel_data_t *data);
int help_event(struct xuni_t *xuni, struct panel_data_t *data);
int help_paint(struct xuni_t *xuni, struct panel_data_t *data);
int help_click(struct xuni_t *xuni, struct panel_data_t *data);
int help_free(struct xuni_t *xuni, struct panel_data_t *data);

#ifdef __cplusplus
}
#endif

#endif
