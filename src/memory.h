/*! \file memory.h

*/

#ifndef XUNI_GUARD_MEMORY_H
#define XUNI_GUARD_MEMORY_H

#include "SDL.h"
#include "version.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef VERSION_STDC_C99
    #define RECORD_FUNCTION_NAMES
#endif

enum xuni_memory_block_type_t {
    MEMORY_BLOCK_TYPE_NORMAL,
    MEMORY_BLOCK_TYPE_SDL_SURFACE,
    MEMORY_BLOCK_TYPE_LOADSO_OBJECT,  /* <- unused */
    MEMORY_BLOCK_TYPES
};

size_t allocated_sdl_surface(SDL_Surface *surface);
void decrement_allocated(size_t size);
void increment_allocated(size_t size);

void xuni_memory_initialize(void);
void *xuni_memory_allocate_func(size_t size, const char *func);
char *xuni_memory_duplicate_string(const char *data);
char *xuni_memory_duplicate_string_len(const char *data, size_t len);
void *xuni_memory_resize_func(void *data, size_t size, const char *func);
void xuni_memory_add_block_func(void *data, const char *func,
    enum xuni_memory_block_type_t type);
void xuni_memory_increment(void *data);
void *xuni_memory_decrement(void *data);
/*void xuni_memory_add_count(void *data, int count);
size_t xuni_memory_get_count(void *data);*/
void xuni_memory_free(void *data);
void xuni_memory_keep_freed_blocks(int keep);
void xuni_memory_free_all(void);

#ifdef RECORD_FUNCTION_NAMES
    #define xuni_memory_allocate(size) \
        xuni_memory_allocate_func(size, __func__)
    #define xuni_memory_add_block(data, type) \
        xuni_memory_add_block_func(data, __func__, type)
    #define xuni_memory_resize(data, size) \
        xuni_memory_resize_func(data, size, __func__)
#else
    #define xuni_memory_allocate(size) \
        xuni_memory_allocate_func(size, "unknown")
    #define xuni_memory_add_block(data, type) \
        xuni_memory_add_block_func(data, "unknown", type)
    #define xuni_memory_resize(data, size) \
        xuni_memory_resize_func(data, size, "unknown")
#endif

#ifdef __cplusplus
}
#endif

#endif
