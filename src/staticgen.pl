#!/usr/bin/perl

my $firstfunc = 1;
my $path = '';
my @funclist = ();

print_header();

foreach my $file (@ARGV) {
    $path = '';
    get_list(get_path($file), read_file($file));
}

print_funclist();

print_footer();

sub get_path {
    my $file = shift;
    $file =~ /(.*)\// && return $1;
    
    return '';
}

sub get_filename {
    my $file = shift;
    $file =~ /\/([^\/]+)/;
    
    return $1;
}

sub read_file {
    my $file = shift;
    
    #print "--- $file\n";
    
    open(FILE, "$file");
    my @data = <FILE>;
    close FILE;
    
    return @data;
}

sub get_list {
    my $path = shift;
    my @data = @_;
    my $in = 0;
    my @list = ();
    
    foreach my $line (@data) {
        if($in) {
            if($line =~ /<\/handler/) {
                $in = 0;
            }
            else {
                if($line =~ /<[\w\d_]+>([\w\d_]+)<\/[\w\d_]+>/) {
                    push(@funclist, $1);
                }
            }
        }
        else {
            if($line =~ /<include>(.*)<\/include>/) {
                my $file = $1;
                my $addpath = get_path($file);
                my $temppath = $path;
                
                if($addpath ne '') {
                    $temppath .= "/$addpath";
                }
                
                get_list($temppath,
                    read_file("$temppath/" . get_filename($file)));
            }
            
            if($line =~ /<handler/) {
                $in = 1;
                next;
            }
        }
        
        if($in) {
            push(@list, $_);
        }
    }
    
    return @list;
}

sub print_header {
    print <<EOF;
#ifdef LOADSO_STATIC_VERSION
func_point_t xuni_loadso_load_function(loadso_t object, const char *func) {
    struct string_function_t data[] = {
EOF
}

sub print_funclist {
    my $prev = '';
    
    foreach my $func (sort @funclist) {
        if($func ne $prev) {
            print_func($func);
        }
        
        $prev = $func;
    }
}

sub print_func {
    my $func = shift;
    
    if(!$firstfunc) {
        print ",\n";
    }
    
    print "        {\"$func\", (func_point_t)$func}";
    
    $firstfunc = 0;
}

sub print_footer {
    print <<EOF;

    };
    func_point_t funcp
        = string_to_function(data, sizeof(data) / sizeof(*data), func);
    
    if(!funcp) {
        log_message(ERROR_TYPE_RESOURCE, 0, __FILE__, __LINE__,
            "Unknown function: \\"%s\\"", func);
    }
    
    return funcp;
}
#endif
EOF
}
