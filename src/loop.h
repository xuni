/*! \file loop.h

*/

#ifndef XUNI_GUARD_LOOP_H
#define XUNI_GUARD_LOOP_H

#include "graphics.h"
#include "gui.h"
#include "resource/resource.h"

#ifdef __cplusplus
extern "C" {
#endif

void call_init_funcs(struct xuni_t *xuni, struct widget_t *widget,
    struct resource_t *settings);
void call_free_funcs(struct xuni_t *xuni, struct widget_t *widget);
int panel_event_recursive(struct xuni_t *xuni, struct panel_data_t *data,
    enum panel_event_type_t type, struct widget_t *widget);
void call_deactivate_func(struct xuni_t *xuni, struct widget_t *widget);

int default_panel_sel(struct xuni_t *xuni, struct panel_data_t *data);
int set_default_widget_sel(struct xuni_t *xuni, panel_type_t mode,
    int xp, int yp, int click, void *vdata);

/*void set_panel_callbacks(struct widget_t *widget, void *vdata,
    int frameupdate,
    panel_event_func_t init_func,
    panel_event_func_t start_func,
    panel_event_func_t event_func,
    panel_event_func_t set_widget_sel_func,
    panel_event_func_t perform_click_func,
    panel_event_func_t deactivate_func,
    panel_event_func_t paint_func,
    panel_event_func_t free_func);*/

void execute_callback(struct xuni_t *xuni, struct xuni_callback_t *callback);
void main_loop(struct xuni_t *xuni, struct xuni_callback_t *always,
    panel_type_t mode);

#ifdef __cplusplus
}
#endif

#endif
