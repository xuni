#!/usr/bin/perl

use warnings;

my $LOCATION = 0;

my @data = `nm -A \$(find -name '*.o')`;
my %func;

classify_functions(@data);
print_symbols();

sub classify_functions {
    foreach my $line (@_) {
        if($line =~ /^(.*):([a-fA-F\d]|\s)+\s+(.)\s+(\S+)/) {
            my $name = $4;
            my $location = $1;
            my $type = $3;
            
            $location =~ s|(CMakeFiles/)?\w+.dir/||;
            
            push(@{$func{$name}}, {
                'location' => $location,
                'type' => $type
            });
        }
    }
}

sub print_symbols {
    foreach my $symbol (keys %func) {
        printf("%-32s ", $symbol);
        
        my $type = '';
        foreach my $instance (@{$func{$symbol}}) {
            $type .= $instance->{'type'};
        }
        
        if($type =~ /T/) {
            if($type =~ /U/) {
                print "(must be extern) ";
            }
            else {
                print "(could be static) ";
            }
        }
        else {
            if($type =~ /U/) {
                print "(unresolved) ";
            }
        }
        
        if($type =~ /t/) {
            print "(static) ";
        }
        
        for(my $pos = 0; ; ) {
            my $new = index($type, 'T', $pos);
            
            if($new < $pos) {
                last;
            }
            
            if($LOCATION) {
                print "(location @{$func{$symbol}}[$pos]->{'location'}) ";
            }
            
            $pos = $new + 1;
        }
        
        print "($type)\n";
    }
}
