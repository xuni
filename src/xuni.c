/*! \mainpage
    
    xuni, a flexible SDL GUI widget toolkit which supports rescaling \n
    Copyright (C) 2008 DWK
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
    
    The GNU General Public License version 2 is included in the file COPYING.
    
    \author DWK
        dwks@theprogrammingsite.com \n
        http://dwks.theprogrammingsite.com \n
    
    \version 0.3.0
    
    At the time of this writing, xuni can be obtained from:
        http://dwks.theprogrammingsite.com/myprogs/xuni.htm
    
    ----
    
    xuni stands for "Explore the Universe". It's entirely coincidental that
    shifting one of the letters in "xuni" spells UNIX, and that reversing the
    letters and adding another letter spells Linux. "xuni" is pronounced
    "zoo-nee" in the author's opinion. But "ecks-you-nee" or
    "ecks-you-en-eye" or other variations work as well.
    
    xuni was written from the ground up without incorporating any code from my
    numerous previous SDL projects. It uses SDL_image, SDL_gfx, and SDL_ttf in
    addition to the SDL, along with expat.
*/

/*! \file xuni.c

*/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "SDL.h"

#include "error.h"
#include "graphics.h"
#include "gui.h"
#include "loop.h"
#include "memory.h"
#include "resource/resource.h"
#include "version.h"
#include "xuni.h"

static void validate_xuni_structure(struct xuni_t *xuni);

/*! Dynamically allocates enough memory for a xuni_t structure. The memory is
    uninitialized. init_xuni() must eventually be called on the returned
    structure to get a useful xuni_t object.
    \return The newly allocated xuni_t structure.
*/
struct xuni_t *allocate_xuni(void) {
    struct xuni_t *xuni = xuni_memory_allocate(sizeof(*xuni));
    
    xuni->smode = xuni_memory_allocate(sizeof(*xuni->smode));
    xuni->theme = xuni_memory_allocate(sizeof(*xuni->theme));
    xuni->gui = xuni_memory_allocate(sizeof(*xuni->gui));
    xuni->wtype = xuni_memory_allocate(sizeof(*xuni->wtype));
    xuni->loadso = xuni_memory_allocate(sizeof(*xuni->loadso));
    
    return xuni;
}

/*! Makes sure that the xuni_t structure \a xuni can be executed, i.e. has a
    root widget.
    
    Also checks to see if any themes were loaded. If not, this is not a fatal
    error, but still warrants an informative message, because xuni will not
    look very good once it is running.
    
    \param xuni The xuni structure to check for a root widget etc.
*/
static void validate_xuni_structure(struct xuni_t *xuni) {
    if(!xuni->gui->widget) {
        log_message(ERROR_TYPE_FATAL, 0, __FILE__, __LINE__,
            "No root widget");
        exit(1);
    }
    else if(!xuni->theme->current) {
        log_message(ERROR_TYPE_RESOURCE, 0, __FILE__, __LINE__,
            "No themes loaded");
    }
}

/*! Initializes a xuni_t structure, after a resource_t structure has been
    created, perhaps by reading data from an XML file. This should be the
    first function called on a newly allocated xuni_t structure.
    \param xuni The xuni_t structure to initialize.
    \param settings The settings to use while initializing \a xuni.
    \param icon The icon to use for the xuni window.
*/
void init_xuni(struct xuni_t *xuni, struct resource_t *settings,
    const char *icon) {
    
    init_smode(xuni->smode, settings);
    init_gui(xuni);
    
    init_sdl_libraries(xuni->smode, icon);
    
    init_loadso(xuni->loadso);
    init_wtype(xuni, xuni->wtype);
    
    init_theme_structure(xuni->theme);
    load_resource_widgets(xuni, &xuni->gui->widget, settings->data);
    
    validate_xuni_structure(xuni);
}

/*! Frees the memory allocated for a xuni_t structure (\a xuni). This includes
    not only the memory allocated in init_xuni() but also the widget tree etc.
    
    This function cannot yet be replaced with a call to xuni_memory_free_all()
    -- it calls other freeing function too, for example
    xuni_loadso_free_object().
    \param xuni The xuni_t structure to free all of the allocated memory of.
*/
void free_xuni(struct xuni_t *xuni) {
    free_smode(xuni->smode);
    free_theme(xuni->theme);
    free_gui(xuni);
    free_wtype(xuni->wtype);
    free_loadso(xuni->loadso);
    
    xuni_memory_free(xuni->smode);
    xuni_memory_free(xuni->theme);
    xuni_memory_free(xuni->gui);
    xuni_memory_free(xuni->wtype);
    
    xuni_memory_free(xuni);
}

/*! Returns the version of xuni, as defined by VERSION (in version.h).
    \return The current version of xuni.
*/
const char *get_xuni_version_string(void) {
    return XUNI_VERSION;
}
