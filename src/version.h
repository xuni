/*! \file version.h

*/

#ifndef XUNI_GUARD_VERSION_H
#define XUNI_GUARD_VERSION_H

#ifdef __cplusplus
extern "C" {
#endif

#define DIRECTORY_SEPARATORS "/\\"

/* Version of C. */
#ifdef WIN32
    #define VERSION_WIN32
#endif

/*#ifdef __STDC__
    #if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
        #define VERSION_STDC_C99
    #endif
#endif*/

#define VERSION_STDC_C99

/*#ifdef VERSION_STDC_C99
    #include <stdbool.h>
#else
    #define __func__ __FILE__
    typedef enum { false, true } bool;
#endif*/

/*! Git version of xuni, with a default value when not defined. */
#ifndef XUNI_GIT_VERSION
    #define XUNI_GIT_VERSION "unknown"
#endif

/*! The version number of xuni (as a string). */
#define XUNI_VERSION_NUMBER "0.3.0"

/*! Full version string of xuni. */
#define XUNI_VERSION \
    "xuni version " XUNI_VERSION_NUMBER "-git-" XUNI_GIT_VERSION \
        " (" __DATE__ " " __TIME__ ")"

#ifdef __cplusplus
}
#endif

#endif
