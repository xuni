/*! \file checkbox.c

*/

#include "../graphics.h"
#include "../memory.h"
#include "widgets.h"
#include "checkbox.h"
#include "button.h"
#include "label.h"
#include "box.h"  /* !!! for set_box_type() hack below */

static void free_checkbox(struct xuni_t *xuni, struct widget_t *widget);
static void paint_checkbox(struct xuni_t *xuni, struct widget_t *widget);
static void reposition_checkbox(struct xuni_t *xuni, struct widget_t *widget);
static void rescale_checkbox(struct xuni_t *xuni, struct widget_t *widget);

void checkbox_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event) {
    
    static void (*function[])(struct xuni_t *xuni, struct widget_t *widget)
        = {
        
        free_checkbox,
        0,
        paint_checkbox,
        reposition_checkbox,
        rescale_checkbox
    };
    
    call_widget_event_func(xuni, widget, event, function,
        sizeof(function) / sizeof(*function));
}

void init_checkbox(struct widget_t *widget, struct xuni_t *xuni,
    size_t font, const char *text, int checked) {
    
    double width = 100.0 / 5;
    
    widget->type = WIDGET_CHECKBOX;
    widget->p.checkbox = xuni_memory_allocate(sizeof(*widget->p.checkbox));
    
    widget->p.checkbox->checked = checked;
    
    /* button */
    /*sub = allocate_widget("check image", widget);
    
    init_widget_pos(sub, 0, 0,
        width, 100.0, POS_PACK_NONE);
    init_image(sub, widget_nameid_access(xuni->theme->current,
        THEME_CHECKBOX)->p.image->filename, 0);*/
    
    add_allocate_widget_compose(widget, "button");
    
    init_widget_pos(last_compose_widget(widget), 0, 0,
        width, 100.0, POS_PACK_NONE);
    init_button(last_compose_widget(widget), 0);
    
    init_button_image(xuni, last_compose_widget(widget),
        get_theme_widget(xuni, THEME_CHECKBOX));
    
    last_compose_widget(widget)->compose->widget[WID_BUTTON_BOX]->selwidget
        = widget;
    
    last_compose_widget(widget)->visibility &= ~WIDGET_VISIBILITY_INDEPENDENT;
    
    /* label */
    add_allocate_widget_compose(widget, "check label");
    
    init_widget_pos(last_compose_widget(widget),
        width * 1.2, 0,
        100.0 - width * 1.2, 100.0,
        POS_PACK_NONE);
    init_label(last_compose_widget(widget), font, text, LABEL_ALIGN_LEFT,
        255, 255, 255);
    last_compose_widget(widget)->visibility &= ~WIDGET_VISIBILITY_INDEPENDENT;
    
    widget->visibility &= ~WIDGET_VISIBILITY_NOT_COMPOSE;
}

static void free_checkbox(struct xuni_t *xuni, struct widget_t *widget) {
    xuni_memory_free(widget->p.checkbox);
}

static void paint_checkbox(struct xuni_t *xuni, struct widget_t *widget) {
    int checked;
    struct widget_t *box = widget->compose->widget[WID_CHECKBOX_BUTTON]
        ->compose->widget[WID_BUTTON_BOX];
    
    checked = /*widget->compose->widget[0]->compose->widget[0]->p.box->style
        & BOX_STYLE_INVERTED*/ widget->p.checkbox->checked;
    
    /* !!! hack to set p.box->state -- see below */
    set_box_type(xuni->gui, box->selwidget, box);
    
    if(box->p.box->state == (BOX_STATE_ACTIVE | BOX_STATE_HOVER)) {
        checked = !checked;
    }
    
    if(checked) {
        widget->compose->widget[WID_CHECKBOX_BUTTON]->compose->widget
            [WID_BUTTON_LABEL]->visibility |= WIDGET_VISIBILITY_VISIBLE;
    }
    else {
        widget->compose->widget[WID_CHECKBOX_BUTTON]->compose->widget
            [WID_BUTTON_LABEL]->visibility &= ~WIDGET_VISIBILITY_VISIBLE;
    }
    
    /* !!! doesn't work properly because set_box_type() is only called, and
        thus the p.box->state is only set, inside the following paint widget
        event. set_widget_sel() needs to call new functions like
        set_button_sel(), etc. See above hack. */
    
    widget_event(xuni, widget->compose->widget[WID_CHECKBOX_BUTTON],
        WIDGET_EVENT_PAINT);
    widget_event(xuni, widget->compose->widget[WID_CHECKBOX_LABEL],
        WIDGET_EVENT_PAINT);
}

/*! Toggles the selected state of the checkbox \a widget. In other words, if
    the checkbox is set, it becomes unset, and vise versa.
    
    \param widget The checkbox to toggle.
*/
void toggle_checkbox(struct widget_t *widget) {
    enum box_style_t *style
        = &widget->compose->widget[0]->compose->widget[0]->p.box->style;
    
    widget->p.checkbox->checked = !widget->p.checkbox->checked;
    /*widget->compose->widget[0]->compose->widget[0]->p.box->state
        ^= BOX_STATE_INVERTED;*/
    
    if(*style == BOX_STYLE_NORMAL) {
        *style = BOX_STYLE_INVERTED;
    }
    else *style = BOX_STYLE_NORMAL;
}

/*! Sets the selected state of the checkbox \a widget.
    \param widget The checkbox to set the state of.
    \param checked The state to set the checkbox to, either true or false.
*/
void set_checkbox(struct widget_t *widget, int checked) {
    if(checked) {
        widget->compose->widget[WID_CHECKBOX_BUTTON]->compose
            ->widget[WID_BUTTON_BOX]->p.box->style = BOX_STYLE_INVERTED;
    }
    else {
        widget->compose->widget[WID_CHECKBOX_BUTTON]->compose
            ->widget[WID_BUTTON_BOX]->p.box->style = BOX_STYLE_NORMAL;
    }
    
    widget->p.checkbox->checked = checked;
}

static void reposition_checkbox(struct xuni_t *xuni,
    struct widget_t *widget) {
    
}

static void rescale_checkbox(struct xuni_t *xuni, struct widget_t *widget) {
    
}
