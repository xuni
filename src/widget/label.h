/*! \file label.h

*/

#ifndef XUNI_GUARD_LABEL_H
#define XUNI_GUARD_LABEL_H

#include "widgets.h"

#ifdef __cplusplus
extern "C" {
#endif

enum label_type_t {
    LABEL_ALIGN_LEFT,
    LABEL_ALIGN_CENTRE,
    LABEL_ALIGN_RIGHT
};

struct label_t {
    enum label_type_t type;
    const char *text;
    size_t font;
    SDL_Surface *label;
    int scroll;
    struct {
        Uint8 r, g, b;
    } col;
};

void label_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event);

void init_label(struct widget_t *widget, size_t fontid, const char *data,
    enum label_type_t type, Uint8 r, Uint8 g, Uint8 b);
void reposition_label_data(struct xuni_t *xuni, struct label_t *label,
    struct widget_edit_t *edit, size_t pos);
size_t width_to_label_pos(struct xuni_t *xuni, int pos, struct widget_t *font,
    char *data);

#ifdef __cplusplus
}
#endif

#endif
