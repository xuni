/*! \file dump.h

*/

#ifndef XUNI_GUARD_DUMP_H
#define XUNI_GUARD_DUMP_H

#include "../graphics.h"
#include "widgets.h"

#ifdef __cplusplus
extern "C" {
#endif

void print_widget_backtrace(struct xuni_t *xuni, struct widget_t *widget);
const char *get_widget_type_name(struct xuni_t *xuni,
    enum widget_type_t type);
void print_inline_widget_backtrace(struct widget_t *widget);
void print_sel_widgets(struct widget_t *widget);
void print_negative_sels(struct widget_t *widget);
void print_widget_clip(struct widget_t *widget);
void dump_widget_tree(struct xuni_t *xuni, struct widget_t *widget);
void dump_widget_tree_xml(struct xuni_t *xuni, struct widget_t *widget,
    const char *filename);
void dump_widgets_need_repaint(struct widget_t *widget);

#ifdef __cplusplus
}
#endif

#endif
