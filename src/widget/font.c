/*! \file font.c
    Font handling functions.
*/

#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <math.h>

#include "SDL.h"
#include "SDL_ttf.h"
#include "SDL_gfxPrimitives.h"
#include "SDL_rotozoom.h"

/* split cannot parse gfxPrimitivesFontdata[] */
#ifdef SPLINT
#define GFX_FONTDATAMAX (8*256)

extern unsigned char gfxPrimitivesFontdata[GFX_FONTDATAMAX];
#else
#include "SDL_gfxPrimitives_font.h"
#endif

#include "../graphics.h"
#include "../memory.h"
#include "../error.h"
#include "widgets.h"
#include "font.h"

#define GFX_FONT_WIDTH CHAR_BIT
#define GFX_FONT_HEIGHT (GFX_FONTDATAMAX / UCHAR_MAX)

static void free_font(struct xuni_t *xuni, struct widget_t *widget);
static void rescale_font(struct xuni_t *xuni, struct widget_t *widget);

static void new_font_t(struct widget_t *widget, const char *name);
static int pick_font_point(struct xuni_t *xuni);
static SDL_Surface *render_gfx_text(const char *text, int r, int g, int b);

void font_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event) {
    
    static void (*function[])(struct xuni_t *xuni, struct widget_t *widget)
        = {
        
        free_font,
        0,
        0,
        0,
        rescale_font
    };
    
    call_widget_event_func(xuni, widget, event, function,
        sizeof(function) / sizeof(*function));
}

void init_font(struct widget_t *widget, const char *name) {
    widget->type = WIDGET_FONT;
    new_font_t(widget, name);
}

static void new_font_t(struct widget_t *widget, const char *name) {
    widget->p.font = xuni_memory_allocate(sizeof(*widget->p.font));
    
    widget->p.font->name = xuni_memory_duplicate_string(name);
    widget->p.font->point = 0;
    widget->p.font->ttf = 0;
}

/*! Finds or creates a font widget with the file name \a name. In other words,
    if a font of this name has been loaded in the past, this existing widget
    is used. Otherwise, a new widget is created and returned. Presumably this
    will be added to the widget tree and can be found by a later call to this
    function.
    
    \param widget The structure to search through for existing font widgets.
        (Actually, all searching is done in the first widget found called
        "font".)
    \param name The name of the font to find, or if not found, to create.
*/
struct widget_t *load_font(struct widget_t *widget, const char *name) {
    struct widget_t *fontpanel = find_widget(widget, "font");  /* !!! */
    size_t x;
    
    if(!fontpanel || !name || !*name) return 0;
    
    for(x = 0; x < fontpanel->compose->widgets; x ++) {
        if(fontpanel->compose->widget[x]->type == WIDGET_FONT
            && !strcmp(fontpanel->compose->widget[x]->p.font->name, name)) {
            
            return fontpanel->compose->widget[x];
        }
    }
    
    add_allocate_widget(fontpanel, (char *)name);
    init_widget_pos(last_compose_widget(fontpanel), 0, 0, 1, 1,
        POS_PACK_NONE);
    init_font(last_compose_widget(fontpanel), name);
    
    return last_compose_widget(fontpanel);
}

void use_font_by_name(struct xuni_t *xuni, size_t n,
    struct widget_t *widget) {
    
    struct widget_t **font;
    
    if(get_theme_widget(xuni, n)) {
        font = &xuni->theme->current->p.theme->nameid->widget[n].widget;
        free_font(xuni, *font);
        if(widget) {
            xuni_memory_increment(widget->p.font);
            (*font)->p.font = widget->p.font;
        }
        else new_font_t(*font, "");
        
        widget_event(xuni, *font, WIDGET_EVENT_RESCALE);
        widget_event(xuni, xuni->gui->widget, WIDGET_EVENT_UPDATE_TEXT);
    }
}

static void free_font(struct xuni_t *xuni, struct widget_t *widget) {
    if(xuni_memory_decrement(widget->p.font)) {
        if(widget->p.font->ttf) TTF_CloseFont(widget->p.font->ttf);
        else {
            /* hack to free SDL_gfx's statically stored, dynamically allocated
                memory for each character (glyph) */
            gfxPrimitivesSetFont(gfxPrimitivesFontdata,
                GFX_FONT_WIDTH, GFX_FONT_HEIGHT);
        }
        
        xuni_memory_free((void *)widget->p.font->name);
        
        free(widget->p.font);
    }
}

static void rescale_font(struct xuni_t *xuni, struct widget_t *widget) {
    struct font_t *font = widget->p.font;
    
    font->point = pick_font_point(xuni);
    
    if(font->ttf) TTF_CloseFont(font->ttf);
    font->ttf = TTF_OpenFont(font->name, font->point);
    
    if(font->ttf) {
        TTF_SetFontStyle(font->ttf, TTF_STYLE_NORMAL);
    }
    else {
        if(!font->name || *font->name) {
            log_message(ERROR_TYPE_WARNING, 0, __FILE__, __LINE__,
                "Can't open font \"%s\", using built-in fixed-size %ix%i"
                " bitmap font", font->name, GFX_FONT_WIDTH, GFX_FONT_HEIGHT);
        }
        
        gfxPrimitivesSetFont(gfxPrimitivesFontdata,
            GFX_FONT_WIDTH, GFX_FONT_HEIGHT);
    }
}

double get_font_ratio(struct xuni_t *xuni) {
    return 1.0;
    return (xuni->smode->screen->w / (double)xuni->smode->screen->h)
        / (4.0 / 3.0);
}

/*! Picks a reasonable font size for the current screen resolution, according
    to the following formula:
        point-size = round(min(screen-width / ratio, screen-height) / 30.0)
    \param xuni Contains information about the current screen resolution that
        xuni is using.
    \return The right font size for the current screen resolution.
*/
static int pick_font_point(struct xuni_t *xuni) {
    double ratio = get_font_ratio(xuni);
    int mindim = (xuni->smode->width / ratio < xuni->smode->height
        ? xuni->smode->width / ratio : xuni->smode->height);
    int point = (int)(mindim / 30.0 + 0.5);
    
    /*printf("Picking font size %i\n", (int)point);*/
    
    /* restrict the font point size to a minimum of 2
        libfreetype6 2.3.5-1+b1 often crashes with point size 1
    */
    if(point <= 1) return 2;
    
    return point;
}

int font_height(struct xuni_t *xuni, struct widget_t *widget) {
    if(widget && widget->p.font->ttf) {
        return TTF_FontHeight(widget->p.font->ttf);
    }
    else return GFX_FONT_HEIGHT;
}

int font_string_width(struct xuni_t *xuni, struct widget_t *widget,
    const char *str) {
    
    int width;
    
    if(widget && widget->p.font->ttf) {
        TTF_SizeText(widget->p.font->ttf, str, &width, 0);
    }
    else width = GFX_FONT_WIDTH * strlen(str);
    
    return (int)(width * get_font_ratio(xuni) + 0.5);
}

/*! Creates a new surface with text blitted to it via SDL_gfx's string*()
    code; that is, with a built-in bitmap font. A surface is generated to make
    this function compatible with SDL_ttf's TTF_RenderText_*() functions.
    
    Uses a bit of a hack to do the transparency, because SDL_gfx's string*()
    functions calculate their own alpha. For example, when the surface to blit
    to has an alpha of 128, and SDL_gfx is requested to blit an alpha of 128,
    the actual alpha in the end is (128/256)*(128/256)*256 = 64, not the
    expected 128.
    
    \param text The string to blit to the new surface.
    \param r The red component of the colour of the new surface
        (0 to 255 inclusive, like \a g and \a b).
    \param g The green component of the colour of the text on the new surface.
    \param b The blue component of the text colour, again from 0 to 255
        inclusive.
    \return A new SDL_Surface with the string in \a text blitted to it in the
        colour (\a r, \a g, \a b).
*/
static SDL_Surface *render_gfx_text(const char *text, int r, int g, int b) {
    int bg = ((r + g + b) / 3.0 < 128) ? 255 : 0;
    
    SDL_Surface *image = new_surface(
        GFX_FONT_WIDTH * strlen(text), GFX_FONT_HEIGHT, 32, 0);
    SDL_FillRect(image, NULL, SDL_MapRGB(image->format, bg, bg, bg));
    
    stringRGBA(image, 0, 0, text, r, g, b, 255);
    
    SDL_SetColorKey(image, SDL_SRCCOLORKEY,
        SDL_MapRGB(image->format, bg, bg, bg));
    
    return image;
}

SDL_Surface *render_unrescaled_text(struct widget_t *widget, const char *text,
    Uint8 r, Uint8 g, Uint8 b) {
    
    if(!text || !*text) return NULL;
    
    if(widget && widget->p.font->ttf) {
        SDL_Color col;
        
        col.r = r;
        col.g = g;
        col.b = b;
        
        return TTF_RenderText_Blended(widget->p.font->ttf, text, col);
    }
    
    return render_gfx_text(text, r, g, b);
}

SDL_Surface *render_text(struct xuni_t *xuni, struct widget_t *widget,
    const char *text, Uint8 r, Uint8 g, Uint8 b) {
    
    SDL_Surface *rescaled, *unrescaled;
    double ratio, xratio, yratio;
    
    unrescaled = render_unrescaled_text(widget, text, r, g, b);
    
    ratio = get_font_ratio(xuni);
    
    if(!unrescaled || fabs(ratio - 1.0) < 0.001) return unrescaled;
    
#if 0
    xratio = (ratio > 1.0) ? ratio : 1.0;
    yratio = (ratio < 1.0) ? /*1.0 /*/ ratio : 1.0;
#else
    if(ratio > 1.0) {
        xratio = ratio;
        yratio = 1.0;
    }
    else {
        xratio = ratio;
        yratio = 1.0 / ratio;
    }
#endif
    
#if 0
    rescaled = zoomSurface(unrescaled,
        /*widget->pos->scale.w / 100.0
            / ((double)xuni->smode->screen->w / widget->base->pos->real.w)
            * xuni->smode->screen->w / temp->w*/ xratio,
        /*widget->pos->scale.h / 100.0
            / ((double)xuni->smode->screen->h / widget->base->pos->real.h)
            * xuni->smode->screen->h / temp->h*/ yratio,
        SMOOTHING_ON);
#elif 0
    rescaled = sge_transform_surface(unrescaled,
        SDL_MapRGB(xuni->smode->screen->format, 0, 0, 0),
        0.0f, xratio, yratio, 0);
#else
    rescaled = zoomSurface(unrescaled,
        (int)(unrescaled->w * xratio + 0.5) / (double)unrescaled->w,
        (int)(unrescaled->h * yratio + 0.5) / (double)unrescaled->h,
        SMOOTHING_ON);
#endif
    
    SDL_FreeSurface(unrescaled);
    
    return rescaled;
}
