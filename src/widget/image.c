/*! \file image.c

*/

#include <stdlib.h>

#include "SDL_rotozoom.h"
/*#include "sge_rotation.h"*/

#include "../graphics.h"
#include "../memory.h"
#include "../error.h"
#include "../utility.h"
#include "widgets.h"
#include "image.h"

static void free_image(struct xuni_t *xuni, struct widget_t *widget);
static void reposition_image(struct xuni_t *xuni, struct widget_t *widget);
static void rescale_image(struct xuni_t *xuni, struct widget_t *widget);
static void paint_image(struct xuni_t *xuni, struct widget_t *widget);
static void generate_image(struct xuni_t *xuni, struct widget_t *widget);

void image_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event) {
    
    static void (*function[])(struct xuni_t *xuni, struct widget_t *widget)
        = {
        
        free_image,
        0,
        paint_image,
        reposition_image,
        rescale_image
    };
    
    call_widget_event_func(xuni, widget, event, function,
        sizeof(function) / sizeof(*function));
}

void init_image(struct widget_t *widget, const char *filename, double angle,
    enum image_load_t load, enum image_rescale_t rescale) {
    
    widget->type = WIDGET_IMAGE;
    widget->p.image = xuni_memory_allocate(sizeof(*widget->p.image));
    
    xuni_memory_increment((void *)filename);
    widget->p.image->filename = filename;
    
    widget->p.image->original = 0;
    widget->p.image->image = 0;
    widget->p.image->angle = angle;
    widget->p.image->keep.load = load;
    widget->p.image->keep.rescale = rescale;
}

static void free_image(struct xuni_t *xuni, struct widget_t *widget) {
    xuni_memory_free((void *)widget->p.image->filename);
    
    if(xuni_memory_decrement(widget->p.image)) {
        free_surface(widget->p.image->original);
        
        free_surface(xuni_memory_decrement(widget->p.image->image));
        
        free(widget->p.image);
    }
}

static void paint_image(struct xuni_t *xuni, struct widget_t *widget) {
    int x, y;
    
    prepare_paint_image(xuni, widget);
    
    x = widget->pos->real.x;
    y = widget->pos->real.y;
    
    if(widget->pos->clip) {
        /*printf("%i %i\n", widget->pos->clip->xoff,
            widget->pos->clip->yoff);*/
        x += widget->pos->clip->xoff;
        y += widget->pos->clip->yoff;
    }
    
    blit_surface(xuni->smode->screen, widget->p.image->image, x, y);
}

static void reposition_image(struct xuni_t *xuni, struct widget_t *widget) {
    
}

static void rescale_image(struct xuni_t *xuni, struct widget_t *widget) {
    if(widget->p.image->keep.rescale == IMAGE_RESCALE_ALWAYS) {
        generate_image(xuni, widget);
    }
    else {
        free_surface(xuni_memory_decrement(widget->p.image->image));
        widget->p.image->image = 0;
    }
}

void prepare_paint_image(struct xuni_t *xuni, struct widget_t *widget) {
    if(!widget->p.image->image) generate_image(xuni, widget);
}

#if 1
static void generate_image(struct xuni_t *xuni, struct widget_t *widget) {
    SDL_Surface *use;
    
    if(!widget->p.image->original) {
        if(!widget->p.image->filename) {
            log_message(ERROR_TYPE_WARNING, 0, __FILE__, __LINE__,
                "NULL image of name \"%s\"", widget->name);
            return;
        }
        
        widget->p.image->original = load_image(widget->p.image->filename);
        if(!widget->p.image->original) return;
    }
    
    free_surface(xuni_memory_decrement(widget->p.image->image));
    
    /*widget->p.image->image = zoomSurface(widget->p.image->original,
        widget->pos->scale.w / 100.0
            * widget->p.image->screen->w / widget->p.image->original->w,
        widget->pos->scale.h / 100.0
            * widget->p.image->screen->h / widget->p.image->original->h,
        SMOOTHING_ON);*/
    
    use = widget->p.image->original;
    
    if(widget->p.image->angle) {
        use = rotozoomSurface(widget->p.image->original,
            radians_to_degrees(widget->p.image->angle), 1.0, SMOOTHING_ON);
    }
    
    widget->p.image->image = zoomSurface(use,
        widget->pos->scale.w / 100.0
            / ((double)xuni->smode->screen->w / widget->base->pos->real.w)
            * xuni->smode->screen->w / widget->p.image->original->w,
        widget->pos->scale.h / 100.0
            / ((double)xuni->smode->screen->h / widget->base->pos->real.h)
            * xuni->smode->screen->h / widget->p.image->original->h,
        SMOOTHING_ON);
    
    if(widget->p.image->angle) {
        SDL_FreeSurface(use);
    }
    
    /*if(widget->p.image->image && widget->p.image->angle) {
        SDL_Surface *temp = rotozoomSurface(widget->p.image->image,
            radians_to_degrees(widget->p.image->angle), 1.0, SMOOTHING_ON);
        SDL_FreeSurface(widget->p.image->image);
        widget->p.image->image = temp;
    }*/
    
    xuni_memory_add_block(widget->p.image->image,
        MEMORY_BLOCK_TYPE_SDL_SURFACE);
    
    /*printf("%i -> %i:\n",
        (int)allocated_sdl_surface(widget->p.image->original),
        (int)allocated_sdl_surface(widget->p.image->image));
    print_widget_backtrace(widget);*/
    
    /* Discard the original, unresized image if the filename of the image is
        known (i.e., the original image can be loaded again if required), and
        if that is how this image should behave.
    */
    if(widget->p.image->keep.load == IMAGE_LOAD_FREE
        && widget->p.image->filename) {
        
        SDL_FreeSurface(widget->p.image->original);  /* will never be NULL */
        widget->p.image->original = 0;
    }
}
#elif 1
static void rescale_image(struct xuni_t *xuni, struct widget_t *widget) {
    if(!widget->p.image->original) {
        if(!widget->p.image->filename) {
            log_message(ERROR_TYPE_WARNING, 0, __FILE__, __LINE__,
                "NULL image of name \"%s\"", widget->name);
            return;
        }
        
        widget->p.image->original = load_image(widget->p.image->filename);
        if(!widget->p.image->original) return;
    }
    
    free_surface(xuni_memory_decrement(widget->p.image->image));
    
    widget->p.image->image = sge_transform_surface(widget->p.image->original,
        SDL_MapRGB(xuni->smode->screen->format, 0, 0, 0),
        radians_to_degrees(widget->p.image->angle),
        widget->pos->scale.w / 100.0
            / ((double)xuni->smode->screen->w / widget->base->pos->real.w)
            * xuni->smode->screen->w / widget->p.image->original->w,
        widget->pos->scale.h / 100.0
            / ((double)xuni->smode->screen->h / widget->base->pos->real.h)
            * xuni->smode->screen->h / widget->p.image->original->h,
        0);
    
    xuni_memory_add_block(widget->p.image->image,
        MEMORY_BLOCK_TYPE_SDL_SURFACE);
    
    /*printf("%i -> %i:\n",
        (int)allocated_sdl_surface(widget->p.image->original),
        (int)allocated_sdl_surface(widget->p.image->image));
    print_widget_backtrace(widget);*/
    
    /* Discard the original, unresized image if the filename of the image is
        known (i.e., the original image can be loaded again if required). */
    if(widget->p.image->filename) {
        SDL_FreeSurface(widget->p.image->original);  /* will never be NULL */
        widget->p.image->original = 0;
    }
}
#elif 0
static void rescale_image(struct widget_t *widget) {
    free_surface(widget->p.image->image);
    
    widget->p.image->image
        = SDL_ResizeXY(load_image(widget->p.image->filename), 1, 1, 7);
    
    /*if(widget->p.image->angle) {
        SDL_Surface *temp = rotozoomSurface(widget->p.image->image,
            angle_to_degrees(widget->p.image->angle), 1.0, SMOOTHING_ON);
        SDL_FreeSurface(widget->p.image->image);
        widget->p.image->image = temp;
    }*/
    
    SDL_SaveBMP(widget->p.image->image, "bmp.bmp");
    
    widget->p.image->original = 0;
}
#else
static void rescale_image(struct widget_t *widget) {
    free_surface(widget->p.image->image);
    
    if(!widget->p.image->original) {
        widget->p.image->original = load_image(widget->p.image->filename);
    }
    
    printf("Attempting to resize \"%s\" from %ix%i to %fx%f\n",
        widget->p.image->filename,
        widget->p.image->original->w,
        widget->p.image->original->h,
        widget->pos->scale.w / 100.0 * widget->p.image->screen->w,
        widget->pos->scale.h / 100.0 * widget->p.image->screen->h);
        
    widget->p.image->image = SDL_ResizeXY(widget->p.image->original,
        widget->pos->scale.w / 100.0 * widget->p.image->screen->w,
        widget->pos->scale.h / 100.0 * widget->p.image->screen->h, 5);
    
    widget->p.image->original = 0;
}
#endif
