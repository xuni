/*! \file font.h

*/

#ifndef XUNI_GUARD_FONT_H
#define XUNI_GUARD_FONT_H

#include "widgets.h"

#include "SDL.h"
#include "SDL_ttf.h"

#ifdef __cplusplus
extern "C" {
#endif

struct font_t {
    const char *name;
    int point;
    
    /* private */
    TTF_Font *ttf;
};

void font_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event);

void init_font(struct widget_t *widget, const char *name);
struct widget_t *load_font(struct widget_t *widget, const char *name);
void use_font_by_name(struct xuni_t *xuni, size_t n,
    struct widget_t *widget);
double get_font_ratio(struct xuni_t *xuni);
int font_height(struct xuni_t *xuni, struct widget_t *widget);
int font_string_width(struct xuni_t *xuni, struct widget_t *widget,
    const char *str);
SDL_Surface *render_unrescaled_text(struct widget_t *widget, const char *text,
    Uint8 r, Uint8 g, Uint8 b);
SDL_Surface *render_text(struct xuni_t *xuni, struct widget_t *widget,
    const char *text, Uint8 r, Uint8 g, Uint8 b);

#ifdef __cplusplus
}
#endif

#endif
