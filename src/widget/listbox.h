/*! \file listbox.h

*/

#ifndef XUNI_GUARD_LISTBOX_H
#define XUNI_GUARD_LISTBOX_H

#include "widgets.h"
#include "scrollbar.h"

#ifdef __cplusplus
extern "C" {
#endif

enum wid_listbox_t {
    WID_LISTBOX_BOX,
    WID_LISTBOX_VSCROLL,
    WID_LISTBOX_HSCROLL,
    WID_LISTBOX_DATA
};

enum scrollbar_use_t {
    SCROLLBAR_USE_NONE       = 0,
    SCROLLBAR_USE_VERTICAL   = 1 << 0,
    SCROLLBAR_USE_HORIZONTAL = 1 << 1,
    SCROLLBAR_USE_BOTH       = (1 << 2) - 1
};

struct allow_scrollbar_t {
    enum scrollbar_use_t allow, force, current;
};

struct listbox_t {
    struct allow_scrollbar_t scroll;
    int maxwidth;
};

void listbox_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event);

double real_scale_width(struct widget_t *widget);
double real_scale_height(struct widget_t *widget);
double calculate_scrollbar_width(struct widget_t *widget,
    struct xuni_t *xuni);
double calculate_scrollbar_height(struct widget_t *widget,
    struct xuni_t *xuni);
void init_listbox(struct widget_t *widget, struct xuni_t *xuni,
    enum scrollbar_use_t allow, enum scrollbar_use_t force);
void add_listbox_item(struct xuni_t *xuni, struct widget_t *widget,
    size_t font, const char *data);
struct widget_t *listbox_sel_item(struct widget_t *widget);
void listbox_calculate_scrollbar_max(struct xuni_t *xuni,
    struct widget_t *widget);

#ifdef __cplusplus
}
#endif

#endif
