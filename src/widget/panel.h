/*! \file panel.h

*/

#ifndef XUNI_GUARD_PANEL_H
#define XUNI_GUARD_PANEL_H

#include "widgets.h"
#include "../resource/resource.h"

#ifdef __cplusplus
extern "C" {
#endif

struct panel_t {
    struct widget_t *base;
    struct widget_t **widget;
    size_t widgets;
};

void panel_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event);

void init_panel(struct widget_t *widget);
void init_panel_data(struct widget_t *widget);
void set_panel_data(struct widget_t *widget, void *vdata, int frameupdate);
void set_panel_callback(struct widget_t *widget, enum panel_event_type_t type,
    panel_event_func_t func);
void init_panel_from_resource(struct xuni_t *xuni, struct widget_t *widget,
    struct resource_list_t *list);
void free_panel(struct xuni_t *xuni, struct panel_t *panel);

#ifdef __cplusplus
}
#endif

#endif
