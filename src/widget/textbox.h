/*! \file textbox.h

*/

#ifndef XUNI_GUARD_TEXTBOX_H
#define XUNI_GUARD_TEXTBOX_H

#ifdef __cplusplus
extern "C" {
#endif

enum wid_textbox_t {
    WID_TEXTBOX_BOX,
    WID_TEXTBOX_LABEL
};

struct textbox_t {
    size_t font;
    /*struct edit_string_t data;*/
    /*int editable;*/
    int leftpos;
};

void textbox_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event);

void init_textbox(struct widget_t *widget, struct xuni_t *xuni,
    const char *text, size_t font);

int textbox_is_empty(struct widget_t *widget);
const char *get_textbox_data(struct widget_t *widget);
void clear_textbox_data(struct xuni_t *xuni, struct widget_t *widget);
void set_textbox_data(struct xuni_t *xuni, struct widget_t *widget,
    const char *text);

#ifdef __cplusplus
}
#endif

#endif
