/*! \file theme.h

*/

#ifndef XUNI_GUARD_THEME_H
#define XUNI_GUARD_THEME_H

#include "widgets.h"

#ifdef __cplusplus
extern "C" {
#endif

enum wid_theme_t {
    THEME_FONT_MONO,
    THEME_FONT_SANS,
    THEME_DEFAULT_CURSOR,
    THEME_CHECKBOX,
    THEME_CORNERS_IN_NORMAL,
    THEME_CORNERS_IN_HOVER,
    THEME_CORNERS_IN_ACTIVE,
    THEME_CORNERS_OUT_NORMAL,
    THEME_CORNERS_OUT_HOVER,
    THEME_CORNERS_OUT_ACTIVE,
    THEME_SCROLL_UP,
    THEME_SCROLL_DOWN,
    THEME_SCROLL_LEFT,
    THEME_SCROLL_RIGHT,
    THEME_WIDGETS
};

struct theme_data_t {
    double width, height;
    
    struct nameid_t *nameid;
};

void theme_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event);

double get_box_width(struct xuni_t *xuni, struct widget_t *widget);
double get_box_height(struct xuni_t *xuni, struct widget_t *widget);
void init_theme(struct widget_t *widget, double width, double height);
void build_theme(struct xuni_t *xuni, struct widget_t *widget,
    double width, double height,
    const char *font_mono, const char *font_sans, const char *default_cursor,
    const char *checkbox, const char *corners_in_normal,
    const char *corners_in_hover, const char *corners_in_active,
    const char *corners_out_normal, const char *corners_out_hover,
    const char *corners_out_active, const char *scroll_up,
    const char *scroll_down);
void use_theme_roundness(struct xuni_t *xuni, struct widget_t *theme,
    double roundness);
void set_theme_nameid(struct widget_t *widget);
void use_theme(struct xuni_t *xuni, struct widget_t *current);
int use_theme_by_name(struct xuni_t *xuni, const char *name);
struct widget_t *get_theme_widget(struct xuni_t *xuni, size_t id);
struct widget_t *get_any_theme_widget(struct xuni_t *xuni,
    struct widget_t *theme, size_t id);

#ifdef __cplusplus
}
#endif

#endif
