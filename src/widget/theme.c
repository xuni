/*! \file theme.c

*/

#include <string.h>

#include "../graphics.h"
#include "../memory.h"
#include "../error.h"
#include "dump.h"
#include "widgets.h"
#include "panel.h"
#include "theme.h"

static void free_theme_data(struct xuni_t *xuni, struct widget_t *widget);
static void rescale_theme(struct xuni_t *xuni, struct widget_t *widget);

static void build_theme_image(struct xuni_t *xuni, struct widget_t *widget,
    const char *name, const char *path);

void theme_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event) {
    
    static void (*function[])(struct xuni_t *xuni, struct widget_t *widget)
        = {
        
        free_theme_data,
        0,
        0,
        0,
        rescale_theme
    };
    
    call_widget_event_func(xuni, widget, event, function,
        sizeof(function) / sizeof(*function));
}

double get_box_width(struct xuni_t *xuni, struct widget_t *widget) {
    if(!widget) return 0.0;
    
    if(widget->type != WIDGET_THEME) {
        log_message(ERROR_TYPE_DATASTRUCT, 0, __FILE__, __LINE__,
            "get_box_width() expected a theme widget, got a %s",
            get_widget_type_name(xuni, widget->type));
        
        return 100.0 / 60.0;
    }
    
    return widget->p.theme->width;
}

double get_box_height(struct xuni_t *xuni, struct widget_t *widget) {
    if(!widget) return 0.0;
    
    if(widget->type != WIDGET_THEME) {
        log_message(ERROR_TYPE_DATASTRUCT, 0, __FILE__, __LINE__,
            "get_box_height() expected a theme widget, got a %s",
            get_widget_type_name(xuni, widget->type));
        
        return 100.0 / 60.0;
    }
    
    return widget->p.theme->height;
}

void init_theme(struct widget_t *widget, double width, double height) {
    widget->type = WIDGET_THEME;
    widget->p.theme = xuni_memory_allocate(sizeof(*widget->p.theme));
    
    widget->p.theme->width = width;
    widget->p.theme->height = height;
    
    widget->p.theme->nameid = 0;
}

static void build_theme_image(struct xuni_t *xuni, struct widget_t *widget,
    const char *name, const char *path) {
    
    const char *p;
    
    if(!path) return;
    
    p = xuni_memory_duplicate_string(path);
    
    add_allocate_widget(widget, (char *)name);
    init_widget_pos(last_compose_widget(widget), 0, 0, 0, 0, POS_PACK_NONE);
    init_image(last_compose_widget(widget), p, 0,
        IMAGE_LOAD_FREE, IMAGE_RESCALE_LAZY);
    
    xuni_memory_decrement((void *)p);
}

void build_theme(struct xuni_t *xuni, struct widget_t *widget,
    double width, double height,
    const char *font_mono, const char *font_sans, const char *default_cursor,
    const char *checkbox, const char *corners_in_normal,
    const char *corners_in_hover, const char *corners_in_active,
    const char *corners_out_normal, const char *corners_out_hover,
    const char *corners_out_active, const char *scroll_up,
    const char *scroll_down) {
    
    init_theme(widget, width, height);
    
    add_allocate_widget_compose(widget, "mono");
    init_widget_pos(last_compose_widget(widget), 0, 0, 0, 0, POS_PACK_NONE);
    init_font(last_compose_widget(widget), font_mono);
    
    add_allocate_widget(widget, "sans");
    init_widget_pos(last_compose_widget(widget), 0, 0, 0, 0, POS_PACK_NONE);
    init_font(last_compose_widget(widget), font_sans);
    
    build_theme_image(xuni, widget, "default cursor", default_cursor);
    build_theme_image(xuni, widget, "check", checkbox);
    build_theme_image(xuni, widget, "corners-in-normal", corners_in_normal);
    build_theme_image(xuni, widget, "corners-in-hover", corners_in_hover);
    build_theme_image(xuni, widget, "corners-in-active", corners_in_active);
    build_theme_image(xuni, widget, "corners-out-normal", corners_out_normal);
    build_theme_image(xuni, widget, "corners-out-hover", corners_out_hover);
    build_theme_image(xuni, widget, "corners-out-active", corners_out_active);
    build_theme_image(xuni, widget, "scroll-up", scroll_up);
    build_theme_image(xuni, widget, "scroll-down", scroll_down);
    
    set_theme_nameid(widget);
    
    widget_event(xuni, widget, WIDGET_EVENT_RESCALE);
}

void use_theme_roundness(struct xuni_t *xuni, struct widget_t *theme,
    double roundness) {
    
    theme->p.theme->width = theme->p.theme->height = roundness;
    
    widget_event(xuni, xuni->theme->current, WIDGET_EVENT_RESCALE);
}

static void free_theme_data(struct xuni_t *xuni, struct widget_t *widget) {
    if(widget->p.theme->nameid) {
        xuni_memory_free(widget->p.theme->nameid->widget);
        xuni_memory_free(widget->p.theme->nameid);
    }
    
    xuni_memory_free(widget->p.theme);
}

static void rescale_theme(struct xuni_t *xuni, struct widget_t *widget) {
    double width = get_box_width(xuni, widget);
    double height = get_box_height(xuni, widget);
    struct widget_t *box;
    enum wid_theme_t x;
    
    if(widget->compose) {
        for(x = THEME_CORNERS_IN_NORMAL; x <= THEME_CORNERS_OUT_ACTIVE;
            x ++) {
            
            box = widget_nameid_access(widget, x);
            if(box) {
                box->pos->scale.w = width * 2;
                box->pos->scale.h = height * 2;
                
                widget_event(xuni, box, WIDGET_EVENT_RESCALE);
            }
        }
    }
}

/* !!! */
struct nameid_t *allocate_nameid(void);
void init_wid_possible_zero(struct widget_t *base, struct nameid_t *nameid,
    size_t id, const char *name);

void set_theme_nameid(struct widget_t *widget) {
    struct nameid_t *nameid;
    
    if(!widget) return;
    
    nameid = allocate_nameid();
    
    init_wid_possible_zero(widget, nameid, THEME_FONT_MONO,
        "font-mono");
    init_wid_possible_zero(widget, nameid, THEME_FONT_SANS,
        "font-sans");
    init_wid_possible_zero(widget, nameid, THEME_DEFAULT_CURSOR,
        "default cursor");
    init_wid_possible_zero(widget, nameid, THEME_CHECKBOX, "check");
    init_wid_possible_zero(widget, nameid, THEME_CORNERS_IN_NORMAL,
        "corners-in-normal");
    init_wid_possible_zero(widget, nameid, THEME_CORNERS_IN_HOVER,
        "corners-in-hover");
    init_wid_possible_zero(widget, nameid, THEME_CORNERS_IN_ACTIVE,
        "corners-in-active");
    init_wid_possible_zero(widget, nameid, THEME_CORNERS_OUT_NORMAL,
        "corners-out-normal");
    init_wid_possible_zero(widget, nameid, THEME_CORNERS_OUT_HOVER,
        "corners-out-hover");
    init_wid_possible_zero(widget, nameid, THEME_CORNERS_OUT_ACTIVE,
        "corners-out-active");
    init_wid_possible_zero(widget, nameid, THEME_SCROLL_UP, "scroll-up");
    init_wid_possible_zero(widget, nameid, THEME_SCROLL_DOWN, "scroll-down");
    init_wid_possible_zero(widget, nameid, THEME_SCROLL_LEFT, "scroll-left");
    init_wid_possible_zero(widget, nameid, THEME_SCROLL_RIGHT,
        "scroll-right");
    
    widget->p.theme->nameid = nameid;
}

/*! Sets the current theme to \a current. Also rescales the new theme in case
    it has not been used before.
    
    \param xuni A pointer to the main xuni structure.
    \param current The theme to start using, which is automatically rescaled.
*/
void use_theme(struct xuni_t *xuni, struct widget_t *current) {
    xuni->theme->current = current;
    rescale_theme(xuni, current);
}

int use_theme_by_name(struct xuni_t *xuni, const char *name) {
    struct widget_t *theme = find_widget(xuni->gui->widget, name);
    
    if(theme && theme->type == WIDGET_THEME) {
        struct widget_t *oldf1 = get_theme_widget(xuni, THEME_FONT_MONO);
        struct widget_t *oldf2 = get_theme_widget(xuni, THEME_FONT_SANS);
        
        use_theme(xuni, theme);
        
        /* Rescale the widget tree if the themes have different fonts. */
        if((!oldf1 || !oldf2) || oldf1->p.font->ttf
            != get_theme_widget(xuni, THEME_FONT_MONO)->p.font->ttf
            || oldf2->p.font->ttf
            != get_theme_widget(xuni, THEME_FONT_SANS)->p.font->ttf) {
            
            widget_event(xuni, xuni->gui->widget, WIDGET_EVENT_UPDATE_TEXT);
        }
        
        return 0;
    }
    else {
        printf("*** Couldn't load theme \"%s\"\n", name);
        
        return 1;
    }
}

struct widget_t *get_theme_widget(struct xuni_t *xuni, size_t id) {
    return get_any_theme_widget(xuni, xuni->theme->current, id);
}

struct widget_t *get_any_theme_widget(struct xuni_t *xuni,
    struct widget_t *theme, size_t id) {
    
    struct widget_t *widget = widget_nameid_access(theme, id);
    
    if(widget && widget->type == WIDGET_IMAGE && !widget->p.image->image) {
        prepare_paint_image(xuni, widget);
    }
    
    return widget;
}
