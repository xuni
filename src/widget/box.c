/*! \file box.c

*/

#include "../graphics.h"
#include "../memory.h"
#include "widgets.h"
#include "box.h"
#include "image.h"

/*enum box_image_type_t {
    BOX_IN_NORMAL,
    BOX_IN_HOVER,
    BOX_IN_ACTIVE,
    BOX_OUT_NORMAL,
    BOX_OUT_HOVER,
    BOX_OUT_ACTIVE
};*/

static void free_box(struct xuni_t *xuni, struct widget_t *widget);
static void paint_box(struct xuni_t *xuni, struct widget_t *widget);
static void reposition_box(struct xuni_t *xuni, struct widget_t *widget);
static void rescale_box(struct xuni_t *xuni, struct widget_t *widget);

static void paint_box_image(SDL_Surface *screen, SDL_Surface *image,
    SDL_Rect *b);
static SDL_Surface *get_box_image(struct xuni_t *xuni, struct widget_t *theme,
    struct box_t *box);

/*! Calls the box function appropriate for handling \a event.
    
    Uses call_widget_event_func() to call the appropriate function from a
    lookup table of handler functions.
    
    \param xuni The xuni structure. Passed on to call_widget_event_func()
        and ultimately the handler functions.
    \param widget The box widget being operated on. Also passed on.
    \param event The event being sent to the box widget \a widget.
*/
void box_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event) {
    
    static void (*function[])(struct xuni_t *xuni, struct widget_t *widget)
        = {
        
        free_box,
        0,
        paint_box,
        reposition_box,
        rescale_box
    };
    
    call_widget_event_func(xuni, widget, event, function,
        sizeof(function) / sizeof(*function));
}

void init_box(struct widget_t *widget, enum box_style_t style,
    struct widget_t *selwidget) {
    
    widget->type = WIDGET_BOX;
    widget->p.box = xuni_memory_allocate(sizeof(*widget->p.box));
    
    widget->p.box->style = style;
    
    widget->p.box->state = 0;
    
    widget->selwidget = selwidget;
}

void set_box_type(struct gui_t *gui, struct widget_t *widget,
    struct widget_t *box) {
    
    int xp, yp;
    
    box->p.box->state = 0;
    
    /*if(box->visibility & WIDGET_VISIBILITY_CLICKABLE == 0) {
        printf("Unselable box: \"%s\"\n", widget->name);
    }*/
    
    /* !!! */
    if((gui->sel.p.widget == widget || gui->active.widget == widget
        || box->sel) && widget->visibility & WIDGET_VISIBILITY_CLICKABLE) {
        
        SDL_GetMouseState(&xp, &yp);
        
        if(pos_in_rect(xp, yp, widget->pos)) {
            box->p.box->state |= BOX_STATE_HOVER;
        }
        
        if(gui->active.widget == widget || gui->active.widget == box
            || gui->sel.clickin) {
            
            box->p.box->state |= BOX_STATE_ACTIVE;
        }
    }
}

static void paint_box_image(SDL_Surface *screen, SDL_Surface *image,
    SDL_Rect *b) {
    
    int cw, ch;
    int boxw, boxh;
    
    if(!image) return;
    
    boxw = image->w / 2;
    boxh = image->h / 2;
    
    cw = b->w - boxw * 2;
    ch = b->h - boxh * 2;
    
    if(image) {
        /* !!! there must be a better way to calculate the dimensions of the
            component images of the box */
        if(image->w % 2) cw --;
        if(image->h % 2) ch --;
        
        if(cw < 0) cw = 0;
        if(ch < 0) ch = 0;
        
        /* upper left */
        blit_surface_area(screen, image,
            b->x, b->y,
            0, 0, boxw, boxh);
        
        /* lower left */
        blit_surface_area(screen, image,
            b->x, b->y + boxh + ch,
            0, boxh, boxw, boxh);
        
        /* upper right */
        blit_surface_area(screen, image,
            b->x + boxw + cw, b->y,
            boxw, 0, boxw, boxh);
        
        /* lower right */
        blit_surface_area(screen, image,
            b->x + boxw + cw, b->y + boxh + ch,
            boxw, boxh, boxw, boxh);
        
        /* top */
        blit_surface_repeat_area(screen, image,
            b->x + boxw, b->y, cw, boxh,
            boxw, 0, 1, boxh);
        
        /* bottom */
        blit_surface_repeat_area(screen, image,
            b->x + boxw, b->y + boxh + ch,
            cw, boxh, boxw, boxh,
            1, boxh);
        
        /* left */
        blit_surface_repeat_area(screen, image,
            b->x, b->y + boxh, boxw, ch,
            0, boxh, boxw, 1);
        
        /* right */
        blit_surface_repeat_area(screen, image,
            b->x + boxw + cw, b->y + boxh,
            boxw, ch, boxw, boxh,
            boxw, 1);
        
        /*printf("(%5i %5i %5i %5i) with (%5i %5i) and box (%5i %5i)\n",
            b->x + boxw, b->y + boxh, cw, ch,
            image->w / 2,
            image->h / 2,
            boxw, boxh);*/
        
        /* middle -- calls SDL_FillRect(), if possible, which is very fast */
        fill_area(screen, b->x + boxw, b->y + boxh, cw, ch,
            image, image->w / 2, image->h / 2);
    }
}

static SDL_Surface *get_box_image(struct xuni_t *xuni, struct widget_t *theme,
    struct box_t *box) {
    
    enum wid_theme_t image = THEME_CORNERS_OUT_NORMAL;
    struct widget_t *found;
    
    if(box->style == BOX_STYLE_NORMAL) {
        if(box->state == 0) image = THEME_CORNERS_OUT_NORMAL;
        else if(box->state == BOX_STATE_HOVER) {
            image = THEME_CORNERS_OUT_HOVER;
        }
        else if(box->state == BOX_STATE_ACTIVE) {
            image = THEME_CORNERS_OUT_ACTIVE;
        }
        else if(box->state == (BOX_STATE_HOVER | BOX_STATE_ACTIVE)) {
            image = THEME_CORNERS_IN_ACTIVE;
        }
    }
    else if(box->style == BOX_STYLE_INVERTED) {
        if(box->state == 0) image = THEME_CORNERS_IN_NORMAL;
        else if(box->state == BOX_STATE_HOVER) {
            image = THEME_CORNERS_IN_HOVER;
        }
        else if(box->state == BOX_STATE_ACTIVE) {
            image = THEME_CORNERS_IN_ACTIVE;
        }
        else if(box->state == (BOX_STATE_HOVER | BOX_STATE_ACTIVE)) {
            image = THEME_CORNERS_OUT_ACTIVE;
        }
    }
    else if(box->style == BOX_STYLE_ALWAYSIN) {
        if(box->state == 0) image = THEME_CORNERS_IN_NORMAL;
        else if(box->state == BOX_STATE_HOVER) image = THEME_CORNERS_IN_HOVER;
        else image = THEME_CORNERS_IN_ACTIVE;
    }
    else {  /* ALWAYSOUT */
        image = THEME_CORNERS_OUT_NORMAL;
    }
    
    found = get_any_theme_widget(xuni, theme, (size_t)image);
    return found ? found->p.image->image : 0;
}

void paint_box_previous_state(struct xuni_t *xuni, struct widget_t *widget) {
#if 0
    if(widget->base->id == xuni->gui->tab.sel
        && widget->base->base->type == WIDGET_PANEL
        && widget->base->base->id == xuni->gui->tab.panel) {
        
        struct widget_t *box = widget_nameid_access(xuni->theme->current,
            THEME_CORNERS_IN_HOVER);
        
        if(box && box->p.image->image) {
            paint_box_image(xuni->smode->screen,
                box->p.image->image,
                xuni->theme->current, &widget->pos->real);
            return;
        }
    }
#endif
    
    paint_box_image(xuni->smode->screen,
        get_box_image(xuni, xuni->theme->current, widget->p.box),
            &widget->pos->real);
}

static void free_box(struct xuni_t *xuni, struct widget_t *widget) {
    xuni_memory_free(widget->p.box);
}

static void paint_box(struct xuni_t *xuni, struct widget_t *widget) {
    /*if(widget->base->base && widget->base->base->type == WIDGET_CHECKBOX) {
        set_box_type(xuni->gui, widget->base->base, widget);
    }
    else {
        set_box_type(xuni->gui, widget, widget);
    }*/
    
    set_box_type(xuni->gui, widget->selwidget ? widget->selwidget : widget,
        widget);
    
    paint_box_previous_state(xuni, widget);
}

static void reposition_box(struct xuni_t *xuni, struct widget_t *widget) {

}

static void rescale_box(struct xuni_t *xuni, struct widget_t *widget) {
    
}
