/*! \file textbox.c
    
    Implements textbox widgets. Some textbox-specific code is sprinkled
    elsewhere at the moment too, such as in label.c and in functions in gui.c
    like activate_widget() and revert_widget().
*/

#include "SDL_gfxPrimitives.h"  /* for vlineRGBA() */

#include "../graphics.h"
#include "../memory.h"
#include "font.h"
#include "widgets.h"
#include "textbox.h"
#include "box.h"
#include "label.h"

static void free_textbox(struct xuni_t *xuni, struct widget_t *widget);
static void reposition_textbox(struct xuni_t *xuni, struct widget_t *widget);
static void rescale_textbox(struct xuni_t *xuni, struct widget_t *widget);
static void paint_textbox(struct xuni_t *xuni, struct widget_t *widget);

void textbox_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event) {
    
    static void (*function[])(struct xuni_t *xuni, struct widget_t *widget)
        = {
        
        free_textbox,
        0,
        paint_textbox,
        reposition_textbox,
        rescale_textbox
    };
    
    call_widget_event_func(xuni, widget, event, function,
        sizeof(function) / sizeof(*function));
}

void init_textbox(struct widget_t *widget, struct xuni_t *xuni,
    const char *text, size_t font) {
    
    widget->type = WIDGET_TEXTBOX;
    widget->p.textbox = xuni_memory_allocate(sizeof(*widget->p.textbox));
    
    widget->p.textbox->leftpos = 0;
    
    add_allocate_widget_compose(widget, "alwaysinbox");
    
    init_widget_pos(last_compose_widget(widget), 0, 0, 100.0, 100.0,
        POS_PACK_NONE);
    init_box(last_compose_widget(widget), BOX_STYLE_ALWAYSIN, 0);
    /*last_compose_widget(widget)->visibility &= ~WIDGET_VISIBILITY_SELABLE;*/
    last_compose_widget(widget)->visibility &= ~WIDGET_VISIBILITY_INDEPENDENT;
    
    add_allocate_widget_compose(widget, "text");
    
    init_widget_pos(last_compose_widget(widget), 0, 0, 100.0, 100.0,
        POS_PACK_NONE);
    init_label(last_compose_widget(widget), font, text, LABEL_ALIGN_LEFT,
        255, 255, 255);
    /* !!! why does reposition_widget() not work? */
    /* !!! why was this here? */
    /*widget_event(xuni, widget, WIDGET_EVENT_RESCALE);*/
    last_compose_widget(widget)->visibility &= ~WIDGET_VISIBILITY_INDEPENDENT;
    
    widget->visibility &= ~WIDGET_VISIBILITY_NOT_COMPOSE;
}

static void free_textbox(struct xuni_t *xuni, struct widget_t *widget) {
    xuni_memory_free(widget->p.textbox);
}

static void paint_textbox(struct xuni_t *xuni, struct widget_t *widget) {
    set_box_type(xuni->gui, widget,
        widget->compose->widget[WID_TEXTBOX_BOX]);
    paint_box_previous_state(xuni,
        widget->compose->widget[WID_TEXTBOX_BOX]);
    
    /*widget_event(xuni, widget->compose->widget[WID_TEXTBOX_BOX],
        WIDGET_EVENT_PAINT);*/
    
    widget_event(xuni, widget->compose->widget[WID_TEXTBOX_LABEL],
        WIDGET_EVENT_PAINT);
    
    /*paint_textbox_text(xuni->smode->screen, xuni->font, xuni->gui,
        widget->p.textbox->data.scroll.text,
        &widget->pos->real,
        xuni->gui->active.widget == widget->compose->widget[0]);*/
}

static void reposition_textbox(struct xuni_t *xuni, struct widget_t *widget) {
    
}

static void rescale_textbox(struct xuni_t *xuni, struct widget_t *widget) {
    /*struct widget_t *label = widget->compose->widget[WID_TEXTBOX_LABEL];
    
    if(label->p.label && label->p.label->label) {
        if(label->p.label->label->w > widget->pos->real.w) {
            int offset = label->p.label->label->w - widget->pos->real.w;
            
            set_widget_clip(label, 0, 0, offset, 0,
                label->p.label->label->w - offset, label->p.label->label->h);
        }
    }*/
}

int textbox_is_empty(struct widget_t *widget) {
    const char *data = get_textbox_data(widget);
    
    return !data || !*data;
}

const char *get_textbox_data(struct widget_t *widget) {
    return widget->compose->widget[WID_TEXTBOX_LABEL]->p.label->text;
}

/*! Clears the data in the textbox \a widget. Does not affect the textbox is
    the textbox is already empty.
    
    \param xuni A pointer to the main xuni structure.
    \param widget The textbox widget to clear the data of.
*/
void clear_textbox_data(struct xuni_t *xuni, struct widget_t *widget) {
    struct widget_t *label = widget->compose->widget[WID_TEXTBOX_LABEL];
    
    if(label->p.label->text && *label->p.label->text) {
        xuni_memory_free((void *)label->p.label->text);
        label->p.label->text = 0;
        
        widget_event(xuni, widget, WIDGET_EVENT_RESCALE);
    }
}

/*! Sets the data of the textbox \a widget. Automatically rescales the textbox
    after setting the data.
    
    \param xuni A pointer to the main xuni structure.
    \param widget The textbox to set the data of.
    \param text The text to set the textbox data to.
*/
void set_textbox_data(struct xuni_t *xuni, struct widget_t *widget,
    const char *text) {
    
    struct widget_t *label = widget->compose->widget[WID_TEXTBOX_LABEL];
    
    xuni_memory_free((void *)label->p.label->text);
    label->p.label->text = text;
    
    widget_event(xuni, widget, WIDGET_EVENT_RESCALE);
}
