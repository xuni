/*! \file textarea.c

*/

#include "../graphics.h"
#include "../error.h"
#include "../memory.h"
#include "widgets.h"
#include "textarea.h"
#include "listbox.h"
#include "scrollbar.h"

static void free_textarea(struct xuni_t *xuni, struct widget_t *widget);
static void paint_textarea(struct xuni_t *xuni, struct widget_t *widget);
static void rescale_textarea(struct xuni_t *xuni, struct widget_t *widget);

void textarea_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event) {
    
    static void (*function[])(struct xuni_t *xuni, struct widget_t *widget)
        = {
        
        free_textarea,
        0,
        paint_textarea,
        0,
        rescale_textarea
    };
    
    call_widget_event_func(xuni, widget, event, function,
        sizeof(function) / sizeof(*function));
}

void init_textarea(struct widget_t *widget, struct xuni_t *xuni) {
    double width = calculate_scrollbar_width(widget, xuni);
    double height = calculate_scrollbar_height(widget, xuni);
    double boxsize = get_box_width(xuni, xuni->theme->current) * 2;
    double boxsizeh = get_box_height(xuni, xuni->theme->current) * 2;
    
    widget->type = WIDGET_TEXTAREA;
    widget->p.textarea = xuni_memory_allocate(sizeof(*widget->p.textarea));
    
    add_allocate_widget_compose(widget, "textarea area");
    
    init_widget_pos(last_compose_widget(widget), 0, 0,
        100.0 - width, 100.0 - height, POS_PACK_NONE);
    init_box(last_compose_widget(widget), BOX_STYLE_ALWAYSIN, 0);
    last_compose_widget(widget)->visibility &= ~WIDGET_VISIBILITY_INDEPENDENT;
    
    add_allocate_widget_compose(widget, "textarea vertical scrollbar");
    
    init_widget_pos(last_compose_widget(widget),
        100.0 - width, 0, width, 100.0 - height, POS_PACK_NONE);
    init_scrollbar(last_compose_widget(widget), xuni, widget->pos->scale.h,
        SCROLLBAR_ORIENTATION_VERTICAL);
    
    add_allocate_widget_compose(widget, "textarea horizontal scrollbar");
    
    init_widget_pos(last_compose_widget(widget),
        0, 100.0 - height, 100.0 - width, height, POS_PACK_NONE);
    init_scrollbar(last_compose_widget(widget), xuni, widget->pos->scale.h,
        SCROLLBAR_ORIENTATION_HORIZONTAL);
    
    add_allocate_widget_compose(widget, "textarea data");
    
    init_widget_pos(last_compose_widget(widget), boxsize, boxsizeh,
        100.0 - width - boxsize * 2,
        100.0 - height - boxsizeh * 2, POS_PACK_NONE);
    init_panel(last_compose_widget(widget));
    last_compose_widget(widget)->visibility &= ~WIDGET_VISIBILITY_INDEPENDENT;
    
    last_compose_widget(widget)->visibility &= ~WIDGET_VISIBILITY_NOT_COMPOSE;
    
    widget->visibility &= ~WIDGET_VISIBILITY_NOT_COMPOSE;
}

static void free_textarea(struct xuni_t *xuni, struct widget_t *widget) {
    xuni_memory_free((void *)widget->p.textarea);
}

static void paint_textarea(struct xuni_t *xuni, struct widget_t *widget) {
    /*size_t x, start = 3;*/
    int height, width;
    int yclip;
    
    widget_event(xuni, widget->compose->widget[WID_TEXTAREA_BOX],
        WIDGET_EVENT_PAINT);
    
    height = get_box_height(xuni, xuni->theme->current) / 100.0
        * xuni->smode->height;
    width = get_box_width(xuni, xuni->theme->current) / 100.0
        * xuni->smode->width;
    
    yclip = get_scrollbar_pos_int(
        widget->compose->widget[WID_TEXTAREA_VSCROLL]);
    
    widget_event(xuni, widget->compose->widget[WID_TEXTAREA_DATA],
        WIDGET_EVENT_PAINT);
    
    widget_event(xuni, widget->compose->widget[WID_TEXTAREA_VSCROLL],
        WIDGET_EVENT_PAINT);
}

static void rescale_textarea(struct xuni_t *xuni, struct widget_t *widget) {
    
}
