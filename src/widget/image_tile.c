/*! \file image_tile.c

*/

#include <stdlib.h>

#include "../graphics.h"
#include "../memory.h"
#include "../error.h"
#include "../utility.h"
#include "widgets.h"
#include "image_tile.h"

static void free_image_tile(struct xuni_t *xuni, struct widget_t *widget);
static void paint_image_tile(struct xuni_t *xuni, struct widget_t *widget);

void image_tile_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event) {
    
    static void (*function[])(struct xuni_t *xuni, struct widget_t *widget)
        = {
        
        free_image_tile,
        0,
        paint_image_tile,
        0,
        0
    };
    
    call_widget_event_func(xuni, widget, event, function,
        sizeof(function) / sizeof(*function));
}

void init_image_tile(struct widget_t *widget, const char *filename,
    int xinc, int yinc) {
    
    widget->type = WIDGET_IMAGE_TILE;
    widget->p.image_tile = xuni_memory_allocate(sizeof(*widget->p.image));
    
    widget->p.image_tile->xinc = xinc;
    widget->p.image_tile->yinc = yinc;
    widget->p.image_tile->xpos = 0;
    widget->p.image_tile->ypos = 0;
    
    add_allocate_widget_compose(widget, "image");
    
    init_widget_pos(last_compose_widget(widget), 0, 0, 100.0, 100.0,
        POS_PACK_NONE);
    init_image(last_compose_widget(widget), filename, 0.0,
        IMAGE_LOAD_FREE, IMAGE_RESCALE_LAZY);
    last_compose_widget(widget)->visibility &= ~WIDGET_VISIBILITY_INDEPENDENT;
    
    widget->visibility &= ~WIDGET_VISIBILITY_NOT_COMPOSE;
}

static void free_image_tile(struct xuni_t *xuni, struct widget_t *widget) {
    xuni_memory_free(widget->p.image_tile);
}

static void paint_image_tile(struct xuni_t *xuni, struct widget_t *widget) {
#if !1
    clear_widget_clip(xuni, widget->compose->widget[WID_IMAGE_TILE_IMAGE]);
    
    add_widget_clip(xuni, widget->compose->widget[WID_IMAGE_TILE_IMAGE],
        widget->p.image_tile->xpos, widget->p.image_tile->ypos, 0, 0, 0, 0);
#else
    prepare_paint_image(xuni, widget->compose->widget[WID_IMAGE_TILE_IMAGE]);
    
    blit_surface_fill_from(xuni->smode->screen,
        widget->compose->widget[WID_IMAGE_TILE_IMAGE]->p.image->image,
        widget->pos->real.x, widget->pos->real.y,
        widget->pos->real.w, widget->pos->real.h,
        widget->p.image_tile->xpos, widget->p.image_tile->ypos);
#endif
    
    widget->p.image_tile->xpos += widget->p.image_tile->xinc;
    wrap_int(&widget->p.image_tile->xpos, widget->pos->real.w);
    
    widget->p.image_tile->ypos += widget->p.image_tile->yinc;
    wrap_int(&widget->p.image_tile->ypos, widget->pos->real.h);
}
