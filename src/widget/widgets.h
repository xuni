/*! \file widgets.h

*/

#ifndef XUNI_GUARD_WIDGETS_H
#define XUNI_GUARD_WIDGETS_H

#ifdef __cplusplus
extern "C" {
#endif

/*! The type of a widget.
    
    The order of these enumerated values should not matter.
*/
enum widget_type_t {
    WIDGET_NONE = -1,
    WIDGET_BOX,
    WIDGET_BUTTON,
    WIDGET_CHECKBOX,
    WIDGET_COMBOBOX,
    WIDGET_FONT,
    WIDGET_IMAGE,
    WIDGET_IMAGE_TILE,
    WIDGET_LABEL,
    WIDGET_LISTBOX,
    WIDGET_PANEL,
    WIDGET_SCROLLBAR,
    WIDGET_TEXTAREA,
    WIDGET_TEXTBOX,
    WIDGET_THEME,
    WIDGET_LAST
    
#if !1
    /* ideas */
    WIDGET_PULLDOWN,
    WIDGET_MENU,
    WIDGET_TAB,
    WIDGET_GROUP,
    WIDGET_RADIOBUTTON
#endif
};

enum widget_visibility_t {
    /*! If unset, the widget cannot be selected -- that is, when sel members
        are being set, this widget is simply passed over. Parents and children
        of this widget are unaffected by this flag.
    */
    WIDGET_VISIBILITY_SELABLE = 1 << 0,
    
    /*! If unset, when the widget is selected, its parent widget is considered
        for selection. That is, the parent could have its sel member set as
        well. This original widget still gets assigned to the sel->p.widget
        pointer, however.
    */
    WIDGET_VISIBILITY_INDEPENDENT = 1 << 1,
    
    /*! If unset, when the widget is selected, all of its children are
        considered for selection (rather than just the last widget painted).
        
        This is the inverse of WIDGET_VISIBILITY_INDEPENDENT.
    */
    WIDGET_VISIBILITY_NOT_COMPOSE = 1 << 2,
    
    /*! If unset, the widget is always in "normal" mode; the widget does
        not respond to hovering or clicking.
    */
    WIDGET_VISIBILITY_CLICKABLE = 1 << 3,
    
    /*! If unset, the widget is completely invisible. */
    WIDGET_VISIBILITY_VISIBLE = 1 << 4,
    
    /*! All visibility types enabled (default). */
    WIDGET_VISIBILITY_ALL = (1 << 5) - 1
};

typedef size_t widget_id_type_t;

/*! A nameid, a way to efficiently refer to a widget.
    
    When widgets are created from resource files, their names are in strings.
    However, referencing widgets by strings is inefficient. By "binding" a
    number to each widget that needs referencing, much more efficient
    referencing can be achieved.
    
    These numbers are called "wids", short for "widget identifiers". They are
    also frequently referred to as "nameids", or even just "ids".
    
    These wids which are bound to widgets are specific to each panel. That is,
    each panel can have its own wid 0, for example. wids are usually specified
    as enums so that they increment automatically. However, wids need not be
    contiguous. (Though if they are not, memory would be wasted.)
    
    widget_nameid_access(), or, for multiple nested nameids,
    widget_nameid_follow(), can be used to access these numerical widget ids.
*/
struct widget_nameid_t {
    widget_id_type_t id;  /* mostly redundant */
    const char *name;  /* mostly redundant */
    struct widget_t *widget;
};

/*! An array of struct widget_nameid_ts. Used by panels via
    struct panel_data_t.
*/
struct nameid_t {
    struct widget_nameid_t *widget;
    size_t widgets;
};

struct xuni_accelerator_key_t {
    SDL_keysym key;
    struct widget_t *widget;
};

struct xuni_accelerator_t {
    struct xuni_accelerator_key_t *key;
    size_t n;
};

enum widget_event_t {
    WIDGET_EVENT_FREE,
    WIDGET_EVENT_UPDATE_TEXT,
    WIDGET_EVENT_PAINT,
    WIDGET_EVENT_REPOSITION,
    WIDGET_EVENT_RESCALE,
    /*WIDGET_EVENT_ACTIVATE,
    WIDGET_EVENT_REVERT,
    WIDGET_EVENT_DEACTIVATE,*/
    WIDGET_EVENTS
};

typedef void (*widget_event_handler_t)(struct xuni_t *xuni,
    struct widget_t *widget, enum widget_event_t event);

typedef void (*widget_event_func_t)(struct xuni_t *xuni,
    struct widget_t *widget);

struct wtype_t {
    const char *name;
    /*size_t type;*/
    widget_event_handler_t handler;
};

struct wtype_array_t {
    struct wtype_t *type;
    size_t types;
};

/* !!! needs alphabetical ordering */
enum panel_event_type_t {
    PANEL_EVENT_NONE = -1,
    PANEL_EVENT_FREE,
    PANEL_EVENT_EVENT,
    PANEL_EVENT_INIT,
    PANEL_EVENT_PAINT,
    PANEL_EVENT_START,
    PANEL_EVENT_CLICK,
    PANEL_EVENT_DEACTIVATE,
    PANEL_EVENT_SEL,
    PANEL_EVENTS
};

struct panel_event_init_t {
    struct widget_t *panel;
    struct resource_t *settings;
};

struct panel_event_start_t {
    int unused;
};

struct panel_event_event_t {
    panel_type_t *mode;
    SDL_Event *event;
    struct widget_t *widget;
};

struct panel_event_sel_t {
    panel_type_t mode;
    int xp, yp;
    int click;
};

struct panel_event_click_t {
    struct widget_t *widget;
    panel_type_t *mode;
};

struct panel_event_deactivate_t {
    struct widget_t *widget;
};

struct panel_event_paint_t {
    panel_type_t mode;
};

struct panel_event_free_t {
    int unused;
};

struct panel_data_t;

typedef int (*panel_event_func_t)(struct xuni_t *xuni,
    struct panel_data_t *data);

struct panel_event_t {
    enum panel_event_type_t type;
    
    union {
        struct panel_event_init_t init;
        struct panel_event_start_t start;
        struct panel_event_event_t event;
        struct panel_event_sel_t sel;
        struct panel_event_click_t click;
        struct panel_event_deactivate_t deactivate;
        struct panel_event_paint_t paint;
        struct panel_event_free_t free;
    } p;
    
    panel_event_func_t handler;
};

#if 0
typedef void (*init_func_t)(void *vdata, struct xuni_t *xuni,
    struct widget_t *panel, struct resource_t *settings);
typedef void (*start_func_t)(void *vdata, struct xuni_t *xuni);
typedef int (*event_func_t)(void *vdata, struct xuni_t *xuni,
    panel_type_t *mode, SDL_Event *event);
typedef int (*set_widget_sel_func_t)(struct xuni_t *xuni, panel_type_t mode,
    int xp, int yp, int click, void *vdata);
typedef int (*perform_click_func_t)(struct widget_t *widget,
    panel_type_t *mode, void *vdata, struct xuni_t *xuni);
typedef void (*deactivate_func_t)(void *vdata, struct xuni_t *xuni,
    struct widget_t *widget);
typedef void (*paint_func_t)(void *vdata, panel_type_t mode,
    struct xuni_t *xuni);
typedef void (*free_func_t)(void *vdata, struct xuni_t *xuni);
#endif

struct panel_data_t {
    void *data;
    int frameupdate;
    
    struct panel_event_t event[PANEL_EVENTS];
    
    struct nameid_t *nameid;
    
    struct xuni_accelerator_t *accel;
};

/*! Generic GUI widget structure. */
struct widget_t {
    char *name;
    widget_id_type_t id;
    
    struct widget_t *base;
    size_t type;
    
    union {
        struct box_t *box;
        struct button_t *button;
        struct checkbox_t *checkbox;
        struct combobox_t *combobox;
        struct font_t *font;
        struct image_t *image;
        struct image_tile_t *image_tile;
        struct label_t *label;
        struct listbox_t *listbox;
        struct panel_data_t *panel;
        struct scrollbar_t *scrollbar;
        struct textarea_t *textarea;
        struct textbox_t *textbox;
        struct theme_data_t *theme;
        void *p;
    } p;
    
    struct panel_t *compose;
    struct pos_t *pos;
    
    enum widget_visibility_t visibility;
    
    struct widget_t *selwidget;
    
    int sel;
    int repaint;  /* !!! ... */
};

void delete_widget(struct xuni_t *xuni, struct panel_t *panel, size_t n);
void delete_widget_pointer(struct xuni_t *xuni, struct widget_t *widget);
void add_widget(struct panel_t *panel, struct widget_t *widget);
void add_allocate_widget(struct widget_t *base, char *name);
void add_allocate_widget_compose(struct widget_t *base, char *name);
struct panel_t *allocate_panel(struct widget_t *base);
struct widget_t *allocate_widget(char *name, struct widget_t *base);
void init_widget_pos(struct widget_t *widget, double x, double y,
    double w, double h, enum pos_pack_t pack);
void clear_widget_clip(struct xuni_t *xuni, struct widget_t *widget);
void add_panel_clip(struct xuni_t *xuni, struct widget_t *widget,
    int xoff, int yoff, int xclip, int yclip, int wclip, int hclip);
void add_widget_clip_nonrec(struct xuni_t *xuni, struct widget_t *widget,
    int xoff, int yoff, int xclip, int yclip, int wclip, int hclip);
void add_widget_clip(struct xuni_t *xuni, struct widget_t *widget,
    int xoff, int yoff, int xclip, int yclip, int wclip, int hclip);
void add_widget_accelerator(struct xuni_t *xuni, struct widget_t *panel,
    struct widget_t *widget, SDLKey key, SDLMod mod);
struct widget_t *last_compose_widget(struct widget_t *widget);
struct widget_t *widget_nameid_access(struct widget_t *widget, size_t n);
struct widget_t *widget_nameid_follow(struct widget_t *widget, ...);
int widget_is_parent(struct widget_t *search, struct widget_t *widget);
int widget_name_match(const char *string, const char *buffer, size_t len);
struct widget_t *find_widget(struct widget_t *widget, const char *name);
void widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event);
void widget_compose_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event);
int get_real_pos(double scale, int basereal);

void init_wtype(struct xuni_t *xuni, struct wtype_array_t *wtype);
void free_wtype(struct wtype_array_t *wtype);
int wtype_search_name(struct xuni_t *xuni, const char *name, size_t *pos);
void register_widget_type(struct xuni_t *xuni, const char *name, size_t type,
    widget_event_handler_t handler);
void call_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t type);
void call_widget_event_func(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event, widget_event_func_t function[],
    size_t functions);

int assert_widget_type(struct xuni_t *xuni, struct widget_t *widget,
    size_t type, const char *file, int line);

int get_widget_visibility(struct widget_t *widget,
    enum widget_visibility_t visibility);
void set_widget_visibility(struct widget_t *widget,
    enum widget_visibility_t visibility, int set);

#ifdef __cplusplus
}
#endif

/* only because of init_wtype() */
#include "box.h"
#include "button.h"
#include "checkbox.h"
#include "combobox.h"
#include "font.h"
#include "image.h"
#include "image_tile.h"
#include "label.h"
#include "listbox.h"
#include "panel.h"
#include "scrollbar.h"
#include "textarea.h"
#include "textbox.h"
#include "theme.h"

#endif
