/*! \file button.c

*/

#include "../graphics.h"
#include "../memory.h"
#include "../gui.h"
#include "widgets.h"
#include "button.h"
#include "box.h"
#include "image.h"
#include "label.h"

static void free_button(struct xuni_t *xuni, struct widget_t *widget);
static void paint_button(struct xuni_t *xuni, struct widget_t *widget);
static void rescale_button(struct xuni_t *xuni, struct widget_t *widget);

void button_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event) {
    
    static void (*function[])(struct xuni_t *xuni, struct widget_t *widget)
        = {
        
        free_button,
        0,
        paint_button,
        0,
        rescale_button
    };
    
    call_widget_event_func(xuni, widget, event, function,
        sizeof(function) / sizeof(*function));
}

#if !1
int get_boxw(struct gui_t *gui) {
    return widget_nameid_follow(gui->widget,
        PANEL_THEME, THEME_CORNERS_IN_NORMAL, (size_t)-1)
            ->compose->widget[WID_BUTTON_BOX]->pos->real.w / 2;
}

int get_boxh(struct gui_t *gui) {
    return widget_nameid_follow(gui,
        PANEL_THEME, THEME_CORNERS_IN_NORMAL, (size_t)-1)
            ->compose->widget[WID_BUTTON_BOX]->pos->real.h / 2;
}
#endif

struct widget_t *new_sub_label(struct widget_t *base, size_t font,
    const char *text) {
    
    struct widget_t *widget = allocate_widget("label", base);
    
    init_widget_pos(widget, 0, 0,
        100.0, 100.0, POS_PACK_NONE);
    init_label(widget, font, text, LABEL_ALIGN_CENTRE, 255, 255, 255);
    
    return widget;
}

/*struct widget_t *new_sub_image_p(struct widget_t *base, struct image_t *p) {
    struct widget_t *widget = allocate_widget("image", base);
    
    init_widget_pos(widget, 0, 0, base->pos->scale.w, base->pos->scale.w,
        POS_PACK_NONE);
    xuni_memory_increment(p);
    init_image_p(widget, p);
    
    return widget;
}*/

void init_button(struct widget_t *widget, struct widget_t *image) {
    widget->type = WIDGET_BUTTON;
    widget->p.button = xuni_memory_allocate(sizeof(*widget->p.button));
    
    add_allocate_widget_compose(widget, "box");
    
    init_widget_pos(last_compose_widget(widget), 0, 0, 100.0, 100.0,
        POS_PACK_NONE);
    init_box(last_compose_widget(widget), BOX_STYLE_NORMAL, widget);
    /*last_compose_widget(widget)->visibility &= ~WIDGET_VISIBILITY_SELABLE;*/
    last_compose_widget(widget)->visibility &= ~WIDGET_VISIBILITY_INDEPENDENT;
    
    add_widget(widget->compose, image);
    
    if(last_compose_widget(widget)) {
        /*last_compose_widget(widget)->visibility &= ~WIDGET_VISIBILITY_SELABLE;*/
        last_compose_widget(widget)->visibility
            &= ~WIDGET_VISIBILITY_INDEPENDENT;
    }
    
    widget->visibility &= ~WIDGET_VISIBILITY_NOT_COMPOSE;
}

void init_button_image(struct xuni_t *xuni, struct widget_t *widget,
    struct widget_t *copy) {
    
    struct widget_t *image = allocate_widget("button image", widget);
    
    init_widget_pos(image, 0, 0, 100.0, 100.0, POS_PACK_NONE);
    
    if(copy) {
        init_image(image, copy->p.image->filename, copy->p.image->angle,
            IMAGE_LOAD_FREE, IMAGE_RESCALE_LAZY);
    }
    else {
        init_image(image, 0, 0.0, IMAGE_LOAD_FREE, IMAGE_RESCALE_LAZY);
    }
    
    delete_widget(xuni, widget->compose, WID_BUTTON_LABEL);
    add_widget(widget->compose, image);
    
    /*image->visibility &= ~WIDGET_VISIBILITY_SELABLE;*/
    image->visibility &= ~WIDGET_VISIBILITY_INDEPENDENT;
}

static void free_button(struct xuni_t *xuni, struct widget_t *widget) {
    xuni_memory_free(widget->p.button);
}

static void paint_button(struct xuni_t *xuni, struct widget_t *widget) {
    int x = 0;
    
    /*widget->compose->widget[WID_BUTTON_BOX]->visibility = widget->visibility;*/
    
    if(widget->pos->clip) {
        if(widget->compose->widget[WID_BUTTON_LABEL]) {
            widget->compose->widget[WID_BUTTON_LABEL]->pos->real.x
                += widget->pos->clip->xoff;
            widget->compose->widget[WID_BUTTON_LABEL]->pos->real.y
                += widget->pos->clip->yoff;
        }
        
        widget->compose->widget[WID_BUTTON_BOX]->pos->real.x
            += widget->pos->clip->xoff;
        widget->compose->widget[WID_BUTTON_BOX]->pos->real.y
            += widget->pos->clip->yoff;
    }
    
    widget_event(xuni, widget->compose->widget[WID_BUTTON_BOX],
        WIDGET_EVENT_PAINT);
    
    /*set_box_type(xuni->gui, widget,
        widget->compose->widget[WID_BUTTON_BOX]);
    paint_box_previous_state(xuni,
        widget->compose->widget[WID_BUTTON_BOX]);*/
    
    /*if(widget->compose->widget[WID_BUTTON_LABEL]
        && !widget->compose->widget[WID_BUTTON_LABEL]->pos->real.x
        && !widget->compose->widget[WID_BUTTON_LABEL]->pos->real.y) {
        
        printf("Button label at 0,0 for button \"%s\"\n", widget->name);
    }*/
    
    if(widget->compose->widget[WID_BUTTON_LABEL]) {
        x = (widget->compose->widget[WID_BUTTON_BOX]->p.box->state
                & BOX_STATE_HOVER)
            && (widget->compose->widget[WID_BUTTON_BOX]->p.box->state
                & BOX_STATE_ACTIVE);
        
        if(widget->compose->widget[WID_BUTTON_BOX]->p.box->style
            == BOX_STYLE_INVERTED) {
            
            x = !x;
        }
    }
    
    if(x) {
        widget->compose->widget[WID_BUTTON_LABEL]->pos->real.x ++;
        widget->compose->widget[WID_BUTTON_LABEL]->pos->real.y ++;
    }
    
    widget_event(xuni, widget->compose->widget[WID_BUTTON_LABEL],
        WIDGET_EVENT_PAINT);
    
    if(x) {
        widget->compose->widget[WID_BUTTON_LABEL]->pos->real.x --;
        widget->compose->widget[WID_BUTTON_LABEL]->pos->real.y --;
    }
    
    if(widget->pos->clip) {
        if(widget->compose->widget[WID_BUTTON_LABEL]) {
            widget->compose->widget[WID_BUTTON_LABEL]->pos->real.x
                -= widget->pos->clip->xoff;
            widget->compose->widget[WID_BUTTON_LABEL]->pos->real.y
                -= widget->pos->clip->yoff;
        }
        
        widget->compose->widget[WID_BUTTON_BOX]->pos->real.x
            -= widget->pos->clip->xoff;
        widget->compose->widget[WID_BUTTON_BOX]->pos->real.y
            -= widget->pos->clip->yoff;
    }
}

static void rescale_button(struct xuni_t *xuni, struct widget_t *widget) {
    
}
