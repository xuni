/*! \file textarea.h

*/

#ifndef XUNI_GUARD_TEXTAREA_H
#define XUNI_GUARD_TEXTAREA_H

#include "widgets.h"

#ifdef __cplusplus
extern "C" {
#endif

enum wid_textarea_t {
    WID_TEXTAREA_BOX,
    WID_TEXTAREA_VSCROLL,
    WID_TEXTAREA_DATA
};

struct textarea_t {
    int unused;
};

void textarea_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event);
void init_textarea(struct widget_t *widget, struct xuni_t *xuni);

#ifdef __cplusplus
}
#endif

#endif
