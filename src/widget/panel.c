/*! \file panel.c

*/

#include "../graphics.h"
#include "../loadso.h"
#include "../memory.h"
#include "widgets.h"
#include "panel.h"

static func_point_t panel_load_handler(void *object,
    struct resource_list_t *handler, const char *name);

static void free_panel_data(struct xuni_t *xuni, struct widget_t *widget);
static void reposition_panel(struct xuni_t *xuni, struct widget_t *widget);
static void paint_panel(struct xuni_t *xuni, struct widget_t *widget);

void panel_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event) {
    
    static void (*function[])(struct xuni_t *xuni, struct widget_t *widget)
        = {
        
        free_panel_data,
        0,
        paint_panel,
        reposition_panel,
        0
    };
    
    call_widget_event_func(xuni, widget, event, function,
        sizeof(function) / sizeof(*function));
}

void init_panel(struct widget_t *widget) {
    widget->type = WIDGET_PANEL;
    widget->p.panel = xuni_memory_allocate(sizeof(*widget->p.panel));
    
    init_panel_data(widget);
    
    /*widget->compose = allocate_panel(widget);*/
}

void init_panel_data(struct widget_t *widget) {
    enum panel_event_type_t x;
    
    widget->p.panel->data = 0;
    widget->p.panel->frameupdate = 0;
    
    for(x = 0; x < PANEL_EVENTS; x ++) {
        widget->p.panel->event[x].type = x;
        widget->p.panel->event[x].handler = 0;
    }
    
    widget->p.panel->nameid = 0;
    
    widget->p.panel->accel = 0;
}

void set_panel_data(struct widget_t *widget, void *vdata, int frameupdate) {
    widget->p.panel->data = vdata;
    widget->p.panel->frameupdate = frameupdate;
    
    widget->p.panel->nameid = 0;
    widget->p.panel->accel = 0;
}

void set_panel_callback(struct widget_t *widget, enum panel_event_type_t type,
    panel_event_func_t func) {
    
    widget->p.panel->event[type].handler = func;
}

static func_point_t panel_load_handler(void *object,
    struct resource_list_t *handler, const char *name) {
    
    const char *func = first_resource_text(first_resource_tag(handler, name));
    
    if(!func || !*func) return 0;
    
    /*printf("Loading handler \"%s\"\n", func);*/
    
    return xuni_loadso_load_function(object, func);
}

/* !!! */
int default_panel_sel(struct xuni_t *xuni, struct panel_data_t *data);

void init_panel_from_resource(struct xuni_t *xuni, struct widget_t *widget,
    struct resource_list_t *list) {
    
    struct resource_list_t *handler = first_resource_tag(list, "handler");
    loadso_t object = xuni_loadso_load_object(xuni->loadso,
        first_resource_text(first_resource_tag(handler, "file")));
    
    widget->type = WIDGET_PANEL;
    widget->p.panel = xuni_memory_allocate(sizeof(*widget->p.panel));
    
    widget->p.panel->data = 0;
    widget->p.panel->frameupdate = 0;
    widget->p.panel->event[PANEL_EVENT_INIT].handler
        = (panel_event_func_t)panel_load_handler(object, handler, "init");
    widget->p.panel->event[PANEL_EVENT_START].handler
        = (panel_event_func_t)panel_load_handler(object, handler, "start");
    widget->p.panel->event[PANEL_EVENT_EVENT].handler
        = (panel_event_func_t)panel_load_handler(object, handler, "event");
    widget->p.panel->event[PANEL_EVENT_SEL].handler
        = (panel_event_func_t)default_panel_sel;
    widget->p.panel->event[PANEL_EVENT_CLICK].handler
        = (panel_event_func_t)panel_load_handler(object, handler,
            "click");
    widget->p.panel->event[PANEL_EVENT_DEACTIVATE].handler
        = (panel_event_func_t)panel_load_handler(object, handler,
            "deactivate");
    widget->p.panel->event[PANEL_EVENT_PAINT].handler
        = (panel_event_func_t)panel_load_handler(object, handler, "paint");
    widget->p.panel->event[PANEL_EVENT_FREE].handler
        = (panel_event_func_t)panel_load_handler(object, handler, "free");
    
    widget->p.panel->nameid = 0;
    
    widget->p.panel->accel = 0;
}

static void free_panel_data(struct xuni_t *xuni, struct widget_t *widget) {
    xuni_memory_free(widget->p.panel->data);
    
    if(widget->p.panel->nameid) {
        xuni_memory_free(widget->p.panel->nameid->widget);
        xuni_memory_free(widget->p.panel->nameid);
    }
    
    if(widget->p.panel->accel) {
        xuni_memory_free(widget->p.panel->accel->key);
        xuni_memory_free(widget->p.panel->accel);
    }
    
    xuni_memory_free(widget->p.panel);
}

/* !!! This code really needs to be re-written and moved somewhere else --
    it's a big, messy kludge.
*/
#define MOVE_LISTBOX_DATA_DOWN

/* !!! could do away with paint_*(), paint_func_t functions */
static void paint_panel(struct xuni_t *xuni, struct widget_t *widget) {
#ifdef MOVE_LISTBOX_DATA_DOWN
    struct clip_pos_t *oldclip, clip;
    int xdiff, ydiff, hasclip;
#endif
    size_t x;
    
    if(!widget || !widget->compose) return;
    
    for(x = 0; x < widget->compose->widgets; x ++) {
#ifdef MOVE_LISTBOX_DATA_DOWN
        hasclip = (widget->pos->clip != 0);
        if(hasclip) {
            oldclip = widget->compose->widget[x]->pos->clip;
            clip = *widget->pos->clip;
            widget->compose->widget[x]->pos->clip = &clip;
            
            {
                xdiff = (widget->compose->widget[x]->pos->real.x
                    - widget->pos->real.x);
                
                /*clip.wclip -= xdiff;
                clip.wclip += clip.xclip;
                if(clip.wclip < 0) clip.wclip = 0;*/
                
                clip.xclip -= xdiff;
                if(clip.xclip < 0) clip.xclip = 0;
                
                clip.xoff += clip.xclip;
            }
            
            {
                ydiff = (widget->compose->widget[x]->pos->real.y
                    - widget->pos->real.y);
                
                clip.hclip -= ydiff;
                clip.hclip += clip.yclip;
                if(clip.hclip < 0) clip.hclip = 0;
                
                clip.yclip -= ydiff;
                if(clip.yclip < 0) clip.yclip = 0;
                
                clip.yoff += clip.yclip;
            }
        }
#endif
        
        widget_event(xuni, widget->compose->widget[x], WIDGET_EVENT_PAINT);
        
#ifdef MOVE_LISTBOX_DATA_DOWN
        if(hasclip /*&& widget->compose->widget[x]->pos->clip*/) {
            widget->compose->widget[x]->pos->clip = oldclip;
        }
#endif
    }
}

static void reposition_panel(struct xuni_t *xuni, struct widget_t *widget) {
    
}

void free_panel(struct xuni_t *xuni, struct panel_t *panel) {
    size_t x;
    
    if(!panel) return;
    
    for(x = 0; x < panel->widgets; x ++) {
        widget_event(xuni, panel->widget[x], WIDGET_EVENT_FREE);
    }
    
    xuni_memory_free(panel->widget);
    
    panel->widget = 0;
    panel->widgets = 0;
    
    xuni_memory_free(panel);
}
