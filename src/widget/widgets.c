/*! \file widgets.c

*/

#include <stdlib.h>
#include <string.h>

#include "../graphics.h"
#include "../memory.h"
#include "../error.h"
#include "dump.h"
#include "widgets.h"

static struct widget_t *widget_nameid_follow_va(struct widget_t *widget,
    va_list arg);

static void set_default_pos(struct widget_t *widget);
static void set_pos_pack_top(struct xuni_t *xuni, struct widget_t *widget);
static void reposition_widget_pack(struct xuni_t *xuni,
    struct widget_t *widget);
static void reposition_widget_nonrec(struct xuni_t *xuni,
    struct widget_t *widget);
static void free_widget(struct xuni_t *xuni, struct widget_t *widget);
static void update_text_widget(struct xuni_t *xuni, struct widget_t *widget);
static void paint_widget(struct xuni_t *xuni, struct widget_t *widget);
static void reposition_widget(struct xuni_t *xuni, struct widget_t *widget);
static void rescale_widget(struct xuni_t *xuni, struct widget_t *widget);

static int widget_type_compare(void *data, size_t n, void *find);

void delete_widget(struct xuni_t *xuni, struct panel_t *panel, size_t n) {
    free_widget(xuni, panel->widget[n]);
    
    if(n + 1 < panel->widgets) {
        memmove(panel->widget + n, panel->widget + n + 1,
            (panel->widgets - n - 1) * sizeof(*panel->widget));
    }
    
    panel->widgets --;
    panel->widget = xuni_memory_resize(panel->widget, panel->widgets
        * sizeof(*panel->widget));
}

void delete_widget_pointer(struct xuni_t *xuni, struct widget_t *widget) {
    size_t x;
    
    if(!widget) return;
    
    if(widget->base) {
        for(x = 0; x < widget->base->compose->widgets; x ++) {
            if(widget == widget->base->compose->widget[x]) {
                delete_widget(xuni, widget->base->compose, x);
                return;
            }
        }
    }
    else free_widget(xuni, widget);
}

void add_widget(struct panel_t *panel, struct widget_t *widget) {
    panel->widget = xuni_memory_resize(panel->widget, (panel->widgets + 1)
        * sizeof(*panel->widget));
    panel->widget[panel->widgets] = widget;
    panel->widgets ++;
}

void add_allocate_widget(struct widget_t *base, char *name) {
    add_widget(base->compose, allocate_widget(name, base));
}

void add_allocate_widget_compose(struct widget_t *base, char *name) {
    if(!base->compose) base->compose = allocate_panel(base);
    
    add_allocate_widget(base, name);
}

struct panel_t *allocate_panel(struct widget_t *base) {
    struct panel_t *panel;
    
    panel = xuni_memory_allocate(sizeof(*panel));
    panel->base = base;
    panel->widget = 0;
    panel->widgets = 0;
    
    return panel;
}

/*! Returns a new, dynamically allocated widget. The widget's name becomes
    \a name and its parent widget (widget_t::base) is set to \a base.
    Otherwise, the widget is initialized to default values.
    
    This function should be followed immediately by a call to init_*() to
    initialize an instance of a specific type of widget; otherwise the widget
    is virtually useless, with a type of WIDGET_NONE.
    
    \param name The name of the new widget.
    \param base The parent widget of the new widget.
    \return The newly allocated widget.
*/
struct widget_t *allocate_widget(char *name, struct widget_t *base) {
    struct widget_t *widget = xuni_memory_allocate(sizeof(*widget));
    
    /*widget->p.all = xuni_memory_allocate(sizeof_widget_structure(type));*/
    /*widget->p.all = 0;*/
    
    widget->name = name;
    widget->id = (size_t)-1;
    widget->base = base;
    widget->type = WIDGET_NONE;
    
    widget->compose = 0;
    widget->pos = 0;
    
    widget->visibility = WIDGET_VISIBILITY_ALL;
    
    widget->selwidget = 0;
    
    widget->sel = 0;
    widget->repaint = 0;
    
    return widget;
}

void init_widget_pos(struct widget_t *widget, double x, double y,
    double w, double h, enum pos_pack_t pack) {
    
    if(!widget->pos) {
        widget->pos = xuni_memory_allocate(sizeof(*widget->pos));
        
        widget->pos->clip = 0;
    }
    
    widget->pos->pack = pack;
    
    widget->pos->scale.x = x;
    widget->pos->scale.y = y;
    widget->pos->scale.w = w;
    widget->pos->scale.h = h;
    
    /* widget->pos->real.* set by rescale_widget() */
}

void clear_widget_clip(struct xuni_t *xuni, struct widget_t *widget) {
    if(!widget) return;
    
    if(widget->pos->clip) {
        widget->pos->clip->xoff = 0;
        widget->pos->clip->yoff = 0;
        widget->pos->clip->xclip = 0;
        widget->pos->clip->yclip = 0;
        
        reposition_widget_pack(xuni, widget);
        
        /* !!! should this be before the above call or not? */
        widget->pos->clip->wclip = widget->pos->real.w;
        widget->pos->clip->hclip = widget->pos->real.h;
    }
}

void add_panel_clip(struct xuni_t *xuni, struct widget_t *widget,
    int xoff, int yoff, int xclip, int yclip, int wclip, int hclip) {
    
    size_t x;
    
    if(!widget->pos->clip) {
        widget->pos->clip = xuni_memory_allocate(sizeof(*widget->pos->clip));
        clear_widget_clip(xuni, widget);
    }
    
    if(widget && widget->compose) {
        for(x = 0; x < widget->compose->widgets; x ++) {
            add_widget_clip(xuni, widget->compose->widget[x], xoff, yoff,
                xclip, yclip, wclip, hclip);
        }
    }
}

void add_widget_clip_nonrec(struct xuni_t *xuni, struct widget_t *widget,
    int xoff, int yoff, int xclip, int yclip, int wclip, int hclip) {
    
    if(!widget) return;
    
    if(!widget->pos->clip) {
        widget->pos->clip = xuni_memory_allocate(sizeof(*widget->pos->clip));
        clear_widget_clip(xuni, widget);
    }
    
    /*widget->pos->clip->xoff += xoff;
    widget->pos->clip->yoff += yoff;*/
    widget->pos->clip->xclip += xclip;
    widget->pos->clip->yclip += yclip;
    widget->pos->clip->wclip += wclip;
    widget->pos->clip->hclip += hclip;
    
    widget->pos->real.x += xoff;
    widget->pos->real.y += yoff;
    /*widget->pos->real.w += xclip;
    widget->pos->real.h += yclip;*/
    /* !!! need to do something with wclip and hclip, too */
    
    /*printf("add_widget_clip(): \"%s\" = (%i,%i) (%i,%i,%i,%i)",
        widget->base->name, xoff, yoff, xclip, yclip, wclip, hclip);
    printf(" -> (%i,%i) (%i,%i,%i,%i)\n",
        widget->pos->clip->xoff,
        widget->pos->clip->yoff,
        widget->pos->clip->xclip,
        widget->pos->clip->yclip,
        widget->pos->clip->wclip,
        widget->pos->clip->hclip);*/
}

void add_widget_clip(struct xuni_t *xuni, struct widget_t *widget,
    int xoff, int yoff, int xclip, int yclip, int wclip, int hclip) {
    
    size_t x;
    
    if(!widget) return;
    
    add_widget_clip_nonrec(xuni, widget,
        xoff, yoff, xclip, yclip, wclip, hclip);
    
    if(widget->compose) {
        for(x = 0; x < widget->compose->widgets; x ++) {
            add_widget_clip(xuni, widget->compose->widget[x], xoff, yoff,
                0, 0, 0, 0);
        }
    }
    
    /*printf("add_widget_clip(): \"%s\" = (%i,%i) (%i,%i,%i,%i)",
        widget->base->name, xoff, yoff, xclip, yclip, wclip, hclip);
    printf(" -> (%i,%i) (%i,%i,%i,%i)\n",
        widget->pos->clip->xoff,
        widget->pos->clip->yoff,
        widget->pos->clip->xclip,
        widget->pos->clip->yclip,
        widget->pos->clip->wclip,
        widget->pos->clip->hclip);*/
}

void add_widget_accelerator(struct xuni_t *xuni, struct widget_t *panel,
    struct widget_t *widget, SDLKey key, SDLMod mod) {
    
    struct panel_data_t *data;
    
    if(!panel || panel->type != WIDGET_PANEL || !panel->p.panel) {
        printf("*** add_widget_accelerator() called with invalid widget:\n");
        print_widget_backtrace(xuni, widget);
        return;
    }
    
    data = panel->p.panel;
    
    if(!data->accel) {
        data->accel = xuni_memory_allocate(sizeof(*data->accel));
        data->accel->key = 0;
        data->accel->n = 0;
    }
    
    data->accel->key = xuni_memory_resize(data->accel->key,
        (data->accel->n + 1) * sizeof(*data->accel->key));
    data->accel->key[data->accel->n].key.sym = key;
    data->accel->key[data->accel->n].key.mod = mod;
    data->accel->key[data->accel->n].widget = widget;
    
    data->accel->n ++;
}

/*! Returns the last compose widget of \a widget.
    
    This is frequently used in the widget initialization functions to refer
    to the last sub-widget added.
    
    \param widget The widget for which to return the last compose widget.
    \return The last compose widget of \a widget, or NULL if the widget does
        not have any sub-widgets.
*/
struct widget_t *last_compose_widget(struct widget_t *widget) {
    if(!widget->compose || !widget->compose->widgets) return 0;
    
    return widget->compose->widget[widget->compose->widgets - 1];
}

/*struct widget_t *compose_access_widget(struct widget_t *widget, size_t n) {
    if(!widget || !widget->compose || n >= widget->compose->widgets) return 0;
    
    return widget->compose->widget[n];
}

size_t get_compose_widgets(struct widget_t *widget) {
    if(!widget || !widget->compose) return 0;
    
    return widget->compose->widgets;
}*/

/*! Searches \a widget for the registered nameid \a n.
    \param widget The widget to search for the nameid \a n. Should be a panel
        or a theme widget.
    \param n The nameid to search \a widget for.
    \return The widget for the nameid \a n, or zero the nameid was not found.
        Also returns zero if \a widget is not of type panel or theme, or if
        \a widget is NULL.
*/
struct widget_t *widget_nameid_access(struct widget_t *widget, size_t n) {
    if(!widget) return 0;
    
    if(widget->type == WIDGET_PANEL) {
        if(widget->p.panel->nameid
            && n < widget->p.panel->nameid->widgets) {
            
            return widget->p.panel->nameid->widget[n].widget;
        }
    }
    else if(widget->type == WIDGET_THEME) {
        if(widget->p.theme->nameid
            && n < widget->p.theme->nameid->widgets) {
            
            return widget->p.theme->nameid->widget[n].widget;
        }
    }
    
    return 0;
}

/*! Starting at \a widget, follows a series of nameids. This is a private
    implementation, used by widget_nameid_follow(). It takes a \c va_list
    parameter instead of "...".
    
    \param widget The widget to start searching from.
    \param arg The series of nameids to follow, starting at \a widget.
    \return The widget that was eventually found, if any, or NULL otherwise.
*/
static struct widget_t *widget_nameid_follow_va(struct widget_t *widget,
    va_list arg) {
    
    size_t id;
    
    while((id = va_arg(arg, size_t)) != (size_t)-1) {
        widget = widget_nameid_access(widget, id);
        if(!widget) return 0;
    }
    
    return widget;
}

/*! Starting at \a widget, follows a series of nameids. The series must be
    terminated with (size_t)-1.
    
    Equivalent to multiple calls to widget_nameid_access().
    
    \param widget The widget to start searching for the first nameid. Should
        be a panel or theme widget.
    \param ... The series of nameids to follow, terminated with (size_t)-1.
    \return The widget specified by all of the nameids, if found, or NULL
        otherwise.
*/
struct widget_t *widget_nameid_follow(struct widget_t *widget, ...) {
    struct widget_t *found;
    va_list arg;
    
    va_start(arg, widget);
    found = widget_nameid_follow_va(widget, arg);
    va_end(arg);
    
    return found;
}

/*! Returns true if the widget \a search is equal to \a widget or one of its
    ancestors.
    \param search The widget to search for.
    \param widget The widget to search through, following base pointers until
        the root widget is reached.
    \return True if the widget \a search was found in the widget path that
        \a widget denotes.
*/
int widget_is_parent(struct widget_t *search, struct widget_t *widget) {
    while(widget) {
        if(search == widget) return 1;
        widget = widget->base;
    }
    
    return 0;
}

/*! Returns true if \a string and \a len characters of \a buffer match. This
    function is sort of like strncmp() -- however, it compares the lengths of
    the strings before comparing their contents. This means that a calls like
        widget_name_match("some", "some", 5)
        widget_name_match("something", "some", 4)
    will not return true, even though they would for strncmp().
    
    This function is used for matching widget names, unsurprisingly. \a string
    can contain the name of a widget, say "xuni", and \a buffer can contain a
    path to a widget, say "xuni/etc", and this function will be able to match
    the names correctly.
    
    \param string The widget name to look for at the beginning of  \a buffer.
    \param buffer The widget path in which to look at the first \a len
        characters, and to compare with \a string.
    \param len The number of characters in \a buffer that should be
        considered. The length of \a string must also equal this value for a
        match to occur.
    \return True if a match was found, false otherwise.
*/
int widget_name_match(const char *string, const char *buffer, size_t len) {
    if(!string || !buffer) return 0;
    
    return strlen(string) == len && !strncmp(string, buffer, len);
}

/*! Searches for the widget specified by \a name, with respect to \a widget.
    
    \a name is not just the name of a widget. It can also contain slashes to
    specify child widgets; ".." to specify the parent widget; and "." to
    specify the current widget \a widget. (An empty string also specifies
    \a widget.)
    
    Note: this function currently does not handle double slashes (like
    "this//that"). (They are treated as widgets with empty names.) Nor does it
    support backslashes, just slashes.
    
    \param widget The widget to start searching from.
    \param name The path to the widget that is being sought.
    \return The widget specified by \a name starting at \a widget, if found,
        or NULL if not.
*/
struct widget_t *find_widget(struct widget_t *widget, const char *name) {
    struct widget_t *p;
    const char *end;
    size_t x;
    
    if(!name || !widget) return 0;
    
    for(;;) {
        for(end = name; *end && *end != '/'; end ++);
        
        if(widget_name_match(".", name, end - name)) {
            if(!*end) return widget;
            name = end + 1;
        }
        else break;
    }
    
    if(widget_name_match("..", name, end - name)) {
        if(!*end) return widget->base;
        return find_widget(widget->base, end + 1);
    }
    
    if(widget->compose) {
        for(x = 0; x < widget->compose->widgets; x ++) {
            if(widget_name_match(widget->compose->widget[x]->name,
                name, end - name)) {
                
                if(!*end) return widget->compose->widget[x];
                p = find_widget(widget->compose->widget[x], end + 1);
                if(p) return p;
            }
        }
    }
    
    return 0;
}

/*! Sends an event of type \a event to the widget \a widget.
    
    The event is passed through several functions before finally being handled
    by {widget}_widget_event() (e.g., button_widget_event()), which usually
    defers to {event}_{widget}() (e.g., rescale_button()).
    
    \param xuni A pointer to the main xuni_t structure.
    \param widget The widget to send the event to.
    \param event The type of event to send to the widget.
*/
void widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event) {
    
    static void (*function[])(struct xuni_t *xuni, struct widget_t *widget)
        = {
        
        free_widget,
        update_text_widget,
        paint_widget,
        reposition_widget,
        rescale_widget
    };
    
    call_widget_event_func(xuni, widget, event, function,
        sizeof(function) / sizeof(*function));
}

/*! Sends a widget event to all subwidgets of \a widget, but not to \a widget
    itself.
    
    Useful in generic widget event handlers to pass on an event to subwidgets.
    
    \param xuni A pointer to the main xuni structure. Passed on to the widget
        event handling functions.
    \param widget The widget for which to send a widget event to all the
        subwidgets of.
    \param event The widget event type to send to the composing widgets of
        \a widget.
*/
void widget_compose_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event) {
    
    size_t x;
    
    if(!widget->compose) return;
    
    for(x = 0; x < widget->compose->widgets; x ++) {
        widget_event(xuni, widget->compose->widget[x], event);
    }
}

static void set_default_pos(struct widget_t *widget) {
    if(!widget || !widget->pos) return;
    
    widget->pos->real.x = 0;
    widget->pos->real.y = 0;
    widget->pos->real.w = 0;
    widget->pos->real.h = 0;
}

static void set_pos_pack_top(struct xuni_t *xuni, struct widget_t *widget) {
    struct widget_t *prev;
    size_t x;
    
    if(!widget) return;
    
    widget->pos->real.w = 0;
    widget->pos->real.h = 0;
    
    if(!widget->base || !widget->base->pos) {
        set_default_pos(widget);
        return;
    }
    
    for(x = 0; x < widget->base->compose->widgets; x ++) {
        if(widget == widget->base->compose->widget[x]) break;
    }
    
    widget->pos->real.x = widget->base->pos->real.x;
    widget->pos->real.y = widget->base->pos->real.y;
    
    if(x) {
        prev = widget->base->compose->widget[x - 1];
        
        if(prev->type == WIDGET_LABEL) {
            if(prev->p.label->label) {
                widget->pos->real.y
                    = prev->pos->real.y + prev->p.label->label->h;
            }
            else {
                printf("Error: widget's prev label not set: %lu; prev:\n",
                    (unsigned long)x);
                print_inline_widget_backtrace(prev);
                
                widget->pos->real.y = prev->pos->real.y;
            }
        }
        /*else if(prev->pos) {
            widget->pos->real.y += prev->pos->scale.y / 100.0 * smode->height;
        }*/
        else {
            printf("*** Can't pack after widgets of type \"%s\"\n",
                get_widget_type_name(xuni, prev->type));
        }
    }
}

int get_real_pos(double scale, int basereal) {
    return (int)((scale / 100.0) * basereal);
}

static void reposition_widget_pack(struct xuni_t *xuni,
    struct widget_t *widget) {
    
    if(!widget->pos) return;
    
    switch(widget->pos->pack) {
    case POS_PACK_NONE:
        if(widget->base && widget->base->pos) {
            /*printf("using base: (%i,%i) by (%i,%i)\n",
                widget->base->pos->real.x, widget->base->pos->real.y,
                widget->base->pos->real.w, widget->base->pos->real.h);*/
            widget->pos->real.x = get_real_pos(
                widget->pos->scale.x, widget->base->pos->real.w)
                + widget->base->pos->real.x;
            widget->pos->real.y = get_real_pos(
                widget->pos->scale.y, widget->base->pos->real.h)
                + widget->base->pos->real.y;
            
            widget->pos->real.w = get_real_pos(
                widget->pos->scale.w, widget->base->pos->real.w);
            widget->pos->real.h = get_real_pos(
                widget->pos->scale.h, widget->base->pos->real.h);
        }
        else {
            /*puts("(no base)");*/
            widget->pos->real.x
                = widget->pos->scale.x / 100.0 * xuni->smode->width;
            widget->pos->real.y
                = widget->pos->scale.y / 100.0 * xuni->smode->height;
            widget->pos->real.w
                = widget->pos->scale.w / 100.0 * xuni->smode->width;
            widget->pos->real.h
                = widget->pos->scale.h / 100.0 * xuni->smode->height;
        }
        
        /*printf("%20s: (%i,%i) by (%i,%i)\n", widget->name,
            widget->pos->real.x, widget->pos->real.y,
            widget->pos->real.w, widget->pos->real.h);*/
        
        break;
    case POS_PACK_TOP:
        set_pos_pack_top(xuni, widget);
        
        break;
    default:
        printf("*** Invalid pos pack type: %i\n", (int)widget->pos->pack);
        break;
    }
    
    /* !!! temporarily fixes the "listbox scroll problem" */
    if(widget->type == WIDGET_LABEL && !widget->pos->scale.w
        && !widget->pos->scale.h && widget->p.label->label) {
        
        widget->pos->real.w = widget->p.label->label->w;
        widget->pos->real.h = widget->p.label->label->h;
    }
    
#if 0
    if(widget->base && widget->base->pos) {
        widget->pos->real.x += widget->base->pos->real.x;
        widget->pos->real.y += widget->base->pos->real.y;
    }
#endif
}

static void reposition_widget_nonrec(struct xuni_t *xuni,
    struct widget_t *widget) {
    
    clear_widget_clip(xuni, widget);
    
    call_widget_event(xuni, widget, WIDGET_EVENT_REPOSITION);
}

static void reposition_widget(struct xuni_t *xuni, struct widget_t *widget) {
    if(!widget) return;
    
    reposition_widget_pack(xuni, widget);
    
    widget_compose_event(xuni, widget, WIDGET_EVENT_REPOSITION);
    
    reposition_widget_nonrec(xuni, widget);
}

static void rescale_widget(struct xuni_t *xuni, struct widget_t *widget) {
    if(!widget) return;
    
    reposition_widget_pack(xuni, widget);
    
    widget_compose_event(xuni, widget, WIDGET_EVENT_RESCALE);
    
    reposition_widget_nonrec(xuni, widget);
    
    if(widget->type != WIDGET_PANEL) {
        call_widget_event(xuni, widget, WIDGET_EVENT_RESCALE);
    }
}

static void paint_widget(struct xuni_t *xuni, struct widget_t *widget) {
    if(!widget) return;
    
    if(widget->visibility & WIDGET_VISIBILITY_VISIBLE) {
        call_widget_event(xuni, widget, WIDGET_EVENT_PAINT);
        
        if(widget->repaint) {
            widget->repaint = 0;
        }
    }
}

static void update_text_widget(struct xuni_t *xuni, struct widget_t *widget) {
    if(!widget) return;
    
    call_widget_event(xuni, widget, WIDGET_EVENT_UPDATE_TEXT);
    
    widget_compose_event(xuni, widget, WIDGET_EVENT_UPDATE_TEXT);
}

static void free_widget(struct xuni_t *xuni, struct widget_t *widget) {
    if(!widget) return;
    
    if(!xuni_memory_decrement(widget)) return;
    
    call_widget_event(xuni, widget, WIDGET_EVENT_FREE);
    
    if(widget->pos) {
        xuni_memory_free(widget->pos->clip);
        xuni_memory_free(widget->pos);
    }
    
    free_panel(xuni, widget->compose);
    
    free(widget);
}

void init_wtype(struct xuni_t *xuni, struct wtype_array_t *wtype) {
    wtype->type = 0;
    wtype->types = 0;
    
    register_widget_type(xuni, "box", WIDGET_BOX, box_widget_event);
    register_widget_type(xuni, "button", WIDGET_BUTTON, button_widget_event);
    register_widget_type(xuni,
        "checkbox", WIDGET_CHECKBOX, checkbox_widget_event);
    register_widget_type(xuni,
        "combobox", WIDGET_COMBOBOX, combobox_widget_event);
    register_widget_type(xuni, "font", WIDGET_FONT, font_widget_event);
    register_widget_type(xuni, "image", WIDGET_IMAGE, image_widget_event);
    register_widget_type(xuni,
        "image tile", WIDGET_IMAGE_TILE, image_tile_widget_event);
    register_widget_type(xuni, "label", WIDGET_LABEL, label_widget_event);
    register_widget_type(xuni,
        "listbox", WIDGET_LISTBOX, listbox_widget_event);
    register_widget_type(xuni, "panel", WIDGET_PANEL, panel_widget_event);
    register_widget_type(xuni,
        "scrollbar", WIDGET_SCROLLBAR, scrollbar_widget_event);
    register_widget_type(xuni,
        "textarea", WIDGET_TEXTAREA, textarea_widget_event);
    register_widget_type(xuni,
        "textbox", WIDGET_TEXTBOX, textbox_widget_event);
    register_widget_type(xuni, "theme", WIDGET_THEME, theme_widget_event);
}

/*! Frees the memory allocated for a wtype_array_t structure. Used by
    free_xuni() to free the structure of that type that is a memory of struct
    xuni_t.
    \param wtype The structure to free the memory for.
*/
void free_wtype(struct wtype_array_t *wtype) {
    xuni_memory_free(wtype->type);
}

static int widget_type_compare(void *data, size_t n, void *find) {
    struct wtype_t *array = data;
    const char *a = find;
    const char *b = array[n].name;
    
    return strcmp(a, b);
}

int wtype_search_name(struct xuni_t *xuni, const char *name, size_t *pos) {
    return binary_insertion_sort(xuni->wtype->type, xuni->wtype->types,
        name, pos, widget_type_compare);
}

void register_widget_type(struct xuni_t *xuni, const char *name, size_t type,
    widget_event_handler_t handler) {
    
    size_t pos;
    
    if(wtype_search_name(xuni, name, &pos)) {
        log_message(ERROR_TYPE_DATASTRUCT, 0, __FILE__, __LINE__,
            "Adding duplicate widget type \"%s\"", name);
        return;
    }
    
    xuni->wtype->type = xuni_memory_resize(xuni->wtype->type,
        (xuni->wtype->types + 1) * sizeof(*xuni->wtype->type));
    memmove(xuni->wtype->type + pos + 1, xuni->wtype->type + pos,
        (xuni->wtype->types - pos) * sizeof(*xuni->wtype->type));
    xuni->wtype->types ++;
    
    xuni->wtype->type[pos].name = name;
    xuni->wtype->type[pos].handler = handler;
}

void call_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t type) {
    
    /* The first condition is not strictly necessary, being encompassed by the
        second one as it is, but it is included for clarity.
    */
    if(widget->type == (size_t)-1 || widget->type >= xuni->wtype->types) {
        log_message(ERROR_TYPE_WARNING, 0, __FILE__, __LINE__,
            "Invalid or unregistered widget type: %i\n", (int)widget->type);
        return;
    }
    
    xuni->wtype->type[widget->type].handler(xuni, widget, type);
}

void call_widget_event_func(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event, widget_event_func_t function[],
    size_t functions) {
    
    if(event >= functions) {
        log_message(ERROR_TYPE_LOOKUP_TABLE, 0, __FILE__, __LINE__,
            "\"%s\": supports %lu events, tried to access [%lu]\n",
            xuni->wtype->type[widget->type].name,
            (unsigned long)functions, (unsigned long)event);
        return;
    }
    
    if(function[event]) {
        (*function[event])(xuni, widget);
    }
}

/*! Makes sure that the widget \a widget is of type \a type. If it isn't,
    raises an error message and returns nonzero.
    
    Used where a specific widget type is expected, such as for widget event
    handler functions.
    
    \param xuni A pointer to the main xuni structure. The wtype member of this
        structure is used to determine the names of widgets, if the widget
        \a widget did not match the expected type.
    \param widget The widget to examine. Should be of type \a type.
    \param type The type that the widget \a widget ought to be.
    \param file The file in which the calling function is located. Should be
        \c __FILE__. (Used to report widget type mismatches.)
    \param line The line number that this function was called from. Should be
        \c __LINE__. (Used when the widget \a widget did not match the
        expected type.)
    \return Zero if the widget \a widget is of the expected type \a type,
        nonzero otherwise.
*/
int assert_widget_type(struct xuni_t *xuni, struct widget_t *widget,
    size_t type, const char *file, int line) {
    
    if(widget->type != type) {
        log_message(ERROR_TYPE_DATASTRUCT, 0, file, line,
            "Widget type mismatch: expected type %s, got type %s",
            get_widget_type_name(xuni, type),
            get_widget_type_name(xuni, widget->type));
        
        return 1;
    }
    
    return 0;
}

/*! Returns the visibility of \a widget, AND'ed with \a visibility. This means
    that the return value will be \a visibility if all of the OR'd visibility
    types in \a visibility are true.
    
    \param widget The widget to check for the visibilities in \a visibility.
    \param visibility The visibilities to check \a widget for.
    \return The visibility of the widget \a widget AND'ed with \a visibility.
*/
int get_widget_visibility(struct widget_t *widget,
    enum widget_visibility_t visibility) {
    
    if(!widget) return 0;
    
    return widget->visibility & visibility;
}

/*! Sets or unsets the visibility of the widget \a widget.
    \param widget The widget for which to set or unset the visibility of.
    \param visibility An OR'd value of widget_visibility_ts, describing what
        visibility type should be set or unset.
    \param set True or false, to set or unset the visibility \a visibility.
*/
void set_widget_visibility(struct widget_t *widget,
    enum widget_visibility_t visibility, int set) {
    
    if(!widget) return;
    
    set_bit_p(&widget->visibility, visibility, set);
}
