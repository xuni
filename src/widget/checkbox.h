/*! \file checkbox.h

*/

#ifndef XUNI_GUARD_CHECKBOX_H
#define XUNI_GUARD_CHECKBOX_H

#include "widgets.h"

#ifdef __cplusplus
extern "C" {
#endif

enum wid_checkbox_t {
    WID_CHECKBOX_BUTTON,
    WID_CHECKBOX_LABEL
};

struct checkbox_t {
    int checked;  /* True if the checkbox is checked. */
};

void checkbox_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event);

void init_checkbox(struct widget_t *widget, struct xuni_t *xuni,
    size_t font, const char *text, int checked);
void toggle_checkbox(struct widget_t *widget);
void set_checkbox(struct widget_t *widget, int checked);

#ifdef __cplusplus
}
#endif

#endif
