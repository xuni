/*! \file image.h

*/

#ifndef XUNI_GUARD_IMAGE_H
#define XUNI_GUARD_IMAGE_H

#include "widgets.h"

#ifdef __cplusplus
extern "C" {
#endif

/*! Specifies what to do with the original image, if it was loaded from a
    file, once the rescaled image has been generated from it.
*/
enum image_load_t {
    /*! Keep unrescaled images in memory. Usually not a good idea, as
        unrescaled images tend to be very large. On the other hand, it saves
        having to load the image from a file, which saves a lot of time.
    */
    IMAGE_LOAD_KEEP,
    
    /*! Free unrescaled images once they have been used to generate the
        rescaled images. This takes some extra time, to load and free the
        image whenever it needs rescaling. However, images are not generally
        rescaled very often. It also saves a lot of memory.
        
        This should be used unless images are rescaled very frequently.
    */
    IMAGE_LOAD_FREE
};

/*! Specifies when images should be rescaled -- at load time or only when
    absolutely necessary.
*/
enum image_rescale_t {
    /*! Always keep a rescaled version of this image ready to be blitted.
        Takes extra time when the xuni window is resized, for example, but
        eliminates the lag that can happen with IMAGE_RESCALE_LAZY.
    */
    IMAGE_RESCALE_ALWAYS,
    
    /*! Only load and rescale an image at the last possible time, i.e. just
        before it is blitted. This saves a lot of memory, in that images that
        do not need to be displayed at the moment are not rescaled. On the
        other hand, it can make a menu take quite a bit more time to repaint
        on the first frame.
        
        Also note that images are not freed, so if most images get displayed
        at one time or another, this will use just as much memory as
        IMAGE_RESCALE_ALWAYS.
    */
    IMAGE_RESCALE_LAZY
};

struct image_t {
    const char *filename;
    SDL_Surface *original, *image;
    double angle;
    
    struct {
        enum image_load_t load;
        enum image_rescale_t rescale;
    } keep;
};

void image_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event);

void init_image(struct widget_t *widget, const char *filename, double angle,
    enum image_load_t load, enum image_rescale_t rescale);

void prepare_paint_image(struct xuni_t *xuni, struct widget_t *widget);

#ifdef __cplusplus
}
#endif

#endif
