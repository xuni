/*! \file button.h

*/

#ifndef XUNI_GUARD_BUTTON_H
#define XUNI_GUARD_BUTTON_H

#include "widgets.h"

#ifdef __cplusplus
extern "C" {
#endif

enum wid_button_t {
    WID_BUTTON_BOX,
    WID_BUTTON_LABEL
};

struct button_t {
    int unused;
};

void button_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event);

struct widget_t *new_sub_label(struct widget_t *base, size_t font,
    const char *text);
void init_button(struct widget_t *widget, struct widget_t *image);
void init_button_image(struct xuni_t *xuni, struct widget_t *widget,
    struct widget_t *copy);

#ifdef __cplusplus
}
#endif

#endif
