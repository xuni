/*! \file box.h

*/

#ifndef XUNI_GUARD_BOX_H
#define XUNI_GUARD_BOX_H

#include "widgets.h"

#ifdef __cplusplus
extern "C" {
#endif

enum box_state_t {
    BOX_STATE_HOVER    = 1 << 0,
    BOX_STATE_ACTIVE   = 1 << 1
};

enum box_style_t {
    BOX_STYLE_NORMAL,
    BOX_STYLE_INVERTED,
    BOX_STYLE_ALWAYSIN,
    BOX_STYLE_ALWAYSOUT
};

struct box_t {
    enum box_style_t style;
    enum box_state_t state, allowstate;
};

void box_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event);

void init_box(struct widget_t *widget, enum box_style_t style,
    struct widget_t *selwidget);
void set_box_type(struct gui_t *gui, struct widget_t *widget,
    struct widget_t *box);
void paint_box_previous_state(struct xuni_t *xuni, struct widget_t *widget);

#ifdef __cplusplus
}
#endif

#endif
