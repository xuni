/*! \file combobox.c

*/

#include "../graphics.h"
#include "../memory.h"
#include "../error.h"
#include "dump.h"
#include "widgets.h"
#include "combobox.h"

static void free_combobox(struct xuni_t *xuni, struct widget_t *widget);
static void reposition_combobox(struct xuni_t *xuni, struct widget_t *widget);
static void rescale_combobox(struct xuni_t *xuni, struct widget_t *widget);
static void paint_combobox(struct xuni_t *xuni, struct widget_t *widget);

static double calculate_textbox_width(struct widget_t *widget,
    struct xuni_t *xuni);
static double calculate_textbox_height(struct widget_t *widget,
    struct xuni_t *xuni);

void combobox_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event) {
    
    static void (*function[])(struct xuni_t *xuni, struct widget_t *widget)
        = {
        
        free_combobox,
        0,
        paint_combobox,
        reposition_combobox,
        rescale_combobox
    };
    
    call_widget_event_func(xuni, widget, event, function,
        sizeof(function) / sizeof(*function));
}

static double calculate_textbox_width(struct widget_t *widget,
    struct xuni_t *xuni) {
    
    double ratio;
    
    ratio = (widget->pos->scale.w / 100.0 * xuni->smode->screen->w)
        / (widget->pos->scale.h / 100.0 * xuni->smode->screen->h);
    
    return (100.0 / 15.0) / (real_scale_width(widget));
}

static double calculate_textbox_height(struct widget_t *widget,
    struct xuni_t *xuni) {
    
    double ratio;
    
    ratio = (widget->pos->scale.w / 100.0 * xuni->smode->screen->w)
        / (widget->pos->scale.h / 100.0 * xuni->smode->screen->h);
    
    return (100.0 / 15.0) / (real_scale_height(widget));
}

void init_combobox(struct xuni_t *xuni, struct widget_t *widget,
    size_t font) {
    
    double boxwidth = calculate_textbox_width(widget, xuni);
    double height = calculate_textbox_height(widget, xuni);
    
    widget->type = WIDGET_COMBOBOX;
    widget->p.combobox = xuni_memory_allocate(sizeof(*widget->p.combobox));
    
    add_allocate_widget_compose(widget, "textbox");
    
    init_widget_pos(last_compose_widget(widget), 0, 0,
        100.0 - boxwidth, height, POS_PACK_NONE);
    init_textbox(last_compose_widget(widget), xuni, 0, font);
    
    add_allocate_widget_compose(widget, "button");
    
    init_widget_pos(last_compose_widget(widget), 100.0 - boxwidth, 0,
        boxwidth, height, POS_PACK_NONE);
    init_button(last_compose_widget(widget), 0);
    
    init_button_image(xuni, last_compose_widget(widget),
        get_theme_widget(xuni, THEME_SCROLL_DOWN));
    
    add_allocate_widget_compose(widget, "dropdownlistbox");
    
    init_widget_pos(last_compose_widget(widget), 0, height,
        100.0, 100.0 - height, POS_PACK_NONE);
    init_listbox(last_compose_widget(widget), xuni,
        SCROLLBAR_USE_BOTH, SCROLLBAR_USE_BOTH);
    
    last_compose_widget(widget)->visibility &= ~WIDGET_VISIBILITY_VISIBLE;
    /*last_compose_widget(widget)->visibility &= ~WIDGET_VISIBILITY_SELABLE;*/
}

static void free_combobox(struct xuni_t *xuni, struct widget_t *widget) {
    xuni_memory_free(widget->p.combobox);
}

void add_combobox_item(struct xuni_t *xuni, struct widget_t *widget,
    size_t font, const char *data) {
    
    if(!widget) return;
    
    if(widget->type != WIDGET_COMBOBOX) {
        log_message(ERROR_TYPE_DATASTRUCT, 0, __FILE__, __LINE__,
            "add_combobox_item() expected a combobox widget, got a %s",
            get_widget_type_name(xuni, widget->type));
    }
    
    add_listbox_item(xuni, widget->compose->widget[WID_COMBOBOX_DROPDOWN]
        ->compose->widget[WID_LISTBOX_DATA], font, data);
}

static void reposition_combobox(struct xuni_t *xuni, struct widget_t *widget) {

}

static void rescale_combobox(struct xuni_t *xuni, struct widget_t *widget) {

}

static void paint_combobox(struct xuni_t *xuni, struct widget_t *widget) {
    widget_event(xuni, widget->compose->widget[WID_COMBOBOX_TEXTBOX],
        WIDGET_EVENT_PAINT);
    widget_event(xuni, widget->compose->widget[WID_COMBOBOX_BUTTON],
        WIDGET_EVENT_PAINT);
    
    /*printf("paint_combobox(): %i items in drop down listbox\n",
        widget->compose->widget[WID_COMBOBOX_DROPDOWN]->compose->widgets);*/
    widget_event(xuni, widget->compose->widget[WID_COMBOBOX_DROPDOWN],
        WIDGET_EVENT_PAINT);
}
