/*! \file listbox.c

*/

#include <string.h>

#include "../graphics.h"
#include "../error.h"
#include "../memory.h"
#include "widgets.h"
#include "dump.h"
#include "listbox.h"
#include "box.h"
#include "label.h"
#include "scrollbar.h"
#include "panel.h"

static void resize_listbox_view(struct xuni_t *xuni, struct widget_t *widget,
    enum scrollbar_use_t scroll);
static void enable_scrollbar(struct xuni_t *xuni, struct widget_t *widget,
    enum scrollbar_use_t scroll);
static void check_scrollbars(struct xuni_t *xuni, struct widget_t *widget);
static int check_vertical_scrollbar(struct widget_t *widget);
static int check_horizontal_scrollbar(struct widget_t *widget);

static void calculate_vertical_scrollbar_max(struct xuni_t *xuni,
    struct widget_t *widget);
static void calculate_horizontal_scrollbar_max(struct xuni_t *xuni,
    struct widget_t *widget);

static void free_listbox(struct xuni_t *xuni, struct widget_t *widget);
static void reposition_listbox(struct xuni_t *xuni, struct widget_t *widget);
static void rescale_listbox(struct xuni_t *xuni, struct widget_t *widget);
static void paint_listbox(struct xuni_t *xuni, struct widget_t *widget);

void listbox_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event) {
    
    static void (*function[])(struct xuni_t *xuni, struct widget_t *widget)
        = {
        
        free_listbox,
        0,
        paint_listbox,
        reposition_listbox,
        rescale_listbox
    };
    
    call_widget_event_func(xuni, widget, event, function,
        sizeof(function) / sizeof(*function));
}

double real_scale_width(struct widget_t *widget) {
    double w = 1.0;
    
    if(!widget) return 100.0;
    
    w *= widget->pos->scale.w / 100.0;
    if(widget->base) w *= real_scale_width(widget->base);
    
    return w;
}

double real_scale_height(struct widget_t *widget) {
    double h = 1.0;
    
    if(!widget) return 100.0;
    
    h *= widget->pos->scale.h / 100.0;
    if(widget->base) h *= real_scale_height(widget->base);
    
    return h;
}

double calculate_scrollbar_width(struct widget_t *widget,
    struct xuni_t *xuni) {
    
    double ratio;
    
    ratio = (widget->pos->scale.w / 100.0 * xuni->smode->screen->w)
        / (widget->pos->scale.h / 100.0 * xuni->smode->screen->h);
    
    return (100.0 / 15.0) / (real_scale_width(widget));
}

double calculate_scrollbar_height(struct widget_t *widget,
    struct xuni_t *xuni) {
    
    return (100.0 / 15.0) / (real_scale_height(widget));
}

void init_listbox(struct widget_t *widget, struct xuni_t *xuni,
    enum scrollbar_use_t allow, enum scrollbar_use_t force) {
    
    double width = calculate_scrollbar_width(widget, xuni);
    double height = calculate_scrollbar_height(widget, xuni);
    double boxsize = get_box_width(xuni, xuni->theme->current);
    double boxsizeh = get_box_height(xuni, xuni->theme->current);
    int w = 0, h = 0;
    
    boxsize /= real_scale_width(widget);
    boxsizeh /= real_scale_height(widget);
    
    /*printf("%f %f\n", boxsize, boxsizeh);*/
    
    if(force & SCROLLBAR_USE_VERTICAL) w = width;
    if(force & SCROLLBAR_USE_HORIZONTAL) h = height;
    
    widget->type = WIDGET_LISTBOX;
    widget->p.listbox = xuni_memory_allocate(sizeof(*widget->p.listbox));
    
    /* make sure anything that is forced is allowed */
    allow |= force;
    
    widget->p.listbox->scroll.allow = allow;
    widget->p.listbox->scroll.force = force;
    widget->p.listbox->scroll.current = force;
    
    widget->p.listbox->maxwidth = 0;
    /*widget->p.listbox->yclip = 0;*/
    
    add_allocate_widget_compose(widget, "listbox area");
    
    init_widget_pos(last_compose_widget(widget),
        0, 0, 100.0 - w, 100.0 - h, POS_PACK_NONE);
    init_box(last_compose_widget(widget), BOX_STYLE_ALWAYSIN, 0);
    last_compose_widget(widget)->visibility &= ~WIDGET_VISIBILITY_INDEPENDENT;
    
    add_allocate_widget_compose(widget, "listbox vertical scrollbar");
    
    init_widget_pos(last_compose_widget(widget),
        100.0 - width, 0, width, 100.0 - h, POS_PACK_NONE);
    init_scrollbar(last_compose_widget(widget), xuni, widget->pos->scale.h,
        SCROLLBAR_ORIENTATION_VERTICAL);
    if((force & SCROLLBAR_USE_VERTICAL) == 0) {
        last_compose_widget(widget)->visibility
            &= ~WIDGET_VISIBILITY_VISIBLE;
    }
    
    add_allocate_widget_compose(widget, "listbox horizontal scrollbar");
    
    init_widget_pos(last_compose_widget(widget),
        0, 100.0 - height, 100.0 - w, height, POS_PACK_NONE);
    init_scrollbar(last_compose_widget(widget), xuni, widget->pos->scale.h,
        SCROLLBAR_ORIENTATION_HORIZONTAL);
    if((force & SCROLLBAR_USE_HORIZONTAL) == 0) {
        last_compose_widget(widget)->visibility
            &= ~WIDGET_VISIBILITY_VISIBLE;
    }
    
    add_allocate_widget_compose(widget, "listbox data");
    
    init_widget_pos(last_compose_widget(widget), boxsize, boxsizeh,
        100.0 - boxsize * 2 - w, 100.0 - boxsizeh * 2 - h, POS_PACK_NONE);
    init_panel(last_compose_widget(widget));
    last_compose_widget(widget)->visibility &= ~WIDGET_VISIBILITY_INDEPENDENT;
    
    last_compose_widget(widget)->visibility &= ~WIDGET_VISIBILITY_NOT_COMPOSE;
    
    widget->visibility &= ~WIDGET_VISIBILITY_NOT_COMPOSE;
}

/*static void enable_one_scrollbar(struct widget_t *widget,
    enum scrollbar_use_t scroll, enum scrollbar_use_t which) {
    
    if((scroll & which) != (widget->p.listbox->scroll.current & which)) {
        set_bit_p(&widget->compose->widget[WID_LI scroll & which
    }
    
    set_bit_p(&widget->p.listbox->scroll.current, which, scroll & which);
}*/

static void resize_listbox_view(struct xuni_t *xuni, struct widget_t *widget,
    enum scrollbar_use_t scroll) {
    
    double width = calculate_scrollbar_width(widget, xuni);
    double height = calculate_scrollbar_height(widget, xuni);
    double boxsize = get_box_width(xuni, xuni->theme->current);
    double boxsizeh = get_box_height(xuni, xuni->theme->current);
    int w = 0, h = 0;
    
    boxsize /= real_scale_width(widget);
    boxsizeh /= real_scale_height(widget);
    
    if(scroll & SCROLLBAR_USE_VERTICAL) w = width;
    if(scroll & SCROLLBAR_USE_HORIZONTAL) h = height;
    
    /* make sure anything that is forced is allowed */
    scroll |= widget->p.listbox->scroll.force;
    
    init_widget_pos(widget->compose->widget[WID_LISTBOX_BOX],
        0, 0, 100.0 - w, 100.0 - h, POS_PACK_NONE);
    
    init_widget_pos(widget->compose->widget[WID_LISTBOX_VSCROLL],
        100.0 - width, 0, width, 100.0 - h, POS_PACK_NONE);
    
    init_widget_pos(widget->compose->widget[WID_LISTBOX_HSCROLL],
        0, 100.0 - height, 100.0 - w, height, POS_PACK_NONE);
    
    init_widget_pos(widget->compose->widget[WID_LISTBOX_DATA],
        boxsize, boxsizeh,
        100.0 - boxsize * 2 - w, 100.0 - boxsizeh * 2 - h, POS_PACK_NONE);
    
    widget_compose_event(xuni, widget->compose->widget[WID_LISTBOX_DATA],
        WIDGET_EVENT_RESCALE);
}

static void enable_scrollbar(struct xuni_t *xuni, struct widget_t *widget,
    enum scrollbar_use_t scroll) {
    
    scroll |= widget->p.listbox->scroll.force;
    
    /*printf("Set scroll = v:%i h:%i\n", scroll & SCROLLBAR_USE_VERTICAL,
        scroll & SCROLLBAR_USE_HORIZONTAL);*/
    
    if((scroll & SCROLLBAR_USE_VERTICAL)
        != (widget->p.listbox->scroll.current & SCROLLBAR_USE_VERTICAL)) {
        
        set_bit_p(&widget->compose->widget[WID_LISTBOX_VSCROLL]->visibility,
            WIDGET_VISIBILITY_VISIBLE, scroll & SCROLLBAR_USE_VERTICAL);
    }
    
    if((scroll & SCROLLBAR_USE_HORIZONTAL)
        != (widget->p.listbox->scroll.current & SCROLLBAR_USE_HORIZONTAL)) {
        
        set_bit_p(&widget->compose->widget[WID_LISTBOX_HSCROLL]->visibility,
            WIDGET_VISIBILITY_VISIBLE, scroll & SCROLLBAR_USE_HORIZONTAL);
    }
    
    if(widget->p.listbox->scroll.current != scroll) {
        resize_listbox_view(xuni, widget, scroll);
    }
    
    /* redundant -- set by resize_listbox_view() */
    widget->p.listbox->scroll.current = scroll;
}

static void check_scrollbars(struct xuni_t *xuni, struct widget_t *widget) {
    enum scrollbar_use_t use = SCROLLBAR_USE_NONE;
    
    if(check_vertical_scrollbar(widget)) use |= SCROLLBAR_USE_VERTICAL;
    if(check_horizontal_scrollbar(widget)) use |= SCROLLBAR_USE_HORIZONTAL;
    
    enable_scrollbar(xuni, widget, use);
}

static int check_vertical_scrollbar(struct widget_t *widget) {
    struct widget_t *area = widget->compose->widget[WID_LISTBOX_DATA];
    struct widget_t *last = last_compose_widget(area);
    
    if(!last) return 0;
    
    if(last && last->pos->real.y + last->pos->real.h > area->pos->real.h) {
        return 1;
    }
    
    return 0;
}

/* !!! very inefficient, especially since the same sort of thing is done when
    scrollbars are scrolled
*/
static int check_horizontal_scrollbar(struct widget_t *widget) {
    struct widget_t *area = widget->compose->widget[WID_LISTBOX_DATA];
    struct widget_t *widest = last_compose_widget(area);
    size_t x;
    int width, w;
    
    if(area->compose) {
        for(x = 0; x < area->compose->widgets; x ++) {
            width = area->compose->widget[x]->pos->real.w;
            
            if(area->compose->widget[x]->pos->real.w > area->pos->real.w) {
                return 1;
            }
        }
    }
    
    return 0;
}

void add_listbox_item(struct xuni_t *xuni, struct widget_t *widget,
    size_t font, const char *data) {
    
    struct widget_t *prev = last_compose_widget(widget);
    const char *p;
    int x, y;
    
    if(prev && prev->pos) {
        x = prev->pos->scale.x;
        y = prev->pos->scale.y;  /* !!! */
        x = 0;
        y = 0;
    }
    else {
        x = 0 /*widget->pos->scale.x*/;
        y = 0 /*widget->pos->scale.y*/;
    }
    
    add_allocate_widget_compose(widget, "listbox item");
    
    init_widget_pos(last_compose_widget(widget),
        x, y, 0, 0, POS_PACK_TOP);
    
    p = xuni_memory_duplicate_string(data);
    init_label(last_compose_widget(widget),
        font, p, LABEL_ALIGN_LEFT, 255, 255, 255);
    xuni_memory_decrement((void *)p);
    
    last_compose_widget(widget)->visibility &= ~WIDGET_VISIBILITY_INDEPENDENT;
    
    widget_event(xuni, last_compose_widget(widget), WIDGET_EVENT_RESCALE);
}

struct widget_t *listbox_sel_item(struct widget_t *widget) {
    struct widget_t *data = widget->compose->widget[WID_LISTBOX_DATA];
    size_t x;
    
    if(!data->compose) return 0;
    
    for(x = 0; x < data->compose->widgets; x ++) {
        if(data->compose->widget[x]->sel) {
            return data->compose->widget[x];
        }
    }
    
    /*log_message(ERROR_TYPE_WARNING, 0, __FILE__, __LINE__,
        "No listbox item selected");
    print_widget_backtrace(widget);*/
    
    return 0;
}

static void calculate_vertical_scrollbar_max(struct xuni_t *xuni,
    struct widget_t *widget) {
    
    struct widget_t *area = widget->compose->widget[WID_LISTBOX_DATA];
    struct widget_t *first;
    struct widget_t *last = last_compose_widget(area);
    int max, height;
    
    if(last) {
        first = area->compose->widget[0];
        
        height = get_box_height(xuni, xuni->theme->current) / 100.0
            * xuni->smode->height;
        
        max = ((last->pos->real.y - area->pos->real.y)
                - (first->pos->real.y - area->pos->real.y)
                + last->pos->real.h)
            - (area->pos->real.h - last->pos->real.h) - height * 2;
    }
    else max = 0;
    
    set_scrollbar_max(xuni, widget->compose->widget[WID_LISTBOX_VSCROLL],
        max);
}

/* !!! recording the widest label might be a good idea */
static void calculate_horizontal_scrollbar_max(struct xuni_t *xuni,
    struct widget_t *widget) {
    
    struct widget_t *area = widget->compose->widget[WID_LISTBOX_DATA];
    struct widget_t *widest = last_compose_widget(area);
    size_t x;
    int width, w;
    
    widget->p.listbox->maxwidth = 0;
    
    if(area->compose) {
        for(x = 1; x < area->compose->widgets; x ++) {
            width = area->compose->widget[x]->pos->real.w;
            
            if(width > widest->pos->real.w) {
                widest = area->compose->widget[x];
            }
        }
        
        widget->p.listbox->maxwidth = widest->pos->real.w;
        
        w = get_box_width(xuni, xuni->theme->current) / 100.0
            * xuni->smode->width;
        
        widget->p.listbox->maxwidth -= area->pos->real.w;
    }
    
    if(widget->p.listbox->maxwidth < 0) widget->p.listbox->maxwidth = 0;
    
    set_scrollbar_max(xuni, widget->compose->widget[WID_LISTBOX_HSCROLL],
        widget->p.listbox->maxwidth);
}

void listbox_calculate_scrollbar_max(struct xuni_t *xuni,
    struct widget_t *widget) {
    
    calculate_vertical_scrollbar_max(xuni, widget);
    calculate_horizontal_scrollbar_max(xuni, widget);
}

static void free_listbox(struct xuni_t *xuni, struct widget_t *widget) {
    xuni_memory_free(widget->p.listbox);
}

static void paint_listbox(struct xuni_t *xuni, struct widget_t *widget) {
    /*size_t x, start = 3;*/
    /*int height, width;*/
    int yclip;
    
    widget_event(xuni, widget->compose->widget[WID_LISTBOX_BOX],
        WIDGET_EVENT_PAINT);
    
    /*height = get_box_height(xuni, xuni->theme->current) / 100.0
        * xuni->smode->height;
    width = get_box_width(xuni, xuni->theme->current) / 100.0
        * xuni->smode->width;*/
    
    yclip = get_scrollbar_pos_int(
        widget->compose->widget[WID_LISTBOX_VSCROLL]);
    /*wclip = widget->compose->widget[WID_LISTBOX_BOX]->pos->real.w - width * 2;
    hclip = widget->compose->widget[WID_LISTBOX_BOX]->pos->real.h;*/
    
#if !1
    /*clear_widget_clip(xuni, widget->compose->widget[WID_LISTBOX_DATA]);*/
    
    /* !!! why does +width work but +height not? */
    add_widget_clip(xuni, widget->compose->widget[WID_LISTBOX_DATA],
        0, -yclip + width, 0, yclip /*+ width*/, /*-width * 2*/0, 0);
#endif
    
    widget_event(xuni, widget->compose->widget[WID_LISTBOX_DATA],
        WIDGET_EVENT_PAINT);
    
#if 0
    /* !!! why does +width work but +height not? */
    add_widget_clip(xuni, widget->compose->widget[WID_LISTBOX_DATA],
        0, -(-yclip + width), 0, -yclip /*+ width*/, /*-width * 2*/0, 0);
#endif
    
    widget_event(xuni, widget->compose->widget[WID_LISTBOX_VSCROLL],
        WIDGET_EVENT_PAINT);
    widget_event(xuni, widget->compose->widget[WID_LISTBOX_HSCROLL],
        WIDGET_EVENT_PAINT);
}

static void reposition_listbox(struct xuni_t *xuni, struct widget_t *widget) {
    int yclip = get_scrollbar_pos_int(
        widget->compose->widget[WID_LISTBOX_VSCROLL]);
    int xclip = get_scrollbar_pos_int(
        widget->compose->widget[WID_LISTBOX_HSCROLL]);
    
    check_scrollbars(xuni, widget);
    
    listbox_calculate_scrollbar_max(xuni, widget);
    
    clear_widget_clip(xuni, widget->compose->widget[WID_LISTBOX_DATA]);
    
    add_panel_clip(xuni, widget->compose->widget[WID_LISTBOX_DATA],
        -xclip, -yclip, xclip, yclip, 0, 0);
    /*add_widget_clip(xuni, widget->compose->widget[WID_LISTBOX_DATA],
        width, 0, 0, 0, -width * 2, 0);*/
}

static void rescale_listbox(struct xuni_t *xuni, struct widget_t *widget) {
#if 0
    double width = calculate_scrollbar_width(widget, xuni);
    double boxsize = (get_box_width(xuni->theme->current) / 100.0) * 2.0
        * widget->pos->real.w;
    double boxsizeh = (get_box_height(xuni->theme->current) / 100.0) * 2.0
        * widget->pos->real.h;
    /*double boxsizeh = get_box_height(xuni->theme->current)
        * (get_box_height(xuni->theme->current) / 100.0);*/
    struct widget_t *data = widget->compose->widget[WID_LISTBOX_DATA];
    
    data->pos->scale.x = boxsize;
    data->pos->scale.y = boxsizeh;
    data->pos->scale.w = 100.0 - width - boxsize * 2;
    data->pos->scale.h = 100.0 - boxsizeh * 2;
    
    widget_compose_event(xuni, widget, WIDGET_EVENT_RESCALE);
#endif
}
