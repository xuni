/*! \file combobox.h

*/

#ifndef XUNI_GUARD_COMBOBOX_H
#define XUNI_GUARD_COMBOBOX_H

#include "widgets.h"

#ifdef __cplusplus
extern "C" {
#endif

enum wid_combobox_t {
    WID_COMBOBOX_TEXTBOX,
    WID_COMBOBOX_BUTTON,
    WID_COMBOBOX_DROPDOWN
};

struct combobox_t {
    int unused;
};

void combobox_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event);

void init_combobox(struct xuni_t *xuni, struct widget_t *widget,
    size_t font);
void add_combobox_item(struct xuni_t *xuni, struct widget_t *widget,
    size_t font, const char *data);

#ifdef __cplusplus
}
#endif

#endif
