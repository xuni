/*! \file image_tile.h

*/

#ifndef XUNI_GUARD_IMAGE_TILE_H
#define XUNI_GUARD_IMAGE_TILE_H

#include "widgets.h"

#ifdef __cplusplus
extern "C" {
#endif

enum wid_image_tile_t {
    WID_IMAGE_TILE_IMAGE
};

struct image_tile_t {
    int xinc, yinc, xpos, ypos;
};

void image_tile_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event);

void init_image_tile(struct widget_t *widget, const char *filename,
    int xinc, int yinc);

#ifdef __cplusplus
}
#endif

#endif
