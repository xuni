/*! \file scrollbar.h

*/

#ifndef XUNI_GUARD_SCROLLBAR_H
#define XUNI_GUARD_SCROLLBAR_H

#include "widgets.h"

#ifdef __cplusplus
extern "C" {
#endif

enum wid_scrollbar_t {
    WID_SCROLLBAR_SEEKBAR,
    WID_SCROLLBAR_UP,
    WID_SCROLLBAR_SEEK,
    WID_SCROLLBAR_DOWN
};

enum scrollbar_orientation_t {
    SCROLLBAR_ORIENTATION_VERTICAL,
    SCROLLBAR_ORIENTATION_HORIZONTAL
};

struct scrollbar_t {
    double pos;
    int max;
    enum scrollbar_orientation_t orientation;
};

void scrollbar_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event);

void init_scrollbar(struct widget_t *widget, struct xuni_t *xuni, int max,
    enum scrollbar_orientation_t orientation);
void set_scrollbar_max(struct xuni_t *xuni, struct widget_t *widget,
    int max);
void move_scrollbar(struct xuni_t *xuni, struct widget_t *widget,
    int amount);

double get_scrollbar_pos(struct widget_t *widget);
void set_scrollbar_pos(struct widget_t *widget, double pos);
int get_scrollbar_pos_int(struct widget_t *widget);

#ifdef __cplusplus
}
#endif

#endif
