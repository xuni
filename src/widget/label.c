/*! \file label.c

*/

#include <string.h>
#include <stdlib.h>

#include "SDL_gfxPrimitives.h"

#include "../graphics.h"
#include "../memory.h"
#include "font.h"
#include "widgets.h"
#include "label.h"
#include "theme.h"

static void free_label(struct xuni_t *xuni, struct widget_t *widget);
static void reposition_label(struct xuni_t *xuni, struct widget_t *widget);
static void rescale_label(struct xuni_t *xuni, struct widget_t *widget);
static void paint_label(struct xuni_t *xuni, struct widget_t *widget);
static void update_text_label(struct xuni_t *xuni, struct widget_t *widget);

static void get_label_image_pos(struct xuni_t *xuni, struct widget_t *widget,
    int *x, int *y);
static int label_pos_to_width(struct xuni_t *xuni, struct widget_edit_t *edit,
    struct widget_t *font);
static void paint_label_clip(SDL_Surface *image, SDL_Surface *screen,
    int x, int y, struct clip_pos_t *clip);
static void paint_edit_label(struct xuni_t *xuni, struct widget_edit_t *edit,
    struct widget_t *font, int x, int y);

void label_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event) {
    
    static void (*function[])(struct xuni_t *xuni, struct widget_t *widget)
        = {
        
        free_label,
        update_text_label,
        paint_label,
        reposition_label,
        rescale_label
    };
    
    call_widget_event_func(xuni, widget, event, function,
        sizeof(function) / sizeof(*function));
}

void init_label(struct widget_t *widget, size_t font, const char *data,
    enum label_type_t type, Uint8 r, Uint8 g, Uint8 b) {
    
    widget->type = WIDGET_LABEL;
    widget->p.label = xuni_memory_allocate(sizeof(*widget->p.label));
    
    widget->p.label->type = type;
    xuni_memory_increment((void *)data);
    widget->p.label->text = data;
    widget->p.label->font = font;
    widget->p.label->label = 0;
    widget->p.label->scroll = 0;
    
    widget->p.label->col.r = r;
    widget->p.label->col.g = g;
    widget->p.label->col.b = b;
}

static void free_label(struct xuni_t *xuni, struct widget_t *widget) {
    if(xuni_memory_decrement(widget->p.label)) {
        free_surface(widget->p.label->label);
        
        xuni_memory_free((void *)widget->p.label->text);
        
        free(widget->p.label);
    }
}

static void get_label_image_pos(struct xuni_t *xuni, struct widget_t *widget,
    int *x, int *y) {
    
    const SDL_Rect *real = &widget->pos->real;
    int height
        = font_height(xuni, get_theme_widget(xuni, widget->p.label->font));
    int width = 0;
    
    if(widget->p.label->label) {
        width = widget->p.label->label->w;
    }
    
    switch(widget->p.label->type) {
    case LABEL_ALIGN_LEFT:
        *y += (real->h - height) / 2;
        break;
    case LABEL_ALIGN_CENTRE:
        *x += (real->w - width) / 2;
        *y += (real->h - height) / 2;
        break;
    case LABEL_ALIGN_RIGHT:
        *x += real->w - width;
        *y += (real->h - height) / 2;
        break;
    default:
        printf("*** Unknown label type: %i\n", (int)widget->p.label->type);
        break;
    }
}

static void paint_label_clip(SDL_Surface *image, SDL_Surface *screen,
    int x, int y, struct clip_pos_t *clip) {
    
    if(!image) return;
    
    if(!clip) {
        blit_surface(screen, image, x, y);
    }
    else {
        blit_surface_area(screen, image,
            x + clip->xoff,
            y + clip->yoff,
            clip->xclip,
            clip->yclip,
            clip->wclip,
            clip->hclip);
    }
}

static int label_pos_to_width(struct xuni_t *xuni, struct widget_edit_t *edit,
    struct widget_t *font) {
    
    int w, swap;
    char temp;
    
    if(!edit->data->text || !*edit->data->text) return 0;
    
    if(edit->pos != edit->len) swap = 1;
    
    if(swap) {
        temp = edit->data->text[edit->pos];
        ((char *)edit->data->text)[edit->pos] = 0;
    }
    
    /* !!! if pos->real.w is set by reposition_label(), it could be used here
        when edit->pos == edit->len
    */
    w = font_string_width(xuni, font, edit->data->text);
    
    if(swap) {
        ((char *)edit->data->text)[edit->pos] = temp;
    }
    
    return w;
}

/* !!! very inefficient */
size_t width_to_label_pos(struct xuni_t *xuni, int pos, struct widget_t *font,
    char *data) {
    
    int w = 0, prevw;
    char temp;
    size_t len = strlen(data), x = len + 1;
    
    do {
        x --;
        temp = data[x];
        data[x] = 0;
        
        prevw = w;
        w = font_string_width(xuni, font, data);
        
        /*printf("is %i>%i for \"%s\"?\n", w, pos, data);*/
        
        data[x] = temp;
    } while(w > pos && x);
    
    /*printf("%i<=%i<=%i: %i %i (%i)\n", prevw, pos, w,
        abs(pos - prevw), abs(pos - w), (int)x);*/
    
    /* Because <= is used instead of <, it opts to move the cursor to the
        right when the exact centre of a character is clicked on. This only
        matters for characters that are an odd number of pixels wide. */
    if(prevw - pos <= pos - w) x ++;
    
    if(x == len + 1) x --;
    
    return x;
}

static void paint_edit_label(struct xuni_t *xuni, struct widget_edit_t *edit,
    struct widget_t *font, int x, int y) {
    
    int height, width;
    
    width = label_pos_to_width(xuni, edit, font);
    height = font_height(xuni, font);
    /*vlineRGBA(smode->screen, x + widget->pos->real.w + 1,
        y, y + widget->pos->real.h, 255, 255, 255, 255);*/
    /*printf("width=%i, height=%i, pos=(%i,%i)\n", width, height, x, y);*/
    vlineRGBA(xuni->smode->screen, x + width, y, y + height,
        255, 255, 255, 255);
}

static void paint_label(struct xuni_t *xuni, struct widget_t *widget) {
    int x = 0, y = 0;
    
    /*if(widget->sel) {
        SDL_FillRect(xuni->smode->screen, &widget->pos->real,
            SDL_MapRGB(xuni->smode->screen->format, 0, 0, 128));
    }*/
    
    /*if(widget->p.label->label->h !=
        font_height(xuni, get_theme_widget(xuni, widget->p.label->font))) {
        
        printf("Not equal!\n");
    }*/
    
    get_label_image_pos(xuni, widget, &x, &y);
    
    x += widget->pos->real.x;
    y += widget->pos->real.y;
    
    paint_label_clip(widget->p.label->label, xuni->smode->screen,
        x, y, widget->pos->clip);
    
    /* use xuni->gui->edit.datawidget instead of xuni->gui->active.widget? */
    if(widget->base && widget->base->type == WIDGET_TEXTBOX
        && xuni->gui->active.widget == widget->base) {
        
        if(widget->pos->clip) {
            x -= widget->pos->clip->xclip;
            x += widget->pos->clip->xoff;
            y += widget->pos->clip->yoff;
        }
        
        paint_edit_label(xuni, &xuni->gui->edit,
            get_theme_widget(xuni, widget->p.label->font), x, y);
    }
}

void reposition_label_data(struct xuni_t *xuni, struct label_t *label,
    struct widget_edit_t *edit, size_t pos) {
    
    int poswidth = label_pos_to_width(xuni, edit,
        get_theme_widget(xuni, label->font));
    double boxwidth = get_box_width(xuni, xuni->theme->current) / 100.0
        * xuni->smode->width;
    int labelw, offset, newleft, basewidth;
    
    /* !!! would use widget->p.label->label->w, but the label is not resized, nor pos->real.w set */
    if(label->text) {
        labelw = font_string_width(xuni, 
            get_theme_widget(xuni, label->font), label->text);
    }
    else labelw = 0;
    
    offset = labelw - edit->datawidget->base->pos->real.w + boxwidth * 2;
    
    /*printf("reposition_label_data() offset=%i, poswidth=%i, labelw=%i\n",
        offset, poswidth, labelw);*/
    
    if(offset > 0) {
        if(poswidth < offset) newleft = poswidth;
        else newleft = offset;
    }
    else {
        /* all of the text in the textbox fits into one view */
        newleft = 0;
    }
    
    basewidth = edit->datawidget->base->pos->real.w - boxwidth * 2;
    
    /* If the new left position is out of range of the displayed one, set the
        displayed left position so that newleft is visible.
    */
    if(poswidth < edit->datawidget->base->p.textbox->leftpos) {
        edit->datawidget->base->p.textbox->leftpos = poswidth;
    }
    else if(poswidth > edit->datawidget->base->p.textbox->leftpos
        + basewidth) {
        
        edit->datawidget->base->p.textbox->leftpos = poswidth - basewidth;
    }
    
    /*poswidth = edit->leftpos;*/
    
    if(offset > edit->datawidget->base->p.textbox->leftpos) {
        /*printf("offset by %i\n", offset - poswidth);*/
        add_widget_clip(xuni, edit->datawidget, 0, 0,
            -(offset - edit->datawidget->base->p.textbox->leftpos), 0, 0, 0);
    }
    
    /*if(offset > 0) {
        add_widget_clip(xuni, edit->datawidget, 0, 0, offset, 0, 0, 0);
    }*/
    
    /*add_widget_clip(widget, width, 0, 0, 0, -width * 2, 0);*/
}

static void reposition_label(struct xuni_t *xuni, struct widget_t *widget) {
    int offset;
    int scroll = 0;
    
    /* !!! hack to allow textbox scrolling */
    if(/*widget->p.label->label &&*/ widget->base && widget->base->pos) {
        if(widget->base->type == WIDGET_TEXTBOX) scroll = 1;
        
        if(widget->base->type == WIDGET_PANEL && widget->base->base
            && widget->base->base->type == WIDGET_LISTBOX) {
            
            /*scroll = 1;*/
        }
    }
    
    if(scroll) {
        double width = get_box_width(xuni, xuni->theme->current) / 100.0
            * xuni->smode->width;
        int labelw;
        
        /* !!! would use widget->p.label->label->w, but the label is not resized, nor pos->real.w set */
        if(widget->p.label->text) {
            labelw = font_string_width(xuni,
                get_theme_widget(xuni, widget->p.label->font),
                widget->p.label->text);
        }
        else labelw = 0;
        
        offset = labelw - widget->base->pos->real.w + width * 2;
        /*printf("base width: %i\n", widget->base->pos->real.w);
        print_inline_widget_backtrace(widget);*/
        
        if(offset > 0) {
            add_widget_clip(xuni, widget, 0, 0, offset, 0, 0, 0);
        }
        
        add_widget_clip(xuni, widget, width, 0, 0, 0, -width * 2, 0);
        
        if(offset < 0) widget->pos->real.w = labelw;
        else {
            widget->pos->real.w = widget->base->pos->real.w - width * 2;
        }
        
        if(widget == xuni->gui->edit.datawidget) {
            reposition_label_data(xuni, xuni->gui->edit.data,
                &xuni->gui->edit, xuni->gui->edit.pos);
        }
        /*else {
            add_widget_clip(xuni, widget, 0, 0,
                -widget->base->p.textbox->leftpos, 0, 0, 0);
        }*/
        
        /* !!! too small most likely */
        /*widget->pos->real.h = font_height(widget->p.label->font);*/
    }
}

static void rescale_label(struct xuni_t *xuni, struct widget_t *widget) {
    /*{
        static int count = 0;
        
        printf("rescale_label() #%3i \"%s\"\n", ++count,
            widget->p.label->text);
    }*/
    
    /*if(widget->p.label->font) {*/
    
    free_surface(widget->p.label->label);
    widget->p.label->label
        = render_text(xuni,
            get_theme_widget(xuni, widget->p.label->font),
            widget->p.label->text,
            widget->p.label->col.r,
            widget->p.label->col.g,
            widget->p.label->col.b);
    
#if !1
    /* it's better to do this in font.c */
    if(widget->p.label->label) {
        SDL_Surface *temp = widget->p.label->label;
        double ratio, xratio, yratio;
        
        ratio = (xuni->smode->screen->w / (double)xuni->smode->screen->h)
            / get_font_ratio(xuni);
        xratio = (ratio > 1.0) ? ratio : 1.0;
        yratio = (ratio < 1.0) ? 1.0 / ratio : 1.0;
        
        widget->p.label->label = zoomSurface(temp,
            /*widget->pos->scale.w / 100.0
                / ((double)xuni->smode->screen->w / widget->base->pos->real.w)
                * xuni->smode->screen->w / temp->w*/ xratio,
            /*widget->pos->scale.h / 100.0
                / ((double)xuni->smode->screen->h / widget->base->pos->real.h)
                * xuni->smode->screen->h / temp->h*/ yratio,
            SMOOTHING_ON);
        
        SDL_FreeSurface(temp);
    }
#endif
    
    /*widget->pos->scale.w
        = widget->p.label->label->w * 100.0 / smode->width;
    widget->pos->scale.h
        = widget->p.label->label->h * 100.0 / smode->height;*/
    
    if(!widget->pos->scale.w && !widget->pos->scale.h
        && widget->p.label->label) {
        
        widget->pos->real.w = widget->p.label->label->w;
        widget->pos->real.h = widget->p.label->label->h;
    }
}

static void update_text_label(struct xuni_t *xuni, struct widget_t *widget) {
    rescale_label(xuni, widget);
}
