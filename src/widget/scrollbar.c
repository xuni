/*! \file scrollbar.c

*/

#include "../graphics.h"
#include "../memory.h"
#include "../error.h"
#include "dump.h"
#include "widgets.h"
#include "scrollbar.h"
#include "box.h"
#include "button.h"

static void init_vertical_scrollbar(struct widget_t *widget,
    struct xuni_t *xuni);
static void init_horizontal_scrollbar(struct widget_t *widget,
    struct xuni_t *xuni);

static void free_scrollbar(struct xuni_t *xuni, struct widget_t *widget);
static void reposition_scrollbar(struct xuni_t *xuni,
    struct widget_t *widget);
static void rescale_scrollbar(struct xuni_t *xuni, struct widget_t *widget);
static void paint_scrollbar(struct xuni_t *xuni, struct widget_t *widget);

static double calculate_seek_height(struct widget_t *widget,
    struct xuni_t *xuni);
static double calculate_seek_width(struct widget_t *widget,
    struct xuni_t *xuni);

void scrollbar_widget_event(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_event_t event) {
    
    static void (*function[])(struct xuni_t *xuni, struct widget_t *widget)
        = {
        
        free_scrollbar,
        0,
        paint_scrollbar,
        reposition_scrollbar,
        rescale_scrollbar
    };
    
    call_widget_event_func(xuni, widget, event, function,
        sizeof(function) / sizeof(*function));
}

void init_scrollbar(struct widget_t *widget, struct xuni_t *xuni, int max,
    enum scrollbar_orientation_t orientation) {
    
    widget->type = WIDGET_SCROLLBAR;
    widget->p.scrollbar = xuni_memory_allocate(sizeof(*widget->p.scrollbar));
    
    widget->p.scrollbar->pos = 0;
    widget->p.scrollbar->max = max;
    widget->p.scrollbar->orientation = orientation;
    
    if(orientation == SCROLLBAR_ORIENTATION_VERTICAL) {
        init_vertical_scrollbar(widget, xuni);
    }
    else if(orientation == SCROLLBAR_ORIENTATION_HORIZONTAL) {
        init_horizontal_scrollbar(widget, xuni);
    }
    else {
        printf("Unsupported scrollbar orientation: %i\n", (int)orientation);
    }
}

static void init_vertical_scrollbar(struct widget_t *widget,
    struct xuni_t *xuni) {
    
    double height = calculate_scrollbar_height(widget, xuni) * 0.75;
    /*printf("height:%f csh:%f\n", height,
        calculate_scrollbar_height(widget, xuni));*/
    
    /* seek bar */
    add_allocate_widget_compose(widget, "alwaysinbox");
    
    init_widget_pos(last_compose_widget(widget), 0, height,
        100.0, 100.0 - height * 2, POS_PACK_NONE);
    init_box(last_compose_widget(widget), BOX_STYLE_ALWAYSIN, 0);
    
    /*last_compose_widget(widget)->visibility &= ~WIDGET_VISIBILITY_INDEPENDENT;*/
    
    /* up button */
    add_allocate_widget_compose(widget, "less");
    
    init_widget_pos(last_compose_widget(widget), 0, 0, 100.0, height,
        POS_PACK_NONE);
    init_button(last_compose_widget(widget), 0);
    
    init_button_image(xuni, last_compose_widget(widget),
        get_theme_widget(xuni, THEME_SCROLL_UP));
    
    /*last_compose_widget(widget)->visibility &= ~WIDGET_VISIBILITY_INDEPENDENT;*/
    
    /* seek button */
    add_allocate_widget_compose(widget, "seek");
    
    init_widget_pos(last_compose_widget(widget), 0, height, 100.0, height,
        POS_PACK_NONE);
    init_button(last_compose_widget(widget), 0);
    
    /*last_compose_widget(widget)->visibility &= ~WIDGET_VISIBILITY_INDEPENDENT;*/
    
    /* down button */
    add_allocate_widget_compose(widget, "more");
    
    init_widget_pos(last_compose_widget(widget), 0, 100.0 - height,
        100.0, height, POS_PACK_NONE);
    init_button(last_compose_widget(widget), 0);
    
    init_button_image(xuni, last_compose_widget(widget),
        get_theme_widget(xuni, THEME_SCROLL_DOWN));
    
    /*last_compose_widget(widget)->visibility &= ~WIDGET_VISIBILITY_INDEPENDENT;
    
    widget->visibility &= ~WIDGET_VISIBILITY_NOT_COMPOSE;*/
}

static void init_horizontal_scrollbar(struct widget_t *widget,
    struct xuni_t *xuni) {
    
    double width = calculate_scrollbar_width(widget, xuni) * 0.75;
    
    /* seek bar */
    add_allocate_widget_compose(widget, "alwaysinbox");
    
    init_widget_pos(last_compose_widget(widget), width, 0,
        100.0 - width * 2, 100.0, POS_PACK_NONE);
    init_box(last_compose_widget(widget), BOX_STYLE_ALWAYSIN, 0);
    
    /* left button */
    add_allocate_widget_compose(widget, "less");
    
    init_widget_pos(last_compose_widget(widget), 0, 0, width, 100.0,
        POS_PACK_NONE);
    init_button(last_compose_widget(widget), 0);
    
    init_button_image(xuni, last_compose_widget(widget),
        get_theme_widget(xuni, THEME_SCROLL_LEFT));
    
    /* seek button */
    add_allocate_widget_compose(widget, "seek");
    
    init_widget_pos(last_compose_widget(widget), width, 0, width, 100.0,
        POS_PACK_NONE);
    init_button(last_compose_widget(widget), 0);
    
    /* right button */
    add_allocate_widget_compose(widget, "more");
    
    init_widget_pos(last_compose_widget(widget), 100.0 - width, 0,
        width, 100.0, POS_PACK_NONE);
    init_button(last_compose_widget(widget), 0);
    
    init_button_image(xuni, last_compose_widget(widget),
        get_theme_widget(xuni, THEME_SCROLL_RIGHT));
}

static void free_scrollbar(struct xuni_t *xuni, struct widget_t *widget) {
    xuni_memory_free(widget->p.scrollbar);
}

static void paint_scrollbar(struct xuni_t *xuni, struct widget_t *widget) {
    widget_event(xuni, widget->compose->widget[WID_SCROLLBAR_SEEKBAR],
        WIDGET_EVENT_PAINT);
    
    /*set_widget_clip(widget->compose->widget[WID_SCROLLBAR_UP]
        ->compose->widget[WID_BUTTON_LABEL],
        widget->compose->widget[WID_SCROLLBAR_UP]->pos->real.x,
        widget->compose->widget[WID_SCROLLBAR_UP]->pos->real.y,
        0, 0,
        widget->pos->real.w,
        widget->pos->real.h);*/
    
    widget_event(xuni, widget->compose->widget[WID_SCROLLBAR_UP],
        WIDGET_EVENT_PAINT);
    widget_event(xuni, widget->compose->widget[WID_SCROLLBAR_SEEK],
        WIDGET_EVENT_PAINT);
    
    /*set_widget_clip(widget->compose->widget[WID_SCROLLBAR_DOWN]
        ->compose->widget[WID_BUTTON_LABEL],
        widget->compose->widget[WID_SCROLLBAR_DOWN]->pos->real.x,
        widget->compose->widget[WID_SCROLLBAR_DOWN]->pos->real.y,
        0, 0,
        widget->pos->real.w,
        widget->pos->real.h);*/
    
    /*printf("Painting WID_SCROLLBAR_DOWN at (%i,%i)\n",
        widget->compose->widget[WID_SCROLLBAR_DOWN]->pos->real.x,
        widget->compose->widget[WID_SCROLLBAR_DOWN]->pos->real.y);*/
    
    widget_event(xuni, widget->compose->widget[WID_SCROLLBAR_DOWN],
        WIDGET_EVENT_PAINT);
}

static void reposition_scrollbar(struct xuni_t *xuni,
    struct widget_t *widget) {
    
    struct widget_t *seek = widget->compose->widget[WID_SCROLLBAR_SEEK];
    struct widget_t *seekbar = widget->compose->widget[WID_SCROLLBAR_SEEKBAR];
    
    if(widget->p.scrollbar->orientation == SCROLLBAR_ORIENTATION_VERTICAL) {
        add_widget_clip(xuni, seek,
            0, (seekbar->pos->real.h - seek->pos->real.h)
                * widget->p.scrollbar->pos / 100.0,
            0, 0, 0, 0);
    }
    else {
        add_widget_clip(xuni, seek,
            (seekbar->pos->real.w - seek->pos->real.w)
                * widget->p.scrollbar->pos / 100.0, 0,
            0, 0, 0, 0);
    }
}

static void rescale_scrollbar(struct xuni_t *xuni, struct widget_t *widget) {
    /* what did this do?
    
    struct widget_t *seek = widget->compose->widget[WID_SCROLLBAR_SEEK];
    
    seek->pos->real.x += get_scrollbar_pos_int(widget);*/
}

static double calculate_seek_height(struct widget_t *widget,
    struct xuni_t *xuni) {
    
    double ratio;
    
    ratio = (widget->pos->scale.w / 100.0 * xuni->smode->screen->w)
        / (widget->pos->scale.h / 100.0 * xuni->smode->screen->h);
    
    return (100.0 / 15.0) / (real_scale_height(widget));
}

static double calculate_seek_width(struct widget_t *widget,
    struct xuni_t *xuni) {
    
    double ratio;
    
    ratio = (widget->pos->scale.h / 100.0 * xuni->smode->screen->h)
        / (widget->pos->scale.w / 100.0 * xuni->smode->screen->w);
    
    return (100.0 / 15.0) / (real_scale_width(widget));
}

void set_scrollbar_max(struct xuni_t *xuni, struct widget_t *widget,
    int max) {
    
    widget->p.scrollbar->max = max;
    
    if(widget->p.scrollbar->max < 0) widget->p.scrollbar->max = 0;
    
    /* in rescale_scrollbar(), this doesn't work the first time */
    if(widget->p.scrollbar->orientation == SCROLLBAR_ORIENTATION_VERTICAL) {
        struct widget_t *seek, *seekbar;
        double boxheight, minheight, value;
        
        seek = widget->compose->widget[WID_SCROLLBAR_SEEK];
        seekbar = widget->compose->widget[WID_SCROLLBAR_SEEKBAR];
        
        if(max <= 0) {
            seek->pos->scale.h = seekbar->pos->scale.h;
        }
        else {
            value = 1.0 / (1.0 + max / (double)widget->pos->real.h);
            
            /* boxheight not used */
            boxheight = get_box_height(xuni, xuni->theme->current);
            
            minheight = calculate_seek_height(widget, xuni);
            /*if(value < boxheight) {
                seek->pos->scale.h = boxheight;
            }*/
            seek->pos->scale.h = seekbar->pos->scale.h * value;
            if(seek->pos->scale.h < minheight) {
                seek->pos->scale.h = minheight;
                /*printf("set to minimum\n");*/
            }
            
            if(seek->pos->scale.h > seekbar->pos->scale.h) {
                seek->pos->scale.h = seekbar->pos->scale.h;
            }
            
            /*printf("value=%f, minheight=%f, seek->pos->scale.h=%f\n",
                value, minheight, seek->pos->scale.h);*/
        }
        
        widget_event(xuni, widget, WIDGET_EVENT_RESCALE);
    }
    else {
        struct widget_t *seek, *seekbar;
        double minwidth, value;
        
        seek = widget->compose->widget[WID_SCROLLBAR_SEEK];
        seekbar = widget->compose->widget[WID_SCROLLBAR_SEEKBAR];
        
        if(max <= 0) {
            seek->pos->scale.w = seekbar->pos->scale.w;
        }
        else {
            value = 1.0 / (1.0 + max / (double)widget->pos->real.w);
            
            minwidth = calculate_seek_width(widget, xuni);
            
            seek->pos->scale.w = seekbar->pos->scale.w * value;
            if(seek->pos->scale.w < minwidth) {
                seek->pos->scale.w = minwidth;
            }
            
            if(seek->pos->scale.w > seekbar->pos->scale.w) {
                seek->pos->scale.w = seekbar->pos->scale.w;
            }
        }
        
        widget_event(xuni, widget, WIDGET_EVENT_RESCALE);
    }
    
    /*restrict_int(&widget->p.scrollbar->pos, widget->p.scrollbar->max);*/
}

/*! Scrolls the scrollbar \a widget by \a amount, where \a amount is the exact
    pixel value to move the scrollbar by.
    \param xuni The xuni_t structure.
    \param widget The scrollbar widget that should be scrolled. An error will
        ensue if this widget is not a scrollbar.
    \param amount The number of pixels to scroll the scrollbar by.
*/
void move_scrollbar(struct xuni_t *xuni, struct widget_t *widget,
    int amount) {
    
    /*struct widget_t *seek, *seekbar;*/
    
    if(!widget) return;
    
    if(assert_widget_type(xuni, widget, WIDGET_SCROLLBAR,
        __FILE__, __LINE__)) {
        
        return;
    }
    
    widget->p.scrollbar->pos
        += (double)amount / widget->p.scrollbar->max * 100.0;
    
    restrict_pos(&widget->p.scrollbar->pos);
    
    /*seek = widget->compose->widget[WID_SCROLLBAR_SEEK];
    seekbar = widget->compose->widget[WID_SCROLLBAR_SEEKBAR];*/
    
    /*printf("====\n");
    printf("seekbar height: %f\n"
        "seek height: %f\n"
        "range: %f\n",
        seekbar->pos->scale.h,
        seek->pos->scale.h,
        (seekbar->pos->scale.h - seek->pos->scale.h));
    printf("max: %i\n"
        "pos: %i\n"
        "percentage: %f\n",
        widget->p.scrollbar->max,
        widget->p.scrollbar->pos,
        ((double)widget->p.scrollbar->max / widget->p.scrollbar->pos));*/
    
    /*add_widget_clip(seek,
        0, (seekbar->pos->scale.h - seek->pos->scale.h)
            * ((double)widget->p.scrollbar->pos / widget->p.scrollbar->max),
        0, 0, 0, 0);*/
    
    /*widget_event(xuni->smode, widget, WIDGET_EVENT_RESCALE);*/
}

/*! Returns the position of the scrollbar \a widget as a percentage value.
    Note that this value should stay the same between rescalings.
    \param widget The scrollbar to return the position of.
    \return The position that the scrollbar \a widget is scrolled to.
*/
double get_scrollbar_pos(struct widget_t *widget) {
    return widget->p.scrollbar->pos;
}

/*! Sets the position of the scrollbar \a widget, where the position to set is
    a percentage value.
    \param widget The scrollbar to scroll to the position \a pos.
    \param pos The position to scroll the scrollbar to.
*/
void set_scrollbar_pos(struct widget_t *widget, double pos) {
    widget->p.scrollbar->pos = pos;
}

/*! Converts a scrollbar's position to an absolute pixel value. Note that
    because scrollbar values are stored as percentages, this function still
    works if it gets rescaled. The functions that use the value of this
    function will have to call it again, but it will return the right values.
    \param widget The scrollbar widget to examine.
    \return How far the scrollbar is scrolled, as a pixel value.
*/
int get_scrollbar_pos_int(struct widget_t *widget) {
    return widget->p.scrollbar->pos / 100.0 * widget->p.scrollbar->max;
}
