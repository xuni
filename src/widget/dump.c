/*! \file dump.c
    
    Functions that make printing information about widgets easier.
*/

#include <stdlib.h>
#include <string.h>

#include "../graphics.h"
#include "../memory.h"
#include "../error.h"
#include "dump.h"
#include "widgets.h"

static void dump_widget_tree_level(struct xuni_t *xuni,
    struct widget_t *widget, size_t level);

static void dump_indent(FILE *fp, size_t level);
static void dump_xml_tag_string(FILE *fp, size_t level, const char *tagname,
    const char *data);
static void dump_xml_tag_double(FILE *fp, size_t level, const char *tagname,
    double data);
static void dump_widget_tree_xml_level(struct xuni_t *xuni,
    struct widget_t *widget, FILE *fp, size_t level);

void print_widget_backtrace(struct xuni_t *xuni, struct widget_t *widget) {
    printf("print_widget_backtrace():\n");
    
    while(widget) {
        printf(" %9s: " /*"at (%6.2f,%6.2f), size (%6.2f,%6.2f), "*/
            "\"%s\"\n", get_widget_type_name(xuni, widget->type),
            /*widget->pos->scale.x, widget->pos->scale.y,
            widget->pos->scale.w, widget->pos->scale.h,*/ widget->name);
        
        widget = widget->base;
    }
    
    putchar('\n');
}

/*! Returns the name of the widget type \a type. This name is determined with
    the xuni_t::wtype structure.
    
    \param xuni A pointer to the xuni structure. (Only the wtype member is
        used.)
    \param type The type of widget to return the name of.
    \return A string that is the name of the widget type \a type.
*/
const char *get_widget_type_name(struct xuni_t *xuni,
    enum widget_type_t type) {
    
    static char buffer[BUFSIZ];
    
    if((size_t)type < xuni->wtype->types) {
        return xuni->wtype->type[type].name;
    }
    
    sprintf(buffer, "[invalid type: %lu]", (unsigned long)type);
    return buffer;
}

/*! Prints the name of \a widget along with the names of all of its parents,
    with the highest tree element last. Designed to print widget information
    with as little space as possible, often taking only one line per widget.
    \param widget The widget to print a backtrace for.
*/
void print_inline_widget_backtrace(struct widget_t *widget) {
    if(!widget) return;
    
    printf("inline backtrace:");
    
    while(widget) {
        printf(" <- \"%s\"", widget->name);
        widget = widget->base;
    }
    
    printf("\n");
}

void print_sel_widgets(struct widget_t *widget) {
    size_t x;
    
    if(!widget) return;
    
    if(widget->sel) {
        struct widget_t *w;
        
        printf("sel: [vis=%i parent considered=%i children considered=%i]",
            widget->visibility,
            !(widget->visibility & WIDGET_VISIBILITY_INDEPENDENT),
            !(widget->visibility & WIDGET_VISIBILITY_NOT_COMPOSE));
        
        if(widget->type == WIDGET_LABEL) {
            printf("\"%s\"", widget->p.label->text);
        }
        
        for(w = widget; w; w = w->base) {
            printf(" <- \"%s\"", w->name);
        }
        printf("\n");
    }
    
    if(widget->compose) {
        for(x = 0; x < widget->compose->widgets; x ++) {
            print_sel_widgets(widget->compose->widget[x]);
        }
    }
}

void print_widget_clip(struct widget_t *widget) {
    if(!widget) return;
    
    if(!widget->pos->clip) printf("clip: None");
    else {
        printf("clip: off=(%i,%i) clip=(%i,%i) by (%i,%i)",
            widget->pos->clip->xoff, widget->pos->clip->yoff,
            widget->pos->clip->xclip, widget->pos->clip->yclip,
            widget->pos->clip->wclip, widget->pos->clip->hclip);
    }
    
    printf(" name=\"%s\"\n", widget->name);
}

static void dump_widget_tree_level(struct xuni_t *xuni,
    struct widget_t *widget, size_t level) {
    
    size_t x;
    
    if(!widget) return;
    
    for(x = 0; x < level * 4; x ++) putchar(' ');
    
    printf("\"%s\" [%s]\n", widget->name,
        get_widget_type_name(xuni, widget->type));
    
    if(widget->compose) {
        for(x = 0; x < widget->compose->widgets; x ++) {
            dump_widget_tree_level(xuni, widget->compose->widget[x],
                level + 1);
        }
    }
}

void dump_widget_tree(struct xuni_t *xuni, struct widget_t *widget) {
    puts("Widget tree dump:");
    dump_widget_tree_level(xuni, widget, 1);
}

static void dump_indent(FILE *fp, size_t level) {
    size_t x;
    
    for(x = 0; x < level * 4; x ++) fputc(' ', fp);
}

static void dump_xml_tag_string(FILE *fp, size_t level, const char *tagname,
    const char *data) {
    
    dump_indent(fp, level + 1);
    fprintf(fp, "<%s>%s</%s>\n", tagname, data, tagname);
}

static void dump_xml_tag_double(FILE *fp, size_t level, const char *tagname,
    double data) {
    
    dump_indent(fp, level + 1);
    fprintf(fp, "<%s>%f</%s>\n", tagname, data, tagname);
}

static void dump_widget_tree_xml_level(struct xuni_t *xuni,
    struct widget_t *widget, FILE *fp, size_t level) {
    
    size_t x;
    
    if(!widget) return;
    
    dump_indent(fp, level);
    fprintf(fp, "<widget type=\"%s\">\n",
        get_widget_type_name(xuni, widget->type));
    
    dump_xml_tag_string(fp, level, "name", widget->name);
    
    if(widget->pos->scale.x) {
        dump_xml_tag_double(fp, level, "xpos", widget->pos->scale.x);
    }
    if(widget->pos->scale.y) {
        dump_xml_tag_double(fp, level, "ypos", widget->pos->scale.y);
    }
    if(widget->pos->scale.w != 100.0) {
        dump_xml_tag_double(fp, level, "width", widget->pos->scale.w);
    }
    if(widget->pos->scale.h != 100.0) {
        dump_xml_tag_double(fp, level, "height", widget->pos->scale.h);
    }
    
    switch(widget->type) {
    case WIDGET_BUTTON:
        if(widget->compose->widget[WID_BUTTON_LABEL]) {
            dump_xml_tag_string(fp, level, "text",
                widget->compose->widget[WID_BUTTON_LABEL]->p.label->text);
        }
        
        break;
    case WIDGET_PANEL:
        if(widget->compose) {
            for(x = 0; x < widget->compose->widgets; x ++) {
                dump_widget_tree_xml_level(xuni, widget->compose->widget[x],
                    fp, level + 1);
            }
        }
        
        break;
    case WIDGET_BOX:
        break;
    default:
        printf("*** Dumping widgets of type %s not supported\n",
            get_widget_type_name(xuni, widget->type));
        break;
    }
    
    dump_indent(fp, level);
    fputs("</widget>\n", fp);
}

void dump_widget_tree_xml(struct xuni_t *xuni, struct widget_t *widget,
    const char *filename) {
    
    FILE *fp;
    
    rename(filename, "dump.oldfile");
    
    fp = fopen(filename, "w");
    if(!fp) {
        log_message(ERROR_TYPE_SETTING, 0, __FILE__, __LINE__,
            "Can't dump xml file \"%s\"", filename);
        return;
    }
    
    fputs("<?xml version=\"1.0\"?>\n"
        "<!-- ", fp);
    fputs(filename, fp);
    fputs(" -->\n"
        "<xuni-resource>\n", fp);
    
    dump_widget_tree_xml_level(xuni, widget, fp, 1);
    
    fputs("</xuni-resource>\n", fp);
    
    fclose(fp);
}

void dump_widgets_need_repaint(struct widget_t *widget) {
    size_t x;
    
    if(!widget) return;
    
    if(widget->repaint) print_inline_widget_backtrace(widget);
    
    if(widget->compose) {
        for(x = 0; x < widget->compose->widgets; x ++) {
            dump_widgets_need_repaint(widget->compose->widget[x]);
        }
    }
}
