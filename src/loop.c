/*! \file loop.c

*/

#include <stdlib.h>

#include "SDL_framerate.h"

#include "graphics.h"
#include "gui.h"
#include "loop.h"
#include "memory.h"
#include "resource/resource.h"

#include "widget/dump.h"
#include "widget/widgets.h"
#include "widget/listbox.h"

static int call_perform_click_func(struct xuni_t *xuni,
    struct panel_data_t *panel, struct widget_t *widget, panel_type_t *mode);

static SDL_Cursor *sdl_cursor_from_xpm(const char *image[]);
static void set_widget_cursor(struct xuni_t *xuni);

static void set_loop_caption(panel_type_t mode, struct xuni_t *xuni);
static void setup_theme_cursor(struct xuni_t *xuni, struct widget_t *panel);
static int get_event(int frameupdate, SDL_Event *event);
static int process_event(SDL_Event *event, panel_type_t *mode,
    struct xuni_t *xuni);
static void scroll_listbox(struct xuni_t *xuni, int dir, size_t mode);
static int set_loop_widget_sel(panel_type_t mode, int xp, int yp,
    int click, struct xuni_t *xuni);
static int find_widget_accelerator(struct xuni_accelerator_t *accel,
    SDL_keysym *key, struct xuni_t *xuni, panel_type_t *mode);
static int check_sdl_key_mod(SDLMod mod, SDLMod now);
static int event_mouse_button_up(int xp, int yp, panel_type_t *mode,
    struct xuni_t *xuni);
static int perform_loop_click(struct xuni_t *xuni, panel_type_t *mode,
    struct widget_t *widget);
static int event_key_down(struct xuni_t *xuni, SDL_keysym *keysym,
    panel_type_t *mode, int *handled);

/*! Calls the registered initialization functions for \a widget and all of its
    sub-widgets. Only operates on widgets of type panel.
    
    \param xuni A pointer to the main xuni structure.
    \param widget The widget to call the initialization function for, if it is
        a panel widget.
    \param settings A pointer to the resource tree.
*/
void call_init_funcs(struct xuni_t *xuni, struct widget_t *widget,
    struct resource_t *settings) {
    
    size_t x;
    
    if(!widget) return;
    
    if(widget->type == WIDGET_PANEL
        && widget->p.panel->event[PANEL_EVENT_INIT].handler) {
        
        widget->p.panel->event[PANEL_EVENT_INIT].p.init.settings = settings;
        widget->p.panel->event[PANEL_EVENT_INIT].p.init.panel = widget;
        
        (*widget->p.panel->event[PANEL_EVENT_INIT].handler)
            (xuni, widget->p.panel);
    }
    
    if(widget->compose) {
        for(x = 0; x < widget->compose->widgets; x ++) {
            call_init_funcs(xuni, widget->compose->widget[x], settings);
        }
    }
}

void call_free_funcs(struct xuni_t *xuni, struct widget_t *widget) {
    size_t x;
    
    if(!widget) return;
    
    if(widget->type == WIDGET_PANEL
        && widget->p.panel->event[PANEL_EVENT_FREE].handler) {
        
        (*widget->p.panel->event[PANEL_EVENT_FREE].handler)
            (xuni, widget->p.panel);
    }
    
    if(widget->compose) {
        for(x = 0; x < widget->compose->widgets; x ++) {
            call_free_funcs(xuni, widget->compose->widget[x]);
        }
    }
}

/* !!! Note that because of this function, the handler member of the structure
    passed to panel event handlers is not defined.
*/
int panel_event_recursive(struct xuni_t *xuni, struct panel_data_t *data,
    enum panel_event_type_t type, struct widget_t *widget) {
    
    int r = 0;
    size_t x;
    
    if(!widget) return 0;
    
    if(widget->type == WIDGET_PANEL
        && widget->p.panel->event[type].handler) {
        
        (*widget->p.panel->event[type].handler)(xuni, data);
    }
    
    if(widget->compose) {
        for(x = 0; x < widget->compose->widgets; x ++) {
            r = panel_event_recursive(xuni, data, type,
                widget->compose->widget[x]) || r;
        }
    }
    
    return r;
}

static int call_perform_click_func(struct xuni_t *xuni,
    struct panel_data_t *panel, struct widget_t *widget, panel_type_t *mode) {
    
    perform_widget_click(xuni, widget);
    
    /*print_widget_backtrace(xuni->gui->sel.p.widget);*/
    
    /* !!! quick fix to disallow clicking on a panel. Without this, for
        example, clicking on the options graphics panel triggers the
        fullscreen checkbox, as
            PANEL_OPTIONS_GRAPHICS == WID_GRAPHICS_FULLSCREEN */
    if(widget && panel->event[PANEL_EVENT_CLICK].handler
        && widget_nameid_access(xuni->gui->widget, *mode) != widget) {
        
        panel->event[PANEL_EVENT_CLICK].p.click.mode = mode;
        panel->event[PANEL_EVENT_CLICK].p.click.widget = widget;
        
        return (*panel->event[PANEL_EVENT_CLICK].handler)(xuni, panel);
    }
    
    return 0;
}

void call_deactivate_func(struct xuni_t *xuni, struct widget_t *widget) {
    struct widget_t *base = widget;
    
    while(base && base->type != WIDGET_PANEL) base = base->base;
    
    if(base && base->p.panel->event[PANEL_EVENT_DEACTIVATE].handler) {
        base->p.panel->event[PANEL_EVENT_DEACTIVATE].p.deactivate.widget
            = widget;
        
        (*base->p.panel->event[PANEL_EVENT_DEACTIVATE].handler)
            (xuni, base->p.panel);
    }
}

/*#define HIDE_COMBOBOX_POPUPS*/

int default_panel_sel(struct xuni_t *xuni, struct panel_data_t *data) {
    struct panel_event_sel_t *sel = &data->event[PANEL_EVENT_SEL].p.sel;
    
    return set_default_widget_sel(xuni, sel->mode, sel->xp, sel->yp,
        sel->click, data->data);
}

/* !!! this function is obsolete */
int set_default_widget_sel(struct xuni_t *xuni, panel_type_t mode,
    int xp, int yp, int click, void *vdata) {
    
#ifdef HIDE_COMBOBOX_POPUPS
    struct widget_t *widget = xuni->gui->sel.p.widget;
#endif
    int r;
    
    r = set_widget_sel_repaint(xuni, &xuni->gui->sel, xp, yp, click,
        widget_nameid_access(xuni->gui->widget, mode));
    
    /*if(xuni->gui->sel.p.widget) {
        printf("%s\n", xuni->gui->sel.p.widget->name);
    }*/
    
#ifdef HIDE_COMBOBOX_POPUPS
    /* Hide combobox popups that have become unselected. */
    if(widget && widget != xuni->gui->sel.p.widget) {
        struct widget_t *w = xuni->gui->sel.p.widget, *t;
        
        /* See if any comboboxes that were previously selected still are. */
        while(w) {
            if(w->visibility & WIDGET_VISIBILITY_VISIBLE
                && w->type == WIDGET_COMBOBOX) {
                
                for(t = widget; t; t = t->base) {
                    if(t == w) break;
                }
                
                if(t) break;
            }
            
            w = w->base;
        }
        
        /* If no comboboxes were selected and remain selected, hide the popups
            for all comboboxes that were selected.
        */
        if(!w) {
            do {
                if(widget->type == WIDGET_COMBOBOX) {
                    widget->compose->widget[WID_COMBOBOX_DROPDOWN]->visibility
                        &= ~WIDGET_VISIBILITY_VISIBLE;
                }
                
                widget = widget->base;
            } while(widget);
        }
    }
#endif
    
    return r;
}

#if !1
void set_panel_callbacks(struct widget_t *widget, void *vdata,
    int frameupdate,
    panel_event_func_t init_func,
    panel_event_func_t start_func,
    panel_event_func_t event_func,
    panel_event_func_t set_widget_sel_func,
    panel_event_func_t perform_click_func,
    panel_event_func_t deactivate_func,
    panel_event_func_t paint_func,
    panel_event_func_t free_func) {
    
    widget->p.panel->data = vdata;
    widget->p.panel->frameupdate = frameupdate;
    
    widget->p.panel->event[PANEL_EVENT_INIT].handler = init_func;
    widget->p.panel->event[PANEL_EVENT_START].handler = start_func;
    widget->p.panel->event[PANEL_EVENT_EVENT].handler = event_func;
    widget->p.panel->event[PANEL_EVENT_SEL].handler = set_widget_sel_func;
    widget->p.panel->event[PANEL_EVENT_CLICK].handler = perform_click_func;
    widget->p.panel->event[PANEL_EVENT_DEACTIVATE].handler = deactivate_func;
    widget->p.panel->event[PANEL_EVENT_PAINT].handler = paint_func;
    widget->p.panel->event[PANEL_EVENT_FREE].handler = free_func;
    
    widget->p.panel->nameid = 0;
    
    widget->p.panel->accel = 0;
}
#endif

void execute_callback(struct xuni_t *xuni, struct xuni_callback_t *callback) {
    if(callback && callback->func) {
        (*callback->func)(callback->vdata, xuni);
    }
}

/*! The main xuni loop. Indirectly calls all panel event-handling functions,
    including initialization functions, event handlers, repaint functions,
    etc.
    
    This function should only be called once, after everything has been loaded
    and set up. It should only return when the program is exiting -- but it
    will return, so don't neglect that freeing code.
    
    \param xuni The xuni structure, with most of the data required to display
        and manage a Graphical User Interface (GUI, of course).
    \param always A callback function that is executed for every loop of the
        xuni event loop. Note that this can be called considerably more
        frequently than repaint functions.
    \param mode The panel to initially display.
*/
void main_loop(struct xuni_t *xuni, struct xuni_callback_t *always,
    panel_type_t mode) {
    
    panel_type_t pmode = (size_t)-1;
    struct panel_data_t *panel
        = widget_nameid_access(xuni->gui->widget, mode)->p.panel;
    FPSmanager fpsm;
    SDL_Event event;
    int repaint = 1;
    
    /*call_init_funcs(data, smode, font, theme, gui, settings);*/
    
    SDL_initFramerate(&fpsm);
    SDL_setFramerate(&fpsm, 30);
    
    while(mode != (size_t)-1) {
        execute_callback(xuni, always);
        
        /*dump_widgets_need_repaint(xuni->gui->widget);*/
        
        if(get_event(widget_nameid_access(xuni->gui->widget, mode)
            ->p.panel->frameupdate, &event)) {
            
            do {
                repaint = process_event(&event, &mode, xuni) || repaint;
                
                if(mode == (size_t)-1) break;
            } while(SDL_PollEvent(&event));
            
            if(mode == (size_t)-1) break;
        }
        
        if(pmode != mode) {  /* !!! process_event() is called before the start_func */
            panel = widget_nameid_access(xuni->gui->widget, mode)->p.panel;
            set_loop_caption(mode, xuni);  /* !!! also pass pmode */
            
            pmode = mode;
            
            repaint = 1;
        }
        
        /* !!! this needs to go in a hover callback function of sorts */
        set_widget_cursor(xuni);
        
        if(xuni->smode->focus) {
            if(repaint || panel->frameupdate) {
                if(panel->event[PANEL_EVENT_PAINT].handler) {
                    panel->event[PANEL_EVENT_PAINT].p.paint.mode = mode;
                    
                    (*panel->event[PANEL_EVENT_PAINT].handler)(xuni, panel);
                }
            }
        }
        
        repaint = 0;
        
        if(!xuni->smode->focus || panel->frameupdate) {
            SDL_framerateDelay(&fpsm);
        }
    }
    
    set_caption("Quitting . . .");
    
    /*panel_event_recursive(xuni, 0, PANEL_EVENT_FREE, xuni->gui->widget);*/
    call_free_funcs(xuni, xuni->gui->widget);
}

/* An XPM image of a CURSOR_TEXT, used when the mouse is inside textboxes. */
static const char *text_cursor[] = {
  /* width height num_colors chars_per_pixel */
  "    16    16        3            1",
  /* colors */
  "X c #000000",
  ". c #ffffff",
  "  c None",
  /* pixels */
  "     XXXXX      ",
  "    X.....X     ",
  "     XX.XX      ",
  "      X.X       ",
  "      X.X       ",
  "      X.X       ",
  "      X.X       ",
  "      X.X       ",
  "      X.X       ",
  "      X.X       ",
  "      X.X       ",
  "      X.X       ",
  "      X.X       ",
  "     XX.XX      ",
  "    X.....X     ",
  "     XXXXX      ",
  "7,7"
};

static SDL_Cursor *sdl_cursor_from_xpm(const char *image[]) {
    int i, x, y;
    Uint8 data[4*16] = {0};
    Uint8 mask[4*16] = {0};
    int hot_x, hot_y;
    
    i = -1;
    for(x = 4; x < 16 + 4; x ++) {
        for(y = 0; y < 16; y ++) {
            if(y % 8) {
                data[i] <<= 1;
                mask[i] <<= 1;
            }
            else i ++;
            
            switch (image[x][y]) {
            case 'X':
                data[i] |= 0x01;
                mask[i] |= 0x01;
                break;
            case '.':
                mask[i] |= 0x01;
                break;
            case ' ':
                break;
            }
        }
    }
    
    sscanf(image[x], "%d,%d", &hot_x, &hot_y);
    
    return SDL_CreateCursor(data, mask, 16, 16, hot_x, hot_y);
}

static void set_widget_cursor(struct xuni_t *xuni) {
    enum cursor_t now;
    
    if(xuni->gui->sel.p.widget
        && xuni->gui->sel.p.widget->type == WIDGET_TEXTBOX) {
        
        now = CURSOR_TEXT;
    }
    else now = CURSOR_NORMAL;
    
    if(xuni->theme->cursors.which != now) {
        xuni->theme->cursors.which = now;
        
        if(xuni->theme->cursors.which) {
            if(!xuni->theme->cursors.text) {
                xuni->theme->cursors.text = sdl_cursor_from_xpm(text_cursor);
                /* save the normal cursor before it is overwritten */
                xuni->theme->cursors.normal = SDL_GetCursor();
            }
            
            SDL_SetCursor(xuni->theme->cursors.text);
        }
        else {
            if(!xuni->theme->cursors.normal) {
                xuni->theme->cursors.normal = SDL_GetCursor();
            }
            
            SDL_SetCursor(xuni->theme->cursors.normal);
        }
    }
}

static void set_loop_caption(panel_type_t mode, struct xuni_t *xuni) {
    struct widget_t *panel = widget_nameid_access(xuni->gui->widget, mode);
    struct panel_data_t *paneldata = panel->p.panel;
    int xp, yp;
    
    show_cursor(1);
    
    clear_gui(xuni, mode, 0);
    
    /* !!! set all other panels to invisible */
    
    widget_nameid_access(xuni->gui->widget, mode)->visibility
        |= WIDGET_VISIBILITY_VISIBLE;
    
    if(paneldata->event[PANEL_EVENT_START].handler) {
        (*paneldata->event[PANEL_EVENT_START].handler)(xuni, paneldata);
    }
    
    setup_theme_cursor(xuni, widget_nameid_access(xuni->gui->widget, mode));
    SDL_GetMouseState(&xp, &yp);
    set_loop_widget_sel(mode, xp, yp, xuni->gui->sel.clickin, xuni);
}

static void setup_theme_cursor(struct xuni_t *xuni, struct widget_t *panel) {
    struct widget_t *cursor;
    
    if(!panel->p.panel->frameupdate) return;
    
    cursor = get_theme_widget(xuni, THEME_DEFAULT_CURSOR);
    if(!cursor) return;
    
    prepare_paint_image(xuni, cursor);
    
    if(cursor->p.image->image) {
        show_cursor(0);
    }
}

static int get_event(int frameupdate, SDL_Event *event) {
    return frameupdate ? SDL_PollEvent(event) : SDL_WaitEvent(event);
}

/* handled should not be used in some cases. For example, a widget might be
    deselected by a mouse motion event, but a xuni application (like the
    resource editor) might want to know about the mouse motion event just the
    same.
*/
/*! Handles an event from the SDL. What happens depends on the event itself.
    However, this function will always either deal with the event itself, call
    the appropriate event handler function for the currently visible panel, or
    perhaps do a little bit of both.
    
    Some handled events include clicking inside widgets (implemented by
    calling set_loop_widget_sel()), pressing special keypresses like
    printscreen (see event_key_down()), and selecting widgets when the mouse
    moves (set_loop_widget_sel()).
    
    \param event The SDL_Event that needs to be dealt with.
    \param mode The currently visible panel, which could be changed.
    \param xuni The main xuni_t structure.
    \return True if the screen needs to be repainted.
*/
static int process_event(SDL_Event *event, panel_type_t *mode,
    struct xuni_t *xuni) {
    
    struct panel_data_t *panel
        = widget_nameid_access(xuni->gui->widget, *mode)->p.panel;
    int repaint = 0, handled = 0;
    
    switch(event->type) {
    case SDL_MOUSEBUTTONDOWN:
        if(event->button.button == SDL_BUTTON_WHEELUP) {
            scroll_listbox(xuni, -1, *mode);
            
            repaint = 1;
            handled = 1;
        }
        else if(event->button.button == SDL_BUTTON_WHEELDOWN) {
            scroll_listbox(xuni, 1, *mode);
            
            repaint = 1;
            handled = 1;
        }
        
        if(event->button.button != 1) break;
        
        repaint = set_loop_widget_sel(*mode, event->button.x, event->button.y,
            1, xuni);
        
        if(repaint) handled = 1;
        break;
    case SDL_MOUSEBUTTONUP:
        if(event->button.button != 1) break;
        
        repaint = event_mouse_button_up(event->button.x, event->button.y,
            mode, xuni);
        
        if(repaint) handled = 1;
        break;
    case SDL_MOUSEMOTION:
        repaint = set_loop_widget_sel(*mode, event->button.x, event->button.y,
            xuni->gui->sel.clickin, xuni);
        
        /*if(repaint) handled = 1;*/
        break;
    case SDL_VIDEORESIZE:
        if(!resize_screen(xuni->smode, &event->resize)) {
            widget_event(xuni, xuni->gui->widget, WIDGET_EVENT_RESCALE);
            
            xuni->gui->widget->p.panel
                ->event[PANEL_EVENT_EVENT].p.event.mode = mode;
            xuni->gui->widget->p.panel
                ->event[PANEL_EVENT_EVENT].p.event.event = event;
            
            panel_event_recursive(xuni, xuni->gui->widget->p.panel,
                PANEL_EVENT_EVENT, xuni->gui->widget);
        }
        
        if(focus_changed(xuni->smode, SDL_APPACTIVE)) {
            if(xuni->smode->focus) {
                set_loop_caption(*mode, xuni);
            }
            else {
                show_cursor(1);
            }
        }
        
        /*handled = 1;*/
        repaint = 1;
        break;
    case SDL_VIDEOEXPOSE:
        repaint = 1;
        handled = 1;
        break;
    case SDL_KEYDOWN:
        repaint = event_key_down(xuni, &event->key.keysym, mode, &handled);
        
        break;
    case SDL_KEYUP:
        break;
    case SDL_ACTIVEEVENT:
        if(focus_changed(xuni->smode, SDL_APPACTIVE | SDL_APPINPUTFOCUS)) {
            if(xuni->smode->focus) {
                /*int x, y;
                SDL_GetMouseState(&x, &y);
                printf("activated: %i,%i\n", x, y);*/
                /* !!! this is only for the cursor */
                set_loop_caption(*mode, xuni);
                repaint = 1;
            }
            else {
                /* preserve any textbox data etc in the active widget */
                clear_active(xuni, &xuni->gui->active, 1);
                show_cursor(1);
            }
        }
        
        handled = 1;
        break;
    default:
        break;
    }
    
    if(!handled && panel->event[PANEL_EVENT_EVENT].handler) {
        panel->event[PANEL_EVENT_EVENT].p.event.mode = mode;
        panel->event[PANEL_EVENT_EVENT].p.event.event = event;
        
        repaint |= (*panel->event[PANEL_EVENT_EVENT].handler)(xuni, panel);
    }
    
    return repaint;
}

/* !!! bad hack */
static void scroll_listbox(struct xuni_t *xuni, int dir, size_t mode) {
    int step = 5;
    int moved = 0;
    
    if(xuni->gui->sel.p.widget
        && xuni->gui->sel.p.widget->type == WIDGET_LISTBOX) {
        
        move_scrollbar(xuni, xuni->gui->sel.p.widget->compose->widget
            [WID_LISTBOX_VSCROLL], step * dir);
        
        widget_event(xuni, xuni->gui->sel.p.widget, WIDGET_EVENT_REPOSITION);
        moved = 1;
    }
    
    if(xuni->gui->sel.p.widget
        && xuni->gui->sel.p.widget->type == WIDGET_SCROLLBAR) {
        
        move_scrollbar(xuni, xuni->gui->sel.p.widget, step * dir);
        
        widget_event(xuni, xuni->gui->sel.p.widget->base,
            WIDGET_EVENT_REPOSITION);
        moved = 1;
    }
    
    if(xuni->gui->sel.p.widget && xuni->gui->sel.p.widget->base
        && xuni->gui->sel.p.widget->base->type == WIDGET_SCROLLBAR) {
        
        move_scrollbar(xuni, xuni->gui->sel.p.widget->base, step * dir);
        
        widget_event(xuni, xuni->gui->sel.p.widget->base->base,
            WIDGET_EVENT_REPOSITION);
        moved = 1;
    }
    
    if(moved) {
        int xp, yp;
        
        SDL_GetMouseState(&xp, &yp);
        
        set_loop_widget_sel(mode, xp, yp, xuni->gui->sel.clickin, xuni);
    }
}

static int set_loop_widget_sel(panel_type_t mode, int xp, int yp,
    int click, struct xuni_t *xuni) {
    
    struct panel_data_t *panel
        = widget_nameid_access(xuni->gui->widget, mode)->p.panel;
    
    if(!panel->event[PANEL_EVENT_SEL].handler) return 0;
    
    panel->event[PANEL_EVENT_SEL].p.sel.mode = mode;
    panel->event[PANEL_EVENT_SEL].p.sel.xp = xp;
    panel->event[PANEL_EVENT_SEL].p.sel.yp = yp;
    panel->event[PANEL_EVENT_SEL].p.sel.click = click;
    
    return (*panel->event[PANEL_EVENT_SEL].handler)(xuni, panel);
}

/*! Determines whether any accelerator key combinations have been or are being
    pressed, and, if so, handles the click event.
    
    \param accel This panel's accelerator array. Searched for accelerator
        combinations matching the current keyboard state.
    \param key The current keyboard state. Compared with accelerator
        combinations for the current panel for matches.
    \param xuni The xuni structure. Used to get a struct widget_t pointer to
        the current panel.
    \param mode The currently active panel. Only passed so that
        call_perform_click_func() may be called.
*/
static int find_widget_accelerator(struct xuni_accelerator_t *accel,
    SDL_keysym *key, struct xuni_t *xuni, panel_type_t *mode) {
    
    size_t x;
    SDL_keysym *akey;
    
    if(!accel) return 0;
    
    for(x = 0; x < accel->n; x ++) {
        akey = &accel->key[x].key;
        if(akey->sym && akey->sym == key->sym
            && check_sdl_key_mod(akey->mod, key->mod)) {
            
            call_perform_click_func(xuni,
                widget_nameid_access(xuni->gui->widget, *mode)->p.panel,
                accel->key[x].widget, mode);
            
            return 1;
        }
    }
    
    return 0;
}

/* !!! needs to be reworked to allow OR, AND, NOT, etc. */
static int check_sdl_key_mod(SDLMod mod, SDLMod now) {
    if(mod != KMOD_NONE) {
        if((mod & KMOD_CTRL) == KMOD_CTRL) {
            if(now & KMOD_CTRL) mod &= ~KMOD_CTRL;
            else return 0;
        }
        if((mod & KMOD_SHIFT) == KMOD_SHIFT) {
            if(now & KMOD_SHIFT) mod &= ~KMOD_SHIFT;
            else return 0;
        }
        if((mod & KMOD_ALT) == KMOD_ALT) {
            if(now & KMOD_ALT) mod &= ~KMOD_ALT;
            else return 0;
        }
    }
    
    if(now != mod) return 0;
    
    return 1;
}

static int event_mouse_button_up(int xp, int yp, panel_type_t *mode,
    struct xuni_t *xuni) {
    
    struct panel_data_t *panel
        = widget_nameid_access(xuni->gui->widget, *mode)->p.panel;
    int repaint = 0, realclick;
    
    if(xuni->gui->sel.p.widget && xuni->gui->sel.clickin
        && pos_in_rect(xp, yp, xuni->gui->sel.p.widget->pos)) {
        
        /*xuni->gui->sel.clickin = 0;*/
        realclick = !set_widget_sel(&xuni->gui->sel, xp, yp, 1,
            widget_nameid_access(xuni->gui->widget, *mode));
        
        if(realclick) {
            /* !!! return value ignored: repaint is always set to 1 */
            call_perform_click_func(xuni, panel, xuni->gui->sel.p.widget,
                mode);
        }
        
        xuni->gui->sel.wasin = 1;
        xuni->gui->sel.clickin = 0;
        repaint = 1;
        
        if(realclick) {
            if(xuni->gui->sel.p.widget &&
                widget_can_be_active(xuni->gui->sel.p.widget)) {
                
                xuni->gui->active = xuni->gui->sel.p;
                activate_widget(xuni->gui->active.widget, xuni, xp, yp);
                enable_unicode(xuni->gui->active.widget);
            }
            else if(xuni->gui->active.widget) {
                repaint = 1;
                
                clear_active(xuni, &xuni->gui->active, 1);
            }
            
            if(*mode != (size_t)-1) {
                set_loop_widget_sel(*mode, xp, yp, xuni->gui->sel.clickin,
                    xuni);
            }
        }
    }
    else {
        repaint = set_loop_widget_sel(*mode, xp, yp, 0, xuni);
        
        if(xuni->gui->active.widget &&
            !pos_in_rect(xp, yp, xuni->gui->active.widget->pos)) {
            
            repaint = 1;
            
            clear_active(xuni, &xuni->gui->active, 1);
        }
    }
    
    return repaint;
}

static int perform_loop_click(struct xuni_t *xuni, panel_type_t *mode,
    struct widget_t *widget) {
    
    struct panel_data_t *panel
        = widget_nameid_access(xuni->gui->widget, *mode)->p.panel;
    int repaint, xp = 0, yp = 0;  /* !!! */
    
    /* !!! return value ignored: repaint is always set to 1 */
    call_perform_click_func(xuni, panel, widget, mode);
    
    xuni->gui->sel.wasin = 1;
    xuni->gui->sel.clickin = 0;
    repaint = 1;
    
    if(xuni->gui->sel.p.widget &&
        widget_can_be_active(xuni->gui->sel.p.widget)) {
        
        xuni->gui->active = xuni->gui->sel.p;
        activate_widget(xuni->gui->active.widget, xuni, xp, yp);
        enable_unicode(xuni->gui->active.widget);
    }
    else if(xuni->gui->active.widget) {
        repaint = 1;
        
        clear_active(xuni, &xuni->gui->active, 1);
    }
    
    if(*mode != (size_t)-1) {
        set_loop_widget_sel(*mode, xp, yp, xuni->gui->sel.clickin,
            xuni);
    }
    
    return repaint;
}

/* Perhaps functions for handling these keypresses could be registered?
    
    Note: a lot of debugging keypresses are here, usually F-keys.
*/
/*! Handles the keypresses that are common to all xuni widgets. This includes:
    - escape, which reverts widgets;
    - tab and shift-tab, which cycle though widgets;
    - enter, which might deactivate or select widgets; and
    - printscreen, which saves a screenshot.
    
    \param xuni A pointer to the main xuni structure.
    \param keysym A structure describing the state of the keyboard, e.g. the
        key that was pressed along with the state of any modifier keys etc.
    \param mode The currently visible panel. This could be changed indirectly
        in the call to perform_loop_click().
    \param handled Set to true if the keypress described by \a keysym was
        handled. If the keypress was handled, the xuni application will never
        see the event.
    \return True if a repaint of the screen is required. This happens when,
        for example, the keypress resulted in a widget being deactivated.
*/
static int event_key_down(struct xuni_t *xuni, SDL_keysym *keysym,
    panel_type_t *mode, int *handled) {
    
    int repaint = 0;
    
    switch(keysym->sym) {
    case SDLK_ESCAPE:
        if(xuni->gui->active.widget) {
            clear_active(xuni, &xuni->gui->active, 0);
            
            *handled = 1;
            repaint = 1;
        }
        
        break;
    case SDLK_PRINT:
        save_screenshot(xuni->smode->screen);
        
        *handled = 1;
        break;
    case SDLK_RETURN:
        if(keysym->mod & KMOD_ALT) {
            if(!toggle_fullscreen(xuni->smode)) repaint = 1;
            
            *handled = 1;
        }
        else if(xuni->gui->tab.panel != (size_t)-1
            && xuni->gui->tab.sel != (size_t)-1) {
            
            repaint |= perform_loop_click(xuni, mode,
                widget_nameid_follow(xuni->gui->widget,
                    xuni->gui->tab.panel, xuni->gui->tab.sel,
                    (size_t)-1));
        }
        
        break;
    case SDLK_TAB:
        if(xuni->gui->tab.panel == (size_t)-1) break;
        
        if(keysym->mod & KMOD_SHIFT) {
            if(xuni->gui->tab.sel-- == (size_t)-1) {
                xuni->gui->tab.sel =
                    widget_nameid_access(xuni->gui->widget,
                        xuni->gui->tab.panel)->compose->widgets - 1;
            }
        }
        else {
            if(++xuni->gui->tab.sel ==
                widget_nameid_access(xuni->gui->widget,
                    xuni->gui->tab.panel)->compose->widgets) {
                
                xuni->gui->tab.sel = (size_t)-1;
            }
        }
        
        repaint = 1;
        *handled = 1;
        break;
    case SDLK_F9:
        if(keysym->mod & KMOD_ALT) {
            if(!SDL_WM_IconifyWindow()) {
                /* error !!! */
            }
            else {
                *handled = 1;
            }
        }
        break;
    /* keypresses for debugging purposes */
    case SDLK_F8:
        if(xuni->gui->sel.p.widget) {
            print_widget_backtrace(xuni, xuni->gui->sel.p.widget);
        }
        
        *handled = 1;
        break;
    case SDLK_F7:
        if(xuni->gui->active.widget) {
            print_widget_backtrace(xuni, xuni->gui->active.widget);
        }
        
        *handled = 1;
        break;
    case SDLK_F6:
        puts("====");
        print_sel_widgets(xuni->gui->widget);
        *handled = 1;
        break;
    case SDLK_F5:
        {
            int x, y, xrel, yrel;
            Uint8 state;
            
            state = SDL_GetMouseState(&x, &y);
            SDL_GetRelativeMouseState(&xrel, &yrel);
            
            printf("Mouse: at (%i,%i), moved (%i,%i), buttons %i%i%i\n",
                x, y, xrel, yrel, state & SDL_BUTTON(1),
                state & SDL_BUTTON(2), state & SDL_BUTTON(3));
            
            SDL_WarpMouse(x, y);
        }
        break;
    case SDLK_F4:
        /* force a repaint */
        repaint = 1;
        break;
    case SDLK_F3:
        if(xuni->gui->edit.datawidget) {
            printf("textbox data: \"%s\"\n",
                xuni->gui->edit.datawidget->p.label->text);
        }
        else puts("No label selected");
        break;
    case SDLK_F2:
        if(xuni->gui->sel.p.widget->type == WIDGET_TEXTBOX) {
            printf("textbox leftpos: %i\n",
                xuni->gui->sel.p.widget->p.textbox->leftpos);
        }
        break;
    default:
        break;
    }
    
    if(*mode != (size_t)-1) {
        if(!*handled && find_widget_accelerator(
            widget_nameid_access(xuni->gui->widget, *mode)
            ->p.panel->accel, keysym, xuni, mode)) {
            
            *handled = 1;
        }
        
        if(xuni->gui->active.widget) {
            repaint = widget_process_character(xuni, keysym);
            
            update_widget(xuni, xuni->gui->active.widget);
        }
    }
    
    if(repaint) *handled = 1;
    
    return repaint;
}
