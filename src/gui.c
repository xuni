/*! \file gui.c

*/

#include <stdlib.h>
#include <string.h>  /* for memmove(), strcmp(), strlen(), etc. */
#include <ctype.h>

#include "SDL_gfxPrimitives.h"

#include "error.h"
#include "loop.h"
#include "graphics.h"
#include "gui.h"
#include "memory.h"
#include "utility.h"

#include "resource/calcfunc.h"

#include "widget/widgets.h"

#include "widget/box.h"
#include "widget/button.h"
#include "widget/checkbox.h"
#include "widget/dump.h"
#include "widget/font.h"
#include "widget/image.h"
#include "widget/image_tile.h"
#include "widget/label.h"
#include "widget/listbox.h"
#include "widget/panel.h"
#include "widget/scrollbar.h"
#include "widget/textbox.h"
#include "widget/theme.h"

static int set_widget_sel_compose(struct widget_sel_t *sel, int xp, int yp,
    struct widget_t *widget);
static int set_scrollbar_sel(struct xuni_t *xuni, struct widget_sel_t *sel,
    int xp, int yp, int click);
static int compare_widget_type(void *data, size_t n, void *find);
static enum widget_type_t get_widget_type(struct xuni_t *xuni,
    const char *find);
static double get_angle(struct resource_list_t *list);
static size_t find_font_name(struct resource_list_t *list);
static int init_resource_widget_ref(struct xuni_t *xuni,
    struct widget_t *widget, enum widget_type_t type,
    struct resource_list_t *list);
static enum label_type_t get_label_alignment(const char *name);
static enum scrollbar_use_t get_scrollbar_use(const char *name);
static void init_resource_widget(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_type_t type, struct resource_list_t *list);
static enum pos_pack_t get_pos_pack(const char *name);
static void process_widget_accelerators(struct xuni_t *xuni,
    struct widget_t *panel, struct widget_t *widget,
    struct resource_list_t *list);
static void widget_accelerator_from_string(struct xuni_t *xuni,
    struct widget_t *panel, struct widget_t *widget, const char *data);
static SDLMod get_key_mod_from_string(const char *name);
static SDLKey get_key_identifier_from_string(const char *name);
static void add_resource_widget(struct xuni_t *xuni, struct widget_t **base,
    struct resource_data_t *data);

static size_t move_cursor_word(struct widget_edit_t *edit, size_t startpos,
    int dir, int flip);

void init_theme_structure(struct theme_t *theme) {
    theme->current = 0;
    theme->cursors.normal = 0;
    theme->cursors.text = 0;
    theme->cursors.which = CURSOR_NORMAL;
}

/*! Initializes the gui member of the structure \a xuni.
    \param xuni The xuni structure to initialize the gui member of.
*/
void init_gui(struct xuni_t *xuni) {
    /*gui->panel = allocate_panel(0);*/
    xuni->gui->widget = 0;
    
    init_edit(&xuni->gui->edit);
    clear_gui(xuni, (size_t)-1, -1);
}

void init_edit(struct widget_edit_t *edit) {
    edit->data = 0;
    edit->prev = 0;
    edit->datawidget = 0;
    edit->alloc = 0;
    edit->len = 0;
    edit->pos = 0;
}

void clear_gui(struct xuni_t *xuni, panel_type_t panel, int keep) {
    clear_widget_sel(xuni->gui->widget);
    clear_sel(&xuni->gui->sel);
    clear_active(xuni, &xuni->gui->active, keep);  /* before clear_edit() */
    clear_edit(&xuni->gui->edit);
    clear_tab(&xuni->gui->tab, panel);
}

void clear_sel(struct widget_sel_t *sel) {
    sel->wasin = 0;
    sel->clickin = 0;
    sel->p.widget = 0;
}

void clear_edit(struct widget_edit_t *edit) {
    edit->data = 0;
    
    if(edit->prev) {
        xuni_memory_free(edit->prev);
        edit->prev = 0;
    }
    
    edit->datawidget = 0;
    
    edit->alloc = 0;
    edit->len = 0;
    edit->pos = 0;
}

void clear_active(struct xuni_t *xuni, struct widget_p_t *wp, int keep) {
    if(!keep) {
        revert_widget(xuni, wp->widget);
        
        widget_event(xuni, wp->widget, WIDGET_EVENT_RESCALE);
    }
    else if(keep > 0) {
        deactivate_widget(wp->widget, xuni);
    }
    
    wp->widget = 0;
    
    SDL_EnableUNICODE(0);
}

void clear_tab(struct widget_tab_t *tab, panel_type_t panel) {
    tab->panel = panel;
    tab->sel = (size_t)-1;
}

/* !!! lots of const -> non-const in this function */
void edit_add_char(struct widget_edit_t *edit, char c) {
    if(edit->len + 1 >= edit->alloc) {
        if(!edit->alloc) edit->alloc = 2;
        else edit->alloc *= 2;
        
        edit->data->text
            = xuni_memory_resize((char *)edit->data->text,
                edit->alloc);
    }
    
    if(edit->pos != edit->len) {
        memmove((char *)edit->data->text + edit->pos + 1,
            (char *)edit->data->text + edit->pos,
            edit->len - edit->pos + 1);
    }
    
    ((char *)edit->data->text)[edit->pos] = c;
    
    if(edit->pos == edit->len) {
        ((char *)edit->data->text)[edit->pos + 1] = 0;
    }
    
    edit->pos ++;
    edit->len ++;
}

/* !!! lots of const -> non-const in this function */
void edit_del_chars(struct widget_edit_t *edit, size_t n) {
    if(edit->pos + n < edit->len) {
        memmove((char *)edit->data->text + edit->pos,
            (char *)edit->data->text + edit->pos + n,
            edit->len - edit->pos + n - 1);
    }
    else ((char *)edit->data->text)[edit->pos] = 0;
    
    edit->len -= n;
}

/*! Clears the sel member of the widget \a widget and all of its child
    widgets. Returns true if a repaint will be required; i.e., if any widget
    that had its sel member set to false was originally true. (If this was the
    case, a widget will have changed from selected to not selected, and a
    repaint would be necessary.)
    
    \param widget The widget at the top of the tree of widgets to clear the
        sel members of.
    \return True if a repaint is needed, that is, if any widget had a sel
        member set to true.
*/
int clear_widget_sel(struct widget_t *widget) {
    size_t x;
    int repaint = 0;
    
    if(!widget) return 0;
    
    if(widget->sel) repaint = 1;
    widget->sel = 0;
    
    if(!widget->compose) return 0;
    
    for(x = 0; x < widget->compose->widgets; x ++) {
        repaint = clear_widget_sel(widget->compose->widget[x]) || repaint;
    }
    
    return repaint;
}

struct widget_t *set_widget_sel_rec(struct widget_sel_t *sel, int xp, int yp,
    struct widget_t *widget, int *repaint) {
    
    size_t x;
    struct widget_t *w = 0, *last = 0;
    
#if 0
    if(widget && !strcmp(widget->name, "data")) {
        /*printf("item \"%s\": ", widget->p.label->text);*/
        printf("data: ");
        
        printf("in_sdl_rect()? %s",
            in_sdl_rect(xp, yp, &widget->pos->real) ? "yes" : "no");
        printf("    pos_in_rect()? %s\n",
            pos_in_rect(xp, yp, widget->pos) ? "yes" : "no");
        
        if(!in_sdl_rect(xp, yp, &widget->pos->real)) {
            printf("cursor: (%i,%i)    widget: (%i,%i) to (%i,%i)\n",
                xp, yp,
                widget->pos->real.x,
                widget->pos->real.y,
                widget->pos->real.x + widget->pos->real.w,
                widget->pos->real.y + widget->pos->real.h);
        }
    }
#endif
    
    if(widget && widget->visibility & WIDGET_VISIBILITY_VISIBLE
        && in_sdl_rect(xp, yp, &widget->pos->real)) {
        
        if(widget->compose) {
            for(x = widget->compose->widgets - 1; x != (size_t)-1; x --) {
                w = set_widget_sel_rec(sel, xp, yp,
                    widget->compose->widget[x], repaint);
                if(w) {
                    if(!last && widget->compose->widget[x]->visibility
                        & WIDGET_VISIBILITY_INDEPENDENT) {
                        
                        last = w;
                    }
                    
                    if(!set_widget_sel_compose(sel, xp, yp, w)) break;
                    else last = 0;
                }
            }
            
            if(x != (size_t)-1) x --;
            
            while(x != (size_t)-1) {
                *repaint = clear_widget_sel(widget->compose->widget[x])
                    || *repaint;
                
                x --;
            }
        }
        
        if(!last && widget->visibility & WIDGET_VISIBILITY_SELABLE) {
            last = widget;
            if(!widget->sel) *repaint = 1;
            widget->sel = 1;
        }
        else {
            if(widget->sel) *repaint = 1;
            widget->sel = 0;
        }
    }
    else *repaint = clear_widget_sel(widget) || *repaint;
    
    return last;
}

static int set_widget_sel_compose(struct widget_sel_t *sel, int xp, int yp,
    struct widget_t *widget) {
    
    /*widget->sel = 1;*/
    
    if(widget->base) {
        /*if(!(widget->visibility & WIDGET_VISIBILITY_INDEPENDENT)) {
            widget->base->sel = 1;
        }*/
        
        if(!(widget->base->visibility & WIDGET_VISIBILITY_NOT_COMPOSE)) {
            return 1;
        }
    }
    
    return 0;
}

int set_widget_sel_repaint(struct xuni_t *xuni, struct widget_sel_t *sel,
    int xp, int yp, int click, struct widget_t *widget) {
    
    return set_widget_repaint(xuni, sel, xp, yp, click, widget)
        | set_widget_sel(sel, xp, yp, click, widget);
}

int set_widget_sel(struct widget_sel_t *sel, int xp, int yp, int click,
    struct widget_t *widget) {
    
    struct widget_t *w;
    int repaint = 0;
    
    if(!widget) return 0;
    
    if(sel->p.widget && !sel->clickin && !sel->wasin) clear_sel(sel);
    
    if(!sel->clickin) {
        /*clear_widget_sel(widget);*/
        w = set_widget_sel_rec(sel, xp, yp, widget, &repaint);
        
        /*if(repaint && w == sel->p.widget) puts("inline change");*/
        
        if(w && w != sel->p.widget) {
            sel->p.widget = w;
            sel->wasin = 1;
            sel->clickin = click;
            
            return 1;
        }
    }
    
    return repaint;
}

int set_widget_repaint(struct xuni_t *xuni, struct widget_sel_t *sel,
    int xp, int yp, int click, struct widget_t *widget) {
    
    int r, v = 0;
    
    if(sel->p.widget) {
        v = set_widget_sel(sel, xp, yp, click, widget);
        
        r = pos_in_rect(xp, yp, sel->p.widget->pos);
        
        /* a scrollbar was scrolled via its seek box */
        if(set_scrollbar_sel(xuni, sel, xp, yp, click)) {
            v = 1;
        }
        
        /* !!! set v=1 only if the widget _changes_ upon loss of mouse focus or whatever */
        
        /* the mouse focus in the widget was toggled */
        if(sel->wasin != r) {
            sel->wasin = r;
            v = 1;
        }
        
        /* the mouse button was clicked or released inside the widget */
        if(sel->clickin != click) {
            sel->clickin = click;
            v = 1;
        }
        
        if(v) sel->p.widget->repaint = 1;
    }
    
    return v;
}

static int set_scrollbar_sel(struct xuni_t *xuni, struct widget_sel_t *sel,
    int xp, int yp, int click) {
    
    struct widget_t *seek = sel->p.widget;
    struct widget_t *scroll = seek->base;
    struct widget_t *bar = scroll->compose->widget[WID_SCROLLBAR_SEEKBAR];
    
    if(scroll && scroll->type == WIDGET_SCROLLBAR
        && seek == scroll->compose->widget[WID_SCROLLBAR_SEEK]) {
        
        if(!sel->clickin && click) {
            sel->scrollbar.xp = xp;
            sel->scrollbar.yp = yp;
            sel->scrollbar.original = get_scrollbar_pos(scroll);
        }
        else if(sel->clickin) {
            int amount, max;
            double pos;
            
            if(scroll->p.scrollbar->orientation
                == SCROLLBAR_ORIENTATION_HORIZONTAL) {
                
                amount = xp - sel->scrollbar.xp;
                max = bar->pos->real.w - seek->pos->real.w;
            }
            else {
                amount = yp - sel->scrollbar.yp;
                max = bar->pos->real.h - seek->pos->real.h;
            }
            
            pos = (double)amount / max * 100.0;
            
            if(max > 0) {
                pos += sel->scrollbar.original;
                
                if(pos < 0.0) pos = 0.0;
                if(pos > 100.0) pos = 100.0;
                
                set_scrollbar_pos(scroll, pos);
            }
            
            if(scroll->base) {
                widget_event(xuni, scroll->base, WIDGET_EVENT_REPOSITION);
            }
            
            return 1;
        }
    }
    
    return 0;
}

struct load_resource_widget_t {
    struct xuni_t *xuni;
    struct widget_t **widget;
};

static void load_resource_widgets_callback(void *vdata,
    struct resource_data_t *data);

static void load_resource_widgets_callback(void *vdata,
    struct resource_data_t *data) {
    
    struct load_resource_widget_t *load_data = vdata;
    
    add_resource_widget(load_data->xuni, load_data->widget, data);
}

void load_resource_widgets(struct xuni_t *xuni, struct widget_t **widget,
    struct resource_data_t *data) {
    
    struct load_resource_widget_t callback_data;
    callback_data.xuni = xuni;
    callback_data.widget = widget;
    
    search_resource_tag(data, "widget", 1,
        load_resource_widgets_callback, &callback_data);
}

static enum widget_type_t get_widget_type(struct xuni_t *xuni,
    const char *find) {
    
    size_t pos;
    
    if(!wtype_search_name(xuni, find, &pos)) {
        log_message(ERROR_TYPE_RESOURCE, 0, __FILE__, __LINE__,
            "Invalid widget type: \"%s\"", find);
        
        return WIDGET_NONE;
    }
    
    return (enum widget_type_t)pos;
}

/*! Returns the value of the angle stored in \a list. \a list should contain
    the text representing the angle as well as an optional tag "unit", which
    should be either "degrees" or "radians" and specifies the unit type of the
    original angle. Note that the returned value is always in radians, no
    matter what the original angle's unit type was.
    
    \param list The resource tag to get the angle from.
    \return The angle, in radians, that was represented in \a list.
*/
static double get_angle(struct resource_list_t *list) {
    double angle;
    const char *unit;
    
    unit = find_resource_text(list, "unit");
    angle = get_expression_value(first_resource_text(list), 0);
    
    if(unit) {
        if(!strcmp(unit, "degrees")) angle = degrees_to_radians(angle);
        else if(strcmp(unit, "radians")) {
            printf("*** Unrecognized unit type \"%s\" for \"angle\"\n",
                unit);
        }
    }
    
    return angle;
}

/* this could handle all theme widgets */
static size_t find_font_name(struct resource_list_t *list) {
    const char *name = find_resource_text(list, "font");
    enum wid_theme_t which = THEME_FONT_SANS;
    
    if(name) {
        if(!strcmp(name, "sans")) which = THEME_FONT_SANS;
        else if(!strcmp(name, "mono")) which = THEME_FONT_MONO;
        else {
            printf("*** Unknown font name \"%s\"\n", name);
        }
    }
    
    return (size_t)which;
}

static int init_resource_widget_ref(struct xuni_t *xuni,
    struct widget_t *widget, enum widget_type_t type,
    struct resource_list_t *list) {
    
    const char *refname;
    struct widget_t *ref;
    
    refname = find_resource_text(list, "ref");
    if(!refname) return 0;
    
    ref = find_widget(xuni->gui->widget, refname);
    if(!ref) return 0;
    
    widget->type = type;
    
    switch(type) {
    case WIDGET_FONT:
        xuni_memory_increment(ref->p.font);
        widget->p.font = ref->p.font;
        
        break;
    default:
        printf("*** Error: <ref> not supported for widgets of type %s\n",
            get_widget_type_name(xuni, type));
        break;
    }
    
    return 1;
}

static enum label_type_t get_label_alignment(const char *name) {
    struct string_index_t data[] = {
        {"center", LABEL_ALIGN_CENTRE},
        {"centre", LABEL_ALIGN_CENTRE},
        {"left", LABEL_ALIGN_LEFT},
        {"right", LABEL_ALIGN_RIGHT}
    };
    size_t index;
    
    if(!name) return LABEL_ALIGN_LEFT;
    
    index = string_to_index(data, sizeof(data) / sizeof(*data), name);
    
    if(index == (size_t)-1) {
        log_message(ERROR_TYPE_RESOURCE, 0, __FILE__, __LINE__,
            "Invalid label alignment type: \"%s\"", name);
        return LABEL_ALIGN_LEFT;
    }
    
    return index;
}

static enum scrollbar_use_t get_scrollbar_use(const char *name) {
    struct string_index_t data[] = {
        {"both", SCROLLBAR_USE_BOTH},
        {"horizontal", SCROLLBAR_USE_HORIZONTAL},
        {"none", SCROLLBAR_USE_NONE},
        {"vertical", SCROLLBAR_USE_VERTICAL}
    };
    size_t index;
    
    if(!name) return SCROLLBAR_USE_VERTICAL;
    
    index = string_to_index(data, sizeof(data) / sizeof(*data), name);
    
    if(index == (size_t)-1) {
        log_message(ERROR_TYPE_RESOURCE, 0, __FILE__, __LINE__,
            "Invalid scrollbar use: \"%s\"", name);
        return SCROLLBAR_USE_VERTICAL;
    }
    
    return index;
}

/*! Calls the right initialization function for a widget of type \a type.
    Passes the data found in the resource structure \a list on to the
    initialization functions.
    
    Note that it would be difficult to replace this switch with an array of
    function pointers or some such structure, because the initialization
    functions all take different parameters.
    
    \param xuni A pointer to the main xuni structure.
    \param widget The widget to initialize. This should have been allocated
        by allocate_widget(), and its position should have been initialized
        by init_widget_pos(), prior to calling this function. However, this
        widget will be a generic widget, without its type set.
    \param type The type of the widget \a widget.
    \param list The resource file structure from which to obtain the data to
        initialize the widget \a widget with.
*/
static void init_resource_widget(struct xuni_t *xuni, struct widget_t *widget,
    enum widget_type_t type, struct resource_list_t *list) {
    
    if(init_resource_widget_ref(xuni, widget, type, list)) return;
    
    switch(type) {
    case WIDGET_BOX:
        init_box(widget, BOX_STYLE_NORMAL, 0);
        break;
    case WIDGET_BUTTON:
        init_button(widget, new_sub_label(widget,
            find_font_name(list),
            find_resource_text(list, "text")));
        break;
    case WIDGET_CHECKBOX:
        init_checkbox(widget, xuni,
            find_font_name(list),
            find_resource_text(list, "text"), 0);
        break;
    case WIDGET_COMBOBOX:
        init_combobox(xuni, widget, find_font_name(list));
        break;
    case WIDGET_FONT:
        init_font(widget, find_resource_text(list, "path"));
        break;
    case WIDGET_IMAGE:
        init_image(widget, find_resource_text(list, "path"),
            get_angle(first_resource_tag(list, "angle")),
            IMAGE_LOAD_FREE, IMAGE_RESCALE_LAZY);
        break;
    case WIDGET_IMAGE_TILE:
        init_image_tile(widget, find_resource_text(list, "path"),
            (int)get_expression_value(find_resource_text(list, "xinc"), 0),
            (int)get_expression_value(find_resource_text(list, "yinc"), 0));
        break;
    case WIDGET_LABEL:
        init_label(widget, find_font_name(list),
            find_resource_text(list, "text"),
            get_label_alignment(find_resource_text(list, "align")),
            (Uint8)get_expression_value(find_resource_text(list,
                "red"), 255),
            (Uint8)get_expression_value(find_resource_text(list,
                "green"), 255),
            (Uint8)get_expression_value(find_resource_text(list,
                "blue"), 255));
        break;
    case WIDGET_LISTBOX:
        init_listbox(widget, xuni,
            get_scrollbar_use(find_resource_text(list, "scrollbar-allow")),
            get_scrollbar_use(find_resource_text(list, "scrollbar-force")));
        break;
    case WIDGET_PANEL:
        init_panel_from_resource(xuni, widget, list);
        /*init_panel(widget);*/
        break;
    case WIDGET_TEXTAREA:
        init_textarea(widget, xuni);
        break;
    case WIDGET_TEXTBOX:
        init_textbox(widget, xuni, find_resource_text(list, "text"),
            find_font_name(list));
        break;
    case WIDGET_THEME:
        init_theme(widget,
            get_expression_value(find_resource_text(list, "box-width"),
                100 / 60.0),
            get_expression_value(find_resource_text(list, "box-height"),
                100 / 60.0));
        break;
    default:
        log_message(ERROR_TYPE_RESOURCE, 0, __FILE__, __LINE__,
            "Widgets of type %s cannot be instantiated in resource files",
            get_widget_type_name(xuni, type));
        break;
    }
}

static enum pos_pack_t get_pos_pack(const char *name) {
    struct string_index_t data[] = {
        {"bottom", POS_PACK_BOTTOM},
        {"none", POS_PACK_NONE},
        {"top", POS_PACK_TOP}
    };
    size_t index;
    
    if(!name) return POS_PACK_NONE;
    
    index = string_to_index(data, sizeof(data) / sizeof(*data), name);
    
    if(index == (size_t)-1) {
        log_message(ERROR_TYPE_RESOURCE, 0, __FILE__, __LINE__,
            "Invalid position pack type: \"%s\"", name);
        return POS_PACK_NONE;  /* a sensible default */
    }
    
    return index;
}

static void process_widget_accelerators(struct xuni_t *xuni,
    struct widget_t *panel, struct widget_t *widget,
    struct resource_list_t *list) {
    
    size_t x;
    
    for(x = 0; x < list->n; x ++) {
        if(list->data[x]->type != RESOURCE_TAG
            || strcmp(list->data[x]->data.tag->name, "accelerator")) {
            
            continue;
        }
        
        if(panel->type != WIDGET_PANEL) {
            printf("Attempted to add an accelerator to a \"%s\"\n",
                get_widget_type_name(xuni, panel->type));
        }
        else {
            widget_accelerator_from_string(xuni, panel, widget,
                first_resource_text(list->data[x]->data.tag->list));
        }
    }
}

enum { ACCELERATOR_ERROR = 0xf000 };

static void widget_accelerator_from_string(struct xuni_t *xuni,
    struct widget_t *panel, struct widget_t *widget, const char *data) {
    
    const char *end;
    char c;
    SDLKey key = (size_t)-1;
    SDLMod mod = KMOD_NONE, m = KMOD_NONE;
    
    if(!data) return;
    
    do {
        for(end = data; isalnum(*end); end ++);
        
        c = *end;
        *(char *)end = 0;
        /*printf("accelerator: \"%s\"\n", data);*/
        m = get_key_mod_from_string(data);
        if(m == ACCELERATOR_ERROR) {
            if(key != ACCELERATOR_ERROR) {
                key = get_key_identifier_from_string(data);
                if(key == ACCELERATOR_ERROR) {
                    log_message(ERROR_TYPE_RESOURCE, 0, __FILE__, __LINE__,
                        "Unknown key/mod name \"%s\"\n", data);
                }
            }
            else {
                log_message(ERROR_TYPE_RESOURCE, 0, __FILE__, __LINE__,
                    "Multiple key specified: \"%s\"\n", data);
            }
        }
        else mod = m;
        
        *(char *)end = c;
        
        for(data = end; isspace(*data); data ++);
        end = data;
    } while(*data);
    
    add_widget_accelerator(xuni, panel, widget, key, mod);
}

static SDLMod get_key_mod_from_string(const char *name) {
    struct string_index_t data[] = {
        {"alt", KMOD_ALT},
        {"control", KMOD_CTRL},
        {"ctrl", KMOD_CTRL},
        {"lalt", KMOD_LALT},
        {"lcontrol", KMOD_LCTRL},
        {"lctrl", KMOD_LCTRL},
        {"lshift", KMOD_LSHIFT},
        {"none", KMOD_NONE},
        {"nothing", KMOD_NONE},
        {"ralt", KMOD_RALT},
        {"rcontrol", KMOD_RCTRL},
        {"rctrl", KMOD_RCTRL},
        {"rshift", KMOD_RSHIFT},
        {"shift", KMOD_SHIFT}
    };
    size_t index;
    
    if(!name) return ACCELERATOR_ERROR;
    
    index = string_to_index(data, sizeof(data) / sizeof(*data), name);
    
    if(index == (size_t)-1) return ACCELERATOR_ERROR;
    /*printf("Found mod accelerator: %x\n", (unsigned)index);*/
    
    return index;
}

static SDLKey get_key_identifier_from_string(const char *name) {
    struct string_index_t data[] = {
        {"f1", SDLK_F1},
        {"f10", SDLK_F10},
        {"f11", SDLK_F11},
        {"f12", SDLK_F12},
        {"f2", SDLK_F2},
        {"f3", SDLK_F3},
        {"f4", SDLK_F4},
        {"f5", SDLK_F5},
        {"f6", SDLK_F6},
        {"f7", SDLK_F7},
        {"f8", SDLK_F8},
        {"f9", SDLK_F9}
    };
    size_t index;
    
    if(!name) return ACCELERATOR_ERROR;
    
    if(*name && !name[1]) {
        /* ... */
    }
    
    index = string_to_index(data, sizeof(data) / sizeof(*data), name);
    
    if(index == (size_t)-1) {
        log_message(ERROR_TYPE_RESOURCE, 0, __FILE__, __LINE__,
            "Unknown key name \"%s\"", name);
        return ACCELERATOR_ERROR;
    }
    
    return index;
}

/*! Creates a widget based on the information in the resource tree node
    \a data.
    
    Also sets the current theme to the first theme that is found in the
    resource tree.
    
    \param xuni The xuni_t structure, to pass on to other functions and set
        the current theme in and so on.
    \param base The widget to add the new widget to as a compose widget. If
        \a base is NULL, it means that the root widget is being created, in
        which case \a base is set to point to the new widget.
    \param data The resource tree node representing a {widget}, from which to
        create a widget.
*/
static void add_resource_widget(struct xuni_t *xuni, struct widget_t **base,
    struct resource_data_t *data) {
    
    struct widget_t *widget;
    struct resource_list_t *list;
    enum widget_type_t type;
    
    list = data->data.tag->list;
    
#if 0
    widget = init_resource_widget_ref(xuni, list);
    if(widget) {
        type = widget->type;
    }
    else {
#endif
    type = get_widget_type(xuni, find_resource_text(list, "type"));
    if(type == WIDGET_NONE) return;
    
    if(!*base) {
        *base = allocate_widget(find_resource_text(list, "name"), 0);
        widget = *base;
    }
    else {
        if(!(*base)->compose) (*base)->compose = allocate_panel(*base);
        
        add_allocate_widget(*base, find_resource_text(list, "name"));
        
        widget = last_compose_widget(*base);
    }
    
    init_widget_pos(widget,
        get_expression_value(find_resource_text(list, "xpos"), 0),
        get_expression_value(find_resource_text(list, "ypos"), 0),
        get_expression_value(find_resource_text(list, "width"), 1),
        get_expression_value(find_resource_text(list, "height"), 1),
        get_pos_pack(find_resource_text(list, "pack")));
    
    if(!get_expression_value(find_resource_text(list, "enabled"), 1)) {
        widget->visibility &= ~WIDGET_VISIBILITY_CLICKABLE;
    }
    if(!get_expression_value(find_resource_text(list, "selable"), 1)) {
        widget->visibility &= ~WIDGET_VISIBILITY_SELABLE;
    }
    if(!get_expression_value(find_resource_text(list, "visible"), 1)) {
        widget->visibility &= ~WIDGET_VISIBILITY_VISIBLE;
    }
    
    process_widget_accelerators(xuni, *base, widget, list);
    
    init_resource_widget(xuni, widget, type, list);
    
    widget_event(xuni, widget, WIDGET_EVENT_RESCALE);
    
    if(widget->type == WIDGET_THEME) {
        if(!xuni->theme->current) {
            xuni->theme->current = widget;
        }
        
        /* recurse (indirectly) */
        load_resource_widgets(xuni, &widget, data);
        
        set_theme_nameid(widget);
        widget_event(xuni, widget, WIDGET_EVENT_RESCALE);
    }
    else if(widget->type == WIDGET_PANEL) {
        /* recurse (indirectly) */
        load_resource_widgets(xuni, &widget, data);
    }
}

#if 0
/* This code returns a widget based on a list of names to dereference. It
    works, but using WIDs is much more efficient, and this is not used at the
    moment.
*/
static int compare_widget_name_nonrec(struct widget_t *widget, va_list arg) {
    const char *match = va_arg(arg, const char *);
    
    if(!widget || !match) return !match;
    
    return strcmp(match, widget->name) == 0
        && compare_widget_name_nonrec(widget->base, arg);
}

static int compare_widget_name_nonrec_reverse(struct widget_t **widget,
    va_list arg) {
    
    const char *match = va_arg(arg, const char *);
    
    if(!*widget || !match) return !match;
    
    if(!compare_widget_name_nonrec_reverse(widget, arg)
        || !(*widget)->name) {
        
        return 0;
    }
    
    if(strcmp(match, (*widget)->name) == 0) {
        *widget = (*widget)->base;
        return 1;
    }
    
    return 0;
}

int compare_widget_name(struct widget_t *widget, ...) {
    int r;
    
    va_list arg;
    va_start(arg, widget);
    
    r = compare_widget_name_nonrec_reverse(&widget, arg);
    
    va_end(arg);
    
    return r;
}
#endif

/*! Allocates enough space for a struct widget_nameid_t.
    \return A pointer to the newly allocated nameid structure.
*/
struct nameid_t *allocate_nameid(void) {
    struct nameid_t *nameid = xuni_memory_allocate(sizeof(*nameid));
    
    nameid->widget = 0;
    nameid->widgets = 0;
    
    return nameid;
}

/*! Adds the widget \a widget to \a nameid.
    
    \param nameid The array of nameids to add the new wid to.
    \param widget The widget to add to \a nameid.
    \param id The wid to use for this widget.
    \param name The path to the widget \a widget relative to the panel widget
        that \a nameid is a part of.
*/
void init_wid_widget(struct nameid_t *nameid, struct widget_t *widget,
    size_t id, const char *name) {
    
    size_t x;
    
    if(nameid->widgets <= id) {
        nameid->widget = xuni_memory_resize(nameid->widget,
            (id + 1) * sizeof(*nameid->widget));
        
        /* clear any new nameids allocated between nameid->widgets and id */
        for(x = nameid->widgets; x <= id; x ++) {
            nameid->widget[x].id = 0;
            nameid->widget[x].name = 0;
            nameid->widget[x].widget = 0;
        }
        
        nameid->widgets = id + 1;
    }
    
    if(nameid->widget[id].id || nameid->widget[id].name
        || nameid->widget[id].widget) {
        
        log_message(ERROR_TYPE_WARNING, 0, __FILE__, __LINE__,
            "Duplicate wid: old: \"%s\" [%lu], new: \"%s\" [%lu]",
            nameid->widget[id].name, (unsigned long)nameid->widget[id].id,
            name, (unsigned long)id);
    }
    else {
        nameid->widget[id].id = id;
        if(widget) widget->id = id;
        nameid->widget[id].name = name;
        nameid->widget[id].widget = widget;
    }
}

/*! Just like init_wid(), but allows the widget referred to be \a name to not
    exist. (init_wid() would display an error in that case.)
    
    Mainly useful for widgets that do not have to be loaded. Prime examples
    of this are the various widget images of themes, like the checkbox image.
    
    \param base The base of the widget, from which to use find_widget() to
        find the widget represented by the widget path \a name.
    \param nameid The nameid array to add the new nameid to.
    \param id The wid of the new widget -- usually a client-defined enum
        value.
    \param name The path to the widget, starting from \a base.
*/
void init_wid_possible_zero(struct widget_t *base, struct nameid_t *nameid,
    size_t id, const char *name) {
    
    init_wid_widget(nameid, find_widget(base, name), id, name);
}

/*! Adds a widget to the nameid array of \a use. The widget is located using
    the name in \a name with the base \a base.
    
    \param base The base of the widget, from which to use find_widget() to
        find the widget represented by the widget path \a name.
    \param use The panel widget to add the new nameid to.
    \param id The wid of the new widget -- usually a client-defined enum
        value.
    \param name The path to the widget, starting from \a base.
*/
void init_wid(struct widget_t *base, struct widget_t *use, size_t id,
    const char *name) {
    
    struct widget_t *widget = find_widget(base, name);
    
    if(!use->p.panel->nameid) {
        use->p.panel->nameid = allocate_nameid();
    }
    
    if(widget) {
        init_wid_widget(use->p.panel->nameid, widget, id, name);
    }
    else {
        log_message(ERROR_TYPE_RESOURCE, 0, __FILE__, __LINE__,
            "Could not find widget \"%s\" [wid %lu]", name,
            (unsigned long)id);
    }
}

void init_wid_array(struct widget_t *base, struct widget_t *use,
    struct wid_init_t *init, size_t n) {
    
    size_t x;
    
    for(x = 0; x < n; x ++) {
        init_wid(base, use, init[x].id, init[x].name);
    }
}

static size_t move_cursor_word(struct widget_edit_t *edit, size_t startpos,
    int dir, int flip) {
    
    int space;
    size_t x, pos = startpos;
    
    while((dir < 0) ? pos : (pos < edit->len)) {
        x = pos;
        if(dir < 0) x --;
        
        space = (isspace(edit->data->text[x]) ? 1 : 0);
        if(space != flip) break;
        
        pos += dir;
    }
    
    return pos;
}

int widget_process_character(struct xuni_t *xuni, SDL_keysym *sym) {
    struct widget_t *widget = xuni->gui->active.widget;
    
    if(!widget) return 0;
    
    /*printf("widget_process_character(): widget=%p, key=%i [%c]\n",
        (void *)widget, sym->unicode,
        isprint(sym->unicode) ? sym->unicode : '-');*/
    
    /* !!! */
    if(widget->type == WIDGET_TEXTBOX) {
        widget = widget->compose->widget[WID_TEXTBOX_LABEL];
    }
    
    if(widget->type == WIDGET_LABEL) {
        if(isprint(sym->unicode)) {
            edit_add_char(&xuni->gui->edit, sym->unicode);
        }
        else if(sym->unicode == '\b' && xuni->gui->edit.pos) {
            if(sym->mod & KMOD_CTRL) {
                size_t end = xuni->gui->edit.pos;
                
                xuni->gui->edit.pos = move_cursor_word(&xuni->gui->edit,
                    xuni->gui->edit.pos, -1, 1);
                xuni->gui->edit.pos = move_cursor_word(&xuni->gui->edit,
                    xuni->gui->edit.pos, -1, 0);
                
                edit_del_chars(&xuni->gui->edit, end - xuni->gui->edit.pos);
            }
            else {
                xuni->gui->edit.pos --;
                edit_del_chars(&xuni->gui->edit, 1);
            }
        }
        else if(sym->unicode == '\r') {
            clear_active(xuni, &xuni->gui->active, 1);
        }
        else if(sym->sym == SDLK_DELETE
            && xuni->gui->edit.pos < xuni->gui->edit.len) {
            
            if(sym->mod & KMOD_CTRL) {
                size_t end;
                
                end = move_cursor_word(&xuni->gui->edit,
                    xuni->gui->edit.pos, 1, 0);
                end = move_cursor_word(&xuni->gui->edit, end, 1, 1);
                
                edit_del_chars(&xuni->gui->edit, end - xuni->gui->edit.pos);
            }
            else {
                edit_del_chars(&xuni->gui->edit, 1);
            }
        }
        else if(sym->sym == SDLK_LEFT && xuni->gui->edit.pos) {
            if(sym->mod & KMOD_CTRL) {
                xuni->gui->edit.pos = move_cursor_word(&xuni->gui->edit,
                    xuni->gui->edit.pos, -1, 1);
                xuni->gui->edit.pos = move_cursor_word(&xuni->gui->edit,
                    xuni->gui->edit.pos, -1, 0);
            }
            else {
                xuni->gui->edit.pos --;
            }
        }
        else if(sym->sym == SDLK_RIGHT
            && xuni->gui->edit.pos < xuni->gui->edit.len) {
            
            if(sym->mod & KMOD_CTRL) {
                xuni->gui->edit.pos = move_cursor_word(&xuni->gui->edit,
                    xuni->gui->edit.pos, 1, 0);
                xuni->gui->edit.pos = move_cursor_word(&xuni->gui->edit,
                    xuni->gui->edit.pos, 1, 1);
            }
            else {
                xuni->gui->edit.pos ++;
            }
        }
        else if(sym->sym == SDLK_END) {
            xuni->gui->edit.pos = xuni->gui->edit.len;
        }
        else if(sym->sym == SDLK_HOME) {
            xuni->gui->edit.pos = 0;
        }
        else {
            return 0;
        }
        
        if(xuni->gui->edit.data) {
            reposition_label_data(xuni, xuni->gui->edit.data,
                &xuni->gui->edit, xuni->gui->edit.pos);
        }
        
        return 1;
    }
    
    /*printf("data: \"%s\"\n", xuni->gui->edit.data);*/
    
    return 0;
}

/*! Returns true if the widget \a widget can be activated. This should depend
    only on the type of \a widget -- for example, textboxes can be activated
    (after which they trap keyboard input), but buttons cannot.
    
    \param widget The widget to examine the type of.
    \return True if the widget \a widget can be activated.
*/
int widget_can_be_active(struct widget_t *widget) {
    if(!widget) return 0;
    
    if(widget->type == WIDGET_TEXTBOX) return 1;
    
    return 0;
}

void update_widget(struct xuni_t *xuni, struct widget_t *widget) {
    if(!widget) return;
    
    if(widget->type == WIDGET_TEXTBOX) {
        /*gui->sel.p.widget->base->compose
            ->widget[WID_TEXTBOX_LABEL]->p.label->text = gui->edit.data;*/
        
        widget_event(xuni, widget, WIDGET_EVENT_RESCALE);
    }
}

void enable_unicode(const struct widget_t *widget) {
    int v = 0;
    
    if(!widget) return;
    
    if(widget->type == WIDGET_TEXTBOX) {
        v = 1;
    }
    
    if(SDL_EnableUNICODE(-1) != v) SDL_EnableUNICODE(v);
}

void activate_widget(struct widget_t *widget, struct xuni_t *xuni,
    int xp, int yp) {
    
    struct widget_edit_t *edit = &xuni->gui->edit;
    struct widget_t *w;
    int x;
    
    if(!widget) return;
    
    if(widget->type == WIDGET_TEXTBOX) {
        struct label_t *label = widget->compose->widget[WID_TEXTBOX_LABEL]
            ->p.label;
        size_t len = label->text ? strlen(label->text) : 0;
        
        if(edit->data) deactivate_widget(xuni->gui->active.widget, xuni);
        
        edit->data = label;
        edit->datawidget = widget->compose->widget[WID_TEXTBOX_LABEL];
        
        edit->prev = xuni_memory_allocate(sizeof(*edit->prev));
        *edit->prev = *label;
        
        edit->data->text = xuni_memory_duplicate_string_len(label->text, len);
        
        edit->len = edit->alloc = (label->text ? len : 0);
        
        if(label->text) {
            w = widget->compose->widget[WID_TEXTBOX_LABEL];
            
            x = xp - w->pos->real.x;
            if(w->pos->clip) x += w->pos->clip->xclip;
            
            edit->pos = width_to_label_pos(xuni, x,
                get_theme_widget(xuni, label->font),
                (char *)label->text);  /* !!! const */
        }
        else {
            edit->pos = 0;
        }
        
        /* Reposition the textbox so that the cursor is inside the visible
            portion of the textbox. If a half-invisible character is selected,
            for example, this will scroll the textbox slightly so that the
            cursor is visible.
            
            This would be unnecessary if textbox selection was based on the
            label instead of the box of the textbox.
        */
        widget_event(xuni, widget, WIDGET_EVENT_REPOSITION);
    }
}

void revert_widget(struct xuni_t *xuni, struct widget_t *widget) {
    if(!widget) return;
    
    if(widget->type == WIDGET_TEXTBOX) {
        struct widget_t *label = widget->compose->widget[WID_TEXTBOX_LABEL];
        
        /*printf("revert_widget(): edit: %p \"%s\"\n", label->p.label->text,
            label->p.label->text);
        printf("revert_widget(): prev: %p \"%s\"\n",
            xuni->gui->edit.prev->text, xuni->gui->edit.prev->text);*/
        
        xuni_memory_free((char *)label->p.label->text);  /* !!! const */
        /*SDL_FreeSurface(label->p.label->label);*/
        
        if(xuni->gui->edit.prev) {
            label->p.label->text = xuni->gui->edit.prev->text;
            
            xuni_memory_free(xuni->gui->edit.prev);
        }
        else label->p.label->text = xuni_memory_duplicate_string_len("", 0);
        
        widget_event(xuni, label, WIDGET_EVENT_RESCALE);
        
        init_edit(&xuni->gui->edit);
    }
}

void deactivate_widget(struct widget_t *widget, struct xuni_t *xuni) {
    if(!widget) return;
    
    if(widget->type == WIDGET_TEXTBOX) {
        /* makes comboboxes have a history of what was typed in them */
        /*if(widget->base
            && widget->base->type == WIDGET_COMBOBOX) {
            
            struct widget_t *label = widget->compose
                ->widget[WID_TEXTBOX_LABEL];
            
            add_combobox_item(xuni, widget->base, label->p.label->font,
                label->p.label->text);
            widget_event(xuni, widget->base->compose->widget
                [WID_COMBOBOX_DROPDOWN], WIDGET_EVENT_RESCALE);
        }*/
        
        call_deactivate_func(xuni, widget);
        
        xuni_memory_free((char *)xuni->gui->edit.prev->text);  /* !!! const */
        /*free_surface(xuni->gui->edit.prev->label);*/
        xuni_memory_free(xuni->gui->edit.prev);
        
        init_edit(&xuni->gui->edit);
    }
}

void perform_widget_click(struct xuni_t *xuni, struct widget_t *widget) {
    if(!widget) return;
    
    if(widget->base) {
        if(widget->type == WIDGET_CHECKBOX) {
            toggle_checkbox(widget);
        }
        else if(widget->base->type == WIDGET_SCROLLBAR) {
            if(widget->base->compose->widget[WID_SCROLLBAR_UP]
                == widget) {
                
                move_scrollbar(xuni, widget->base, -10);
            }
            else if(widget->base->compose->widget
                [WID_SCROLLBAR_DOWN] == widget) {
                
                move_scrollbar(xuni, widget->base, 10);
            }
            
            /*reposition_widget(xuni, widget->base);*/
            widget_event(xuni, xuni->gui->sel.p.widget->base->base,
                WIDGET_EVENT_REPOSITION);
        }
        else if(widget->base->type == WIDGET_COMBOBOX) {
            if(widget->base->compose->widget[WID_COMBOBOX_BUTTON] == widget) {
                widget->base->compose->widget[WID_COMBOBOX_DROPDOWN]
                    ->visibility ^= WIDGET_VISIBILITY_VISIBLE;
            }
        }
    }
}

/*! Frees the memory allocated for the gui_t structure (xuni_t::gui).
    \param xuni A pointer to the main xuni structure. Only the gui member of
        this structure is used.
*/
void free_gui(struct xuni_t *xuni) {
    widget_event(xuni, xuni->gui->widget, WIDGET_EVENT_FREE);
}
