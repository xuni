#!/usr/bin/perl
# A script to calculate the number and percentage of entities and functions
# that are documented (with Doxygen documentation comments).
#
# It operates by parsing the output of Doxygen. It could break for different
# versions of Doxygen -- it is known to work with version 1.5.5.
#
# Written by DWK

use strict;
use warnings;

&process_files('html/*_8c.html');

sub process_files {
    my $pattern = shift;
    my %total = &new_list();
    
    &print_header();
    
    foreach my $file (glob $pattern) {
        my ($srcfile, %count) = &parse_file($file);
        &print_line($srcfile, %count);
        
        foreach my $group (keys %total) {
            $total{$group}->{'total'} += $count{$group}->{'total'};
            $total{$group}->{'documented'} += $count{$group}->{'documented'};
        }
    }
    
    &print_line("    *** Total ***", %total);
}

sub new_list {
    my %list = (
        'all' => {'documented' => 0, 'total' => 0},
        'static' => {'documented' => 0, 'total' => 0},
        'normal' => {'documented' => 0, 'total' => 0}
    );
    
    return %list;
}

sub parse_file {
    my $file = shift;
    my $line = '';
    my $srcfile = '';
    my $mode = 'initial';
    my $name = '';
    my $section = '';
    
    my %count = &new_list();
    
    open(FILE, "<$file");
    
    while($line = <FILE>) {
        chomp($line);
        
        if($line =~ m|^<title>\w+: (.*) File Reference</title>$|) {
            $srcfile = $1;
        }
        elsif($line =~ m|^<hr><h2>(\w+) Documentation</h2>$|) {
            $section = $1;
        }
        
        if($mode eq 'initial') {
            if($line eq '<div class="memproto">') {
                $mode = 'findname';
            }
        }
        elsif($mode eq 'findname') {
            if($line =~ m|<td class="memname">([^<]+)|) {
                $name = $1;
                $name =~ s/^\s+//;
                $name =~ s/\s+$//;
                
                $mode = 'finddoc';
            }
        }
        elsif($mode eq 'finddoc') {
            if($line eq '<div class="memdoc">') {
                $mode = 'findp';
            }
        }
        elsif($mode eq 'findp') {
            if($line eq '<p>') {
                $mode = 'findline';
            }
        }
        elsif($mode eq 'findline') {
            my $documented = 0;
            if($line =~ /\S/) {
                $documented = 1;
            }
            
            $count{'all'}->{'total'} ++;
            $count{'all'}->{'documented'} ++ if $documented;
            
            if($section eq 'Function') {
                if($name =~ /^static/) {
                    $count{'static'}->{'total'} ++;
                    $count{'static'}->{'documented'} ++ if $documented;
                }
                else {
                    $count{'normal'}->{'total'} ++;
                    $count{'normal'}->{'documented'} ++ if $documented;
                }
            }
            
            $mode = 'initial';
        }
    }
    
    close(FILE);
    
    return ($srcfile, %count);
}

sub print_header {
    print "    *** Number and percentage of entities and functions "
        . "that are documented ***\n";
    printf("%-25s | %-15s | %-15s | %-15s\n",
        "Source file name", "All entities", "Normal funcs", "Static funcs");
}

sub print_line {
    my $file = shift;
    my %count = @_;
    
    printf("%-25s", $file);
    
    # Do not use a foreach loop because a specific order is desired
    &print_group('all', %count);
    &print_group('normal', %count);
    &print_group('static', %count);
    
    print "\n";
}

sub print_group {
    my $group = shift;
    my %count = @_;
    
    printf(" | %3d/%3d",
        $count{$group}->{'documented'}, $count{$group}->{'total'});
    if($count{$group}->{'total'}) {
        printf(" %6.2f%%",
            $count{$group}->{'documented'} / $count{$group}->{'total'}
                * 100.0);
    }
    else {
        print " 100.00%";
    }
}
