#!/bin/sh

files=

for file in $(find src \( -name '*.c' -o -name '*.h' \) ); do
    root=$(echo $file | sed 's/\.[ch]//')
    ext=$(echo $file | sed 's/.*\.//')
    
    if [ .$ext = '.c' -a -f $root.y ]; then
        file=$root.y
    fi
    
    files="$files $file"
done

wc $files
