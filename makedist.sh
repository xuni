#!/bin/sh

# Determine the version of xuni
VERSION="$(./version.sh)"

bin_files="editor test src/test/test.so"
force_reconf=1

function ensure_src {
    if [ ! -f "xuni-$VERSION-src.tar.gz" ]; then
        echo make_src >/dev/null
    fi
}

function make_src {
    if [ $force_reconf != "0" ]; then
        ./autotool.sh && make clean && make
    fi
    
    make dist
    
    #./makedistar.sh xuni-$VERSION src $(./dist_files.sh)
    mv xuni-$VERSION.tar.gz xuni-$VERSION-src.tar.gz
}

function make_64 {
    ensure_src
    cp xuni-$VERSION-src.tar.gz xuni-$VERSION-lin64.tar.gz
    ./makedistar.sh --update xuni-$VERSION lin64 $bin_files
}

function make_32 {
    ensure_src
    
    if [ $force_reconf != "0" ]; then
        ./make32.sh
    fi
    
    make clean && make
    
    cp xuni-$VERSION-src.tar.gz xuni-$VERSION-lin32.tar.gz
    ./makedistar.sh --update xuni-$VERSION lin32 $bin_files
}

function make_win {
    win_bin_files="editor.exe test.exe" # src/test/test.so
    
    ensure_src
    
    #make clean && echo SVNVERSION="$(./svnversion.sh)"
    make clean && ./winmake.sh #SVNVERSION="$(./svnversion.sh)"
    
    for dll in dll/*.dll; do
        cp $dll .
    done
    
    cp xuni-$VERSION-src.tar.gz xuni-$VERSION-win32.tar.gz
    ./makedistar.sh --update xuni-$VERSION win32 $win_bin_files *.dll
    
    rm *.dll
}

function make_doc {
    doc_files="$(svn list -R | grep doc | grep -v '/$') \
        $(find doc/html -type f)"
    
    ./makedistar.sh --create xuni-$VERSION doc $doc_files
}

for arg in $*; do
    case $arg in
    --force-reconf)
        force_reconf=1
        ;;
    --no-force-reconf)
        force_reconf=0
        ;;
    --make-src)
        make_src
        ;;
    --make-64)
        make_64
        ;;
    --make-32)
        make_32
        ;;
    --make-win)
        make_win
        ;;
    --make-doc)
        make_doc
        ;;
    --make-all)
        make_src
        make_64
        make_32
        make_win
        make_doc
        ;;
    esac
done
