#!/bin/sh

path="$1"

if [ -z "$path" ]; then
    path="."
fi

find "$path" -exec ./chwinmod1.sh '{}' \;
