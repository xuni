#!/bin/sh

svn list -R > dist-svnlist
svn status > dist-svnstatus
grep ^A dist-svnstatus > dist-svnstatusA
grep ^D dist-svnstatus > dist-svnstatusD

echo -n $(perl -ne 'chomp; if(!-d $_ && `grep "$_" dist-svnstatusD` eq "") { print "$_\n" }' dist-svnlist)
echo -n ' '
echo -n $(sed 's/A......//' dist-svnstatusA)
echo

rm dist-svnlist dist-svnstatus dist-svnstatusA dist-svnstatusD
