#!/bin/sh

# Setup variables
mode=$1 ; shift
prefix=$1 ; shift
name=$1 ; shift
pwd=$(pwd)
output="$prefix-$name.tar.gz"

# Set up the temporary directory
tempdir=$(mktemp -d)
dir="$tempdir/$prefix"
mkdir $dir

echo -n "archive: "

# Copy the files specified in the remaining arguments to the temporary dir
cp --parents -l $* $dir

# Create the actual archive
if [ -f "$output" ]; then
    if [ "$mode" = "--create" ]; then
        rm $output
    fi
    
    tar -C $tempdir -xzf $pwd/$output 2>/dev/null
fi

tar -C $tempdir -czf $pwd/$output $prefix 2>/dev/null

# Print a message about the number of files that were added to the archive
printf '%-40s%4d extra    %4d total\n' \
    $output ${#*} $(find $tempdir -type f | wc -w)

# Remove temporary directory
if [ -n "$(echo $tempdir | grep ^/tmp)" ]; then
    rm -rf $tempdir
else
    echo Error: temporary directory not in /tmp: $tempdir
fi
