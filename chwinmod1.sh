#!/bin/sh

file="$1"

if [ -f "$file" ]; then
    if [ -n "$(echo $file | file -f - | grep executable)" ]; then
        echo $file: executable
        chmod 755 "$file"
    else
        echo $file: normal
        chmod 644 "$file"
    fi
elif [ -d "$file" ]; then
    echo $file: directory
    chmod 755 "$file"
else
    echo $file: ignored
fi
